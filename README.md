# **Business Rules: Pricing**

## Overview
This is a library to be used in order to calculate the best rules for pricing. 
Project is written in Java and works on most platforms with installed Java Runtime Environment.

##DOCUMENTATION
Please, before using this library, refer to this file:
* [USER MANUAL](documentation/USER MANUAL.md)

## Built With
* [Java 8](https://www.java.com/) - Written Language  
* [Gradle](https://gradle.org) - Dependency Management


## Third Party

* [J-Easy Rules](https://github.com/j-easy/easy-rules) - Simple rules engine for Java
* [Log4j2](https://logging.apache.org/log4j/2.x/) - Logging System

## Version
Automatic handled version by [Gradle Release Plugin](https://github.com/netzwerg/gradle-release-plugin)

## Revision [README.md](README.md)
**19/02/2018** - *v1.0*
**19/03/2018** - *v1.1*

## Authors
* **Miguel Hurtado** - *Developer* 
