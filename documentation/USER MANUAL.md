# **USER MANUAL**

## Overview
This is the Definitive Guide for this library.

## Price Rule
Rules are define with factors:
* fromThruDate: it defines the range of dates in which the rule can be satisfy.
* fromThruTotalQty: it defines the range of quantities that the total quantities in the sale order must have in order to satisfy the rule
* fromThruTotalAmount: it defines the range of amount that the total amount of the sale order must have in order to satisfy the rule
* PaymentCondition: it defines the payment condition that the sale order must have in order to satisfy the rule
* DeliveryMode: it defines the delivery mode that the sale order must have in order to satisfy the rule.
* BusinessUnits: at least one business unit defined in the rule must be in the sale order in order to satisfy the rule.
* Agencies: at least one business unit defined in the rule must be in the sale order in order to satisfy the rule.
* Role Types: at least one role type defined in the rule must be in customer of the sale order in order to satisfy the rule.
* Customers: at least one customer defined in the rule must match the customer of the sale order in order to satisfy the rule.
* Party Classifications: at least one classification defined in the rule must match the customer classification in the sale order in order to satisfy the rule.
* Geographic Boundaries: at least one geographic boundary defined in the rule must match the customer's geographic boundaries in the sale order in order to satisfy the rule.

All factors seen it until here are valid for restricted and general type. These last factors have a different behaviour:
* Products: In Restricted Mode, all products defined in the rule must match the ones in the Sale Order. 
In General Mode: at least one product in the Sale Order must match one of the defined in the rule.  
* Product Features: In Restricted Mode, all product features in the Sale Order must match the ones defined in the rule. 
                    In General Mode: at least one feature in the Sale Order must match one of the defined in the rule.  
* Product Classifications: In Restricted Mode, all product classifications in the Sale Order must match the ones defined in the rule. 
In General Mode: at least one classification in the Sale Order must match one of the defined in the rule.  

## Usage
In order to use this library, you will need to create an object of the type: *PriceComponentRule*
The constructor asks for two inputs:
* List of rules (you can get it from database or create your own rules)
* Resolution Mode (right now there are two resolution modes, but we advice you to use BY_BEST_VALUE_FOR_CUSTOMER)

Example (We're using lombok builder in this example):
```java
 public class Example {
     
     PriceComponentRule priceComponentRule = PriceComponentRule.builder()
                                                             .rules(rules)
                                                             .resolutionMode(PriceRuleResolutionModeEnum.BY_BEST_VALUE_FOR_CUSTOMER)
                                                             .allowSetupLogger(false)
                                                             .build();
 }
```

You only need to call one method in order to get the results: *computate*. It has only one input and its the sale order:
 ```java
 public class Example {
     
     PriceComponentRule priceComponentRule;
     
     public List<PriceComponentRuleInputSaleOrderProduct> calculate(InputSaleOrder saleOrder){
          return componentRule.computate(saleOrder);
     }
     
 }
```
This method will return a List of products were the rules have been applied.

##Configurations
The engine now supports a config file in .xml format.
Please put this file under a folder named "configs" in the root. Like this configs/default.xml
All settings in this file will override the default values.
By default, there is a file in the Resource folder that will be loaded always.

##Subjects
Only one subject must be placed in the rule, this means if you add product, product feature and/or product classification, you will face that the total gets in order of priority, meaning Classification goes first, then features and last the products.

##Target
All Rules have target. This means that the outcome will be apply to the products defined in the rule or the whole sale order. If you select target = Products and you dont define any subjects (product, product classification, product features) this will behave as the target was the sale Order. You will receive a warning in the logs.

##Scale Type (Validation Type)
In every rule, there is a field called scaleType. it does not matter whether you use scales, frequency or single value in the outcome, you MUST define the scale Type in order to calculate the best rule by "Best Value For Customer".
It can be:
* Quantity: This means that it will use the quantities in order to compare.
* Amount: This means that it will use the total amount in order to compare.

##Empty Results
This can happen for various reasons, but the most common are:
* The outcome produces negative values (is greater than the price), with this the rule does not find the best rule.
* There are no best rules found.

##Configurations File
These are the available configurations fields:
* outcome.forceZeroIfNegative: If the calculation of the outcome return a negative value, you can force to return Zero if its negative, this can help you to avoid not finding the best rule.

By default, this enginne will not override the log system in your enviroment. If you need to do this, please specify in the PriceRuleComponent:

```java
public class Example {
    
    PriceComponentRule priceComponentRule;
    
    public void setAllowSetupLogger(){
        priceComponentRule.setAllowSetupLogger(true);
    }
    
}
```

or in the constructor just like we explain before.

##Calculations

If you set a discount higher than the amount of the product total, therefore the discount (percentage/amount) will set to the total amount of the products. In other words, it will be set to 100% discount.
Be aware of this and be careful when you write your rules.

##Rules Application Mode
The engine only applies the best rule found, this can be either only one rule no accumulative or several accumulative rules. The outcomes are compare and depending of the user configuration, it returns the best value for the SALE ORDER, this means that takes in consideration all the discounts that give the user the best value and selects the best rule. However, you can change this behaviour changing the Resolution Mode and set it to BY_PRODUCT. When you select BY_PRODUCT, the outcomes will compare only with the values by each product and not considering the possible value in the whole sale order.  

```java
public class Example {
    
    PriceComponentRule priceComponentRule;

    
    public void changeApplicationModeAllByProduct(){
        componentRule.setRulesApplicationMode(PriceRuleApplicationModeEnum.BY_PRODUCT);
    }
}
```

##Fixed Price
The fixed price is a type of outcome that changes the price, depending of your settings this can be the highest or the lowest value. Accumulative fixed rules does not accumulate so we advice you to declare this rules as non accumulative. The behaviour is the same in both modes.

##Results
Everytime a rule is fired you will recieve a List with all the products affected by rules.
Sometimes you will find a product with no modification (except in rules applied variable)
this happens because that product was te condition to fire some rules (Ex. in bonus or surcharge products this is the case).

##Bonus/Surcharge Products
* When the output has some bonus products, this will be distinguish with an id of 0.
* When the output has some surcharge products, this will be distinguish with an id of -1.
Also in the output you will see the product that fired that bonus/surcharge products.

## Revision
**19/02/2018** - *Miguel Hurtado*
**27/02/2018** - *Miguel Hurtado*
**07/03/2018** - *Miguel Hurtado*
**12/03/2018** - *Miguel Hurtado*
**19/03/2018** - *Miguel Hurtado*

## Authors
* **Miguel Hurtado** - *Developer* 
