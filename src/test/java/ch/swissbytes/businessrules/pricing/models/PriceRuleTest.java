package ch.swissbytes.businessrules.pricing.models;

import ch.swissbytes.businessrules.pricing.enums.*;
import ch.swissbytes.businessrules.pricing.factors.*;
import ch.swissbytes.businessrules.pricing.inputs.PriceComponentRuleInputSaleOrder;
import ch.swissbytes.businessrules.pricing.inputs.PriceComponentRuleInputSaleOrderProduct;
import ch.swissbytes.businessrules.pricing.log.Logger;
import ch.swissbytes.businessrules.pricing.log.LoggerImpl;
import ch.swissbytes.businessrules.pricing.outcomes.PriceComponentRuleOutcomeOutput;
import ch.swissbytes.businessrules.pricing.outcomes.PriceComponentRuleOutcomeProduct;
import ch.swissbytes.businessrules.pricing.subjects.PriceComponentRuleSubjectProduct;
import ch.swissbytes.businessrules.pricing.subjects.PriceComponentRuleSubjectProductFeature;
import ch.swissbytes.businessrules.pricing.utils.LogUtil;
import ch.swissbytes.businessrules.pricing.utils.RandomUtil;
import ch.swissbytes.businessrules.pricing.utils.TestUtil;
import ch.swissbytes.businessrules.pricing.utils.ToJsonString;
import com.tomgibara.bits.BitVector;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.*;


class PriceRuleTest {

    private static final Logger Log = LoggerImpl.getLogger();

    //region Validations for each output
    @Tag("production")
    @Test
    void validateFactorDeliveryMode() {

        PriceComponentRuleInputSaleOrder saleOrder4448All = TestUtil.createSaleOrderRamiro4448WithAllFactors(
                DeliveryModeEnum.MOBILE_STORE, PaymentConditionsTypeEnum.CASH);
        //region General Mode
        PriceRule rule = TestUtil.createRuleOnlyDeliveryMode1001(TestUtil.createAllDeliveryModes(), true, TargetEnum.SALE_ORDER, PriceRuleTypeEnum.GENERAL);
        PriceRule ruleEmpty = TestUtil.createRuleOnlyDeliveryMode1001(Collections.emptyList(), true, TargetEnum.SALE_ORDER, PriceRuleTypeEnum.GENERAL);
        PriceRule ruleFail = TestUtil.createRuleOnlyDeliveryMode1001(Collections.singletonList(TestUtil.createDeliveryModeInWarehouse()), true, TargetEnum.SALE_ORDER, PriceRuleTypeEnum.GENERAL);
        //endregion

        //region Restricted Mode
        PriceRule ruleR = TestUtil.createRuleOnlyDeliveryMode1001(TestUtil.createAllDeliveryModes(), true, TargetEnum.SALE_ORDER, PriceRuleTypeEnum.RESTRICTED);
        PriceRule ruleEmptyR = TestUtil.createRuleOnlyDeliveryMode1001(Collections.emptyList(), true, TargetEnum.SALE_ORDER, PriceRuleTypeEnum.RESTRICTED);
        PriceRule ruleFailR = TestUtil.createRuleOnlyDeliveryMode1001(Collections.singletonList(TestUtil.createDeliveryModeInWarehouse()), true,
                TargetEnum.SALE_ORDER, PriceRuleTypeEnum.RESTRICTED);
        //endregion

        //region Validation General
        assertTrue(rule.isApplicable(saleOrder4448All));
        assertTrue(ruleEmpty.isApplicable(saleOrder4448All));
        assertFalse(ruleFail.isApplicable(saleOrder4448All));
        //endregion

        //region Validation Restricted
        assertTrue(ruleR.isApplicable(saleOrder4448All));
        assertTrue(ruleEmptyR.isApplicable(saleOrder4448All));
        assertFalse(ruleFailR.isApplicable(saleOrder4448All));
        //endregion
    }

    @Tag("production")
    @Test
    void validateFactorPaymentCondition() {
        PriceComponentRuleInputSaleOrder saleOrder4448All = TestUtil.createSaleOrderRamiro4448WithAllFactors(
                DeliveryModeEnum.MOBILE_STORE, PaymentConditionsTypeEnum.CASH);
        PriceRule rule = TestUtil.createRuleOnlyPaymentCondition1002(PaymentConditionsTypeEnum.CASH, true, TargetEnum.SALE_ORDER, PriceRuleTypeEnum.GENERAL);
        PriceRule ruleFail = TestUtil.createRuleOnlyPaymentCondition1002(PaymentConditionsTypeEnum.CREDIT, true, TargetEnum.SALE_ORDER, PriceRuleTypeEnum.GENERAL);

        assertTrue(rule.isApplicable(saleOrder4448All));
        assertFalse(ruleFail.isApplicable(saleOrder4448All));
    }

    @Tag("production")
    @Test
    void validateFactorDeliveryModePaymentCondition() {
        PriceComponentRuleInputSaleOrder saleOrder4448All = TestUtil.createSaleOrderRamiro4448WithAllFactors(
                DeliveryModeEnum.MOBILE_STORE, PaymentConditionsTypeEnum.CASH);

        //region Validation General
        PriceRule rule = TestUtil.createRuleOnlyDeliveryModePaymentCondition1003(
                Collections.singletonList(TestUtil.createDeliveryModeMobileStoreFast()),
                PaymentConditionsTypeEnum.CASH, true, TargetEnum.SALE_ORDER, PriceRuleTypeEnum.GENERAL);
        PriceRule ruleFailDelivery = TestUtil.createRuleOnlyDeliveryModePaymentCondition1003(
                Collections.singletonList(TestUtil.createDeliveryWithDeliveryMode(DeliveryModeEnum.IN_WAREHOUSE)),
                PaymentConditionsTypeEnum.CASH, true, TargetEnum.SALE_ORDER, PriceRuleTypeEnum.GENERAL);
        PriceRule ruleFailPayment = TestUtil.createRuleOnlyDeliveryModePaymentCondition1003(
                Collections.singletonList(TestUtil.createDeliveryModeMobileStoreFast()),
                PaymentConditionsTypeEnum.CREDIT, true, TargetEnum.SALE_ORDER, PriceRuleTypeEnum.GENERAL);
        PriceRule ruleFailAll = TestUtil.createRuleOnlyDeliveryModePaymentCondition1003(
                Collections.singletonList(TestUtil.createDeliveryWithDeliveryMode(DeliveryModeEnum.MOBILE_STORE)),
                PaymentConditionsTypeEnum.CREDIT, true, TargetEnum.SALE_ORDER, PriceRuleTypeEnum.GENERAL);

        assertTrue(rule.isApplicable(saleOrder4448All));
        assertFalse(ruleFailDelivery.isApplicable(saleOrder4448All));
        assertFalse(ruleFailPayment.isApplicable(saleOrder4448All));
        assertFalse(ruleFailAll.isApplicable(saleOrder4448All));
        //endregion
    }

    @Tag("production")
    @Test
    void validateFactorBusinessUnit() {
        PriceComponentRuleInputSaleOrder saleOrder4448All = TestUtil.createSaleOrderRamiro4448WithAllFactors(
                DeliveryModeEnum.MOBILE_STORE, PaymentConditionsTypeEnum.CASH);

        List<PriceComponentRuleFactorBusinessUnit> businessUnits = new ArrayList<>(0);
        businessUnits.add(TestUtil.createBusinessUnitRetail());
        businessUnits.add(TestUtil.createBusinessUnitEntidadesPrivadas());

        List<PriceComponentRuleFactorBusinessUnit> businessUnitsFail = new ArrayList<>(0);
        businessUnitsFail.add(TestUtil.createBusinessUnitEntidadesPrivadas());
        businessUnitsFail.add(TestUtil.createBusinessUnitDistribuidores());

        PriceRule rule = TestUtil.createRuleOnlyBusinessUnit1004(businessUnits, true, TargetEnum.SALE_ORDER, PriceRuleTypeEnum.GENERAL);
        PriceRule ruleFail = TestUtil.createRuleOnlyBusinessUnit1004(businessUnitsFail, true, TargetEnum.SALE_ORDER, PriceRuleTypeEnum.GENERAL);
        PriceRule ruleEmpty = TestUtil.createRuleOnlyBusinessUnit1004(Collections.emptyList(), true, TargetEnum.SALE_ORDER, PriceRuleTypeEnum.GENERAL);

        PriceRule ruleR = TestUtil.createRuleOnlyBusinessUnit1004(businessUnits, true, TargetEnum.SALE_ORDER, PriceRuleTypeEnum.RESTRICTED);
        PriceRule ruleFailR = TestUtil.createRuleOnlyBusinessUnit1004(businessUnitsFail, true, TargetEnum.SALE_ORDER, PriceRuleTypeEnum.RESTRICTED);
        PriceRule ruleEmptyR = TestUtil.createRuleOnlyBusinessUnit1004(Collections.emptyList(), true, TargetEnum.SALE_ORDER, PriceRuleTypeEnum.RESTRICTED);

        PriceRule ruleEmptyNone = TestUtil.createEmptyRule(true, PriceRuleTypeEnum.GENERAL);

        assertTrue(rule.isApplicable(saleOrder4448All));
        assertTrue(ruleEmpty.isApplicable(saleOrder4448All));
        assertTrue(ruleEmptyNone.isApplicable(saleOrder4448All));
        assertFalse(ruleFail.isApplicable(saleOrder4448All));

        assertTrue(ruleR.isApplicable(saleOrder4448All));
        assertTrue(ruleEmptyR.isApplicable(saleOrder4448All));
        assertTrue(ruleEmptyNone.isApplicable(saleOrder4448All));
        assertFalse(ruleFailR.isApplicable(saleOrder4448All));
    }

    @Tag("production")
    @Test
    void validateFactorDeliveryModePaymentConditionBusinessUnit() {
        PriceComponentRuleInputSaleOrder saleOrder4448All = TestUtil.createSaleOrderRamiro4448WithAllFactors(
                DeliveryModeEnum.MOBILE_STORE, PaymentConditionsTypeEnum.CASH);

        List<PriceComponentRuleFactorBusinessUnit> businessUnits = new ArrayList<>(0);
        businessUnits.add(TestUtil.createBusinessUnitRetail());
        businessUnits.add(TestUtil.createBusinessUnitEntidadesPrivadas());

        List<PriceComponentRuleFactorBusinessUnit> businessUnitsFail = new ArrayList<>(0);
        businessUnitsFail.add(TestUtil.createBusinessUnitEntidadesPublicas());
        businessUnitsFail.add(TestUtil.createBusinessUnitEntidadesPrivadas());

        //region General
        PriceRule rule = TestUtil.createRuleOnlyDeliveryPaymentBusinessUnit1005(
                Collections.singletonList(TestUtil.createDeliveryModeMobileStoreFast()),
                PaymentConditionsTypeEnum.CASH,
                businessUnits, true, TargetEnum.SALE_ORDER, PriceRuleTypeEnum.GENERAL);

        PriceRule ruleFailDelivery = TestUtil.createRuleOnlyDeliveryPaymentBusinessUnit1005(
                Collections.singletonList(TestUtil.createDeliveryWithDeliveryMode(DeliveryModeEnum.MOBILE_STORE)),
                PaymentConditionsTypeEnum.CREDIT_ON_DELIVERY,
                businessUnits,
                true, TargetEnum.SALE_ORDER, PriceRuleTypeEnum.GENERAL);

        PriceRule ruleFailPayment = TestUtil.createRuleOnlyDeliveryPaymentBusinessUnit1005(
                Collections.singletonList(TestUtil.createDeliveryModeMobileStoreFast()),
                PaymentConditionsTypeEnum.CREDIT,
                businessUnits,
                true, TargetEnum.SALE_ORDER, PriceRuleTypeEnum.GENERAL);

        PriceRule ruleFailBusinesUnit = TestUtil.createRuleOnlyDeliveryPaymentBusinessUnit1005(
                Collections.singletonList(TestUtil.createDeliveryModeMobileStoreFast()),
                PaymentConditionsTypeEnum.CASH,
                businessUnitsFail, true, TargetEnum.SALE_ORDER, PriceRuleTypeEnum.GENERAL);

        PriceRule ruleFailAll = TestUtil.createRuleOnlyDeliveryPaymentBusinessUnit1005(
                Collections.singletonList(TestUtil.createDeliveryWithDeliveryMode(DeliveryModeEnum.MOBILE_STORE)),
                PaymentConditionsTypeEnum.CREDIT,
                businessUnitsFail, true, TargetEnum.SALE_ORDER, PriceRuleTypeEnum.GENERAL);
        //endregion


        PriceRule ruleR = TestUtil.createRuleOnlyDeliveryPaymentBusinessUnit1005(
                Collections.singletonList(TestUtil.createDeliveryModeMobileStoreFast()),
                PaymentConditionsTypeEnum.CASH,
                businessUnits, true, TargetEnum.SALE_ORDER, PriceRuleTypeEnum.RESTRICTED);

        PriceRule ruleFailDeliveryR = TestUtil.createRuleOnlyDeliveryPaymentBusinessUnit1005(
                Collections.singletonList(TestUtil.createDeliveryWithDeliveryMode(DeliveryModeEnum.MOBILE_STORE)),
                PaymentConditionsTypeEnum.CREDIT_ON_DELIVERY,
                businessUnits,
                true, TargetEnum.SALE_ORDER, PriceRuleTypeEnum.RESTRICTED);

        PriceRule ruleFailPaymentR = TestUtil.createRuleOnlyDeliveryPaymentBusinessUnit1005(
                Collections.singletonList(TestUtil.createDeliveryModeMobileStoreFast()),
                PaymentConditionsTypeEnum.CREDIT,
                businessUnits,
                true, TargetEnum.SALE_ORDER, PriceRuleTypeEnum.RESTRICTED);

        PriceRule ruleFailBusinesUnitR = TestUtil.createRuleOnlyDeliveryPaymentBusinessUnit1005(
                Collections.singletonList(TestUtil.createDeliveryModeMobileStoreFast()),
                PaymentConditionsTypeEnum.CASH,
                businessUnitsFail, true, TargetEnum.SALE_ORDER, PriceRuleTypeEnum.RESTRICTED);

        PriceRule ruleFailAllR = TestUtil.createRuleOnlyDeliveryPaymentBusinessUnit1005(
                Collections.singletonList(TestUtil.createDeliveryWithDeliveryMode(DeliveryModeEnum.MOBILE_STORE)),
                PaymentConditionsTypeEnum.CREDIT,
                businessUnitsFail, true, TargetEnum.SALE_ORDER, PriceRuleTypeEnum.RESTRICTED);

        assertTrue(rule.isApplicable(saleOrder4448All));
        assertFalse(ruleFailDelivery.isApplicable(saleOrder4448All));
        assertFalse(ruleFailPayment.isApplicable(saleOrder4448All));
        assertFalse(ruleFailBusinesUnit.isApplicable(saleOrder4448All));
        assertFalse(ruleFailAll.isApplicable(saleOrder4448All));


        assertTrue(ruleR.isApplicable(saleOrder4448All));
        assertFalse(ruleFailDeliveryR.isApplicable(saleOrder4448All));
        assertFalse(ruleFailPaymentR.isApplicable(saleOrder4448All));
        assertFalse(ruleFailBusinesUnitR.isApplicable(saleOrder4448All));
        assertFalse(ruleFailAllR.isApplicable(saleOrder4448All));
    }

    @Tag("production")
    @Test
    void validateFactorAgency() {
        PriceComponentRuleInputSaleOrder saleOrder4448All = TestUtil.createSaleOrderRamiro4448WithAllFactors(
                DeliveryModeEnum.MOBILE_STORE, PaymentConditionsTypeEnum.CASH);

        List<PriceComponentRuleFactorAgency> agencies = new ArrayList<>(0);
        agencies.add(TestUtil.createAgencySC());
        agencies.add(TestUtil.createAgencyLP());

        List<PriceComponentRuleFactorAgency> agenciesFail = new ArrayList<>(0);
        agenciesFail.add(TestUtil.createAgencyCBBA());
        agenciesFail.add(TestUtil.createAgencyLP());

        PriceRule rule = TestUtil.createRuleOnlyAgency1006(agencies, true, TargetEnum.SALE_ORDER, PriceRuleTypeEnum.GENERAL);
        PriceRule ruleFail = TestUtil.createRuleOnlyAgency1006(agenciesFail, true, TargetEnum.SALE_ORDER, PriceRuleTypeEnum.GENERAL);
        PriceRule ruleEmpty = TestUtil.createRuleOnlyAgency1006(Collections.emptyList(), true, TargetEnum.SALE_ORDER, PriceRuleTypeEnum.GENERAL);
        PriceRule ruleEmptyNone = TestUtil.createEmptyRule(true, PriceRuleTypeEnum.GENERAL);

        assertTrue(rule.isApplicable(saleOrder4448All));
        assertTrue(ruleEmpty.isApplicable(saleOrder4448All));
        assertTrue(ruleEmptyNone.isApplicable(saleOrder4448All));
        assertFalse(ruleFail.isApplicable(saleOrder4448All));
    }

    @Tag("production")
    @Test
    void validateFactorDeliveryModePaymentConditionBusinessUnitAgency() {
        PriceComponentRuleInputSaleOrder saleOrder4448All = TestUtil.createSaleOrderRamiro4448WithAllFactors(
                DeliveryModeEnum.MOBILE_STORE, PaymentConditionsTypeEnum.CASH);

        List<PriceComponentRuleFactorBusinessUnit> businessUnits = new ArrayList<>(0);
        businessUnits.add(TestUtil.createBusinessUnitRetail());
        businessUnits.add(TestUtil.createBusinessUnitEntidadesPrivadas());

        List<PriceComponentRuleFactorBusinessUnit> businessUnitsFail = new ArrayList<>(0);
        businessUnitsFail.add(TestUtil.createBusinessUnitEntidadesPublicas());
        businessUnitsFail.add(TestUtil.createBusinessUnitEntidadesPrivadas());

        List<PriceComponentRuleFactorAgency> agencies = new ArrayList<>(0);
        agencies.add(TestUtil.createAgencySC());
        agencies.add(TestUtil.createAgencyLP());

        List<PriceComponentRuleFactorAgency> agenciesFail = new ArrayList<>(0);
        agenciesFail.add(TestUtil.createAgencyCBBA());
        agenciesFail.add(TestUtil.createAgencyLP());

        //region General
        PriceRule rule = TestUtil.createRuleOnlyDeliveryPaymentBusinessUnitAgency1007(
                Collections.singletonList(TestUtil.createDeliveryModeMobileStoreFast()),
                PaymentConditionsTypeEnum.CASH,
                businessUnits,
                agencies,
                true, TargetEnum.SALE_ORDER, PriceRuleTypeEnum.GENERAL);

        PriceRule ruleFailDelivery = TestUtil.createRuleOnlyDeliveryPaymentBusinessUnitAgency1007(
                Collections.singletonList(TestUtil.createDeliveryWithDeliveryMode(DeliveryModeEnum.IN_WAREHOUSE)),
                PaymentConditionsTypeEnum.CASH,
                businessUnits,
                agencies,
                true, TargetEnum.SALE_ORDER, PriceRuleTypeEnum.GENERAL);

        PriceRule ruleFailPayment = TestUtil.createRuleOnlyDeliveryPaymentBusinessUnitAgency1007(
                Collections.singletonList(TestUtil.createDeliveryModeMobileStoreFast()),
                PaymentConditionsTypeEnum.CREDIT,
                businessUnits,
                agencies,
                true, TargetEnum.SALE_ORDER, PriceRuleTypeEnum.GENERAL);

        PriceRule ruleFailBusinesUnit = TestUtil.createRuleOnlyDeliveryPaymentBusinessUnitAgency1007(
                Collections.singletonList(TestUtil.createDeliveryModeMobileStoreFast()),
                PaymentConditionsTypeEnum.CASH,
                businessUnitsFail,
                agencies,
                true, TargetEnum.SALE_ORDER, PriceRuleTypeEnum.GENERAL);

        PriceRule ruleFailAgency = TestUtil.createRuleOnlyDeliveryPaymentBusinessUnitAgency1007(
                Collections.singletonList(TestUtil.createDeliveryModeMobileStoreFast()),
                PaymentConditionsTypeEnum.CASH,
                businessUnits,
                agenciesFail,
                true, TargetEnum.SALE_ORDER, PriceRuleTypeEnum.GENERAL);

        PriceRule ruleFailAll = TestUtil.createRuleOnlyDeliveryPaymentBusinessUnitAgency1007(
                Collections.singletonList(TestUtil.createDeliveryWithDeliveryMode(DeliveryModeEnum.MOBILE_STORE)),
                PaymentConditionsTypeEnum.CREDIT,
                businessUnitsFail,
                agenciesFail, true, TargetEnum.SALE_ORDER, PriceRuleTypeEnum.GENERAL);
        //endregion

        assertTrue(rule.isApplicable(saleOrder4448All));
        assertFalse(ruleFailDelivery.isApplicable(saleOrder4448All));
        assertFalse(ruleFailPayment.isApplicable(saleOrder4448All));
        assertFalse(ruleFailBusinesUnit.isApplicable(saleOrder4448All));
        assertFalse(ruleFailAgency.isApplicable(saleOrder4448All));
        assertFalse(ruleFailAll.isApplicable(saleOrder4448All));

    }

    @Tag("production")
    @Test
    void validateFactorRoleType() {
        PriceComponentRuleInputSaleOrder saleOrder4448All = TestUtil.createSaleOrderRamiro4448WithAllFactors(
                DeliveryModeEnum.MOBILE_STORE, PaymentConditionsTypeEnum.CASH);

        List<PriceComponentRuleFactorRoleType> roleTypes = new ArrayList<>(0);
        roleTypes.add(TestUtil.createRoleTypeCustomer());
        roleTypes.add(TestUtil.createRoleTypeSeller());
        roleTypes.add(TestUtil.createRoleTypeProspect());

        List<PriceComponentRuleFactorRoleType> roleTypesFail = TestUtil.createRoleTypesEmployee();

        PriceRule rule = TestUtil.createRuleOnlyRoleType1008(roleTypes, true, TargetEnum.SALE_ORDER, PriceRuleTypeEnum.GENERAL);
        PriceRule ruleFail = TestUtil.createRuleOnlyRoleType1008(roleTypesFail, true, TargetEnum.SALE_ORDER, PriceRuleTypeEnum.GENERAL);
        PriceRule ruleEmpty = TestUtil.createRuleOnlyRoleType1008(Collections.emptyList(), true, TargetEnum.SALE_ORDER, PriceRuleTypeEnum.GENERAL);
        PriceRule ruleEmptyNone = TestUtil.createEmptyRule(true, PriceRuleTypeEnum.GENERAL);

        assertTrue(rule.isApplicable(saleOrder4448All));
        assertTrue(ruleEmpty.isApplicable(saleOrder4448All));
        assertTrue(ruleEmptyNone.isApplicable(saleOrder4448All));
        assertFalse(ruleFail.isApplicable(saleOrder4448All));
    }

    @Tag("production")
    @Test
    void validateFactorDeliveryModePaymentConditionBusinessUnitAgencyRoleType() {
        PriceComponentRuleInputSaleOrder saleOrder4448All = TestUtil.createSaleOrderRamiro4448WithAllFactors(
                DeliveryModeEnum.MOBILE_STORE, PaymentConditionsTypeEnum.CASH);

        List<PriceComponentRuleFactorBusinessUnit> businessUnits = new ArrayList<>(0);
        businessUnits.add(TestUtil.createBusinessUnitRetail());
        businessUnits.add(TestUtil.createBusinessUnitEntidadesPrivadas());

        List<PriceComponentRuleFactorBusinessUnit> businessUnitsFail = new ArrayList<>(0);
        businessUnitsFail.add(TestUtil.createBusinessUnitEntidadesPublicas());
        businessUnitsFail.add(TestUtil.createBusinessUnitEntidadesPrivadas());

        List<PriceComponentRuleFactorAgency> agencies = new ArrayList<>(0);
        agencies.add(TestUtil.createAgencySC());
        agencies.add(TestUtil.createAgencyLP());

        List<PriceComponentRuleFactorAgency> agenciesFail = new ArrayList<>(0);
        agenciesFail.add(TestUtil.createAgencyCBBA());
        agenciesFail.add(TestUtil.createAgencyLP());

        List<PriceComponentRuleFactorRoleType> roleTypes = new ArrayList<>(0);
        roleTypes.add(TestUtil.createRoleTypeCustomer());
        roleTypes.add(TestUtil.createRoleTypeSeller());
        roleTypes.add(TestUtil.createRoleTypeProspect());

        List<PriceComponentRuleFactorRoleType> roleTypesFail = TestUtil.createRoleTypesEmployee();

        //region General
        PriceRule rule = TestUtil.createRuleOnlyDeliveryPaymentBusinessUnitAgencyRoleType1009(
                Collections.singletonList(TestUtil.createDeliveryModeMobileStoreFast()),
                PaymentConditionsTypeEnum.CASH,
                businessUnits,
                agencies,
                roleTypes,
                true, TargetEnum.SALE_ORDER, PriceRuleTypeEnum.GENERAL);

        PriceRule ruleFailDelivery = TestUtil.createRuleOnlyDeliveryPaymentBusinessUnitAgencyRoleType1009(
                Collections.singletonList(TestUtil.createDeliveryWithDeliveryMode(DeliveryModeEnum.IN_WAREHOUSE)),
                PaymentConditionsTypeEnum.CASH,
                businessUnits,
                agencies,
                roleTypes,
                true, TargetEnum.SALE_ORDER, PriceRuleTypeEnum.GENERAL);

        PriceRule ruleFailPayment = TestUtil.createRuleOnlyDeliveryPaymentBusinessUnitAgencyRoleType1009(
                Collections.singletonList(TestUtil.createDeliveryModeMobileStoreFast()),
                PaymentConditionsTypeEnum.CREDIT,
                businessUnits,
                agencies,
                roleTypes,
                true, TargetEnum.SALE_ORDER, PriceRuleTypeEnum.GENERAL);

        PriceRule ruleFailBusinesUnit = TestUtil.createRuleOnlyDeliveryPaymentBusinessUnitAgencyRoleType1009(
                Collections.singletonList(TestUtil.createDeliveryModeMobileStoreFast()),
                PaymentConditionsTypeEnum.CASH,
                businessUnitsFail,
                agencies,
                roleTypes,
                true, TargetEnum.SALE_ORDER, PriceRuleTypeEnum.GENERAL);

        PriceRule ruleFailAgency = TestUtil.createRuleOnlyDeliveryPaymentBusinessUnitAgencyRoleType1009(
                Collections.singletonList(TestUtil.createDeliveryModeMobileStoreFast()),
                PaymentConditionsTypeEnum.CASH,
                businessUnits,
                agenciesFail,
                roleTypes,
                true, TargetEnum.SALE_ORDER, PriceRuleTypeEnum.GENERAL);

        PriceRule ruleFailRoleType = TestUtil.createRuleOnlyDeliveryPaymentBusinessUnitAgencyRoleType1009(
                Collections.singletonList(TestUtil.createDeliveryModeMobileStoreFast()),
                PaymentConditionsTypeEnum.CASH,
                businessUnits,
                agencies,
                roleTypesFail,
                true, TargetEnum.SALE_ORDER, PriceRuleTypeEnum.GENERAL);

        PriceRule ruleFailAll = TestUtil.createRuleOnlyDeliveryPaymentBusinessUnitAgencyRoleType1009(
                Collections.singletonList(TestUtil.createDeliveryWithDeliveryMode(DeliveryModeEnum.MOBILE_STORE)),
                PaymentConditionsTypeEnum.CREDIT,
                businessUnitsFail,
                agenciesFail,
                roleTypesFail,
                true, TargetEnum.SALE_ORDER, PriceRuleTypeEnum.GENERAL);
        //endregion

        assertTrue(rule.isApplicable(saleOrder4448All));
        assertFalse(ruleFailDelivery.isApplicable(saleOrder4448All));
        assertFalse(ruleFailPayment.isApplicable(saleOrder4448All));
        assertFalse(ruleFailBusinesUnit.isApplicable(saleOrder4448All));
        assertFalse(ruleFailAgency.isApplicable(saleOrder4448All));
        assertFalse(ruleFailRoleType.isApplicable(saleOrder4448All));
        assertFalse(ruleFailAll.isApplicable(saleOrder4448All));

    }

    @Tag("production")
    @Test
    void validateFactorCustomer() {
        PriceComponentRuleInputSaleOrder saleOrder4448All = TestUtil.createSaleOrderRamiro4448WithAllFactors(
                DeliveryModeEnum.MOBILE_STORE, PaymentConditionsTypeEnum.CASH);

        List<PriceComponentRuleFactorCustomer> customers = new ArrayList<>(0);
        customers.add(TestUtil.createPartyRoleCustomerRamiroAll());
        customers.add(TestUtil.createPartyRoleCustomerTelmaAll());

        List<PriceComponentRuleFactorCustomer> customersFail = new ArrayList<>(0);
        customersFail.add(TestUtil.createPartyRoleCustomerTelmaAll());
        customersFail.add(TestUtil.createPartyRoleEmployeeMalcom());

        PriceRule rule = TestUtil.createRuleOnlyCustomer1010(customers, true, TargetEnum.SALE_ORDER, PriceRuleTypeEnum.GENERAL);
        PriceRule ruleFail = TestUtil.createRuleOnlyCustomer1010(customersFail, true, TargetEnum.SALE_ORDER, PriceRuleTypeEnum.GENERAL);
        PriceRule ruleEmpty = TestUtil.createRuleOnlyCustomer1010(Collections.emptyList(), true, TargetEnum.SALE_ORDER, PriceRuleTypeEnum.GENERAL);
        PriceRule ruleEmptyNone = TestUtil.createEmptyRule(true, PriceRuleTypeEnum.GENERAL);

        assertTrue(rule.isApplicable(saleOrder4448All));
        assertTrue(ruleEmpty.isApplicable(saleOrder4448All));
        assertTrue(ruleEmptyNone.isApplicable(saleOrder4448All));
        assertFalse(ruleFail.isApplicable(saleOrder4448All));
    }

    @Tag("production")
    @Test
    void validateFactorDeliveryModePaymentConditionBusinessUnitAgencyRoleTypeCustomer() {
        PriceComponentRuleInputSaleOrder saleOrder4448All = TestUtil.createSaleOrderRamiro4448WithAllFactors(
                DeliveryModeEnum.MOBILE_STORE, PaymentConditionsTypeEnum.CASH);

        List<PriceComponentRuleFactorBusinessUnit> businessUnits = new ArrayList<>(0);
        businessUnits.add(TestUtil.createBusinessUnitRetail());
        businessUnits.add(TestUtil.createBusinessUnitEntidadesPrivadas());

        List<PriceComponentRuleFactorBusinessUnit> businessUnitsFail = new ArrayList<>(0);
        businessUnitsFail.add(TestUtil.createBusinessUnitEntidadesPublicas());
        businessUnitsFail.add(TestUtil.createBusinessUnitEntidadesPrivadas());

        List<PriceComponentRuleFactorAgency> agencies = new ArrayList<>(0);
        agencies.add(TestUtil.createAgencySC());
        agencies.add(TestUtil.createAgencyLP());

        List<PriceComponentRuleFactorAgency> agenciesFail = new ArrayList<>(0);
        agenciesFail.add(TestUtil.createAgencyCBBA());
        agenciesFail.add(TestUtil.createAgencyLP());

        List<PriceComponentRuleFactorRoleType> roleTypes = new ArrayList<>(0);
        roleTypes.add(TestUtil.createRoleTypeCustomer());
        roleTypes.add(TestUtil.createRoleTypeSeller());
        roleTypes.add(TestUtil.createRoleTypeProspect());

        List<PriceComponentRuleFactorRoleType> roleTypesFail = TestUtil.createRoleTypesEmployee();

        List<PriceComponentRuleFactorCustomer> customers = new ArrayList<>(0);
        customers.add(TestUtil.createPartyRoleCustomerRamiroAll());
        customers.add(TestUtil.createPartyRoleCustomerTelmaAll());

        List<PriceComponentRuleFactorCustomer> customersFail = new ArrayList<>(0);
        customersFail.add(TestUtil.createPartyRoleCustomerTelmaAll());
        customersFail.add(TestUtil.createPartyRoleEmployeeMalcom());

        //region General
        PriceRule rule = TestUtil.createRuleOnlyDeliveryPaymentBusinessUnitAgencyRoleTypeCustomer1011(
                Collections.singletonList(TestUtil.createDeliveryModeMobileStoreFast()),
                PaymentConditionsTypeEnum.CASH,
                businessUnits,
                agencies,
                roleTypes,
                customers,
                true, TargetEnum.SALE_ORDER, PriceRuleTypeEnum.GENERAL);

        PriceRule ruleFailDelivery = TestUtil.createRuleOnlyDeliveryPaymentBusinessUnitAgencyRoleTypeCustomer1011(
                Collections.singletonList(TestUtil.createDeliveryWithDeliveryMode(DeliveryModeEnum.CUSTOMER_DELIVERY_POINT)),
                PaymentConditionsTypeEnum.CASH,
                businessUnits,
                agencies,
                roleTypes,
                customers,
                true, TargetEnum.SALE_ORDER, PriceRuleTypeEnum.GENERAL);

        PriceRule ruleFailPayment = TestUtil.createRuleOnlyDeliveryPaymentBusinessUnitAgencyRoleTypeCustomer1011(
                Collections.singletonList(TestUtil.createDeliveryModeMobileStoreFast()),
                PaymentConditionsTypeEnum.CREDIT,
                businessUnits,
                agencies,
                roleTypes,
                customers,
                true, TargetEnum.SALE_ORDER, PriceRuleTypeEnum.GENERAL);

        PriceRule ruleFailBusinesUnit = TestUtil.createRuleOnlyDeliveryPaymentBusinessUnitAgencyRoleTypeCustomer1011(
                Collections.singletonList(TestUtil.createDeliveryModeMobileStoreFast()),
                PaymentConditionsTypeEnum.CASH,
                businessUnitsFail,
                agencies,
                roleTypes,
                customers,
                true, TargetEnum.SALE_ORDER, PriceRuleTypeEnum.GENERAL);

        PriceRule ruleFailAgency = TestUtil.createRuleOnlyDeliveryPaymentBusinessUnitAgencyRoleTypeCustomer1011(
                Collections.singletonList(TestUtil.createDeliveryModeMobileStoreFast()),
                PaymentConditionsTypeEnum.CASH,
                businessUnits,
                agenciesFail,
                roleTypes,
                customers,
                true, TargetEnum.SALE_ORDER, PriceRuleTypeEnum.GENERAL);

        PriceRule ruleFailRoleType = TestUtil.createRuleOnlyDeliveryPaymentBusinessUnitAgencyRoleTypeCustomer1011(
                Collections.singletonList(TestUtil.createDeliveryModeMobileStoreFast()),
                PaymentConditionsTypeEnum.CASH,
                businessUnits,
                agencies,
                roleTypesFail,
                customers,
                true, TargetEnum.SALE_ORDER, PriceRuleTypeEnum.GENERAL);

        PriceRule ruleFailCustomer = TestUtil.createRuleOnlyDeliveryPaymentBusinessUnitAgencyRoleTypeCustomer1011(
                Collections.singletonList(TestUtil.createDeliveryModeMobileStoreFast()),
                PaymentConditionsTypeEnum.CASH,
                businessUnits,
                agencies,
                roleTypes,
                customersFail,
                true, TargetEnum.SALE_ORDER, PriceRuleTypeEnum.GENERAL);

        PriceRule ruleFailAll = TestUtil.createRuleOnlyDeliveryPaymentBusinessUnitAgencyRoleTypeCustomer1011(
                Collections.singletonList(TestUtil.createDeliveryWithDeliveryMode(DeliveryModeEnum.MOBILE_STORE)),
                PaymentConditionsTypeEnum.CREDIT,
                businessUnitsFail,
                agenciesFail,
                roleTypesFail,
                customersFail,
                true, TargetEnum.SALE_ORDER, PriceRuleTypeEnum.GENERAL);
        //endregion

        assertTrue(rule.isApplicable(saleOrder4448All));
        assertFalse(ruleFailDelivery.isApplicable(saleOrder4448All));
        assertFalse(ruleFailPayment.isApplicable(saleOrder4448All));
        assertFalse(ruleFailBusinesUnit.isApplicable(saleOrder4448All));
        assertFalse(ruleFailAgency.isApplicable(saleOrder4448All));
        assertFalse(ruleFailRoleType.isApplicable(saleOrder4448All));
        assertFalse(ruleFailCustomer.isApplicable(saleOrder4448All));
        assertFalse(ruleFailAll.isApplicable(saleOrder4448All));

    }

    @Tag("production")
    @Test
    void validateFactorPartyClassification() {
        PriceComponentRuleInputSaleOrder saleOrder4448All = TestUtil.createSaleOrderRamiro4448WithAllFactors(
                DeliveryModeEnum.MOBILE_STORE, PaymentConditionsTypeEnum.CASH);

        List<PriceComponentRuleFactorPartyClassification> partyClassifications = TestUtil.createPartyClassificationList();

        List<PriceComponentRuleFactorPartyClassification> partyClassificationsFail = new ArrayList<>(0);
        partyClassificationsFail.add(TestUtil.createPartyClassificationDirectivos());
        partyClassificationsFail.add(TestUtil.createPartyClassificationAdministrativos());
        partyClassificationsFail.add(TestUtil.createPartyClassificationEmployeeThirthLvl());

        PriceRule rule = TestUtil.createRuleOnlyPartyClassification1012(partyClassifications, true, TargetEnum.SALE_ORDER, PriceRuleTypeEnum.GENERAL);
        PriceRule ruleFail = TestUtil.createRuleOnlyPartyClassification1012(partyClassificationsFail, true, TargetEnum.SALE_ORDER, PriceRuleTypeEnum.GENERAL);
        PriceRule ruleEmpty = TestUtil.createRuleOnlyPartyClassification1012(Collections.emptyList(), true, TargetEnum.SALE_ORDER, PriceRuleTypeEnum.GENERAL);
        PriceRule ruleEmptyNone = TestUtil.createEmptyRule(true, PriceRuleTypeEnum.GENERAL);

        assertTrue(rule.isApplicable(saleOrder4448All));
        assertTrue(ruleEmpty.isApplicable(saleOrder4448All));
        assertTrue(ruleEmptyNone.isApplicable(saleOrder4448All));
        assertFalse(ruleFail.isApplicable(saleOrder4448All));
    }

    @Tag("production")
    @Test
    void validateFactorDeliveryModePaymentConditionBusinessUnitAgencyRoleTypeCustomerPartyClassification() {
        PriceComponentRuleInputSaleOrder saleOrder4448All = TestUtil.createSaleOrderRamiro4448WithAllFactors(
                DeliveryModeEnum.MOBILE_STORE, PaymentConditionsTypeEnum.CASH);

        List<PriceComponentRuleFactorBusinessUnit> businessUnits = new ArrayList<>(0);
        businessUnits.add(TestUtil.createBusinessUnitRetail());
        businessUnits.add(TestUtil.createBusinessUnitEntidadesPrivadas());

        List<PriceComponentRuleFactorBusinessUnit> businessUnitsFail = new ArrayList<>(0);
        businessUnitsFail.add(TestUtil.createBusinessUnitEntidadesPublicas());
        businessUnitsFail.add(TestUtil.createBusinessUnitEntidadesPrivadas());

        List<PriceComponentRuleFactorAgency> agencies = new ArrayList<>(0);
        agencies.add(TestUtil.createAgencySC());
        agencies.add(TestUtil.createAgencyLP());

        List<PriceComponentRuleFactorAgency> agenciesFail = new ArrayList<>(0);
        agenciesFail.add(TestUtil.createAgencyCBBA());
        agenciesFail.add(TestUtil.createAgencyLP());

        List<PriceComponentRuleFactorRoleType> roleTypes = new ArrayList<>(0);
        roleTypes.add(TestUtil.createRoleTypeCustomer());
        roleTypes.add(TestUtil.createRoleTypeSeller());
        roleTypes.add(TestUtil.createRoleTypeProspect());

        List<PriceComponentRuleFactorRoleType> roleTypesFail = TestUtil.createRoleTypesEmployee();

        List<PriceComponentRuleFactorCustomer> customers = new ArrayList<>(0);
        customers.add(TestUtil.createPartyRoleCustomerRamiroAll());
        customers.add(TestUtil.createPartyRoleCustomerTelmaAll());

        List<PriceComponentRuleFactorCustomer> customersFail = new ArrayList<>(0);
        customersFail.add(TestUtil.createPartyRoleCustomerTelmaAll());
        customersFail.add(TestUtil.createPartyRoleEmployeeMalcom());

        List<PriceComponentRuleFactorPartyClassification> partyClassifications = new ArrayList<>(0);
        partyClassifications.add(TestUtil.createPartyClassificationDirectivos());
        partyClassifications.add(TestUtil.createPartyClassificationAdministrativos());
        partyClassifications.add(TestUtil.createPartyClassificationCustomerOztomizados());

        List<PriceComponentRuleFactorPartyClassification> partyClassificationsFail = new ArrayList<>(0);
        partyClassificationsFail.add(TestUtil.createPartyClassificationEmployeeThirthLvl());
        partyClassificationsFail.add(TestUtil.createPartyClassificationEmployeeSecondLvl());
        partyClassificationsFail.add(TestUtil.createPartyClassificationAdministrativos());

        //region General
        PriceRule rule = TestUtil.createRuleOnlyDeliveryPaymentBusinessUnitAgencyRoleTypeCustomerPartyClassification1013(
                Collections.singletonList(TestUtil.createDeliveryModeMobileStoreFast()),
                PaymentConditionsTypeEnum.CASH,
                businessUnits,
                agencies,
                roleTypes,
                customers,
                partyClassifications,
                true, TargetEnum.SALE_ORDER, PriceRuleTypeEnum.GENERAL);

        PriceRule ruleFailDelivery = TestUtil.createRuleOnlyDeliveryPaymentBusinessUnitAgencyRoleTypeCustomerPartyClassification1013(
                Collections.singletonList(TestUtil.createDeliveryWithDeliveryMode(DeliveryModeEnum.IN_WAREHOUSE)),
                PaymentConditionsTypeEnum.CASH,
                businessUnits,
                agencies,
                roleTypes,
                customers,
                partyClassifications,
                true, TargetEnum.SALE_ORDER, PriceRuleTypeEnum.GENERAL);

        PriceRule ruleFailPayment = TestUtil.createRuleOnlyDeliveryPaymentBusinessUnitAgencyRoleTypeCustomerPartyClassification1013(
                Collections.singletonList(TestUtil.createDeliveryModeMobileStoreFast()),
                PaymentConditionsTypeEnum.CREDIT,
                businessUnits,
                agencies,
                roleTypes,
                customers,
                partyClassifications,
                true, TargetEnum.SALE_ORDER, PriceRuleTypeEnum.GENERAL);

        PriceRule ruleFailBusinesUnit = TestUtil.createRuleOnlyDeliveryPaymentBusinessUnitAgencyRoleTypeCustomerPartyClassification1013(
                Collections.singletonList(TestUtil.createDeliveryModeMobileStoreFast()),
                PaymentConditionsTypeEnum.CASH,
                businessUnitsFail,
                agencies,
                roleTypes,
                customers,
                partyClassifications,
                true, TargetEnum.SALE_ORDER, PriceRuleTypeEnum.GENERAL);

        PriceRule ruleFailAgency = TestUtil.createRuleOnlyDeliveryPaymentBusinessUnitAgencyRoleTypeCustomerPartyClassification1013(
                Collections.singletonList(TestUtil.createDeliveryModeMobileStoreFast()),
                PaymentConditionsTypeEnum.CASH,
                businessUnits,
                agenciesFail,
                roleTypes,
                customers,
                partyClassifications,
                true, TargetEnum.SALE_ORDER, PriceRuleTypeEnum.GENERAL);

        PriceRule ruleFailRoleType = TestUtil.createRuleOnlyDeliveryPaymentBusinessUnitAgencyRoleTypeCustomerPartyClassification1013(
                Collections.singletonList(TestUtil.createDeliveryModeMobileStoreFast()),
                PaymentConditionsTypeEnum.CASH,
                businessUnits,
                agencies,
                roleTypesFail,
                customers,
                partyClassifications,
                true, TargetEnum.SALE_ORDER, PriceRuleTypeEnum.GENERAL);

        PriceRule ruleFailCustomer = TestUtil.createRuleOnlyDeliveryPaymentBusinessUnitAgencyRoleTypeCustomerPartyClassification1013(
                Collections.singletonList(TestUtil.createDeliveryModeMobileStoreFast()),
                PaymentConditionsTypeEnum.CASH,
                businessUnits,
                agencies,
                roleTypes,
                customersFail,
                partyClassifications,
                true, TargetEnum.SALE_ORDER, PriceRuleTypeEnum.GENERAL);

        PriceRule ruleFailPartyClassification = TestUtil.createRuleOnlyDeliveryPaymentBusinessUnitAgencyRoleTypeCustomerPartyClassification1013(
                Collections.singletonList(TestUtil.createDeliveryModeMobileStoreFast()),
                PaymentConditionsTypeEnum.CASH,
                businessUnits,
                agencies,
                roleTypes,
                customers,
                partyClassificationsFail,
                true, TargetEnum.SALE_ORDER, PriceRuleTypeEnum.GENERAL);

        PriceRule ruleFailAll = TestUtil.createRuleOnlyDeliveryPaymentBusinessUnitAgencyRoleTypeCustomerPartyClassification1013(
                Collections.singletonList(TestUtil.createDeliveryWithDeliveryMode(DeliveryModeEnum.MOBILE_STORE)),
                PaymentConditionsTypeEnum.CREDIT,
                businessUnitsFail,
                agenciesFail,
                roleTypesFail,
                customersFail,
                partyClassificationsFail,
                true, TargetEnum.SALE_ORDER, PriceRuleTypeEnum.GENERAL);
        //endregion

        assertTrue(rule.isApplicable(saleOrder4448All));
        assertFalse(ruleFailDelivery.isApplicable(saleOrder4448All));
        assertFalse(ruleFailPayment.isApplicable(saleOrder4448All));
        assertFalse(ruleFailBusinesUnit.isApplicable(saleOrder4448All));
        assertFalse(ruleFailAgency.isApplicable(saleOrder4448All));
        assertFalse(ruleFailRoleType.isApplicable(saleOrder4448All));
        assertFalse(ruleFailCustomer.isApplicable(saleOrder4448All));
        assertFalse(ruleFailPartyClassification.isApplicable(saleOrder4448All));
        assertFalse(ruleFailAll.isApplicable(saleOrder4448All));

    }

    @Tag("production")
    @Test
    void validateFactorGeographicBoundary() {
        PriceComponentRuleInputSaleOrder saleOrder4448All = TestUtil.createSaleOrderRamiro4448WithAllFactors(
                DeliveryModeEnum.MOBILE_STORE, PaymentConditionsTypeEnum.CASH);

        List<PriceComponentRuleFactorGeographicBoundary> geographicBoundaries = new ArrayList<>(0);
        geographicBoundaries.add(TestUtil.createGeographicBoundaryLP());
        geographicBoundaries.add(TestUtil.createGeographicBoundarySC());

        List<PriceComponentRuleFactorGeographicBoundary> geographicBoundariesFail = new ArrayList<>(0);
        geographicBoundariesFail.add(TestUtil.createGeographicBoundaryLP());
        geographicBoundariesFail.add(TestUtil.createGeographicBoundarySimpsons());

        PriceRule rule = TestUtil.createRuleOnlyGeographicBoundary1014(geographicBoundaries, true, TargetEnum.SALE_ORDER, PriceRuleTypeEnum.GENERAL);
        PriceRule ruleFail = TestUtil.createRuleOnlyGeographicBoundary1014(geographicBoundariesFail, true, TargetEnum.SALE_ORDER, PriceRuleTypeEnum.GENERAL);
        PriceRule ruleEmpty = TestUtil.createRuleOnlyGeographicBoundary1014(Collections.emptyList(), true, TargetEnum.SALE_ORDER, PriceRuleTypeEnum.GENERAL);
        PriceRule ruleEmptyNone = TestUtil.createEmptyRule(true, PriceRuleTypeEnum.GENERAL);

        assertTrue(rule.isApplicable(saleOrder4448All));
        assertTrue(ruleEmpty.isApplicable(saleOrder4448All));
        assertTrue(ruleEmptyNone.isApplicable(saleOrder4448All));
        assertFalse(ruleFail.isApplicable(saleOrder4448All));
    }

    @Tag("production")
    @Test
    void validateFactorDeliveryModePaymentConditionBusinessUnitAgencyRoleTypeCustomerPartyClassificationGeographicBoundary() {
        PriceComponentRuleInputSaleOrder saleOrder4448All = TestUtil.createSaleOrderRamiro4448WithAllFactors(
                DeliveryModeEnum.MOBILE_STORE, PaymentConditionsTypeEnum.CASH);

        List<PriceComponentRuleFactorBusinessUnit> businessUnits = new ArrayList<>(0);
        businessUnits.add(TestUtil.createBusinessUnitRetail());
        businessUnits.add(TestUtil.createBusinessUnitEntidadesPrivadas());

        List<PriceComponentRuleFactorBusinessUnit> businessUnitsFail = new ArrayList<>(0);
        businessUnitsFail.add(TestUtil.createBusinessUnitEntidadesPublicas());
        businessUnitsFail.add(TestUtil.createBusinessUnitEntidadesPrivadas());

        List<PriceComponentRuleFactorAgency> agencies = new ArrayList<>(0);
        agencies.add(TestUtil.createAgencySC());
        agencies.add(TestUtil.createAgencyLP());

        List<PriceComponentRuleFactorAgency> agenciesFail = new ArrayList<>(0);
        agenciesFail.add(TestUtil.createAgencyCBBA());
        agenciesFail.add(TestUtil.createAgencyLP());

        List<PriceComponentRuleFactorRoleType> roleTypes = new ArrayList<>(0);
        roleTypes.add(TestUtil.createRoleTypeCustomer());
        roleTypes.add(TestUtil.createRoleTypeSeller());
        roleTypes.add(TestUtil.createRoleTypeProspect());

        List<PriceComponentRuleFactorRoleType> roleTypesFail = TestUtil.createRoleTypesEmployee();

        List<PriceComponentRuleFactorCustomer> customers = new ArrayList<>(0);
        customers.add(TestUtil.createPartyRoleCustomerRamiroAll());
        customers.add(TestUtil.createPartyRoleCustomerTelmaAll());

        List<PriceComponentRuleFactorCustomer> customersFail = new ArrayList<>(0);
        customersFail.add(TestUtil.createPartyRoleCustomerTelmaAll());
        customersFail.add(TestUtil.createPartyRoleEmployeeMalcom());

        List<PriceComponentRuleFactorPartyClassification> partyClassifications = new ArrayList<>(0);
        partyClassifications.add(TestUtil.createPartyClassificationDirectivos());
        partyClassifications.add(TestUtil.createPartyClassificationAdministrativos());
        partyClassifications.add(TestUtil.createPartyClassificationCustomerOztomizados());

        List<PriceComponentRuleFactorPartyClassification> partyClassificationsFail = new ArrayList<>(0);
        partyClassificationsFail.add(TestUtil.createPartyClassificationEmployeeThirthLvl());
        partyClassificationsFail.add(TestUtil.createPartyClassificationEmployeeSecondLvl());
        partyClassificationsFail.add(TestUtil.createPartyClassificationAdministrativos());


        List<PriceComponentRuleFactorGeographicBoundary> geographicBoundaries = new ArrayList<>(0);
        geographicBoundaries.add(TestUtil.createGeographicBoundaryLP());
        geographicBoundaries.add(TestUtil.createGeographicBoundarySC());

        List<PriceComponentRuleFactorGeographicBoundary> geographicBoundariesFail = new ArrayList<>(0);
        geographicBoundariesFail.add(TestUtil.createGeographicBoundaryLP());
        geographicBoundariesFail.add(TestUtil.createGeographicBoundarySimpsons());

        //region General
        PriceRule rule = TestUtil.createRuleOnlyDlvPymntBuUnitAgencyRoleCstmrPartyClasGeo1015(
                Collections.singletonList(TestUtil.createDeliveryModeMobileStoreFast()),
                PaymentConditionsTypeEnum.CASH,
                businessUnits,
                agencies,
                roleTypes,
                customers,
                partyClassifications,
                geographicBoundaries,
                true, TargetEnum.SALE_ORDER, PriceRuleTypeEnum.GENERAL);

        PriceRule ruleFailDelivery = TestUtil.createRuleOnlyDlvPymntBuUnitAgencyRoleCstmrPartyClasGeo1015(
                Collections.singletonList(TestUtil.createDeliveryWithDeliveryMode(DeliveryModeEnum.IN_WAREHOUSE)),
                PaymentConditionsTypeEnum.CASH,
                businessUnits,
                agencies,
                roleTypes,
                customers,
                partyClassifications,
                geographicBoundaries,
                true, TargetEnum.SALE_ORDER, PriceRuleTypeEnum.GENERAL);

        PriceRule ruleFailPayment = TestUtil.createRuleOnlyDlvPymntBuUnitAgencyRoleCstmrPartyClasGeo1015(
                Collections.singletonList(TestUtil.createDeliveryModeMobileStoreFast()),
                PaymentConditionsTypeEnum.CREDIT,
                businessUnits,
                agencies,
                roleTypes,
                customers,
                partyClassifications,
                geographicBoundaries,
                true, TargetEnum.SALE_ORDER, PriceRuleTypeEnum.GENERAL);

        PriceRule ruleFailBusinesUnit = TestUtil.createRuleOnlyDlvPymntBuUnitAgencyRoleCstmrPartyClasGeo1015(
                Collections.singletonList(TestUtil.createDeliveryModeMobileStoreFast()),
                PaymentConditionsTypeEnum.CASH,
                businessUnitsFail,
                agencies,
                roleTypes,
                customers,
                partyClassifications,
                geographicBoundaries,
                true, TargetEnum.SALE_ORDER, PriceRuleTypeEnum.GENERAL);

        PriceRule ruleFailAgency = TestUtil.createRuleOnlyDlvPymntBuUnitAgencyRoleCstmrPartyClasGeo1015(
                Collections.singletonList(TestUtil.createDeliveryModeMobileStoreFast()),
                PaymentConditionsTypeEnum.CASH,
                businessUnits,
                agenciesFail,
                roleTypes,
                customers,
                partyClassifications,
                geographicBoundaries,
                true, TargetEnum.SALE_ORDER, PriceRuleTypeEnum.GENERAL);

        PriceRule ruleFailRoleType = TestUtil.createRuleOnlyDlvPymntBuUnitAgencyRoleCstmrPartyClasGeo1015(
                Collections.singletonList(TestUtil.createDeliveryModeMobileStoreFast()),
                PaymentConditionsTypeEnum.CASH,
                businessUnits,
                agencies,
                roleTypesFail,
                customers,
                partyClassifications,
                geographicBoundaries,
                true, TargetEnum.SALE_ORDER, PriceRuleTypeEnum.GENERAL);

        PriceRule ruleFailCustomer = TestUtil.createRuleOnlyDlvPymntBuUnitAgencyRoleCstmrPartyClasGeo1015(
                Collections.singletonList(TestUtil.createDeliveryModeMobileStoreFast()),
                PaymentConditionsTypeEnum.CASH,
                businessUnits,
                agencies,
                roleTypes,
                customersFail,
                partyClassifications,
                geographicBoundaries,
                true, TargetEnum.SALE_ORDER, PriceRuleTypeEnum.GENERAL);

        PriceRule ruleFailPartyClassification = TestUtil.createRuleOnlyDlvPymntBuUnitAgencyRoleCstmrPartyClasGeo1015(
                Collections.singletonList(TestUtil.createDeliveryModeMobileStoreFast()),
                PaymentConditionsTypeEnum.CASH,
                businessUnits,
                agencies,
                roleTypes,
                customers,
                partyClassificationsFail,
                geographicBoundaries,
                true, TargetEnum.SALE_ORDER, PriceRuleTypeEnum.GENERAL);

        PriceRule ruleFailGeographicBoundary = TestUtil.createRuleOnlyDlvPymntBuUnitAgencyRoleCstmrPartyClasGeo1015(
                Collections.singletonList(TestUtil.createDeliveryModeMobileStoreFast()),
                PaymentConditionsTypeEnum.CASH,
                businessUnits,
                agencies,
                roleTypes,
                customers,
                partyClassifications,
                geographicBoundariesFail,
                true, TargetEnum.SALE_ORDER, PriceRuleTypeEnum.GENERAL);

        PriceRule ruleFailAll = TestUtil.createRuleOnlyDlvPymntBuUnitAgencyRoleCstmrPartyClasGeo1015(
                Collections.singletonList(TestUtil.createDeliveryWithDeliveryMode(DeliveryModeEnum.MOBILE_STORE)),
                PaymentConditionsTypeEnum.CREDIT,
                businessUnitsFail,
                agenciesFail,
                roleTypesFail,
                customersFail,
                partyClassificationsFail,
                geographicBoundariesFail,
                true, TargetEnum.SALE_ORDER, PriceRuleTypeEnum.GENERAL);
        //endregion

        assertTrue(rule.isApplicable(saleOrder4448All));
        assertFalse(ruleFailDelivery.isApplicable(saleOrder4448All));
        assertFalse(ruleFailPayment.isApplicable(saleOrder4448All));
        assertFalse(ruleFailBusinesUnit.isApplicable(saleOrder4448All));
        assertFalse(ruleFailAgency.isApplicable(saleOrder4448All));
        assertFalse(ruleFailRoleType.isApplicable(saleOrder4448All));
        assertFalse(ruleFailCustomer.isApplicable(saleOrder4448All));
        assertFalse(ruleFailPartyClassification.isApplicable(saleOrder4448All));
        assertFalse(ruleFailGeographicBoundary.isApplicable(saleOrder4448All));
        assertFalse(ruleFailAll.isApplicable(saleOrder4448All));

    }

    @Tag("production")
    @Test
    void validateProductsByRestrictionSingleOneProduct() {
        PriceComponentRuleInputSaleOrder saleOrder4448All = TestUtil.createSaleOrderRamiro4448WithAllFactors(
                DeliveryModeEnum.MOBILE_STORE, PaymentConditionsTypeEnum.CASH);

        //Single Product Test
        List<PriceComponentRuleSubjectProduct> products = new ArrayList<>(0);
        products.add(TestUtil.createProductRule44());

        List<PriceComponentRuleSubjectProduct> productsFail = new ArrayList<>(0);
        productsFail.add(TestUtil.createProductRule47());

        //region Restricted Test
        PriceRule ruleR = TestUtil.createRuleOnlyProductSingleOutput1016(products, ScaleTypeEnum.AMOUNT,
                true, TargetEnum.SALE_ORDER, PriceRuleTypeEnum.RESTRICTED);
        PriceRule ruleFailR = TestUtil.createRuleOnlyProductSingleOutput1016(productsFail, ScaleTypeEnum.AMOUNT,
                true, TargetEnum.SALE_ORDER, PriceRuleTypeEnum.RESTRICTED);
        PriceRule ruleEmptyR = TestUtil.createRuleOnlyProductSingleOutput1016(Collections.emptyList(), ScaleTypeEnum.AMOUNT,
                true, TargetEnum.SALE_ORDER, PriceRuleTypeEnum.RESTRICTED);
        PriceRule ruleEmptyNoneR = TestUtil.createEmptyRule(true, PriceRuleTypeEnum.RESTRICTED);
        //endregion

        //region General Test
        PriceRule rule = TestUtil.createRuleOnlyProductSingleOutput1016(products, ScaleTypeEnum.AMOUNT,
                true, TargetEnum.SALE_ORDER, PriceRuleTypeEnum.GENERAL);
        PriceRule ruleFail = TestUtil.createRuleOnlyProductSingleOutput1016(productsFail, ScaleTypeEnum.AMOUNT,
                true, TargetEnum.SALE_ORDER, PriceRuleTypeEnum.GENERAL);
        PriceRule ruleEmpty = TestUtil.createRuleOnlyProductSingleOutput1016(Collections.emptyList(), ScaleTypeEnum.AMOUNT,
                true, TargetEnum.SALE_ORDER, PriceRuleTypeEnum.GENERAL);
        PriceRule ruleEmptyNone = TestUtil.createEmptyRule(true, PriceRuleTypeEnum.GENERAL);
        //endregion

        assertTrue(ruleR.isApplicable(saleOrder4448All));
        assertTrue(ruleEmptyR.isApplicable(saleOrder4448All));
        assertTrue(ruleEmptyNoneR.isApplicable(saleOrder4448All));
        assertFalse(ruleFailR.isApplicable(saleOrder4448All));

        assertTrue(rule.isApplicable(saleOrder4448All));
        assertTrue(ruleEmpty.isApplicable(saleOrder4448All));
        assertTrue(ruleEmptyNone.isApplicable(saleOrder4448All));
        assertFalse(ruleFail.isApplicable(saleOrder4448All));
    }


    @Tag("production")
    @Test
    void validateProductsByRestrictionSingleAllProduct() {
        PriceComponentRuleInputSaleOrder saleOrder4448All = TestUtil.createSaleOrderRamiro4448WithAllFactors(
                DeliveryModeEnum.MOBILE_STORE, PaymentConditionsTypeEnum.CASH);

        //Single Product Test
        List<PriceComponentRuleSubjectProduct> products = new ArrayList<>(0);
        products.add(TestUtil.createProductRule44());
        products.add(TestUtil.createProductRule48());

        List<PriceComponentRuleSubjectProduct> productsFail = new ArrayList<>(0);
        productsFail.add(TestUtil.createProductRule44());
        productsFail.add(TestUtil.createProductRule48());
        productsFail.add(TestUtil.createProductRule47());

        //region Restricted Test
        PriceRule ruleR = TestUtil.createRuleOnlyProductSingleOutput1016(products, ScaleTypeEnum.AMOUNT,
                true, TargetEnum.SALE_ORDER, PriceRuleTypeEnum.RESTRICTED);
        PriceRule ruleFailR = TestUtil.createRuleOnlyProductSingleOutput1016(productsFail, ScaleTypeEnum.AMOUNT,
                true, TargetEnum.SALE_ORDER, PriceRuleTypeEnum.RESTRICTED);
        PriceRule ruleEmptyR = TestUtil.createRuleOnlyProductSingleOutput1016(Collections.emptyList(), ScaleTypeEnum.AMOUNT,
                true, TargetEnum.SALE_ORDER, PriceRuleTypeEnum.RESTRICTED);
        PriceRule ruleEmptyNoneR = TestUtil.createEmptyRule(true, PriceRuleTypeEnum.RESTRICTED);
        //endregion

        //region General Test
        PriceRule rule = TestUtil.createRuleOnlyProductSingleOutput1016(products, ScaleTypeEnum.AMOUNT,
                true, TargetEnum.SALE_ORDER, PriceRuleTypeEnum.GENERAL);
        PriceRule ruleFail = TestUtil.createRuleOnlyProductSingleOutput1016(productsFail, ScaleTypeEnum.AMOUNT,
                true, TargetEnum.SALE_ORDER, PriceRuleTypeEnum.GENERAL);
        PriceRule ruleEmpty = TestUtil.createRuleOnlyProductSingleOutput1016(Collections.emptyList(), ScaleTypeEnum.AMOUNT,
                true, TargetEnum.SALE_ORDER, PriceRuleTypeEnum.GENERAL);
        PriceRule ruleEmptyNone = TestUtil.createEmptyRule(true, PriceRuleTypeEnum.GENERAL);
        //endregion

        assertTrue(ruleR.isApplicable(saleOrder4448All));
        assertTrue(ruleEmptyR.isApplicable(saleOrder4448All));
        assertTrue(ruleEmptyNoneR.isApplicable(saleOrder4448All));
        assertFalse(ruleFailR.isApplicable(saleOrder4448All));

        assertTrue(rule.isApplicable(saleOrder4448All));
        assertTrue(ruleEmpty.isApplicable(saleOrder4448All));
        assertTrue(ruleEmptyNone.isApplicable(saleOrder4448All));
        assertTrue(ruleFail.isApplicable(saleOrder4448All));
        productsFail.clear();
        productsFail.add(TestUtil.createProductRule47());
        assertFalse(ruleFail.isApplicable(saleOrder4448All));
    }


    @Tag("production")
    @Test
    void validateProductsByRestrictionFrequencyProduct() {
        PriceComponentRuleInputSaleOrder saleOrder4448All = TestUtil.createSaleOrderRamiro4448WithAllFactors(
                DeliveryModeEnum.MOBILE_STORE, PaymentConditionsTypeEnum.CASH);

        //Single Product Test
        List<PriceComponentRuleSubjectProduct> products = new ArrayList<>(0);
        products.add(TestUtil.createProductRule44());
        products.add(TestUtil.createProductRule48());

        List<PriceComponentRuleSubjectProduct> productsFailR = new ArrayList<>(0);
        productsFailR.add(TestUtil.createProductRule47());
        productsFailR.add(TestUtil.createProductRule48());

        List<PriceComponentRuleSubjectProduct> productsFailG = new ArrayList<>(0);
        productsFailG.add(TestUtil.createProductRule44());

        BigDecimal frequency = new BigDecimal(1000);
        //region Restricted Test
        PriceRule ruleR = TestUtil.createRuleOnlyProductFrequencyOutput1017(products, ScaleTypeEnum.AMOUNT,
                frequency,
                true, TargetEnum.SALE_ORDER, PriceRuleTypeEnum.RESTRICTED);
        PriceRule ruleFailR = TestUtil.createRuleOnlyProductFrequencyOutput1017(productsFailR, ScaleTypeEnum.AMOUNT,
                frequency,
                true, TargetEnum.SALE_ORDER, PriceRuleTypeEnum.RESTRICTED);
        PriceRule ruleEmptyR = TestUtil.createRuleOnlyProductFrequencyOutput1017(Collections.emptyList(), ScaleTypeEnum.AMOUNT,
                frequency, true, TargetEnum.SALE_ORDER, PriceRuleTypeEnum.RESTRICTED);
        PriceRule ruleEmptyNoneR = TestUtil.createEmptyRule(true, PriceRuleTypeEnum.RESTRICTED);
        //endregion

        assertTrue(ruleR.isApplicable(saleOrder4448All));
        assertTrue(ruleEmptyR.isApplicable(saleOrder4448All));
        assertTrue(ruleEmptyNoneR.isApplicable(saleOrder4448All));
        assertFalse(ruleFailR.isApplicable(saleOrder4448All));


        //region General Test
        products.clear();
        products.add(TestUtil.createProductRule48());
        products.add(TestUtil.createProductRule47());

        PriceRule rule = TestUtil.createRuleOnlyProductFrequencyOutput1017(products, ScaleTypeEnum.AMOUNT,
                frequency,
                true, TargetEnum.SALE_ORDER, PriceRuleTypeEnum.GENERAL);
        PriceRule ruleFail = TestUtil.createRuleOnlyProductFrequencyOutput1017(productsFailG, ScaleTypeEnum.AMOUNT,
                frequency,
                true, TargetEnum.SALE_ORDER, PriceRuleTypeEnum.GENERAL);
        PriceRule ruleEmpty = TestUtil.createRuleOnlyProductFrequencyOutput1017(Collections.emptyList(), ScaleTypeEnum.AMOUNT,
                frequency, true, TargetEnum.SALE_ORDER, PriceRuleTypeEnum.GENERAL);
        PriceRule ruleEmptyNone = TestUtil.createEmptyRule(true, PriceRuleTypeEnum.GENERAL);
        //endregion

        assertTrue(rule.isApplicable(saleOrder4448All));
        assertTrue(ruleEmpty.isApplicable(saleOrder4448All));
        assertTrue(ruleEmptyNone.isApplicable(saleOrder4448All));
        assertFalse(ruleFail.isApplicable(saleOrder4448All));
    }


    @Tag("production")
    @Test
    void validateProductsByRestrictionScaleProduct() {
        PriceComponentRuleInputSaleOrder saleOrder4448All = TestUtil.createSaleOrderRamiro4448WithAllFactors(
                DeliveryModeEnum.MOBILE_STORE, PaymentConditionsTypeEnum.CASH);

        //Single Product Test
        List<PriceComponentRuleSubjectProduct> products = new ArrayList<>(0);
        products.add(TestUtil.createProductRule44());
        products.add(TestUtil.createProductRule48());

        List<PriceComponentRuleSubjectProduct> productsFailR = new ArrayList<>(0);
        productsFailR.add(TestUtil.createProductRule44());

        List<PriceComponentRuleSubjectProduct> productsFailG = new ArrayList<>(0);
        productsFailG.add(TestUtil.createProductRule44());

        List<PriceComponentRuleFactorScale> scales = new ArrayList<>(0);
        scales.add(TestUtil.createScale(new BigDecimal(500), new BigDecimal(1000), ScaleTypeEnum.AMOUNT, TestUtil.createOutcome10DiscountPerc()));
        scales.add(TestUtil.createScale(new BigDecimal(1001), new BigDecimal(1500), ScaleTypeEnum.AMOUNT, TestUtil.createOutcome15DiscountPerc()));

        //region Restricted Test
        PriceRule ruleR = TestUtil.createRuleOnlyProductScaleOutput1018(products, ScaleTypeEnum.AMOUNT,
                scales,
                true, TargetEnum.SALE_ORDER, PriceRuleTypeEnum.RESTRICTED);
        PriceRule ruleFailR = TestUtil.createRuleOnlyProductScaleOutput1018(productsFailR, ScaleTypeEnum.AMOUNT,
                scales,
                true, TargetEnum.SALE_ORDER, PriceRuleTypeEnum.RESTRICTED);
        PriceRule ruleEmptyR = TestUtil.createRuleOnlyProductScaleOutput1018(Collections.emptyList(), ScaleTypeEnum.AMOUNT,
                scales, true, TargetEnum.SALE_ORDER, PriceRuleTypeEnum.RESTRICTED);
        PriceRule ruleEmptyNoneR = TestUtil.createEmptyRule(true, PriceRuleTypeEnum.RESTRICTED);
        //endregion

        assertTrue(ruleR.isApplicable(saleOrder4448All));
        assertTrue(ruleEmptyR.isApplicable(saleOrder4448All));
        assertTrue(ruleEmptyNoneR.isApplicable(saleOrder4448All));
        assertFalse(ruleFailR.isApplicable(saleOrder4448All));

        //region General Test
        scales.clear();
        scales.add(TestUtil.createScale(new BigDecimal(301), new BigDecimal(500), ScaleTypeEnum.AMOUNT, TestUtil.createOutcome15DiscountPerc()));
        scales.add(TestUtil.createScale(new BigDecimal(501), new BigDecimal(1500), ScaleTypeEnum.AMOUNT, TestUtil.createOutcome10DiscountPerc()));

        products.clear();
        products.add(TestUtil.createProductRule48());
        products.add(TestUtil.createProductRule47());

        PriceRule rule = TestUtil.createRuleOnlyProductScaleOutput1018(products, ScaleTypeEnum.AMOUNT,
                scales,
                true, TargetEnum.SALE_ORDER, PriceRuleTypeEnum.GENERAL);
        PriceRule ruleFail = TestUtil.createRuleOnlyProductScaleOutput1018(productsFailG, ScaleTypeEnum.AMOUNT,
                scales,
                true, TargetEnum.SALE_ORDER, PriceRuleTypeEnum.GENERAL);
        PriceRule ruleEmpty = TestUtil.createRuleOnlyProductScaleOutput1018(Collections.emptyList(), ScaleTypeEnum.AMOUNT,
                scales, true, TargetEnum.SALE_ORDER, PriceRuleTypeEnum.GENERAL);
        PriceRule ruleEmptyNone = TestUtil.createEmptyRule(true, PriceRuleTypeEnum.GENERAL);
        //endregion
        assertTrue(rule.isApplicable(saleOrder4448All));
        assertTrue(ruleEmpty.isApplicable(saleOrder4448All));
        assertTrue(ruleEmptyNone.isApplicable(saleOrder4448All));
        assertFalse(ruleFail.isApplicable(saleOrder4448All));
    }
    //endregion

    @Tag("production")
    @Test
    void validateProductFeaturesByRestrictionScale() {
        PriceComponentRuleInputSaleOrder saleOrder4448All = TestUtil.createSaleOrderRamiro4448WithAllFactors(
                DeliveryModeEnum.MOBILE_STORE, PaymentConditionsTypeEnum.CASH);

        List<PriceComponentRuleSubjectProductFeature> products = new ArrayList<>(0);
        products.add(TestUtil.createProductFeatureBrandHollister());

        List<PriceComponentRuleSubjectProductFeature> productsFail = new ArrayList<>(0);
        productsFail.add(TestUtil.createProductFeature(5001, RandomUtil.getRandomName(), RandomUtil.getRandomName()));
        productsFail.add(TestUtil.createProductFeature(5002, RandomUtil.getRandomName(), RandomUtil.getRandomName()));
        productsFail.add(TestUtil.createProductFeature(5003, RandomUtil.getRandomName(), RandomUtil.getRandomName()));

        List<PriceComponentRuleFactorScale> scales = new ArrayList<>(0);
        scales.add(TestUtil.createScale(new BigDecimal(0), new BigDecimal(20), ScaleTypeEnum.QUANTITY, TestUtil.createOutcome10DiscountPerc()));

        PriceRule rule = TestUtil.createRuleOnlyProductFeaturesScaleOutput1019(products, ScaleTypeEnum.QUANTITY,
                scales,
                true, TargetEnum.SALE_ORDER, PriceRuleTypeEnum.GENERAL);
        PriceRule ruleFail = TestUtil.createRuleOnlyProductFeaturesScaleOutput1019(productsFail, ScaleTypeEnum.QUANTITY,
                scales,
                true, TargetEnum.SALE_ORDER, PriceRuleTypeEnum.GENERAL);
        PriceRule ruleEmpty = TestUtil.createRuleOnlyProductFeaturesScaleOutput1019(Collections.emptyList(), ScaleTypeEnum.QUANTITY,
                scales, true, TargetEnum.SALE_ORDER, PriceRuleTypeEnum.GENERAL);
        PriceRule ruleEmptyNone = TestUtil.createEmptyRule(true, PriceRuleTypeEnum.GENERAL);

        assertTrue(rule.isApplicable(saleOrder4448All));
        assertTrue(ruleEmpty.isApplicable(saleOrder4448All));
        assertTrue(ruleEmptyNone.isApplicable(saleOrder4448All));
        assertFalse(ruleFail.isApplicable(saleOrder4448All));
    }


    @Tag("production")
    @Test
    void validateProductClassificationByRestrictionScale() {
        PriceComponentRuleInputSaleOrder saleOrder4448All = TestUtil.createSaleOrderRamiro4448WithAllFactors(
                DeliveryModeEnum.MOBILE_STORE, PaymentConditionsTypeEnum.CASH);

        List<PriceComponentRuleFactorProductClassification> classifications = new ArrayList<>(0);
        classifications.add(TestUtil.createProductClassificationPediatria());
        classifications.add(TestUtil.createProductClassificationQuirofano());

        List<PriceComponentRuleFactorProductClassification> classificationsFail = new ArrayList<>(0);
        classificationsFail.add(TestUtil.createProductClassificationAdministrativo());

        List<PriceComponentRuleFactorScale> scales = new ArrayList<>(0);
        scales.add(TestUtil.createScale(new BigDecimal(0), new BigDecimal(5), ScaleTypeEnum.QUANTITY, TestUtil.createOutcome10DiscountPerc()));
        scales.add(TestUtil.createScale(new BigDecimal(6), new BigDecimal(10), ScaleTypeEnum.QUANTITY, TestUtil.createOutcome10DiscountPerc()));
        scales.add(TestUtil.createScale(new BigDecimal(11), new BigDecimal(20), ScaleTypeEnum.QUANTITY, TestUtil.createOutcome10DiscountPerc()));

        List<PriceComponentRuleFactorScale> scalesAmount = new ArrayList<>(0);
        scalesAmount.add(TestUtil.createScale(new BigDecimal(301), new BigDecimal(1500), ScaleTypeEnum.AMOUNT, TestUtil.createOutcome10DiscountPerc()));

        PriceRule rule = TestUtil.createRuleOnlyProductClassificationScaleOutput1020(classifications, ScaleTypeEnum.QUANTITY,
                scales,
                true, TargetEnum.SALE_ORDER, PriceRuleTypeEnum.GENERAL);
        PriceRule ruleFail = TestUtil.createRuleOnlyProductClassificationScaleOutput1020(classificationsFail, ScaleTypeEnum.QUANTITY,
                scales,
                true, TargetEnum.SALE_ORDER, PriceRuleTypeEnum.GENERAL);
        PriceRule rule2 = TestUtil.createRuleOnlyProductClassificationScaleOutput1020(classifications, ScaleTypeEnum.AMOUNT,
                scalesAmount,
                true, TargetEnum.SALE_ORDER, PriceRuleTypeEnum.GENERAL);
        PriceRule ruleFail3 = TestUtil.createRuleOnlyProductClassificationScaleOutput1020(classificationsFail, ScaleTypeEnum.AMOUNT,
                scalesAmount,
                true, TargetEnum.SALE_ORDER, PriceRuleTypeEnum.GENERAL);
        PriceRule ruleEmpty = TestUtil.createRuleOnlyProductClassificationScaleOutput1020(Collections.emptyList(), ScaleTypeEnum.QUANTITY,
                scales, true, TargetEnum.SALE_ORDER, PriceRuleTypeEnum.GENERAL);
        PriceRule ruleEmptyNone = TestUtil.createEmptyRule(true, PriceRuleTypeEnum.GENERAL);

        assertTrue(rule.isApplicable(saleOrder4448All));
        assertTrue(rule2.isApplicable(saleOrder4448All));
        assertTrue(ruleEmpty.isApplicable(saleOrder4448All));
        assertTrue(ruleEmptyNone.isApplicable(saleOrder4448All));
        assertFalse(ruleFail.isApplicable(saleOrder4448All));
        assertFalse(ruleFail3.isApplicable(saleOrder4448All));
    }

    @Tag("production")
    @Test
    void validateCalculateTotalAmount() {

        PriceComponentRuleInputSaleOrder saleOrder4448All = TestUtil.createSaleOrderRamiro4448WithAllFactors(
                DeliveryModeEnum.MOBILE_STORE, PaymentConditionsTypeEnum.CASH);

        List<PriceComponentRuleSubjectProduct> products = new ArrayList<>(0);
        products.add(TestUtil.createProductRule44());

        PriceRule rule = TestUtil.createRuleOnlyProductSingleOutput1016(products, ScaleTypeEnum.AMOUNT,
                true, TargetEnum.SALE_ORDER, PriceRuleTypeEnum.GENERAL);

        BigDecimal expectedOnly44 = new BigDecimal(300);
        BigDecimal total = rule.calculateProductsTotal(saleOrder4448All);
        LogUtil.warn("Calculate Total: " + total.toString());
        assertEquals(expectedOnly44, total);

        BigDecimal expectedOnly48 = new BigDecimal(1000);
        products.clear();
        products.add(TestUtil.createProductRule48());
        total = rule.calculateProductsTotal(saleOrder4448All);
        LogUtil.warn("Calculate Total: " + total.toString());
        assertEquals(expectedOnly48, total);

        BigDecimal expected4448 = new BigDecimal(1300);
        products.add(TestUtil.createProductRule44());
        total = rule.calculateProductsTotal(saleOrder4448All);
        LogUtil.warn("Calculate Total: " + total.toString());
        assertEquals(expected4448, total);
    }

    @Tag("production")
    @Test
    void validateCalculateTotalQty() {

        PriceComponentRuleInputSaleOrder saleOrder4448All = TestUtil.createSaleOrderRamiro4448WithAllFactors(
                DeliveryModeEnum.MOBILE_STORE, PaymentConditionsTypeEnum.CASH);

        List<PriceComponentRuleSubjectProduct> products = new ArrayList<>(0);
        products.add(TestUtil.createProductRule44());

        PriceRule rule = TestUtil.createRuleOnlyProductSingleOutput1016(products, ScaleTypeEnum.QUANTITY,
                true, TargetEnum.SALE_ORDER, PriceRuleTypeEnum.GENERAL);

        BigDecimal expectedOnly44 = new BigDecimal(1);
        BigDecimal total = rule.calculateProductsTotal(saleOrder4448All);
        LogUtil.warn("Calculate Total Qty: " + total.toString());
        assertEquals(expectedOnly44, total);

        BigDecimal expectedOnly48 = new BigDecimal(10);
        products.clear();
        products.add(TestUtil.createProductRule48());
        total = rule.calculateProductsTotal(saleOrder4448All);
        LogUtil.warn("Calculate Total Qty: " + total.toString());
        assertEquals(expectedOnly48, total);

        BigDecimal expected4448 = new BigDecimal(11);
        products.add(TestUtil.createProductRule44());
        total = rule.calculateProductsTotal(saleOrder4448All);
        LogUtil.warn("Calculate Total Qty: " + total.toString());
        assertEquals(expected4448, total);
    }

    @Tag("production")
    @Test
    void isApplicable() {
        PriceComponentRuleInputSaleOrder saleOrder48 = TestUtil.createSaleOrderRamiro48();
        PriceComponentRuleInputSaleOrder saleOrder4448 = TestUtil.createSaleOrderRamiro4448();
        PriceComponentRuleInputSaleOrder saleOrder4448All = TestUtil.createSaleOrderRamiro4448WithAllFactors(
                DeliveryModeEnum.MOBILE_STORE, PaymentConditionsTypeEnum.CASH);
        PriceComponentRuleInputSaleOrder saleOrder444847 = TestUtil.createSaleOrderRamiro444847();
        PriceComponentRuleInputSaleOrder saleOrder47 = TestUtil.createSaleOrderRamiro47(true, DeliveryModeEnum.MOBILE_STORE, PaymentConditionsTypeEnum.CASH);

        PriceRule priceRule = TestUtil.createRule10DiscountPercentageRetailWithProduct4448(true, PriceRuleTypeEnum.RESTRICTED, TargetEnum.SALE_ORDER);
        assertTrue(priceRule.isApplicable(saleOrder4448));
        assertTrue(priceRule.isApplicable(saleOrder444847));
        assertFalse(priceRule.isApplicable(saleOrder48));
        assertFalse(priceRule.isApplicable(saleOrder47));

        PriceRule priceRule1 = TestUtil.createRule10DiscountPercentageRetailWithProduct4448(true, PriceRuleTypeEnum.GENERAL, TargetEnum.SALE_ORDER);
        assertTrue(priceRule1.isApplicable(saleOrder48));
        assertTrue(priceRule1.isApplicable(saleOrder4448));
        assertTrue(priceRule1.isApplicable(saleOrder444847));
        assertFalse(priceRule1.isApplicable(saleOrder47));

        PriceRule priceRule2 = TestUtil.createRule10DiscountPercentageRetailWithProduct44(false, PriceRuleTypeEnum.RESTRICTED, TargetEnum.SALE_ORDER);
        assertTrue(priceRule2.isApplicable(saleOrder4448));
        assertFalse(priceRule2.isApplicable(saleOrder48));
        assertFalse(priceRule2.isApplicable(saleOrder47));

        PriceRule priceRule3 = TestUtil.createRule15DiscountPercentageRetailWithProduct48(true, PriceRuleTypeEnum.GENERAL, TargetEnum.SALE_ORDER);
        assertTrue(priceRule3.isApplicable(saleOrder48));
        assertTrue(priceRule3.isApplicable(saleOrder4448));
        assertFalse(priceRule3.isApplicable(saleOrder47));

        //test with geographic boundaries and party Classifications
        PriceRule priceRule4 = TestUtil.createRuleProductBonification47WithAllFactors(new Date(), null, 0, 20,
                BigDecimal.ZERO, new BigDecimal(10000.0), new BigDecimal(0.0), new BigDecimal(10000),
                Collections.singletonList(TestUtil.createDeliveryModeMobileStoreFast()), PaymentConditionsTypeEnum.CASH
                , false, PriceRuleTypeEnum.RESTRICTED);

        assertFalse(priceRule4.isApplicable(saleOrder48));
        assertTrue(priceRule4.isApplicable(saleOrder4448All));
        assertFalse(priceRule4.isApplicable(saleOrder444847));
        assertFalse(priceRule4.isApplicable(saleOrder47));

    }

    @Tag("production")
    @Test
    void isApplicableOnlyByPartyClassification() {
        PriceComponentRuleInputSaleOrder saleOrder48 = TestUtil.createSaleOrderRamiro48();

        PriceComponentRuleInputSaleOrder saleOrder47 = TestUtil.createSaleOrderRamiro47(false,
                DeliveryModeEnum.MOBILE_STORE, PaymentConditionsTypeEnum.CASH);

        PriceComponentRuleInputSaleOrder saleOrder4448All = TestUtil.createSaleOrderRamiro4448WithAllFactors(
                DeliveryModeEnum.MOBILE_STORE, PaymentConditionsTypeEnum.CASH);

        PriceComponentRuleInputSaleOrder saleOrderFail = TestUtil.createSaleOrderMalcom48(DeliveryModeEnum.MOBILE_STORE,
                PaymentConditionsTypeEnum.CASH);
        saleOrderFail.getCustomer().setPartyClassifications(Collections.singletonList(TestUtil.createPartyClassificationDirectivos()));

        PriceRule priceRule = TestUtil.createRuleProductBonification4748OnlyWithPartyClassifications(
                Collections.singletonList(TestUtil.createDeliveryModeMobileStoreFast()), PaymentConditionsTypeEnum.CASH
                , false, PriceRuleTypeEnum.RESTRICTED);

        assertTrue(priceRule.isApplicable(saleOrder48));
        assertTrue(priceRule.isApplicable(saleOrder4448All));
        assertTrue(priceRule.isApplicable(saleOrder47));
        assertFalse(priceRule.isApplicable(saleOrderFail));
    }

    @Tag("production")
    @Test
    public void validateRoleTypeByRestriction() {
        PriceComponentRuleInputSaleOrder saleOrderCustomerRamiro = TestUtil.createSaleOrderRamiro4448WithAllFactors(
                DeliveryModeEnum.MOBILE_STORE, PaymentConditionsTypeEnum.CASH);

        PriceComponentRuleInputSaleOrder saleOrderCustomerTelma = TestUtil.createSaleOrderRetailTelma444850(
                DeliveryModeEnum.MOBILE_STORE, PaymentConditionsTypeEnum.CASH);

        PriceComponentRuleInputSaleOrder saleOrderEmployeeFail = TestUtil.createSaleOrderMalcom48(
                DeliveryModeEnum.MOBILE_STORE, PaymentConditionsTypeEnum.CASH);

        PriceRule priceRule = TestUtil.createRuleProductBonification47WithAllFactors(new Date(), null, 0, 20,
                BigDecimal.ZERO, new BigDecimal(10000.0), new BigDecimal(0.0), new BigDecimal(10000),
                Collections.singletonList(TestUtil.createDeliveryModeMobileStoreFast()), PaymentConditionsTypeEnum.CASH
                , false, PriceRuleTypeEnum.RESTRICTED);

        assertTrue(priceRule.validateRoleTypeByRestriction(saleOrderCustomerRamiro));
        assertTrue(priceRule.validateRoleTypeByRestriction(saleOrderCustomerTelma));
        assertFalse(priceRule.validateRoleTypeByRestriction(saleOrderEmployeeFail));

    }

    @Tag("production")
    @Test
    public void validatePartyClassificationByRestriction() {
        PriceComponentRuleInputSaleOrder saleOrder = TestUtil.createSaleOrderRamiro4448WithAllFactors(
                DeliveryModeEnum.MOBILE_STORE, PaymentConditionsTypeEnum.CASH);

        PriceComponentRuleInputSaleOrder saleOrderFail = TestUtil.createSaleOrderRetailTelma444850(
                DeliveryModeEnum.MOBILE_STORE, PaymentConditionsTypeEnum.CASH);

        PriceRule priceRule = TestUtil.createRuleProductBonification47WithAllFactors(new Date(), null, 0, 20,
                BigDecimal.ZERO, new BigDecimal(10000.0), new BigDecimal(0.0), new BigDecimal(10000),
                Collections.singletonList(TestUtil.createDeliveryModeMobileStoreFast()), PaymentConditionsTypeEnum.CASH
                , false, PriceRuleTypeEnum.RESTRICTED);

        BitVector vector = new BitVector(2);
        AtomicInteger i = new AtomicInteger();
        checkPartyClassifications(saleOrder, priceRule, vector, i);

        assertTrue(vector.getBit(0));
        assertTrue(vector.getBit(1));

        vector.setAll(false);
        i.set(0);
        checkPartyClassifications(saleOrderFail, priceRule, vector, i);

        assertFalse(vector.getBit(0));
        assertTrue(vector.getBit(1));

    }

    @Tag("production")
    @Test
    public void validateAgencyByRestriction() {
        PriceComponentRuleInputSaleOrder saleOrder = TestUtil.createSaleOrderRamiro4448WithAllFactors(
                DeliveryModeEnum.MOBILE_STORE, PaymentConditionsTypeEnum.CASH);

        PriceComponentRuleInputSaleOrder saleOrderFail = TestUtil.createSaleOrderRetailTelma444850(
                DeliveryModeEnum.MOBILE_STORE, PaymentConditionsTypeEnum.CASH);

        PriceRule priceRule = TestUtil.createRuleProductBonification47WithAllFactors(new Date(), null, 0, 20,
                BigDecimal.ZERO, new BigDecimal(10000.0), new BigDecimal(0.0), new BigDecimal(10000),
                Collections.singletonList(TestUtil.createDeliveryModeMobileStoreFast()), PaymentConditionsTypeEnum.CASH
                , false, PriceRuleTypeEnum.RESTRICTED);

        BitVector vector = new BitVector(2);
        AtomicInteger i = new AtomicInteger();
        checkAgencies(saleOrder, priceRule, vector, i);

        assertFalse(vector.getBit(0));
        assertTrue(vector.getBit(1));

        vector.setAll(false);
        i.set(0);
        checkAgencies(saleOrderFail, priceRule, vector, i);

        assertFalse(vector.getBit(0));
        assertFalse(vector.getBit(1));

    }

    private void checkAgencies(PriceComponentRuleInputSaleOrder saleOrder, PriceRule priceRule, BitVector vector, AtomicInteger i) {
        priceRule.getAgencies().forEach(agency -> {
            boolean ans = agency.isValid(saleOrder);
            Log.info(i.get() + " agency: " + agency.getName() + " -> " + ans + " for so Agency: " + saleOrder.getAgency().getName());
            vector.setBit(i.get(), ans);
            i.getAndIncrement();
        });
    }

    private void checkPartyClassifications(PriceComponentRuleInputSaleOrder saleOrder, PriceRule priceRule, BitVector vector, AtomicInteger i) {
        priceRule.getPartyClassifications().forEach(partyClassification -> {
            boolean ans = partyClassification.isValid(saleOrder);
            Log.info(i.get() + " classification: " + partyClassification.getName() + " -> " + ans);
            vector.setBit(i.get(), ans);
            i.getAndIncrement();
        });
    }

    @Tag("production")
    @Test
    public void validateProductClassificationByActivation() {
        PriceComponentRuleInputSaleOrder saleOrder = TestUtil.createSaleOrderRamiro4448WithAllFactors(
                DeliveryModeEnum.MOBILE_STORE, PaymentConditionsTypeEnum.CASH);

        PriceComponentRuleInputSaleOrder saleOrder2 = TestUtil.createSaleOrderRetailTelma444850(
                DeliveryModeEnum.MOBILE_STORE, PaymentConditionsTypeEnum.CASH);

        PriceComponentRuleInputSaleOrder saleOrderFail = TestUtil.createSaleOrderRamiro48(true, false);

        PriceComponentRuleInputSaleOrder saleOrderFail2 = TestUtil.createSaleOrderRamiro48(false, false);

        PriceRule priceRule = TestUtil.createRuleProductBonification47WithAllFactors(new Date(), null, 0, 20,
                BigDecimal.ZERO, new BigDecimal(10000.0), new BigDecimal(0.0), new BigDecimal(10000),
                Collections.singletonList(TestUtil.createDeliveryModeMobileStoreFast()), PaymentConditionsTypeEnum.CASH
                , false, PriceRuleTypeEnum.RESTRICTED);

        assertTrue(priceRule.validateProductClassificationByActivation(saleOrder));
        assertTrue(priceRule.validateProductClassificationByActivation(saleOrder2));
        assertTrue(priceRule.validateProductClassificationByActivation(saleOrderFail));
        assertFalse(priceRule.validateProductClassificationByActivation(saleOrderFail2));
    }

    @Tag("production")
    @Test
    public void validateTestCase3RulesBestDiscountPerc() {
        PriceComponentRuleInputSaleOrder saleOrder2Products = TestUtil.createSaleOrderRamiro4448WithAllFactors(
                DeliveryModeEnum.MOBILE_STORE, PaymentConditionsTypeEnum.CASH);

        PriceComponentRuleInputSaleOrder saleOrder1Products = TestUtil.createSaleOrderRamiro4448WithAllFactors(
                DeliveryModeEnum.MOBILE_STORE, PaymentConditionsTypeEnum.CASH);
        saleOrder1Products.getProductDetails().remove(1);

        List<PriceComponentRuleSubjectProduct> products = new ArrayList<>(0);
        products.add(TestUtil.createProductRule44());

        PriceComponentRuleOutcomeOutput outputDisc5 = TestUtil.createOutcomeOutput(1L, TestUtil.createOutcomeDiscountPerc(new BigDecimal(5)), ScaleTypeEnum.AMOUNT, OutComeModeEnum.SINGLE, null, null, PriceComponentOutcomeTypeEnum.DISCOUNT_PERCENTAGE);

        PriceComponentRuleOutcomeOutput outputDisc3 = TestUtil.createOutcomeOutput(999L, TestUtil.createOutcomeDiscountPerc(new BigDecimal(3)), ScaleTypeEnum.AMOUNT, OutComeModeEnum.SINGLE, null, null, PriceComponentOutcomeTypeEnum.DISCOUNT_PERCENTAGE);

        PriceComponentRuleOutcomeOutput outputDisc4 = TestUtil.createOutcomeOutput(1000L, TestUtil.createOutcomeDiscountPerc(new BigDecimal(4)), ScaleTypeEnum.AMOUNT, OutComeModeEnum.SINGLE, null, null, PriceComponentOutcomeTypeEnum.DISCOUNT_PERCENTAGE);

        List<PriceComponentRuleFactorBusinessUnit> businessUnits = Collections.singletonList(TestUtil.createBusinessUnitRetail());

        PriceRule ruleOne = TestUtil.createRuleOnlyBusinessUnit(1050L, businessUnits, false, TargetEnum.PRODUCT, PriceRuleTypeEnum.RESTRICTED, ScaleTypeEnum.AMOUNT, outputDisc5, OutComeModeEnum.SINGLE);

        PriceRule ruleTwo = TestUtil.createRuleOnlyProducts(1068L, products, true, TargetEnum.PRODUCT, PriceRuleTypeEnum.RESTRICTED, ScaleTypeEnum.AMOUNT, outputDisc3, OutComeModeEnum.SINGLE);

        PriceRule ruleThree = TestUtil.createRuleOnlyProducts(1069L, products, true, TargetEnum.PRODUCT, PriceRuleTypeEnum.RESTRICTED, ScaleTypeEnum.AMOUNT, outputDisc4, OutComeModeEnum.SINGLE);

        List<PriceRule> rules = new ArrayList<>(0);
        rules.add(ruleOne);
        rules.add(ruleTwo);
        rules.add(ruleThree);

        PriceComponentRule componentRule = PriceComponentRule.builder()
                .rules(rules)
                .resolutionMode(PriceRuleResolutionModeEnum.BY_BEST_VALUE_SALE_ORDER)
                .allowSetupLogger(false)
                .build();

        List<PriceComponentRuleInputSaleOrderProduct> results = componentRule.computate(saleOrder2Products);


        assertThat(results.get(0).getRulesApplied().get(0).getId(), is(1050L));
        assertThat(results.get(1).getRulesApplied().get(0).getId(), is(1050L));

        LogUtil.warn("Results: \n" + ToJsonString.toJson(results));

    }

    @Tag("production")
    @Test
    public void validateTestCase3RulesBestDiscountPercAllRules() {
        PriceComponentRuleInputSaleOrder saleOrder2Products = TestUtil.createSaleOrderRamiro4448WithAllFactors(
                DeliveryModeEnum.MOBILE_STORE, PaymentConditionsTypeEnum.CASH);

        PriceComponentRuleInputSaleOrder saleOrder1Products = TestUtil.createSaleOrderRamiro4448WithAllFactors(
                DeliveryModeEnum.MOBILE_STORE, PaymentConditionsTypeEnum.CASH);
        saleOrder1Products.getProductDetails().remove(1);

        List<PriceComponentRuleSubjectProduct> products = new ArrayList<>(0);
        products.add(TestUtil.createProductRule44());

        PriceComponentRuleOutcomeOutput outputDisc5 = TestUtil.createOutcomeOutput(1L, TestUtil.createOutcomeDiscountPerc(new BigDecimal(5)), ScaleTypeEnum.AMOUNT, OutComeModeEnum.SINGLE, null, null, PriceComponentOutcomeTypeEnum.DISCOUNT_PERCENTAGE);

        PriceComponentRuleOutcomeOutput outputDisc3 = TestUtil.createOutcomeOutput(999L, TestUtil.createOutcomeDiscountPerc(new BigDecimal(3)), ScaleTypeEnum.AMOUNT, OutComeModeEnum.SINGLE, null, null, PriceComponentOutcomeTypeEnum.DISCOUNT_PERCENTAGE);

        PriceComponentRuleOutcomeOutput outputDisc4 = TestUtil.createOutcomeOutput(1000L, TestUtil.createOutcomeDiscountPerc(new BigDecimal(4)), ScaleTypeEnum.AMOUNT, OutComeModeEnum.SINGLE, null, null, PriceComponentOutcomeTypeEnum.DISCOUNT_PERCENTAGE);

        List<PriceComponentRuleFactorBusinessUnit> businessUnits = Collections.singletonList(TestUtil.createBusinessUnitRetail());

        PriceRule ruleOne = TestUtil.createRuleOnlyBusinessUnit(1050L, businessUnits, false, TargetEnum.PRODUCT, PriceRuleTypeEnum.RESTRICTED, ScaleTypeEnum.AMOUNT, outputDisc5, OutComeModeEnum.SINGLE);

        PriceRule ruleTwo = TestUtil.createRuleOnlyProducts(1068L, products, true, TargetEnum.PRODUCT, PriceRuleTypeEnum.RESTRICTED, ScaleTypeEnum.AMOUNT, outputDisc3, OutComeModeEnum.SINGLE);

        PriceRule ruleThree = TestUtil.createRuleOnlyProducts(1069L, products, true, TargetEnum.PRODUCT, PriceRuleTypeEnum.RESTRICTED, ScaleTypeEnum.AMOUNT, outputDisc4, OutComeModeEnum.SINGLE);

        List<PriceRule> rules = new ArrayList<>(0);
        rules.add(ruleOne);
        rules.add(ruleTwo);
        rules.add(ruleThree);

        PriceComponentRule componentRule = PriceComponentRule.builder()
                .rules(rules)
                .resolutionMode(PriceRuleResolutionModeEnum.BY_BEST_VALUE_PRODUCT)
                .allowSetupLogger(false)
                .build();

        List<PriceComponentRuleInputSaleOrderProduct> results = componentRule.computate(saleOrder2Products);
        PriceComponentRuleInputSaleOrderProduct product44 = results.stream().filter(product -> product.getProduct().getId() == 44).findFirst().orElse(null);
        PriceComponentRuleInputSaleOrderProduct product48 = results.stream().filter(product -> product.getProduct().getId() == 48).findFirst().orElse(null);

        if (Objects.isNull(product44))
            fail("Product 44 does not exists");
        assertThat(product44.getRulesApplied().get(0).getId(), is(1068L));
        assertThat(product44.getRulesApplied().get(1).getId(), is(1069L));

        if (Objects.isNull(product48))
            fail("Product 48 does not exists.");
        assertThat(product48.getRulesApplied().get(0).getId(), is(1050L));

        LogUtil.warn("Results: \n" + ToJsonString.toJson(results));
    }

    @Tag("production")
    @Test
    public void validateTestCase3RulesBestDiscountAmountAllRules() {
        PriceComponentRuleInputSaleOrder saleOrder2Products = TestUtil.createSaleOrderRamiro4448WithAllFactors(
                DeliveryModeEnum.MOBILE_STORE, PaymentConditionsTypeEnum.CASH);

        List<PriceComponentRuleSubjectProduct> products = new ArrayList<>(0);
        products.add(TestUtil.createProductRule44());

        PriceComponentRuleOutcomeOutput outputDisc5 = TestUtil.createOutcomeOutput(1L, TestUtil.createOutcomeDiscountAmount(new BigDecimal(15)), ScaleTypeEnum.AMOUNT, OutComeModeEnum.SINGLE, null, null, PriceComponentOutcomeTypeEnum.DISCOUNT_AMOUNT);

        PriceComponentRuleOutcomeOutput outputDisc3 = TestUtil.createOutcomeOutput(999L, TestUtil.createOutcomeDiscountAmount(new BigDecimal(5)), ScaleTypeEnum.AMOUNT, OutComeModeEnum.SINGLE, null, null, PriceComponentOutcomeTypeEnum.DISCOUNT_AMOUNT);

        PriceComponentRuleOutcomeOutput outputDisc4 = TestUtil.createOutcomeOutput(1000L, TestUtil.createOutcomeDiscountAmount(new BigDecimal(11)), ScaleTypeEnum.AMOUNT, OutComeModeEnum.SINGLE, null, null, PriceComponentOutcomeTypeEnum.DISCOUNT_AMOUNT);

        List<PriceComponentRuleFactorBusinessUnit> businessUnits = Collections.singletonList(TestUtil.createBusinessUnitRetail());

        PriceRule ruleOne = TestUtil.createRuleOnlyBusinessUnit(1050L, businessUnits, false, TargetEnum.PRODUCT, PriceRuleTypeEnum.RESTRICTED, ScaleTypeEnum.AMOUNT, outputDisc5, OutComeModeEnum.SINGLE);

        PriceRule ruleTwo = TestUtil.createRuleOnlyProducts(1068L, products, true, TargetEnum.PRODUCT, PriceRuleTypeEnum.RESTRICTED, ScaleTypeEnum.AMOUNT, outputDisc3, OutComeModeEnum.SINGLE);

        PriceRule ruleThree = TestUtil.createRuleOnlyProducts(1069L, products, true, TargetEnum.PRODUCT, PriceRuleTypeEnum.RESTRICTED, ScaleTypeEnum.AMOUNT, outputDisc4, OutComeModeEnum.SINGLE);

        List<PriceRule> rules = new ArrayList<>(0);
        rules.add(ruleOne);
        rules.add(ruleTwo);
        rules.add(ruleThree);

        PriceComponentRule componentRule = PriceComponentRule.builder()
                .rules(rules)
                .resolutionMode(PriceRuleResolutionModeEnum.BY_BEST_VALUE_PRODUCT)
                .allowSetupLogger(false)
                .build();

        List<PriceComponentRuleInputSaleOrderProduct> results = componentRule.computate(saleOrder2Products);

        PriceComponentRuleInputSaleOrderProduct product44 = results.stream().filter(product -> product.getProduct().getId() == 44).findFirst().orElse(null);
        PriceComponentRuleInputSaleOrderProduct product48 = results.stream().filter(product -> product.getProduct().getId() == 48).findFirst().orElse(null);

        if (Objects.isNull(product44))
            fail("Product 44 does not exists");
        assertThat(product44.getRulesApplied().get(0).getId(), is(1068L));
        assertThat(product44.getRulesApplied().get(1).getId(), is(1069L));

        if (Objects.isNull(product48))
            fail("Product 48 does not exists");
        assertThat(product48.getRulesApplied().get(0).getId(), is(1050L));

        LogUtil.warn("Results: \n" + ToJsonString.toJson(results));
    }

    @Tag("production")
    @Test
    public void validateTestCase3RulesBestSurchargePercAllRules() {
        PriceComponentRuleInputSaleOrder saleOrder2Products = TestUtil.createSaleOrderRamiro4448WithAllFactors(
                DeliveryModeEnum.MOBILE_STORE, PaymentConditionsTypeEnum.CASH);

        List<PriceComponentRuleSubjectProduct> products = new ArrayList<>(0);
        products.add(TestUtil.createProductRule44());

        PriceComponentRuleOutcomeOutput outputDisc5 = TestUtil.createOutcomeOutput(1L, TestUtil.createOutcomeSurchargePerc(new BigDecimal(25)), ScaleTypeEnum.AMOUNT, OutComeModeEnum.SINGLE, null, null, PriceComponentOutcomeTypeEnum.SURCHARGE_PERCENTAGE);

        PriceComponentRuleOutcomeOutput outputDisc3 = TestUtil.createOutcomeOutput(999L, TestUtil.createOutcomeSurchargePerc(new BigDecimal(15)), ScaleTypeEnum.AMOUNT, OutComeModeEnum.SINGLE, null, null, PriceComponentOutcomeTypeEnum.SURCHARGE_PERCENTAGE);

        PriceComponentRuleOutcomeOutput outputDisc4 = TestUtil.createOutcomeOutput(1000L, TestUtil.createOutcomeSurchargePerc(new BigDecimal(8)), ScaleTypeEnum.AMOUNT, OutComeModeEnum.SINGLE, null, null, PriceComponentOutcomeTypeEnum.SURCHARGE_PERCENTAGE);

        List<PriceComponentRuleFactorBusinessUnit> businessUnits = Collections.singletonList(TestUtil.createBusinessUnitRetail());

        PriceRule ruleOne = TestUtil.createRuleOnlyBusinessUnit(1050L, businessUnits, false, TargetEnum.PRODUCT, PriceRuleTypeEnum.RESTRICTED, ScaleTypeEnum.AMOUNT, outputDisc5, OutComeModeEnum.SINGLE);

        PriceRule ruleTwo = TestUtil.createRuleOnlyProducts(1068L, products, true, TargetEnum.PRODUCT, PriceRuleTypeEnum.RESTRICTED, ScaleTypeEnum.AMOUNT, outputDisc3, OutComeModeEnum.SINGLE);

        PriceRule ruleThree = TestUtil.createRuleOnlyProducts(1069L, products, true, TargetEnum.PRODUCT, PriceRuleTypeEnum.RESTRICTED, ScaleTypeEnum.AMOUNT, outputDisc4, OutComeModeEnum.SINGLE);

        List<PriceRule> rules = new ArrayList<>(0);
        rules.add(ruleOne);
        rules.add(ruleTwo);
        rules.add(ruleThree);

        PriceComponentRule componentRule = PriceComponentRule.builder()
                .rules(rules)
                .resolutionMode(PriceRuleResolutionModeEnum.BY_BEST_VALUE_PRODUCT)
                .allowSetupLogger(false)
                .build();

        List<PriceComponentRuleInputSaleOrderProduct> results = componentRule.computate(saleOrder2Products);

        PriceComponentRuleInputSaleOrderProduct product44 = results.stream().filter(product -> product.getProduct().getId() == 44).findFirst().orElse(null);
        PriceComponentRuleInputSaleOrderProduct product48 = results.stream().filter(product -> product.getProduct().getId() == 48).findFirst().orElse(null);

        if (Objects.isNull(product44))
            fail("Product 44 does not exists");
        assertThat(product44.getRulesApplied().get(0).getId(), is(1068L));
        assertThat(product44.getRulesApplied().get(1).getId(), is(1069L));

        if (Objects.isNull(product48))
            fail("Product 48 does not exists");
        assertThat(product48.getRulesApplied().get(0).getId(), is(1050L));

        LogUtil.warn("Results: \n" + ToJsonString.toJson(results));
    }

    @Tag("production")
    @Test
    public void validateTestCase3RulesBestSurchargeAmountAllRules() {
        PriceComponentRuleInputSaleOrder saleOrder2Products = TestUtil.createSaleOrderRamiro4448WithAllFactors(
                DeliveryModeEnum.MOBILE_STORE, PaymentConditionsTypeEnum.CASH);

        List<PriceComponentRuleSubjectProduct> products = new ArrayList<>(0);
        products.add(TestUtil.createProductRule44());

        PriceComponentRuleOutcomeOutput outputDisc5 = TestUtil.createOutcomeOutput(1L, TestUtil.createOutcomeSurchargeAmount(new BigDecimal(10)), ScaleTypeEnum.AMOUNT, OutComeModeEnum.SINGLE, null, null, PriceComponentOutcomeTypeEnum.SURCHARGE_AMOUNT);

        PriceComponentRuleOutcomeOutput outputDisc3 = TestUtil.createOutcomeOutput(999L, TestUtil.createOutcomeSurchargeAmount(new BigDecimal(9)), ScaleTypeEnum.AMOUNT, OutComeModeEnum.SINGLE, null, null, PriceComponentOutcomeTypeEnum.SURCHARGE_AMOUNT);

        PriceComponentRuleOutcomeOutput outputDisc4 = TestUtil.createOutcomeOutput(1000L, TestUtil.createOutcomeSurchargeAmount(new BigDecimal(6)), ScaleTypeEnum.AMOUNT, OutComeModeEnum.SINGLE, null, null, PriceComponentOutcomeTypeEnum.SURCHARGE_AMOUNT);

        List<PriceComponentRuleFactorBusinessUnit> businessUnits = Collections.singletonList(TestUtil.createBusinessUnitRetail());

        PriceRule ruleOne = TestUtil.createRuleOnlyBusinessUnit(1050L, businessUnits, false, TargetEnum.PRODUCT, PriceRuleTypeEnum.RESTRICTED, ScaleTypeEnum.AMOUNT, outputDisc5, OutComeModeEnum.SINGLE);

        PriceRule ruleTwo = TestUtil.createRuleOnlyProducts(1068L, products, true, TargetEnum.PRODUCT, PriceRuleTypeEnum.RESTRICTED, ScaleTypeEnum.AMOUNT, outputDisc3, OutComeModeEnum.SINGLE);

        PriceRule ruleThree = TestUtil.createRuleOnlyProducts(1069L, products, true, TargetEnum.PRODUCT, PriceRuleTypeEnum.RESTRICTED, ScaleTypeEnum.AMOUNT, outputDisc4, OutComeModeEnum.SINGLE);

        List<PriceRule> rules = new ArrayList<>(0);
        rules.add(ruleOne);
        rules.add(ruleTwo);
        rules.add(ruleThree);

        PriceComponentRule componentRule = PriceComponentRule.builder()
                .rules(rules)
                .resolutionMode(PriceRuleResolutionModeEnum.BY_BEST_VALUE_PRODUCT)
                .allowSetupLogger(false)
                .build();

        List<PriceComponentRuleInputSaleOrderProduct> results = componentRule.computate(saleOrder2Products);

        PriceComponentRuleInputSaleOrderProduct product44 = results.stream().filter(product -> product.getProduct().getId() == 44).findFirst().orElse(null);
        PriceComponentRuleInputSaleOrderProduct product48 = results.stream().filter(product -> product.getProduct().getId() == 48).findFirst().orElse(null);

        if (Objects.isNull(product44))
            fail("Product 44 does not exists");
        assertThat(product44.getRulesApplied().get(0).getId(), is(1068L));
        assertThat(product44.getRulesApplied().get(1).getId(), is(1069L));

        if (Objects.isNull(product48))
            fail("Product 48 does not exists");
        assertThat(product48.getRulesApplied().get(0).getId(), is(1050L));

        LogUtil.warn("Results: \n" + ToJsonString.toJson(results));
    }

    @Tag("production")
    @Test
    public void validateTestCase3RulesBestFixedPriceAllRulesBySaleOrder() {
        PriceComponentRuleInputSaleOrder saleOrder2Products = TestUtil.createSaleOrderRamiro4448WithAllFactors(
                DeliveryModeEnum.MOBILE_STORE, PaymentConditionsTypeEnum.CASH);

        List<PriceComponentRuleSubjectProduct> products = new ArrayList<>(0);
        products.add(TestUtil.createProductRule44());

        PriceComponentRuleOutcomeOutput outputDisc5 = TestUtil.createOutcomeOutput(1L, TestUtil.createOutcomeFixedPrice(new BigDecimal(95)), ScaleTypeEnum.AMOUNT, OutComeModeEnum.SINGLE, null, null, PriceComponentOutcomeTypeEnum.FIXED_PRICE);

        PriceComponentRuleOutcomeOutput outputDisc3 = TestUtil.createOutcomeOutput(999L, TestUtil.createOutcomeFixedPrice(new BigDecimal(200)), ScaleTypeEnum.AMOUNT, OutComeModeEnum.SINGLE, null, null, PriceComponentOutcomeTypeEnum.FIXED_PRICE);

        PriceComponentRuleOutcomeOutput outputDisc4 = TestUtil.createOutcomeOutput(1000L, TestUtil.createOutcomeFixedPrice(new BigDecimal(80)), ScaleTypeEnum.AMOUNT, OutComeModeEnum.SINGLE, null, null, PriceComponentOutcomeTypeEnum.FIXED_PRICE);

        List<PriceComponentRuleFactorBusinessUnit> businessUnits = Collections.singletonList(TestUtil.createBusinessUnitRetail());

        PriceRule ruleOne = TestUtil.createRuleOnlyBusinessUnit(1050L, businessUnits, false, TargetEnum.PRODUCT, PriceRuleTypeEnum.RESTRICTED, ScaleTypeEnum.AMOUNT, outputDisc5, OutComeModeEnum.SINGLE);

        PriceRule ruleTwo = TestUtil.createRuleOnlyProducts(1068L, products, true, TargetEnum.PRODUCT, PriceRuleTypeEnum.RESTRICTED, ScaleTypeEnum.AMOUNT, outputDisc3, OutComeModeEnum.SINGLE);

        PriceRule ruleThree = TestUtil.createRuleOnlyProducts(1069L, products, true, TargetEnum.PRODUCT, PriceRuleTypeEnum.RESTRICTED, ScaleTypeEnum.AMOUNT, outputDisc4, OutComeModeEnum.SINGLE);

        List<PriceRule> rules = new ArrayList<>(0);
        rules.add(ruleOne);
        rules.add(ruleTwo);
        rules.add(ruleThree);

        PriceComponentRule componentRule = PriceComponentRule.builder()
                .rules(rules)
                .resolutionMode(PriceRuleResolutionModeEnum.BY_BEST_VALUE_SALE_ORDER)
                .allowSetupLogger(false)
                .build();

        List<PriceComponentRuleInputSaleOrderProduct> results = componentRule.computate(saleOrder2Products);

        PriceComponentRuleInputSaleOrderProduct product44 = results.stream().filter(product -> product.getProduct().getId() == 44).findFirst().orElse(null);
        PriceComponentRuleInputSaleOrderProduct product48 = results.stream().filter(product -> product.getProduct().getId() == 48).findFirst().orElse(null);

        if (Objects.isNull(product44))
            fail("Product 44 does not exists");
        if (Objects.nonNull(product48))
            fail("Product 48 exists and it has no application to this rule.");
        assertThat(product44.getRulesApplied().get(0).getId(), is(1069L));

        LogUtil.warn("Results: \n" + ToJsonString.toJson(results));
    }

    @Tag("production")
    @Test
    public void validateTestCase3RulesBestFixedPriceAllRules() {
        PriceComponentRuleInputSaleOrder saleOrder2Products = TestUtil.createSaleOrderRamiro4448WithAllFactors(
                DeliveryModeEnum.MOBILE_STORE, PaymentConditionsTypeEnum.CASH);

        List<PriceComponentRuleSubjectProduct> products = new ArrayList<>(0);
        products.add(TestUtil.createProductRule44());

        PriceComponentRuleOutcomeOutput outputDisc5 = TestUtil.createOutcomeOutput(1L, TestUtil.createOutcomeFixedPrice(new BigDecimal(90)), ScaleTypeEnum.AMOUNT, OutComeModeEnum.SINGLE, null, null, PriceComponentOutcomeTypeEnum.FIXED_PRICE);

        PriceComponentRuleOutcomeOutput outputDisc3 = TestUtil.createOutcomeOutput(999L, TestUtil.createOutcomeFixedPrice(new BigDecimal(80)), ScaleTypeEnum.AMOUNT, OutComeModeEnum.SINGLE, null, null, PriceComponentOutcomeTypeEnum.FIXED_PRICE);

        PriceComponentRuleOutcomeOutput outputDisc4 = TestUtil.createOutcomeOutput(1000L, TestUtil.createOutcomeFixedPrice(new BigDecimal(200)), ScaleTypeEnum.AMOUNT, OutComeModeEnum.SINGLE, null, null, PriceComponentOutcomeTypeEnum.FIXED_PRICE);

        List<PriceComponentRuleFactorBusinessUnit> businessUnits = Collections.singletonList(TestUtil.createBusinessUnitRetail());

        PriceRule ruleOne = TestUtil.createRuleOnlyBusinessUnit(1050L, businessUnits, false, TargetEnum.PRODUCT, PriceRuleTypeEnum.RESTRICTED, ScaleTypeEnum.AMOUNT, outputDisc5, OutComeModeEnum.SINGLE);

        PriceRule ruleTwo = TestUtil.createRuleOnlyProducts(1068L, products, true, TargetEnum.PRODUCT, PriceRuleTypeEnum.RESTRICTED, ScaleTypeEnum.AMOUNT, outputDisc3, OutComeModeEnum.SINGLE);

        PriceRule ruleThree = TestUtil.createRuleOnlyProducts(1069L, products, true, TargetEnum.PRODUCT, PriceRuleTypeEnum.RESTRICTED, ScaleTypeEnum.AMOUNT, outputDisc4, OutComeModeEnum.SINGLE);

        List<PriceRule> rules = new ArrayList<>(0);
        rules.add(ruleOne);
        rules.add(ruleTwo);
        rules.add(ruleThree);

        PriceComponentRule componentRule = PriceComponentRule.builder()
                .rules(rules)
                .resolutionMode(PriceRuleResolutionModeEnum.BY_BEST_VALUE_PRODUCT)
                .allowSetupLogger(false)
                .build();

        List<PriceComponentRuleInputSaleOrderProduct> results = componentRule.computate(saleOrder2Products);

        PriceComponentRuleInputSaleOrderProduct product44 = results.stream().filter(product -> product.getProduct().getId() == 44).findFirst().orElse(null);
        PriceComponentRuleInputSaleOrderProduct product48 = results.stream().filter(product -> product.getProduct().getId() == 48).findFirst().orElse(null);

        if (Objects.isNull(product44))
            fail("Product 44 does not exists");
        assertThat(product44.getRulesApplied().get(0).getId(), is(1068L));

        if (Objects.isNull(product48))
            fail("Product 48 does not exists");
        assertThat(product48.getRulesApplied().get(0).getId(), is(1050L));

        LogUtil.warn("Results: \n" + ToJsonString.toJson(results));
    }

    @Tag("production")
    @Test
    public void validateTestCase3RulesBestBonusProductAllRulesBySaleOrder() {
        PriceComponentRuleInputSaleOrder saleOrder2Products = TestUtil.createSaleOrderRamiro4448WithAllFactors(
                DeliveryModeEnum.MOBILE_STORE, PaymentConditionsTypeEnum.CASH);

        List<PriceComponentRuleSubjectProduct> products = new ArrayList<>(0);
        products.add(TestUtil.createProductRule44());

        List<PriceComponentRuleOutcomeProduct> bonusProductsOne = new ArrayList<>(0);
        bonusProductsOne.add(TestUtil.createProductBonus46(new BigDecimal(2)));

        List<PriceComponentRuleOutcomeProduct> bonusProductsTwo = new ArrayList<>(0);
        bonusProductsTwo.add(TestUtil.createProductBonus46(BigDecimal.ONE));

        List<PriceComponentRuleOutcomeProduct> bonusProductsThree = new ArrayList<>(0);
        bonusProductsThree.add(TestUtil.createProductBonus46(BigDecimal.ONE));
        bonusProductsThree.add(TestUtil.createProductBonus47(new BigDecimal(2)));

        PriceComponentRuleOutcomeOutput outputOne = TestUtil.createOutcomeOutput(18L, TestUtil.createOutcomeProductBonus(bonusProductsOne),
                ScaleTypeEnum.AMOUNT, OutComeModeEnum.SINGLE, null, null, PriceComponentOutcomeTypeEnum.PRODUCT);

        PriceComponentRuleOutcomeOutput outputTwo = TestUtil.createOutcomeOutput(429L, TestUtil.createOutcomeProductBonus(bonusProductsTwo),
                ScaleTypeEnum.AMOUNT, OutComeModeEnum.SINGLE, null, null, PriceComponentOutcomeTypeEnum.PRODUCT);

        PriceComponentRuleOutcomeOutput outputDisc4 = TestUtil.createOutcomeOutput(100L, TestUtil.createOutcomeProductBonus(bonusProductsThree),
                ScaleTypeEnum.AMOUNT, OutComeModeEnum.SINGLE, null, null, PriceComponentOutcomeTypeEnum.PRODUCT);

        List<PriceComponentRuleFactorBusinessUnit> businessUnits = Collections.singletonList(TestUtil.createBusinessUnitRetail());

        PriceRule ruleOne = TestUtil.createRuleOnlyBusinessUnit(1050L, businessUnits, false, TargetEnum.PRODUCT, PriceRuleTypeEnum.RESTRICTED, ScaleTypeEnum.AMOUNT, outputOne, OutComeModeEnum.SINGLE);

        PriceRule ruleTwo = TestUtil.createRuleOnlyProducts(1068L, products, true, TargetEnum.PRODUCT, PriceRuleTypeEnum.RESTRICTED, ScaleTypeEnum.AMOUNT, outputTwo, OutComeModeEnum.SINGLE);

        PriceRule ruleThree = TestUtil.createRuleOnlyProducts(1069L, products, true, TargetEnum.PRODUCT, PriceRuleTypeEnum.RESTRICTED, ScaleTypeEnum.AMOUNT, outputDisc4, OutComeModeEnum.SINGLE);

        List<PriceRule> rules = new ArrayList<>(0);
        rules.add(ruleOne);
        rules.add(ruleTwo);
        rules.add(ruleThree);

        PriceComponentRule componentRule = PriceComponentRule.builder()
                .rules(rules)
                .resolutionMode(PriceRuleResolutionModeEnum.BY_BEST_VALUE_SALE_ORDER)
                .allowSetupLogger(false)
                .build();

        List<PriceComponentRuleInputSaleOrderProduct> results = componentRule.computate(saleOrder2Products);

        PriceComponentRuleInputSaleOrderProduct product44 = results.stream().filter(product -> product.getProduct().getId() == 44).findFirst().orElse(null);
        PriceComponentRuleInputSaleOrderProduct product48 = results.stream().filter(product -> product.getProduct().getId() == 48).findFirst().orElse(null);

        if (Objects.isNull(product44))
            fail("Product 44 does not exists");
        assertThat(product44.getRulesApplied().get(0).getId(), is(1068L));
        assertThat(product44.getRulesApplied().get(1).getId(), is(1069L));
        if (Objects.nonNull(product48))
            fail("Product 48 does exists but must not in the results.");

        LogUtil.warn("Results: \n" + ToJsonString.toJson(results));
    }

    @Tag("production")
    @Test
    public void validateTestCase3RulesBestBonusProductAllRulesByProduct() {
        PriceComponentRuleInputSaleOrder saleOrder2Products = TestUtil.createSaleOrderRamiro4448WithAllFactors(
                DeliveryModeEnum.MOBILE_STORE, PaymentConditionsTypeEnum.CASH);

        List<PriceComponentRuleSubjectProduct> products = new ArrayList<>(0);
        products.add(TestUtil.createProductRule44());

        List<PriceComponentRuleOutcomeProduct> bonusProductsOne = new ArrayList<>(0);
        bonusProductsOne.add(TestUtil.createProductBonus46(new BigDecimal(2)));

        List<PriceComponentRuleOutcomeProduct> bonusProductsTwo = new ArrayList<>(0);
        bonusProductsTwo.add(TestUtil.createProductBonus46(BigDecimal.ONE));

        List<PriceComponentRuleOutcomeProduct> bonusProductsThree = new ArrayList<>(0);
        bonusProductsThree.add(TestUtil.createProductBonus46(BigDecimal.ONE));
        bonusProductsThree.add(TestUtil.createProductBonus47(new BigDecimal(2)));

        PriceComponentRuleOutcomeOutput outputOne = TestUtil.createOutcomeOutput(18L, TestUtil.createOutcomeProductBonus(bonusProductsOne),
                ScaleTypeEnum.AMOUNT, OutComeModeEnum.SINGLE, null, null, PriceComponentOutcomeTypeEnum.PRODUCT);

        PriceComponentRuleOutcomeOutput outputTwo = TestUtil.createOutcomeOutput(429L, TestUtil.createOutcomeProductBonus(bonusProductsTwo),
                ScaleTypeEnum.AMOUNT, OutComeModeEnum.SINGLE, null, null, PriceComponentOutcomeTypeEnum.PRODUCT);

        PriceComponentRuleOutcomeOutput outputDisc4 = TestUtil.createOutcomeOutput(100L, TestUtil.createOutcomeProductBonus(bonusProductsThree),
                ScaleTypeEnum.AMOUNT, OutComeModeEnum.SINGLE, null, null, PriceComponentOutcomeTypeEnum.PRODUCT);

        List<PriceComponentRuleFactorBusinessUnit> businessUnits = Collections.singletonList(TestUtil.createBusinessUnitRetail());

        PriceRule ruleOne = TestUtil.createRuleOnlyBusinessUnit(1050L, businessUnits, false, TargetEnum.PRODUCT, PriceRuleTypeEnum.RESTRICTED, ScaleTypeEnum.AMOUNT, outputOne, OutComeModeEnum.SINGLE);

        PriceRule ruleTwo = TestUtil.createRuleOnlyProducts(1068L, products, true, TargetEnum.PRODUCT, PriceRuleTypeEnum.RESTRICTED, ScaleTypeEnum.AMOUNT, outputTwo, OutComeModeEnum.SINGLE);

        PriceRule ruleThree = TestUtil.createRuleOnlyProducts(1069L, products, true, TargetEnum.PRODUCT, PriceRuleTypeEnum.RESTRICTED, ScaleTypeEnum.AMOUNT, outputDisc4, OutComeModeEnum.SINGLE);

        List<PriceRule> rules = new ArrayList<>(0);
        rules.add(ruleOne);
        rules.add(ruleTwo);
        rules.add(ruleThree);

        PriceComponentRule componentRule = PriceComponentRule.builder()
                .rules(rules)
                .resolutionMode(PriceRuleResolutionModeEnum.BY_BEST_VALUE_PRODUCT)
                .allowSetupLogger(false)
                .build();

        List<PriceComponentRuleInputSaleOrderProduct> results = componentRule.computate(saleOrder2Products);

        PriceComponentRuleInputSaleOrderProduct product44 = results.stream().filter(product -> product.getProduct().getId() == 44).findFirst().orElse(null);
        PriceComponentRuleInputSaleOrderProduct product48 = results.stream().filter(product -> product.getProduct().getId() == 48).findFirst().orElse(null);

        if (Objects.isNull(product44))
            fail("Product 44 does not exists");
        assertThat(product44.getRulesApplied().get(0).getId(), is(1068L));
        assertThat(product44.getRulesApplied().get(1).getId(), is(1069L));

        if (Objects.isNull(product48))
            fail("Product 48 does not exists.");
        assertThat(product48.getRulesApplied().get(0).getId(), is(1050L));

        LogUtil.warn("Results: \n" + ToJsonString.toJson(results));
    }


    @Tag("production")
    @Test
    public void validateTestCase3RulesBestSurchargeProductAllRulesByProduct() {
        PriceComponentRuleInputSaleOrder saleOrder2Products = TestUtil.createSaleOrderRamiro4448WithAllFactors(
                DeliveryModeEnum.MOBILE_STORE, PaymentConditionsTypeEnum.CASH);

        List<PriceComponentRuleSubjectProduct> products = new ArrayList<>(0);
        products.add(TestUtil.createProductRule44());

        List<PriceComponentRuleOutcomeProduct> surchargeProductsOne = new ArrayList<>(0);
        surchargeProductsOne.add(TestUtil.createProductBonus46(BigDecimal.ONE));

        List<PriceComponentRuleOutcomeProduct> surchargeProductsTwo = new ArrayList<>(0);
        surchargeProductsTwo.add(TestUtil.createProductBonus46(new BigDecimal(3)));

        List<PriceComponentRuleOutcomeProduct> surchargeProductsThree = new ArrayList<>(0);
        surchargeProductsThree.add(TestUtil.createProductBonus46(new BigDecimal(4)));
        surchargeProductsThree.add(TestUtil.createProductBonus47(new BigDecimal(2)));

        PriceComponentRuleOutcomeOutput outputOne = TestUtil.createOutcomeOutput(18L, TestUtil.createOutcomeProductSurcharge(surchargeProductsOne),
                ScaleTypeEnum.AMOUNT, OutComeModeEnum.SINGLE, null, null, PriceComponentOutcomeTypeEnum.PRODUCT_SURCHARGE);

        PriceComponentRuleOutcomeOutput outputTwo = TestUtil.createOutcomeOutput(429L, TestUtil.createOutcomeProductSurcharge(surchargeProductsTwo),
                ScaleTypeEnum.AMOUNT, OutComeModeEnum.SINGLE, null, null, PriceComponentOutcomeTypeEnum.PRODUCT_SURCHARGE);

        PriceComponentRuleOutcomeOutput outputDisc4 = TestUtil.createOutcomeOutput(100L, TestUtil.createOutcomeProductSurcharge(surchargeProductsThree),
                ScaleTypeEnum.AMOUNT, OutComeModeEnum.SINGLE, null, null, PriceComponentOutcomeTypeEnum.PRODUCT_SURCHARGE);

        List<PriceComponentRuleFactorBusinessUnit> businessUnits = Collections.singletonList(TestUtil.createBusinessUnitRetail());

        PriceRule ruleOne = TestUtil.createRuleOnlyBusinessUnit(1050L, businessUnits, false, TargetEnum.PRODUCT, PriceRuleTypeEnum.RESTRICTED, ScaleTypeEnum.AMOUNT, outputOne, OutComeModeEnum.SINGLE);

        PriceRule ruleTwo = TestUtil.createRuleOnlyProducts(1068L, products, true, TargetEnum.PRODUCT, PriceRuleTypeEnum.RESTRICTED, ScaleTypeEnum.AMOUNT, outputTwo, OutComeModeEnum.SINGLE);

        PriceRule ruleThree = TestUtil.createRuleOnlyProducts(1069L, products, true, TargetEnum.PRODUCT, PriceRuleTypeEnum.RESTRICTED, ScaleTypeEnum.AMOUNT, outputDisc4, OutComeModeEnum.SINGLE);

        List<PriceRule> rules = new ArrayList<>(0);
        rules.add(ruleOne);
        rules.add(ruleTwo);
        rules.add(ruleThree);

        PriceComponentRule componentRule = PriceComponentRule.builder()
                .rules(rules)
                .resolutionMode(PriceRuleResolutionModeEnum.BY_BEST_VALUE_PRODUCT)
                .allowSetupLogger(false)
                .build();

        List<PriceComponentRuleInputSaleOrderProduct> results = componentRule.computate(saleOrder2Products);

        PriceComponentRuleInputSaleOrderProduct product44 = results.stream().filter(product -> product.getProduct().getId() == 44).findFirst().orElse(null);
        PriceComponentRuleInputSaleOrderProduct product48 = results.stream().filter(product -> product.getProduct().getId() == 48).findFirst().orElse(null);

        if (Objects.isNull(product44))
            fail("Product 44 does not exists");
        assertThat(product44.getRulesApplied().get(0).getId(), is(1068L));
        assertThat(product44.getRulesApplied().get(1).getId(), is(1069L));

        if (Objects.isNull(product48))
            fail("Product 48 does not exists.");
        assertThat(product48.getRulesApplied().get(0).getId(), is(1050L));

        LogUtil.warn("Results: \n" + ToJsonString.toJson(results));
    }


    @Tag("production")
    @Test
    public void validateTestCase3RulesBestDiscountPercScales() {
        PriceComponentRuleInputSaleOrder saleOrder2Products = TestUtil.createSaleOrderRamiro4448WithAllFactors(
                DeliveryModeEnum.MOBILE_STORE, PaymentConditionsTypeEnum.CASH);

        PriceComponentRuleInputSaleOrder saleOrder1Products = TestUtil.createSaleOrderRamiro4448WithAllFactors(
                DeliveryModeEnum.MOBILE_STORE, PaymentConditionsTypeEnum.CASH);
        saleOrder1Products.getProductDetails().remove(1);

        List<PriceComponentRuleSubjectProduct> products = new ArrayList<>(0);
        products.add(TestUtil.createProductRule44());

        List<PriceComponentRuleFactorScale> scalesOne = new ArrayList<>(0);
        scalesOne.add(TestUtil.createScale(BigDecimal.ZERO, null, ScaleTypeEnum.AMOUNT, TestUtil.createOutcomeDiscountPerc(new BigDecimal(5))));

        List<PriceComponentRuleFactorScale> scalesTwo = new ArrayList<>(0);
        scalesTwo.add(TestUtil.createScale(new BigDecimal(50), new BigDecimal(150), ScaleTypeEnum.AMOUNT, TestUtil.createOutcomeDiscountPerc(new BigDecimal(4))));
        scalesTwo.add(TestUtil.createScale(new BigDecimal(151), new BigDecimal(1510), ScaleTypeEnum.AMOUNT, TestUtil.createOutcomeDiscountPerc(new BigDecimal(6))));

        List<PriceComponentRuleFactorScale> scalesThree = new ArrayList<>(0);
        scalesThree.add(TestUtil.createScale(new BigDecimal(50), new BigDecimal(300), ScaleTypeEnum.AMOUNT, TestUtil.createOutcomeDiscountPerc(new BigDecimal(2))));
        scalesThree.add(TestUtil.createScale(new BigDecimal(301), new BigDecimal(510), ScaleTypeEnum.AMOUNT, TestUtil.createOutcomeDiscountPerc(new BigDecimal(4))));
        scalesThree.add(TestUtil.createScale(new BigDecimal(511), null, ScaleTypeEnum.AMOUNT, TestUtil.createOutcomeDiscountPerc(new BigDecimal(5))));

        PriceComponentRuleOutcomeOutput outputOne = TestUtil.createOutcomeOutput(1L, null, ScaleTypeEnum.AMOUNT,
                OutComeModeEnum.SCALE, null, scalesOne, PriceComponentOutcomeTypeEnum.DISCOUNT_PERCENTAGE);

        PriceComponentRuleOutcomeOutput outputTwo = TestUtil.createOutcomeOutput(999L, null, ScaleTypeEnum.AMOUNT,
                OutComeModeEnum.SCALE, null, scalesTwo, PriceComponentOutcomeTypeEnum.DISCOUNT_PERCENTAGE);

        PriceComponentRuleOutcomeOutput outputThree = TestUtil.createOutcomeOutput(1000L, null, ScaleTypeEnum.AMOUNT,
                OutComeModeEnum.SCALE, null, scalesThree, PriceComponentOutcomeTypeEnum.DISCOUNT_PERCENTAGE);

        List<PriceComponentRuleFactorBusinessUnit> businessUnits = Collections.singletonList(TestUtil.createBusinessUnitRetail());

        PriceRule ruleOne = TestUtil.createRuleOnlyBusinessUnit(1050L, businessUnits, false, TargetEnum.PRODUCT,
                PriceRuleTypeEnum.RESTRICTED, ScaleTypeEnum.AMOUNT, outputOne, OutComeModeEnum.SCALE);

        PriceRule ruleTwo = TestUtil.createRuleOnlyProducts(1068L, products, true, TargetEnum.PRODUCT,
                PriceRuleTypeEnum.RESTRICTED, ScaleTypeEnum.AMOUNT, outputTwo, OutComeModeEnum.SCALE);

        PriceRule ruleThree = TestUtil.createRuleOnlyProducts(1069L, products, true, TargetEnum.PRODUCT,
                PriceRuleTypeEnum.RESTRICTED, ScaleTypeEnum.AMOUNT, outputThree, OutComeModeEnum.SCALE);

        List<PriceRule> rules = new ArrayList<>(0);
        rules.add(ruleOne);
        rules.add(ruleTwo);
        rules.add(ruleThree);

        PriceComponentRule componentRule = PriceComponentRule.builder()
                .rules(rules)
                .resolutionMode(PriceRuleResolutionModeEnum.BY_BEST_VALUE_PRODUCT)
                .allowSetupLogger(false)
                .build();

        List<PriceComponentRuleInputSaleOrderProduct> results = componentRule.computate(saleOrder2Products);

        PriceComponentRuleInputSaleOrderProduct product44 = results.stream().filter(product -> product.getProduct().getId() == 44).findFirst().orElse(null);
        PriceComponentRuleInputSaleOrderProduct product48 = results.stream().filter(product -> product.getProduct().getId() == 48).findFirst().orElse(null);

        if (Objects.isNull(product44))
            fail("Product 44 does not exists");
        assertThat(product44.getRulesApplied().get(0).getId(), is(1068L));
        assertThat(product44.getRulesApplied().get(1).getId(), is(1069L));

        if (Objects.isNull(product48))
            fail("Product 48 does not exists.");
        assertThat(product48.getRulesApplied().get(0).getId(), is(1050L));

        LogUtil.warn("Results: \n" + ToJsonString.toJson(results));
    }

    @Tag("production")
    @Test
    public void validateTestCase3RulesBestDiscountPercFrequency() {
        PriceComponentRuleInputSaleOrder saleOrder2Products = TestUtil.createSaleOrderRamiro4448WithAllFactors(
                DeliveryModeEnum.MOBILE_STORE, PaymentConditionsTypeEnum.CASH);
        saleOrder2Products.getProductDetails().get(0).setQty(new BigDecimal(3));
        saleOrder2Products.getProductDetails().get(0).setTotalPriceGross(new BigDecimal(900));
        saleOrder2Products.getProductDetails().get(0).setTotalPrice(new BigDecimal(900));

        PriceComponentRuleInputSaleOrder saleOrder1Products = TestUtil.createSaleOrderRamiro4448WithAllFactors(
                DeliveryModeEnum.MOBILE_STORE, PaymentConditionsTypeEnum.CASH);
        saleOrder1Products.getProductDetails().remove(1);

        List<PriceComponentRuleSubjectProduct> products = new ArrayList<>(0);
        products.add(TestUtil.createProductRule48());

        PriceComponentRuleOutcomeOutput outputOne = TestUtil.createOutcomeOutput(1L, TestUtil.createOutcomeDiscountPerc(new BigDecimal(1)), ScaleTypeEnum.QUANTITY,
                OutComeModeEnum.FREQUENCY, new BigDecimal(1), null, PriceComponentOutcomeTypeEnum.DISCOUNT_PERCENTAGE);

        PriceComponentRuleOutcomeOutput outputTwo = TestUtil.createOutcomeOutput(999L, TestUtil.createOutcomeDiscountPerc(new BigDecimal(2)), ScaleTypeEnum.QUANTITY,
                OutComeModeEnum.FREQUENCY, new BigDecimal(2), null, PriceComponentOutcomeTypeEnum.DISCOUNT_PERCENTAGE);

        PriceComponentRuleOutcomeOutput outputThree = TestUtil.createOutcomeOutput(1000L, TestUtil.createOutcomeDiscountPerc(new BigDecimal(6)), ScaleTypeEnum.QUANTITY,
                OutComeModeEnum.FREQUENCY, new BigDecimal(5), null, PriceComponentOutcomeTypeEnum.DISCOUNT_PERCENTAGE);

        List<PriceComponentRuleFactorBusinessUnit> businessUnits = Collections.singletonList(TestUtil.createBusinessUnitRetail());

        PriceRule ruleOne = TestUtil.createRuleOnlyBusinessUnit(1050L, businessUnits, false, TargetEnum.PRODUCT,
                PriceRuleTypeEnum.RESTRICTED, ScaleTypeEnum.QUANTITY, outputOne, OutComeModeEnum.FREQUENCY);

        PriceRule ruleTwo = TestUtil.createRuleOnlyProducts(1068L, products, true, TargetEnum.PRODUCT,
                PriceRuleTypeEnum.RESTRICTED, ScaleTypeEnum.QUANTITY, outputTwo, OutComeModeEnum.FREQUENCY);

        PriceRule ruleThree = TestUtil.createRuleOnlyProducts(1069L, products, true, TargetEnum.PRODUCT,
                PriceRuleTypeEnum.RESTRICTED, ScaleTypeEnum.QUANTITY, outputThree, OutComeModeEnum.FREQUENCY);

        List<PriceRule> rules = new ArrayList<>(0);
        rules.add(ruleOne);
        rules.add(ruleTwo);
        rules.add(ruleThree);

        PriceComponentRule componentRule = PriceComponentRule.builder()
                .rules(rules)
                .resolutionMode(PriceRuleResolutionModeEnum.BY_BEST_VALUE_PRODUCT)
                .allowSetupLogger(false)
                .build();

        List<PriceComponentRuleInputSaleOrderProduct> results = componentRule.computate(saleOrder2Products);

        PriceComponentRuleInputSaleOrderProduct product44 = results.stream().filter(product -> product.getProduct().getId() == 44).findFirst().orElse(null);
        PriceComponentRuleInputSaleOrderProduct product48 = results.stream().filter(product -> product.getProduct().getId() == 48).findFirst().orElse(null);

        if (Objects.isNull(product44))
            fail("Product 44 does not exists");
        assertThat(product44.getRulesApplied().get(0).getId(), is(1050L));

        if (Objects.isNull(product48))
            fail("Product 48 does not exists.");
        assertThat(product48.getRulesApplied().get(0).getId(), is(1068L));
        assertThat(product48.getRulesApplied().get(1).getId(), is(1069L));

        LogUtil.warn("Results: \n" + ToJsonString.toJson(results));
    }


    @Tag("production")
    @Test
    public void validateTestCase3RulesBestDiscountAmountFrequency() {
        PriceComponentRuleInputSaleOrder saleOrder2Products = TestUtil.createSaleOrderRamiro4448WithAllFactors(
                DeliveryModeEnum.MOBILE_STORE, PaymentConditionsTypeEnum.CASH);
        saleOrder2Products.getProductDetails().get(0).setQty(new BigDecimal(3));
        saleOrder2Products.getProductDetails().get(0).setTotalPriceGross(new BigDecimal(900));
        saleOrder2Products.getProductDetails().get(0).setTotalPrice(new BigDecimal(900));

        PriceComponentRuleInputSaleOrder saleOrder1Products = TestUtil.createSaleOrderRamiro4448WithAllFactors(
                DeliveryModeEnum.MOBILE_STORE, PaymentConditionsTypeEnum.CASH);
        saleOrder1Products.getProductDetails().remove(1);

        List<PriceComponentRuleSubjectProduct> products = new ArrayList<>(0);
        products.add(TestUtil.createProductRule48());

        PriceComponentRuleOutcomeOutput outputOne = TestUtil.createOutcomeOutput(1L, TestUtil.createOutcomeDiscountAmount(new BigDecimal(2)), ScaleTypeEnum.QUANTITY,
                OutComeModeEnum.FREQUENCY, new BigDecimal(1), null, PriceComponentOutcomeTypeEnum.DISCOUNT_AMOUNT);

        PriceComponentRuleOutcomeOutput outputTwo = TestUtil.createOutcomeOutput(999L, TestUtil.createOutcomeDiscountAmount(new BigDecimal(5)), ScaleTypeEnum.QUANTITY,
                OutComeModeEnum.FREQUENCY, new BigDecimal(2), null, PriceComponentOutcomeTypeEnum.DISCOUNT_AMOUNT);

        PriceComponentRuleOutcomeOutput outputThree = TestUtil.createOutcomeOutput(1000L, TestUtil.createOutcomeDiscountAmount(new BigDecimal(12)), ScaleTypeEnum.QUANTITY,
                OutComeModeEnum.FREQUENCY, new BigDecimal(5), null, PriceComponentOutcomeTypeEnum.DISCOUNT_AMOUNT);

        List<PriceComponentRuleFactorBusinessUnit> businessUnits = Collections.singletonList(TestUtil.createBusinessUnitRetail());

        PriceRule ruleOne = TestUtil.createRuleOnlyBusinessUnit(1050L, businessUnits, false, TargetEnum.PRODUCT,
                PriceRuleTypeEnum.RESTRICTED, ScaleTypeEnum.QUANTITY, outputOne, OutComeModeEnum.FREQUENCY);

        PriceRule ruleTwo = TestUtil.createRuleOnlyProducts(1068L, products, true, TargetEnum.PRODUCT,
                PriceRuleTypeEnum.RESTRICTED, ScaleTypeEnum.QUANTITY, outputTwo, OutComeModeEnum.FREQUENCY);

        PriceRule ruleThree = TestUtil.createRuleOnlyProducts(1069L, products, true, TargetEnum.PRODUCT,
                PriceRuleTypeEnum.RESTRICTED, ScaleTypeEnum.QUANTITY, outputThree, OutComeModeEnum.FREQUENCY);

        List<PriceRule> rules = new ArrayList<>(0);
        rules.add(ruleOne);
        rules.add(ruleTwo);
        rules.add(ruleThree);

        PriceComponentRule componentRule = PriceComponentRule.builder()
                .rules(rules)
                .resolutionMode(PriceRuleResolutionModeEnum.BY_BEST_VALUE_PRODUCT)
                .allowSetupLogger(false)
                .build();

        List<PriceComponentRuleInputSaleOrderProduct> results = componentRule.computate(saleOrder2Products);

        PriceComponentRuleInputSaleOrderProduct product44 = results.stream().filter(product -> product.getProduct().getId() == 44).findFirst().orElse(null);
        PriceComponentRuleInputSaleOrderProduct product48 = results.stream().filter(product -> product.getProduct().getId() == 48).findFirst().orElse(null);

        if (Objects.isNull(product44))
            fail("Product 44 does not exists");
        assertThat(product44.getRulesApplied().get(0).getId(), is(1050L));

        if (Objects.isNull(product48))
            fail("Product 48 does not exists.");
        assertThat(product48.getRulesApplied().get(0).getId(), is(1068L));
        assertThat(product48.getRulesApplied().get(1).getId(), is(1069L));

        LogUtil.warn("Results: \n" + ToJsonString.toJson(results));
    }

    @Tag("production")
    @Test
    public void validateTestCase4RulesBestDiscountPercAllRulesAcc() {
        PriceComponentRuleInputSaleOrder saleOrder4Products = TestUtil.createSaleOrderRamiro4448WithAllFactors(
                DeliveryModeEnum.MOBILE_STORE, PaymentConditionsTypeEnum.CASH);
        saleOrder4Products.getProductDetails().add(TestUtil.createSaleOrderProduct47All());
        saleOrder4Products.getProductDetails().add(TestUtil.createSaleOrderProduct46All());

        List<PriceComponentRuleSubjectProduct> products = new ArrayList<>(0);
        products.add(TestUtil.createProductRule44());

        PriceComponentRuleOutcomeOutput outputDiscOne = TestUtil.createOutcomeOutput(1L, TestUtil.createOutcomeDiscountPerc(new BigDecimal(5)), ScaleTypeEnum.AMOUNT, OutComeModeEnum.SINGLE, null, null, PriceComponentOutcomeTypeEnum.DISCOUNT_PERCENTAGE);

        PriceComponentRuleOutcomeOutput outputDiscTwo = TestUtil.createOutcomeOutput(999L, TestUtil.createOutcomeDiscountPerc(new BigDecimal(3)), ScaleTypeEnum.AMOUNT, OutComeModeEnum.SINGLE, null, null, PriceComponentOutcomeTypeEnum.DISCOUNT_PERCENTAGE);

        PriceComponentRuleOutcomeOutput outputDiscThree = TestUtil.createOutcomeOutput(1000L, TestUtil.createOutcomeDiscountPerc(new BigDecimal(4)), ScaleTypeEnum.AMOUNT, OutComeModeEnum.SINGLE, null, null, PriceComponentOutcomeTypeEnum.DISCOUNT_PERCENTAGE);

        PriceComponentRuleOutcomeOutput outputDiscFour = TestUtil.createOutcomeOutput(1001L, TestUtil.createOutcomeDiscountPerc(new BigDecimal(8)), ScaleTypeEnum.AMOUNT, OutComeModeEnum.SINGLE, null, null, PriceComponentOutcomeTypeEnum.DISCOUNT_PERCENTAGE);

        PriceComponentRuleOutcomeOutput outputDiscFive = TestUtil.createOutcomeOutput(1001L, TestUtil.createOutcomeDiscountPerc(new BigDecimal(15)), ScaleTypeEnum.AMOUNT, OutComeModeEnum.SINGLE, null, null, PriceComponentOutcomeTypeEnum.DISCOUNT_PERCENTAGE);

        List<PriceComponentRuleFactorBusinessUnit> businessUnits = Collections.singletonList(TestUtil.createBusinessUnitRetail());

        PriceRule ruleOne = TestUtil.createRuleOnlyBusinessUnit(1L, businessUnits, false, TargetEnum.PRODUCT, PriceRuleTypeEnum.RESTRICTED,
                ScaleTypeEnum.AMOUNT, outputDiscOne, OutComeModeEnum.SINGLE);

        PriceRule ruleTwo = TestUtil.createRuleOnlyProducts(2L, products, true, TargetEnum.PRODUCT, PriceRuleTypeEnum.RESTRICTED,
                ScaleTypeEnum.AMOUNT, outputDiscTwo, OutComeModeEnum.SINGLE);

        PriceRule ruleThree = TestUtil.createRuleOnlyProducts(3L, products, true, TargetEnum.PRODUCT, PriceRuleTypeEnum.RESTRICTED,
                ScaleTypeEnum.AMOUNT, outputDiscThree, OutComeModeEnum.SINGLE);

        PriceRule ruleFour = TestUtil.createRuleOnlyProducts(4L, Collections.singletonList(TestUtil.createProductRule46()), true, TargetEnum.PRODUCT,
                PriceRuleTypeEnum.RESTRICTED, ScaleTypeEnum.AMOUNT, outputDiscFour, OutComeModeEnum.SINGLE);

        PriceRule ruleFive = TestUtil.createRuleOnlyProducts(5L, Collections.singletonList(TestUtil.createProductRule47()), true, TargetEnum.PRODUCT,
                PriceRuleTypeEnum.RESTRICTED, ScaleTypeEnum.AMOUNT, outputDiscFive, OutComeModeEnum.SINGLE);



        List<PriceRule> rules = new ArrayList<>(0);
        rules.add(ruleOne);
        rules.add(ruleTwo);
        rules.add(ruleThree);
        rules.add(ruleFour);
        rules.add(ruleFive);

        PriceComponentRule componentRule = PriceComponentRule.builder()
                .rules(rules)
                .resolutionMode(PriceRuleResolutionModeEnum.BY_BEST_VALUE_PRODUCT)
                .allowSetupLogger(false)
                .build();

        List<PriceComponentRuleInputSaleOrderProduct> results = componentRule.computate(saleOrder4Products);

        PriceComponentRuleInputSaleOrderProduct product44 = results.stream().filter(product -> product.getProduct().getId() == 44).findFirst().orElse(null);
        PriceComponentRuleInputSaleOrderProduct product46 = results.stream().filter(product -> product.getProduct().getId() == 46).findFirst().orElse(null);
        PriceComponentRuleInputSaleOrderProduct product47 = results.stream().filter(product -> product.getProduct().getId() == 47).findFirst().orElse(null);
        PriceComponentRuleInputSaleOrderProduct product48 = results.stream().filter(product -> product.getProduct().getId() == 48).findFirst().orElse(null);

        if (Objects.isNull(product44))
            fail("Product 44 does not exists");
        assertThat(product44.getRulesApplied().get(0).getId(), is(2L));
        assertThat(product44.getRulesApplied().get(1).getId(), is(3L));

        if (Objects.isNull(product46))
            fail("Product 46 does not exists");
        assertThat(product46.getRulesApplied().get(0).getId(), is(4L));

        if (Objects.isNull(product47))
            fail("Product 47 does not exists");
        assertThat(product47.getRulesApplied().get(0).getId(), is(5L));

        if (Objects.isNull(product48))
            fail("Product 48 does not exists.");
        assertThat(product48.getRulesApplied().get(0).getId(), is(1L));

        LogUtil.warn("Results: \n" + ToJsonString.toJson(results));
    }

    @Tag("production")
    @Test
    public void validateTestCase4RulesBestDiscountPerc() {
        PriceComponentRuleInputSaleOrder saleOrder4Products = TestUtil.createSaleOrderRamiro4448WithAllFactors(
                DeliveryModeEnum.MOBILE_STORE, PaymentConditionsTypeEnum.CASH);
        saleOrder4Products.getProductDetails().add(TestUtil.createSaleOrderProduct47All());
        saleOrder4Products.getProductDetails().add(TestUtil.createSaleOrderProduct46All());

        List<PriceComponentRuleSubjectProduct> products = new ArrayList<>(0);
        products.add(TestUtil.createProductRule44());

        PriceComponentRuleOutcomeOutput outputDiscOne = TestUtil.createOutcomeOutput(1L, TestUtil.createOutcomeDiscountPerc(new BigDecimal(5)), ScaleTypeEnum.AMOUNT, OutComeModeEnum.SINGLE, null, null, PriceComponentOutcomeTypeEnum.DISCOUNT_PERCENTAGE);

        PriceComponentRuleOutcomeOutput outputDiscTwo = TestUtil.createOutcomeOutput(999L, TestUtil.createOutcomeDiscountPerc(new BigDecimal(3)), ScaleTypeEnum.AMOUNT, OutComeModeEnum.SINGLE, null, null, PriceComponentOutcomeTypeEnum.DISCOUNT_PERCENTAGE);

        PriceComponentRuleOutcomeOutput outputDiscThree = TestUtil.createOutcomeOutput(1000L, TestUtil.createOutcomeDiscountPerc(new BigDecimal(4)), ScaleTypeEnum.AMOUNT, OutComeModeEnum.SINGLE, null, null, PriceComponentOutcomeTypeEnum.DISCOUNT_PERCENTAGE);

        PriceComponentRuleOutcomeOutput outputDiscFour = TestUtil.createOutcomeOutput(1001L, TestUtil.createOutcomeDiscountPerc(new BigDecimal(8)), ScaleTypeEnum.AMOUNT, OutComeModeEnum.SINGLE, null, null, PriceComponentOutcomeTypeEnum.DISCOUNT_PERCENTAGE);

        PriceComponentRuleOutcomeOutput outputDiscFive = TestUtil.createOutcomeOutput(1001L, TestUtil.createOutcomeDiscountPerc(new BigDecimal(15)), ScaleTypeEnum.AMOUNT, OutComeModeEnum.SINGLE, null, null, PriceComponentOutcomeTypeEnum.DISCOUNT_PERCENTAGE);

        List<PriceComponentRuleFactorBusinessUnit> businessUnits = Collections.singletonList(TestUtil.createBusinessUnitRetail());

        PriceRule ruleOne = TestUtil.createRuleOnlyBusinessUnit(1L, businessUnits, false, TargetEnum.PRODUCT, PriceRuleTypeEnum.RESTRICTED,
                ScaleTypeEnum.AMOUNT, outputDiscOne, OutComeModeEnum.SINGLE);

        PriceRule ruleTwo = TestUtil.createRuleOnlyProducts(2L, products, true, TargetEnum.PRODUCT, PriceRuleTypeEnum.RESTRICTED,
                ScaleTypeEnum.AMOUNT, outputDiscTwo, OutComeModeEnum.SINGLE);

        PriceRule ruleThree = TestUtil.createRuleOnlyProducts(3L, products, true, TargetEnum.PRODUCT, PriceRuleTypeEnum.RESTRICTED,
                ScaleTypeEnum.AMOUNT, outputDiscThree, OutComeModeEnum.SINGLE);

        PriceRule ruleFour = TestUtil.createRuleOnlyProducts(4L, Collections.singletonList(TestUtil.createProductRule46()), false, TargetEnum.PRODUCT,
                PriceRuleTypeEnum.RESTRICTED, ScaleTypeEnum.AMOUNT, outputDiscFour, OutComeModeEnum.SINGLE);

        PriceRule ruleFive = TestUtil.createRuleOnlyProducts(5L, Collections.singletonList(TestUtil.createProductRule47()), false, TargetEnum.PRODUCT,
                PriceRuleTypeEnum.RESTRICTED, ScaleTypeEnum.AMOUNT, outputDiscFive, OutComeModeEnum.SINGLE);



        List<PriceRule> rules = new ArrayList<>(0);
        rules.add(ruleOne);
        rules.add(ruleTwo);
        rules.add(ruleThree);
        rules.add(ruleFour);
        rules.add(ruleFive);

        PriceComponentRule componentRule = PriceComponentRule.builder()
                .rules(rules)
                .resolutionMode(PriceRuleResolutionModeEnum.BY_BEST_VALUE_PRODUCT)
                .allowSetupLogger(false)
                .build();

        List<PriceComponentRuleInputSaleOrderProduct> results = componentRule.computate(saleOrder4Products);

        PriceComponentRuleInputSaleOrderProduct product44 = results.stream().filter(product -> product.getProduct().getId() == 44).findFirst().orElse(null);
        PriceComponentRuleInputSaleOrderProduct product46 = results.stream().filter(product -> product.getProduct().getId() == 46).findFirst().orElse(null);
        PriceComponentRuleInputSaleOrderProduct product47 = results.stream().filter(product -> product.getProduct().getId() == 47).findFirst().orElse(null);
        PriceComponentRuleInputSaleOrderProduct product48 = results.stream().filter(product -> product.getProduct().getId() == 48).findFirst().orElse(null);

        if (Objects.isNull(product44))
            fail("Product 44 does not exists");
        assertThat(product44.getRulesApplied().get(0).getId(), is(2L));
        assertThat(product44.getRulesApplied().get(1).getId(), is(3L));

        if (Objects.isNull(product46))
            fail("Product 46 does not exists");
        assertThat(product46.getRulesApplied().get(0).getId(), is(4L));

        if (Objects.isNull(product47))
            fail("Product 47 does not exists");
        assertThat(product47.getRulesApplied().get(0).getId(), is(5L));

        if (Objects.isNull(product48))
            fail("Product 48 does not exists.");
        assertThat(product48.getRulesApplied().get(0).getId(), is(1L));

        LogUtil.warn("Results: \n" + ToJsonString.toJson(results));
    }
}
