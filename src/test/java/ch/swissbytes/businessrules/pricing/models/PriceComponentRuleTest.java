package ch.swissbytes.businessrules.pricing.models;

import ch.swissbytes.businessrules.pricing.enums.*;
import ch.swissbytes.businessrules.pricing.factors.PriceComponentRuleFactorScale;
import ch.swissbytes.businessrules.pricing.inputs.PriceComponentRuleInputSaleOrder;
import ch.swissbytes.businessrules.pricing.inputs.PriceComponentRuleInputSaleOrderProduct;
import ch.swissbytes.businessrules.pricing.outcomes.PriceComponentRuleOutcomeOutput;
import ch.swissbytes.businessrules.pricing.utils.LogUtil;
import ch.swissbytes.businessrules.pricing.utils.TestUtil;
import ch.swissbytes.businessrules.pricing.utils.ToJsonString;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;


class PriceComponentRuleTest {


    @Tag("production")
    @Test
    void testFindBestRuleDiscountPercentage() {
        PriceComponentRuleInputSaleOrder saleOrder4448 = TestUtil.createSaleOrderRamiro4448WithAllFactors(DeliveryModeEnum.MOBILE_STORE,
                PaymentConditionsTypeEnum.CASH);

        List<PriceRule> rules = new ArrayList<>(0);
        rules.add(TestUtil.createRule10DiscountPercentageRetailWithProduct4448(false, PriceRuleTypeEnum.RESTRICTED, TargetEnum.PRODUCT));
        rules.add(TestUtil.createRule10DiscountPercentageRetailWithProduct44(true, PriceRuleTypeEnum.RESTRICTED, TargetEnum.PRODUCT));
        rules.add(TestUtil.createRule15DiscountPercentageRetailWithProduct48(false, PriceRuleTypeEnum.RESTRICTED, TargetEnum.PRODUCT));
        rules.add(TestUtil.createRule100DiscountAmountRetail(true, PriceRuleTypeEnum.RESTRICTED, TargetEnum.PRODUCT));

        PriceComponentRule componentRule = PriceComponentRule.builder()
                .rules(rules)
                .resolutionMode(PriceRuleResolutionModeEnum.BY_BEST_VALUE_SALE_ORDER)
                .build();

        List<PriceComponentRuleInputSaleOrderProduct> ans = componentRule.computate(saleOrder4448);

        LogUtil.warn("RESULT: \n" + ToJsonString.toJson(ans));

        PriceComponentRuleInputSaleOrderProduct product44 = ans.get(1);
        PriceComponentRuleInputSaleOrderProduct product48 = ans.get(0);

        List<IdNameModel> expectedRuleAppliedProduct44 = new ArrayList<>(0);
        List<IdNameModel> expectedRuleAppliedProduct48 = new ArrayList<>(0);
        expectedRuleAppliedProduct48.add(IdNameModel.builder()
                .id(10)
                .name("DESCUENTO DEL 10% AL CLIENTE RAMIRO, SEGMENTO DE CLIENTES RETAIL EN PRODUCTOS: 44")
                .build());
        expectedRuleAppliedProduct48.add(IdNameModel.builder()
                .id(420)
                .name("DESCUENTO DEL 100 Bs A CLIENTES RETAIL")
                .build());
        expectedRuleAppliedProduct44.add(IdNameModel.builder()
                .id(420)
                .name("DESCUENTO DEL 100 Bs A CLIENTES RETAIL")
                .build());

        assertThat(product44.getRulesApplied(), is(expectedRuleAppliedProduct44));
        assertThat(product48.getRulesApplied(), is(expectedRuleAppliedProduct48));
    }

    @Tag("production")
    @Test
    void testFindBestRuleDiscountAmount() {
        PriceComponentRuleInputSaleOrder saleOrder4448 = TestUtil.createSaleOrderRamiro4448WithAllFactors(DeliveryModeEnum.MOBILE_STORE,
                PaymentConditionsTypeEnum.CASH);

        PriceComponentRuleOutcomeOutput outcomeDiscountAmount = TestUtil.createOutcomeOutputDiscountAmount(new BigDecimal(25),ScaleTypeEnum.QUANTITY, OutComeModeEnum.SINGLE,null, Collections.emptyList());
        PriceComponentRuleOutcomeOutput outcomeDiscountAmount2 = TestUtil.createOutcomeOutputDiscountAmount(new BigDecimal(250),ScaleTypeEnum.QUANTITY, OutComeModeEnum.SINGLE,null, Collections.emptyList());
        PriceComponentRuleOutcomeOutput outcomeDiscountAmount3 = TestUtil.createOutcomeOutputDiscountAmount(new BigDecimal(50),ScaleTypeEnum.QUANTITY, OutComeModeEnum.SINGLE,null, Collections.emptyList());
        PriceComponentRuleOutcomeOutput outcomeDiscountAmount4 = TestUtil.createOutcomeOutputDiscountAmount(new BigDecimal(100),ScaleTypeEnum.QUANTITY, OutComeModeEnum.SINGLE,null, Collections.emptyList());

        List<PriceRule> rules = new ArrayList<>(0);
        rules.add(TestUtil.createRule1012RetailWithProduct4448(false, PriceRuleTypeEnum.RESTRICTED, TargetEnum.PRODUCT,outcomeDiscountAmount,ScaleTypeEnum.QUANTITY));
        rules.add(TestUtil.createRule1013RetailWithProduct44(true, PriceRuleTypeEnum.RESTRICTED, TargetEnum.PRODUCT, outcomeDiscountAmount2,ScaleTypeEnum.QUANTITY));
        rules.add(TestUtil.createRule1014RetailWithProduct48(false, PriceRuleTypeEnum.RESTRICTED, TargetEnum.PRODUCT, outcomeDiscountAmount3,ScaleTypeEnum.QUANTITY));
        rules.add(TestUtil.createRule1015Retail(true, PriceRuleTypeEnum.RESTRICTED, TargetEnum.PRODUCT, outcomeDiscountAmount4, ScaleTypeEnum.QUANTITY));

        PriceComponentRule componentRule = PriceComponentRule.builder()
                .rules(rules)
                .resolutionMode(PriceRuleResolutionModeEnum.BY_BEST_VALUE_SALE_ORDER)
                .build();

        List<PriceComponentRuleInputSaleOrderProduct> ans = componentRule.computate(saleOrder4448);
        LogUtil.warn("RESULT: \n" + ToJsonString.toJson(ans));

        List<PriceRule> bestRules = new ArrayList<>(0);
        bestRules.add(rules.get(1));
        bestRules.add(rules.get(3));

        assertThat(componentRule.getBestRules(), is(bestRules));
    }

    @Tag("production")
    @Test
    void testFindBestRuleDiscountAmountWithScales() {
        PriceComponentRuleInputSaleOrder saleOrder4448 = TestUtil.createSaleOrderRamiro4448WithAllFactors(DeliveryModeEnum.MOBILE_STORE,
                PaymentConditionsTypeEnum.CASH);

        List<PriceComponentRuleFactorScale> scales = new ArrayList<>(0);
        scales.add(TestUtil.createScale(new BigDecimal(100), new BigDecimal(300), ScaleTypeEnum.AMOUNT,TestUtil.createOutcomeDiscountAmount(new BigDecimal(5))));
        scales.add(TestUtil.createScale(new BigDecimal(301), new BigDecimal(500), ScaleTypeEnum.AMOUNT,TestUtil.createOutcomeDiscountAmount(new BigDecimal(10))));
        scales.add(TestUtil.createScale(new BigDecimal(600), new BigDecimal(1300), ScaleTypeEnum.AMOUNT,TestUtil.createOutcomeDiscountAmount(new BigDecimal(15))));

        List<PriceComponentRuleFactorScale> scales2 = new ArrayList<>(0);
        scales2.add(TestUtil.createScale(new BigDecimal(1), new BigDecimal(50), ScaleTypeEnum.AMOUNT,TestUtil.createOutcomeDiscountAmount(new BigDecimal(18))));
        scales2.add(TestUtil.createScale(new BigDecimal(51), new BigDecimal(99), ScaleTypeEnum.AMOUNT,TestUtil.createOutcomeDiscountAmount(new BigDecimal(20))));

        PriceComponentRuleOutcomeOutput outcomeDiscountAmount = TestUtil.createOutcomeOutputDiscountAmount(scales);
        PriceComponentRuleOutcomeOutput outcomeDiscountAmount2 = TestUtil.createOutcomeOutputDiscountAmount(scales2);

        List<PriceRule> rules = new ArrayList<>(0);
        rules.add(TestUtil.createRule1021RetailWithProduct4448(false, PriceRuleTypeEnum.RESTRICTED, TargetEnum.PRODUCT,outcomeDiscountAmount,ScaleTypeEnum.AMOUNT, OutComeModeEnum.SCALE));
        rules.add(TestUtil.createRule1022RetailWithProduct44(false, PriceRuleTypeEnum.RESTRICTED, TargetEnum.PRODUCT, outcomeDiscountAmount2,ScaleTypeEnum.AMOUNT, OutComeModeEnum.SCALE));

        PriceComponentRule componentRule = PriceComponentRule.builder()
                .rules(rules)
                .resolutionMode(PriceRuleResolutionModeEnum.BY_BEST_VALUE_SALE_ORDER)
                .build();

        List<PriceComponentRuleInputSaleOrderProduct> ans = componentRule.computate(saleOrder4448);
        LogUtil.warn("RESULT: \n" + ToJsonString.toJson(ans));

        List<PriceRule> bestRules = new ArrayList<>(0);
        bestRules.add(rules.get(0));

        assertThat(componentRule.getBestRules(), is(bestRules));
        assertThat(ans.get(0).getRulesApplied().get(0).getId(),is(1021L));
    }

    @Tag("production")
    @Test
    void testFindBestRuleOutcomeDiscountPercentage() {
        PriceComponentRuleInputSaleOrder saleOrder4448 = TestUtil.createSaleOrderRamiro4448WithAllFactors(DeliveryModeEnum.MOBILE_STORE,
                PaymentConditionsTypeEnum.CASH);

        PriceComponentRuleOutcomeOutput outcomeDiscountPerc = TestUtil.createOutcomeOutputDiscountPerc(new BigDecimal(12),ScaleTypeEnum.AMOUNT);

        List<PriceRule> rules = new ArrayList<>(0);
        rules.add(TestUtil.createRule1012RetailWithProduct4448(false, PriceRuleTypeEnum.RESTRICTED, TargetEnum.PRODUCT,outcomeDiscountPerc,ScaleTypeEnum.AMOUNT));
        rules.add(TestUtil.createRule1013RetailWithProduct44(true, PriceRuleTypeEnum.RESTRICTED, TargetEnum.PRODUCT, outcomeDiscountPerc,ScaleTypeEnum.AMOUNT));
        rules.add(TestUtil.createRule1014RetailWithProduct48(false, PriceRuleTypeEnum.RESTRICTED, TargetEnum.PRODUCT, outcomeDiscountPerc,ScaleTypeEnum.AMOUNT));
        rules.add(TestUtil.createRule1015Retail(true, PriceRuleTypeEnum.RESTRICTED, TargetEnum.PRODUCT, outcomeDiscountPerc,ScaleTypeEnum.AMOUNT));

        PriceComponentRule componentRule = PriceComponentRule.builder()
                .rules(rules)
                .resolutionMode(PriceRuleResolutionModeEnum.BY_BEST_VALUE_SALE_ORDER)
                .build();

        List<PriceComponentRuleInputSaleOrderProduct> ans = componentRule.computate(saleOrder4448);

        LogUtil.warn("RESULT: \n" + ToJsonString.toJson(ans));

        PriceComponentRuleInputSaleOrderProduct product44 = ans.get(1);
        PriceComponentRuleInputSaleOrderProduct product48 = ans.get(0);

        List<IdNameModel> expectedRuleAppliedProduct44 = new ArrayList<>(0);
        List<IdNameModel> expectedRuleAppliedProduct48 = new ArrayList<>(0);
        expectedRuleAppliedProduct48.add(IdNameModel.builder()
                .id(1013)
                .name("REGLA APLICABLE AL CLIENTE RAMIRO, SEGMENTO DE CLIENTES RETAIL EN PRODUCTOS: 44")
                .build());
        expectedRuleAppliedProduct48.add(IdNameModel.builder()
                .id(1015)
                .name("REGLA APLICABLE A CLIENTES RETAIL")
                .build());
        expectedRuleAppliedProduct44.add(IdNameModel.builder()
                .id(1015)
                .name("REGLA APLICABLE A CLIENTES RETAIL")
                .build());

        assertThat(product44.getRulesApplied(), is(expectedRuleAppliedProduct44));
        assertThat(product48.getRulesApplied(), is(expectedRuleAppliedProduct48));
    }

    private void resetProductIds(List<PriceComponentRuleInputSaleOrderProduct> products, int index) {
        final int[] i = {index};
        products.forEach(saleOrderProduct -> {
            saleOrderProduct.setId(i[0]);
            i[0]++;
        });
    }

}
