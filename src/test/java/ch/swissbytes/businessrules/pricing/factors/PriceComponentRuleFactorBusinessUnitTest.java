package ch.swissbytes.businessrules.pricing.factors;

import ch.swissbytes.businessrules.pricing.enums.DeliveryModeEnum;
import ch.swissbytes.businessrules.pricing.enums.PaymentConditionsTypeEnum;
import ch.swissbytes.businessrules.pricing.inputs.PriceComponentRuleInputSaleOrder;
import ch.swissbytes.businessrules.pricing.utils.TestUtil;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


class PriceComponentRuleFactorBusinessUnitTest {

    @Test
    void isValid() {
        PriceComponentRuleFactorBusinessUnit retail = TestUtil.createBusinessUnitRetail();
        PriceComponentRuleFactorBusinessUnit distribuidores = TestUtil.createBusinessUnitDistribuidores();
        PriceComponentRuleFactorBusinessUnit privadas = TestUtil.createBusinessUnitEntidadesPrivadas();
        PriceComponentRuleFactorBusinessUnit publicas = TestUtil.createBusinessUnitEntidadesPublicas();

        PriceComponentRuleInputSaleOrder saleOrderRetail = TestUtil.createSaleOrderRamiro48();
        PriceComponentRuleInputSaleOrder saleOrderPrivadas = TestUtil.createSaleOrderMalcom48(DeliveryModeEnum.MOBILE_STORE,
                PaymentConditionsTypeEnum.CASH);

        assertTrue(retail.isValid(saleOrderRetail));
        assertFalse(retail.isValid(saleOrderPrivadas));

        assertFalse(distribuidores.isValid(saleOrderRetail));
        assertFalse(distribuidores.isValid(saleOrderPrivadas));

        assertFalse(privadas.isValid(saleOrderRetail));
        assertTrue(privadas.isValid(saleOrderPrivadas));

        assertFalse(publicas.isValid(saleOrderRetail));
        assertFalse(publicas.isValid(saleOrderPrivadas));

        saleOrderRetail.setBusinessUnit(null);
        assertFalse(retail.isValid(saleOrderRetail));
    }
}
