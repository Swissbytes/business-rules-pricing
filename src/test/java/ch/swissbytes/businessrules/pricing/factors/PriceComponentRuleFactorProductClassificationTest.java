package ch.swissbytes.businessrules.pricing.factors;

import ch.swissbytes.businessrules.pricing.enums.DeliveryModeEnum;
import ch.swissbytes.businessrules.pricing.enums.PaymentConditionsTypeEnum;
import ch.swissbytes.businessrules.pricing.inputs.PriceComponentRuleInputSaleOrder;
import ch.swissbytes.businessrules.pricing.inputs.PriceComponentRuleInputSaleOrderProduct;
import ch.swissbytes.businessrules.pricing.utils.TestUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.*;


class PriceComponentRuleFactorProductClassificationTest {

    private PriceComponentRuleInputSaleOrder saleOrderCaboPBisturi;
    private PriceComponentRuleInputSaleOrder saleOrderNoneCabo;
    private PriceComponentRuleInputSaleOrder saleOrderBoth;

    private PriceComponentRuleFactorProductClassification productClassificationQuirofano;
    private PriceComponentRuleFactorProductClassification productClassificationPediatria;
    private PriceComponentRuleFactorProductClassification productClassificationCaboPBisturi;
    private PriceComponentRuleFactorProductClassification productClassificationHaloCefalico;

    private PriceComponentRuleInputSaleOrderProduct product44;
    private PriceComponentRuleInputSaleOrderProduct product47;
    private PriceComponentRuleInputSaleOrderProduct product48;

    private BigDecimal fromAmount;
    private BigDecimal thruAmount;

    private int fromQty;
    private int thruQty;

    @BeforeEach
    void setUp() {
        //Rule Set up
        fromAmount = BigDecimal.ZERO;
        thruAmount = new BigDecimal(15000);
        fromQty = 0;
        thruQty = 0;

        saleOrderCaboPBisturi = TestUtil.createSaleOrderRamiro48();
        saleOrderBoth = TestUtil.createSaleOrderRamiro47(true, DeliveryModeEnum.MOBILE_STORE, PaymentConditionsTypeEnum.CASH);
        saleOrderNoneCabo = TestUtil.createSaleOrderRetailTelma444850(DeliveryModeEnum.MOBILE_STORE,
                PaymentConditionsTypeEnum.CASH);

        productClassificationQuirofano = TestUtil.createProductClassificationQuirofano();
        productClassificationCaboPBisturi = TestUtil.createProductClassificationCaboPBisturi();

        productClassificationPediatria = TestUtil.createProductClassificationPediatria();
        productClassificationHaloCefalico = TestUtil.createProductClassificationHaloCefalico();

        product44 = TestUtil.createSaleOrderProduct44();
        product47 = TestUtil.createSaleOrderProduct47All();
        product48 = TestUtil.createSaleOrderProduct48WithAll();
    }

    @Test
    void existsProductClassification() {
        assertFalse(productClassificationQuirofano.isValidProduct(product44));
        assertTrue(productClassificationQuirofano.isValidProduct(product47));
        assertFalse(productClassificationQuirofano.isValidProduct(product48));

        assertFalse(productClassificationCaboPBisturi.isValidProduct(product44));
        assertFalse(productClassificationCaboPBisturi.isValidProduct(product47));
        assertFalse(productClassificationCaboPBisturi.isValidProduct(product48));

        assertFalse(productClassificationPediatria.isValidProduct(product44));
        assertTrue(productClassificationPediatria.isValidProduct(product47));
        assertTrue(productClassificationPediatria.isValidProduct(product48));

        assertFalse(productClassificationHaloCefalico.isValidProduct(product44));
        assertTrue(productClassificationHaloCefalico.isValidProduct(product47));
        assertTrue(productClassificationHaloCefalico.isValidProduct(product48));
    }

    @Test
    void isValid() {
        assertTrue(productClassificationQuirofano.isValid(saleOrderCaboPBisturi));
        assertTrue(productClassificationQuirofano.isValid(saleOrderNoneCabo));
        assertTrue(productClassificationQuirofano.isValid(saleOrderBoth));

        assertTrue(productClassificationCaboPBisturi.isValid(saleOrderCaboPBisturi));
        assertTrue(productClassificationCaboPBisturi.isValid(saleOrderNoneCabo));
        assertFalse(productClassificationCaboPBisturi.isValid(saleOrderBoth));

        assertFalse(productClassificationPediatria.isValid(saleOrderCaboPBisturi));
        assertFalse(productClassificationPediatria.isValid(saleOrderNoneCabo));
        assertTrue(productClassificationPediatria.isValid(saleOrderBoth));

        assertFalse(productClassificationHaloCefalico.isValid(saleOrderCaboPBisturi));
        assertFalse(productClassificationHaloCefalico.isValid(saleOrderNoneCabo));
        assertTrue(productClassificationHaloCefalico.isValid(saleOrderBoth));
    }
}
