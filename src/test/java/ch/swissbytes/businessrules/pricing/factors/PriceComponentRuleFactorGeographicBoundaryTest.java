package ch.swissbytes.businessrules.pricing.factors;

import ch.swissbytes.businessrules.pricing.enums.DeliveryModeEnum;
import ch.swissbytes.businessrules.pricing.enums.PaymentConditionsTypeEnum;
import ch.swissbytes.businessrules.pricing.inputs.PriceComponentRuleInputSaleOrder;
import ch.swissbytes.businessrules.pricing.utils.TestUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import java.util.Collections;

import static org.junit.jupiter.api.Assertions.*;


class PriceComponentRuleFactorGeographicBoundaryTest {

    private PriceComponentRuleInputSaleOrder saleOrderRetail;
    private PriceComponentRuleInputSaleOrder saleOrderPrivadas;
    private PriceComponentRuleInputSaleOrder saleOrderRetailFail;

    private PriceComponentRuleFactorGeographicBoundary geoSC;
    private PriceComponentRuleFactorGeographicBoundary geoLP;

    @BeforeEach
    void setUp() {
        saleOrderRetail = TestUtil.createSaleOrderRamiro48();
        saleOrderRetailFail = TestUtil.createSaleOrderRetailTelma444850(DeliveryModeEnum.MOBILE_STORE,
                PaymentConditionsTypeEnum.CASH);
        saleOrderPrivadas = TestUtil.createSaleOrderMalcom48(DeliveryModeEnum.MOBILE_STORE,
                PaymentConditionsTypeEnum.CASH);
        geoSC = TestUtil.createGeographicBoundarySC();
        geoLP = TestUtil.createGeographicBoundaryLP();
    }

    @Tag("production")
    @Test
    void isValid() {
        assertTrue(geoSC.isValid(saleOrderRetail));
        assertFalse(geoSC.isValid(saleOrderPrivadas));
        assertFalse(geoSC.isValid(saleOrderRetailFail));

        assertFalse(geoLP.isValid(saleOrderRetail));
        assertFalse(geoLP.isValid(saleOrderPrivadas));
        assertTrue(geoLP.isValid(saleOrderRetailFail));

        saleOrderRetail.getCustomer().setGeographicBoundaries(null);
        saleOrderPrivadas.getCustomer().setGeographicBoundaries(Collections.emptyList());
        saleOrderRetailFail.getCustomer().setGeographicBoundaries(Collections.singletonList(TestUtil.createGeographicBoundarySimpsons()));

        assertFalse(geoSC.isValid(saleOrderRetail));
        assertFalse(geoSC.isValid(saleOrderPrivadas));
        assertFalse(geoSC.isValid(saleOrderRetailFail));

        assertFalse(geoLP.isValid(saleOrderRetail));
        assertFalse(geoLP.isValid(saleOrderPrivadas));
        assertFalse(geoLP.isValid(saleOrderRetailFail));

        geoLP.setId(TestUtil.createGeographicBoundarySimpsons().getId());
        assertTrue(geoLP.isValid(saleOrderRetailFail));
    }
}
