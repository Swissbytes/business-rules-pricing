package ch.swissbytes.businessrules.pricing.factors;

import ch.swissbytes.businessrules.pricing.enums.DeliveryModeEnum;
import ch.swissbytes.businessrules.pricing.enums.PaymentConditionsTypeEnum;
import ch.swissbytes.businessrules.pricing.inputs.PriceComponentRuleInputSaleOrder;
import ch.swissbytes.businessrules.pricing.utils.TestUtil;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


class PriceComponentRuleFactorAgencyTest {

    @Test
    void isValid() {
        PriceComponentRuleFactorAgency sc = TestUtil.createAgencySC();
        PriceComponentRuleFactorAgency lp = TestUtil.createAgencyLP();
        PriceComponentRuleFactorAgency cbba = TestUtil.createAgencyCBBA();

        PriceComponentRuleInputSaleOrder saleOrderRetail = TestUtil.createSaleOrderRamiro48();
        PriceComponentRuleInputSaleOrder saleOrderPrivadas = TestUtil.createSaleOrderMalcom48(DeliveryModeEnum.MOBILE_STORE,
                PaymentConditionsTypeEnum.CASH);

        assertFalse(sc.isValid(saleOrderRetail));
        assertTrue(sc.isValid(saleOrderPrivadas));

        assertTrue(lp.isValid(saleOrderRetail));
        assertFalse(lp.isValid(saleOrderPrivadas));

        assertFalse(cbba.isValid(saleOrderRetail));
        assertFalse(cbba.isValid(saleOrderPrivadas));

        saleOrderRetail.setAgency(null);
        assertFalse(sc.isValid(saleOrderRetail));

        saleOrderRetail.setAgency(TestUtil.createAgencyCBBA());
        assertTrue(cbba.isValid(saleOrderRetail));

    }
}
