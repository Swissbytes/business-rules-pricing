package ch.swissbytes.businessrules.pricing.factors;

import ch.swissbytes.businessrules.pricing.enums.DeliveryModeEnum;
import ch.swissbytes.businessrules.pricing.enums.PaymentConditionsTypeEnum;
import ch.swissbytes.businessrules.pricing.inputs.PriceComponentRuleInputSaleOrder;
import ch.swissbytes.businessrules.pricing.models.PriceComponentRule;
import ch.swissbytes.businessrules.pricing.utils.TestUtil;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


class PriceComponentRuleFactorPartyClassificationTest {

    @Test
    void isValid() {
        PriceComponentRuleFactorPartyClassification partyClassificationCutomerOztomizado = TestUtil.createPartyClassificationCustomerOztomizados();
        PriceComponentRuleFactorPartyClassification partyClassificationEmpLvl1 = TestUtil.createPartyClassificationEmployeeFirstLvl();

        PriceComponentRuleInputSaleOrder saleOrder = TestUtil.createSaleOrderRamiro4448WithAllFactors(DeliveryModeEnum.MOBILE_STORE, PaymentConditionsTypeEnum.CASH);
        assertTrue(partyClassificationEmpLvl1.isValid(saleOrder));
        assertTrue(partyClassificationCutomerOztomizado.isValid(saleOrder));
    }
}
