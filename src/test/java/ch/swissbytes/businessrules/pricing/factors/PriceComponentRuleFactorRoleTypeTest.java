package ch.swissbytes.businessrules.pricing.factors;

import ch.swissbytes.businessrules.pricing.enums.DeliveryModeEnum;
import ch.swissbytes.businessrules.pricing.enums.PaymentConditionsTypeEnum;
import ch.swissbytes.businessrules.pricing.inputs.PriceComponentRuleInputSaleOrder;
import ch.swissbytes.businessrules.pricing.utils.TestUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;


class PriceComponentRuleFactorRoleTypeTest {

    private PriceComponentRuleInputSaleOrder saleOrderCustomer;
    private PriceComponentRuleInputSaleOrder saleOrderEmployee;

    private PriceComponentRuleFactorRoleType factorRoleCustomer;
    private PriceComponentRuleFactorRoleType factorRoleSeller;
    private PriceComponentRuleFactorRoleType factorRoleDriver;
    private PriceComponentRuleFactorRoleType factorRoleProspect;

    @BeforeEach
    void setUp() {
        saleOrderCustomer = TestUtil.createSaleOrderRamiro4448WithAllFactors(DeliveryModeEnum.CUSTOMER_DELIVERY_POINT,
                PaymentConditionsTypeEnum.CASH);

        saleOrderEmployee = TestUtil.createSaleOrderMalcom48(DeliveryModeEnum.CUSTOMER_DELIVERY_POINT,
                PaymentConditionsTypeEnum.CASH);

        factorRoleCustomer = TestUtil.createRoleTypeCustomer();
        factorRoleSeller = TestUtil.createRoleTypeSeller();
        factorRoleDriver = TestUtil.createRoleTypeDriver();
        factorRoleProspect = TestUtil.createRoleTypeProspect();
    }

    @Test
    void isValid() {
        assertTrue(factorRoleCustomer.isValid(saleOrderCustomer));
        assertFalse(factorRoleCustomer.isValid(saleOrderEmployee));

        assertFalse(factorRoleSeller.isValid(saleOrderCustomer));
        assertTrue(factorRoleSeller.isValid(saleOrderEmployee));

        assertFalse(factorRoleDriver.isValid(saleOrderCustomer));
        assertTrue(factorRoleDriver.isValid(saleOrderEmployee));

        assertFalse(factorRoleProspect.isValid(saleOrderCustomer));
        assertFalse(factorRoleProspect.isValid(saleOrderEmployee));
    }
}
