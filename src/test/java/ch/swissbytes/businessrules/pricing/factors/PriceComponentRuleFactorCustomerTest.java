package ch.swissbytes.businessrules.pricing.factors;

import ch.swissbytes.businessrules.pricing.enums.DeliveryModeEnum;
import ch.swissbytes.businessrules.pricing.enums.PaymentConditionsTypeEnum;
import ch.swissbytes.businessrules.pricing.inputs.PriceComponentRuleInputSaleOrder;
import ch.swissbytes.businessrules.pricing.utils.TestUtil;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


class PriceComponentRuleFactorCustomerTest {

    @Test
    void isValid() {

        PriceComponentRuleFactorCustomer customerRamiro = (PriceComponentRuleFactorCustomer) TestUtil.createPartyRoleCustomerRamiroAll();
        PriceComponentRuleFactorCustomer customerTelma = (PriceComponentRuleFactorCustomer) TestUtil.createPartyRoleCustomerTelmaAll();
        PriceComponentRuleInputSaleOrder saleOrder = TestUtil.createSaleOrderRamiro4448WithAllFactors(DeliveryModeEnum.MOBILE_STORE, PaymentConditionsTypeEnum.CASH);
        assertTrue(customerRamiro.isValid(saleOrder));
        assertFalse(customerTelma.isValid(saleOrder));
    }
}
