package ch.swissbytes.businessrules.pricing.main;

import ch.swissbytes.businessrules.pricing.enums.*;
import ch.swissbytes.businessrules.pricing.factors.PriceComponentRuleFactorProductClassification;
import ch.swissbytes.businessrules.pricing.inputs.PriceComponentRuleInputSaleOrder;
import ch.swissbytes.businessrules.pricing.inputs.PriceComponentRuleInputSaleOrderProduct;
import ch.swissbytes.businessrules.pricing.log.Logger;
import ch.swissbytes.businessrules.pricing.log.LoggerImpl;
import ch.swissbytes.businessrules.pricing.models.PriceComponentRule;
import ch.swissbytes.businessrules.pricing.models.PriceRule;
import ch.swissbytes.businessrules.pricing.outcomes.PriceComponentRuleOutcomeOutput;
import ch.swissbytes.businessrules.pricing.outcomes.PriceComponentRuleOutcomeProduct;
import ch.swissbytes.businessrules.pricing.subjects.PriceComponentRuleSubjectProduct;
import ch.swissbytes.businessrules.pricing.subjects.PriceComponentRuleSubjectProductFeature;
import ch.swissbytes.businessrules.pricing.utils.BigDecimalUtil;
import ch.swissbytes.businessrules.pricing.utils.TestUtil;
import ch.swissbytes.businessrules.pricing.utils.ToJsonString;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.math.BigDecimal;
import java.util.*;

import static org.hamcrest.CoreMatchers.hasItem;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

@TestInstance(TestInstance.Lifecycle.PER_METHOD)
public class PriceComponentRuleFrequencyTest {

    private static final Logger Log = LoggerImpl.getLogger();

    private PriceComponentRule componentRule;
    //region SaleOrders
    private PriceComponentRuleInputSaleOrder saleOrderRamiro4448WithAllFactors;
    //endregion

    private List<PriceRule> rules;

    @BeforeEach
    void setUp() {
        Log.info("Begin: Test Frequency Rules...");
        rules = new ArrayList<>(0);
        saleOrderRamiro4448WithAllFactors = TestUtil.createSaleOrderRamiro4448WithAllFactors(DeliveryModeEnum.MOBILE_STORE, PaymentConditionsTypeEnum.CASH);
    }

    @Tag("production")
    @Test
    void testFrequencyDsctPercFail() {
        Log.info("Test Frequency [Discount Percentage]: Rule 1017. Product 44 has 10% every 2 QTY. [Fail]");

        List<PriceComponentRuleSubjectProduct> products = new ArrayList<>(0);
        products.add(TestUtil.createProductRule44());

        BigDecimal frequency = new BigDecimal(2);

        PriceRule frequencyRule1017 = TestUtil.createRuleOnlyProductFrequencyOutput1017(products, ScaleTypeEnum.QUANTITY, frequency, false, TargetEnum.PRODUCT, PriceRuleTypeEnum.GENERAL);

        rules.add(frequencyRule1017);

        componentRule = PriceComponentRule.builder()
                .resolutionMode(PriceRuleResolutionModeEnum.BY_BEST_VALUE_SALE_ORDER)
                .rules(rules)
                .allowSetupLogger(true)
                .build();

        List<PriceComponentRuleInputSaleOrderProduct> results = componentRule.computate(saleOrderRamiro4448WithAllFactors);

        assertThat(results, is(Collections.emptyList()));

        Log.info("RESULT: \n" + ToJsonString.toJson(results));
    }


    @Tag("production")
    @Test
    void testFrequencyDsctPercSuccess() {
        Log.info("Test Frequency [Discount Percentage]: Rule 1017. Product 44 has 10% every 2 QTY. [SUCCESS]");

        List<PriceComponentRuleSubjectProduct> products = new ArrayList<>(0);
        products.add(TestUtil.createProductRule48());

        BigDecimal frequency = new BigDecimal(2);

        PriceRule frequencyRule1017 = TestUtil.createRuleOnlyProductFrequencyOutput1017(products, ScaleTypeEnum.QUANTITY, frequency, false, TargetEnum.PRODUCT, PriceRuleTypeEnum.GENERAL);

        rules.add(frequencyRule1017);

        componentRule = PriceComponentRule.builder()
                .resolutionMode(PriceRuleResolutionModeEnum.BY_BEST_VALUE_SALE_ORDER)
                .rules(rules)
                .allowSetupLogger(true)
                .build();

        List<PriceComponentRuleInputSaleOrderProduct> results = componentRule.computate(saleOrderRamiro4448WithAllFactors);

        assertThat(results.get(0).getDiscountPercentage(), is(new BigDecimal(50)));
        Log.info("RESULT: \n" + ToJsonString.toJson(results));
    }

    @Tag("production")
    @Test
    void testFrequencyDsctPercSuccessWithOtherRules() {
        Log.info("Test Frequency [Discount Percentage]: Rule 1017, 1023,1004. [Expected: 1023]");

        List<PriceComponentRuleSubjectProduct> products = new ArrayList<>(0);
        products.add(TestUtil.createProductRule48());

        BigDecimal frequency = new BigDecimal(2);

        PriceRule frequencyRule1017 = TestUtil.createRuleOnlyProductFrequencyOutput1017(products, ScaleTypeEnum.QUANTITY, frequency, false, TargetEnum.PRODUCT, PriceRuleTypeEnum.GENERAL);

        PriceComponentRuleOutcomeOutput output = TestUtil.createOutcomeOutputDiscountPercFrequency(new BigDecimal(12), ScaleTypeEnum.QUANTITY, frequency);

        PriceRule frequencyRule1023 = TestUtil.createRule1023RetailRamiroAllFactors(products, false, PriceRuleTypeEnum.GENERAL, TargetEnum.PRODUCT, output, ScaleTypeEnum.QUANTITY,
                OutComeModeEnum.FREQUENCY);

        PriceRule rule1004With10PercDsct = TestUtil.createRuleOnlyBusinessUnit1004(Collections.singletonList(TestUtil.createBusinessUnitRetail()), false, TargetEnum.PRODUCT, PriceRuleTypeEnum.GENERAL);

        rules.add(frequencyRule1017);
        rules.add(frequencyRule1023);
        rules.add(rule1004With10PercDsct);

        componentRule = PriceComponentRule.builder()
                .resolutionMode(PriceRuleResolutionModeEnum.BY_BEST_VALUE_SALE_ORDER)
                .rules(rules)
                .allowSetupLogger(true)
                .build();

        List<PriceComponentRuleInputSaleOrderProduct> results = componentRule.computate(saleOrderRamiro4448WithAllFactors);

        assertThat(results.get(0).getDiscountPercentage(), is(new BigDecimal(60)));
        assertThat(results.get(0).getRulesApplied().get(0).getId(), is(1023L));
        Log.info("RESULT: \n" + ToJsonString.toJson(results));
    }

    @Tag("production")
    @Test
    void testFrequencyDsctPercSuccessWithOtherRulesWithDifferentOutcomeType() {
        Log.info("Test Frequency [Discount Percentage]: Rule 1017, 1023, 1004. [Expected: 1023]");

        List<PriceComponentRuleSubjectProduct> products = new ArrayList<>(0);
        products.add(TestUtil.createProductRule48());

        BigDecimal frequency = new BigDecimal(2);

        PriceRule frequencyRule1017 = TestUtil.createRuleOnlyProductFrequencyOutput1017(products, ScaleTypeEnum.QUANTITY, frequency, false, TargetEnum.PRODUCT, PriceRuleTypeEnum.GENERAL);

        PriceComponentRuleOutcomeOutput output = TestUtil.createOutcomeOutputDiscountPercFrequency(new BigDecimal(12), ScaleTypeEnum.QUANTITY, frequency);

        PriceRule frequencyRule1023 = TestUtil.createRule1023RetailRamiroAllFactors(products, false, PriceRuleTypeEnum.GENERAL, TargetEnum.PRODUCT, output, ScaleTypeEnum.QUANTITY,
                OutComeModeEnum.FREQUENCY);

        PriceRule rule1004With10PercDsct = TestUtil.createRuleOnlyBusinessUnit1004(Collections.singletonList(TestUtil.createBusinessUnitRetail()), false, TargetEnum.PRODUCT, PriceRuleTypeEnum.GENERAL);

        PriceComponentRuleOutcomeOutput outputSingle = TestUtil.createOutcomeOutputDiscountAmount(new BigDecimal(80), ScaleTypeEnum.AMOUNT, OutComeModeEnum.SINGLE, null, Collections.emptyList());

        PriceRule rule1024 = TestUtil.createRuleOnlyProducts1024(products, false, TargetEnum.PRODUCT, PriceRuleTypeEnum.GENERAL,
                ScaleTypeEnum.AMOUNT, outputSingle, OutComeModeEnum.SINGLE);

        rules.add(frequencyRule1017);
        rules.add(frequencyRule1023);
        rules.add(rule1004With10PercDsct);
        rules.add(rule1024);

        componentRule = PriceComponentRule.builder()
                .resolutionMode(PriceRuleResolutionModeEnum.BY_BEST_VALUE_SALE_ORDER)
                .rules(rules)
                .allowSetupLogger(true)
                .build();

        List<PriceComponentRuleInputSaleOrderProduct> results = componentRule.computate(saleOrderRamiro4448WithAllFactors);

        assertThat(results.get(0).getDiscountPercentage(), is(new BigDecimal(60)));
        assertThat(results.get(0).getDiscountAmount(), is(new BigDecimal(80)));
        assertThat(results.get(0).getRulesApplied().get(0).getId(), is(1023L));
        assertThat(results.get(0).getRulesApplied().get(1).getId(), is(1024L));
        Log.info("RESULT: \n" + ToJsonString.toJson(results));
    }

    @Tag("production")
    @Test
    void testFrequencyDsctAmountSuccessWithOtherRules() {
        Log.info("Test Frequency [Discount Amount]: Rule 1023, 1024, 1024S. [Expected: 1024]");

        List<PriceComponentRuleSubjectProduct> products = new ArrayList<>(0);
        products.add(TestUtil.createProductRule48());

        PriceComponentRuleOutcomeOutput output = TestUtil.createOutcomeOutputDiscountAmount(new BigDecimal(15), ScaleTypeEnum.AMOUNT, OutComeModeEnum.FREQUENCY,
                new BigDecimal(50), Collections.emptyList());

        PriceRule frequencyRule1023 = TestUtil.createRule1023RetailRamiroAllFactors(products, false, PriceRuleTypeEnum.GENERAL, TargetEnum.PRODUCT, output, ScaleTypeEnum.AMOUNT,
                OutComeModeEnum.FREQUENCY);

        PriceComponentRuleOutcomeOutput outputFreq = TestUtil.createOutcomeOutputDiscountAmount(new BigDecimal(50), ScaleTypeEnum.AMOUNT, OutComeModeEnum.FREQUENCY,
                new BigDecimal(100), Collections.emptyList());

        PriceRule rule1024 = TestUtil.createRuleOnlyProducts1024(products, false, TargetEnum.PRODUCT, PriceRuleTypeEnum.GENERAL,
                ScaleTypeEnum.AMOUNT, outputFreq, OutComeModeEnum.FREQUENCY);

        PriceComponentRuleOutcomeOutput outputSingle = TestUtil.createOutcomeOutputDiscountAmount(new BigDecimal(50), ScaleTypeEnum.AMOUNT, OutComeModeEnum.SINGLE,
                null, Collections.emptyList());

        PriceRule rule1024S = TestUtil.createRuleOnlyProducts1024(products, false, TargetEnum.PRODUCT, PriceRuleTypeEnum.GENERAL,
                ScaleTypeEnum.AMOUNT, outputSingle, OutComeModeEnum.SINGLE);

        rules.add(frequencyRule1023);
        rules.add(rule1024);
        rules.add(rule1024S);

        componentRule = PriceComponentRule.builder()
                .resolutionMode(PriceRuleResolutionModeEnum.BY_BEST_VALUE_SALE_ORDER)
                .rules(rules)
                .allowSetupLogger(true)
                .build();

        List<PriceComponentRuleInputSaleOrderProduct> results = componentRule.computate(saleOrderRamiro4448WithAllFactors);

        assertThat(results.get(0).getDiscountAmount(), is(new BigDecimal(500)));
        assertThat(results.get(0).getRulesApplied().get(0).getId(), is(1024L));

        Log.info("RESULT: \n" + ToJsonString.toJson(results));
    }

    @Tag("production")
    @Test
    void testFrequencySurchargeAmountSuccessWithOtherRules() {
        Log.info("Test Frequency [Surcharge Amount]: Rule 1023, 1024, 1024S. [Expected: 1024]");

        List<PriceComponentRuleSubjectProduct> products = new ArrayList<>(0);
        products.add(TestUtil.createProductRule48());

        PriceComponentRuleOutcomeOutput output = TestUtil.createOutcomeOutputSurchargeAmount(new BigDecimal(15), ScaleTypeEnum.QUANTITY, OutComeModeEnum.FREQUENCY,
                new BigDecimal(5), Collections.emptyList());

        PriceRule frequencyRule1023 = TestUtil.createRule1023RetailRamiroAllFactors(products, false, PriceRuleTypeEnum.GENERAL, TargetEnum.PRODUCT, output, ScaleTypeEnum.QUANTITY,
                OutComeModeEnum.FREQUENCY);

        PriceComponentRuleOutcomeOutput outputFreq = TestUtil.createOutcomeOutputSurchargeAmount(new BigDecimal(50), ScaleTypeEnum.QUANTITY, OutComeModeEnum.FREQUENCY,
                new BigDecimal(10), Collections.emptyList());

        PriceRule rule1024 = TestUtil.createRuleOnlyProducts1024(products, false, TargetEnum.PRODUCT, PriceRuleTypeEnum.GENERAL,
                ScaleTypeEnum.QUANTITY, outputFreq, OutComeModeEnum.FREQUENCY);

        PriceComponentRuleOutcomeOutput outputSingle = TestUtil.createOutcomeOutputSurchargeAmount(new BigDecimal(150), ScaleTypeEnum.QUANTITY, OutComeModeEnum.SINGLE,
                null, Collections.emptyList());

        PriceRule rule1024S = TestUtil.createRuleOnlyProducts1024(products, false, TargetEnum.PRODUCT, PriceRuleTypeEnum.GENERAL,
                ScaleTypeEnum.QUANTITY, outputSingle, OutComeModeEnum.SINGLE);

        rules.add(frequencyRule1023);
        rules.add(rule1024);
        rules.add(rule1024S);

        componentRule = PriceComponentRule.builder()
                .resolutionMode(PriceRuleResolutionModeEnum.BY_BEST_VALUE_SALE_ORDER)
                .rules(rules)
                .allowSetupLogger(true)
                .build();

        List<PriceComponentRuleInputSaleOrderProduct> results = componentRule.computate(saleOrderRamiro4448WithAllFactors);

        assertThat(results.get(0).getSurchargeAmount(), is(new BigDecimal(150)));
        assertThat(results.get(0).getRulesApplied().get(0).getId(), is(1024L));
        Log.info("RESULT: \n" + ToJsonString.toJson(results));
    }

    @Tag("production")
    @Test
    void testFrequencySurchargePercSuccessWithOtherRules() {
        Log.info("Test Frequency [Surcharge Percentage]: Rule 1023, 1024, 1024S. [Expected: 1024]");

        List<PriceComponentRuleSubjectProduct> products = new ArrayList<>(0);
        products.add(TestUtil.createProductRule48());

        PriceComponentRuleOutcomeOutput output = TestUtil.createOutcomeOutputSurchargePerc(new BigDecimal(5), ScaleTypeEnum.QUANTITY, OutComeModeEnum.FREQUENCY,
                new BigDecimal(5), Collections.emptyList());

        PriceRule frequencyRule1023 = TestUtil.createRule1023RetailRamiroAllFactors(products, false, PriceRuleTypeEnum.GENERAL, TargetEnum.PRODUCT, output, ScaleTypeEnum.QUANTITY,
                OutComeModeEnum.FREQUENCY);

        PriceComponentRuleOutcomeOutput outputFreq = TestUtil.createOutcomeOutputSurchargePerc(new BigDecimal(12), ScaleTypeEnum.QUANTITY, OutComeModeEnum.FREQUENCY,
                new BigDecimal(10), Collections.emptyList());

        PriceRule rule1024 = TestUtil.createRuleOnlyProducts1024(products, false, TargetEnum.PRODUCT, PriceRuleTypeEnum.GENERAL,
                ScaleTypeEnum.QUANTITY, outputFreq, OutComeModeEnum.FREQUENCY);

        PriceComponentRuleOutcomeOutput outputSingle = TestUtil.createOutcomeOutputSurchargePerc(new BigDecimal(2), ScaleTypeEnum.QUANTITY, OutComeModeEnum.SINGLE,
                null, Collections.emptyList());

        PriceRule rule1024S = TestUtil.createRuleOnlyProducts1024(products, false, TargetEnum.PRODUCT, PriceRuleTypeEnum.GENERAL,
                ScaleTypeEnum.QUANTITY, outputSingle, OutComeModeEnum.SINGLE);

        rules.add(frequencyRule1023);
        rules.add(rule1024);
        rules.add(rule1024S);

        componentRule = PriceComponentRule.builder()
                .resolutionMode(PriceRuleResolutionModeEnum.BY_BEST_VALUE_SALE_ORDER)
                .rules(rules)
                .allowSetupLogger(true)
                .build();

        List<PriceComponentRuleInputSaleOrderProduct> results = componentRule.computate(saleOrderRamiro4448WithAllFactors);

        assertThat(results.get(0).getSurchargePercentage(), is(new BigDecimal(12)));
        assertThat(results.get(0).getRulesApplied().get(0).getId(), is(1024L));
        Log.info("RESULT: \n" + ToJsonString.toJson(results));
    }

    @Tag("production")
    @Test
    void testFrequencyFixedPriceSuccessWithOtherRules() {
        Log.info("Test Frequency [Fixed Price]: Rule 1023, 1024, 1024S. [Expected: 1024]");

        List<PriceComponentRuleSubjectProduct> products = new ArrayList<>(0);
        products.add(TestUtil.createProductRule48());

        PriceComponentRuleOutcomeOutput output = TestUtil.createOutcomeOutputFixedPrice(new BigDecimal(90), ScaleTypeEnum.QUANTITY, OutComeModeEnum.FREQUENCY,
                new BigDecimal(5), Collections.emptyList());

        PriceRule frequencyRule1023 = TestUtil.createRule1023RetailRamiroAllFactors(products, false, PriceRuleTypeEnum.GENERAL, TargetEnum.PRODUCT, output, ScaleTypeEnum.QUANTITY,
                OutComeModeEnum.FREQUENCY);

        PriceComponentRuleOutcomeOutput outputFreq = TestUtil.createOutcomeOutputFixedPrice(new BigDecimal(80), ScaleTypeEnum.QUANTITY, OutComeModeEnum.FREQUENCY,
                new BigDecimal(10), Collections.emptyList());

        PriceRule rule1024 = TestUtil.createRuleOnlyProducts1024(products, false, TargetEnum.PRODUCT, PriceRuleTypeEnum.GENERAL,
                ScaleTypeEnum.QUANTITY, outputFreq, OutComeModeEnum.FREQUENCY);

        PriceComponentRuleOutcomeOutput outputSingle = TestUtil.createOutcomeOutputFixedPrice(new BigDecimal(180), ScaleTypeEnum.QUANTITY, OutComeModeEnum.SINGLE,
                null, Collections.emptyList());

        PriceRule rule1024S = TestUtil.createRuleOnlyProducts1024(products, false, TargetEnum.PRODUCT, PriceRuleTypeEnum.GENERAL,
                ScaleTypeEnum.QUANTITY, outputSingle, OutComeModeEnum.SINGLE);

        rules.add(frequencyRule1023);
        rules.add(rule1024);
        rules.add(rule1024S);

        componentRule = PriceComponentRule.builder()
                .resolutionMode(PriceRuleResolutionModeEnum.BY_BEST_VALUE_SALE_ORDER)
                .rules(rules)
                .allowSetupLogger(true)
                .build();

        List<PriceComponentRuleInputSaleOrderProduct> results = componentRule.computate(saleOrderRamiro4448WithAllFactors);

        assertThat(results.get(0).getUnitPrice(), is(new BigDecimal(80)));
        assertThat(results.get(0).getRulesApplied().get(0).getId(), is(1024L));
        Log.info("RESULT: \n" + ToJsonString.toJson(results));
    }


    @Tag("production")
    @Test
    void testFrequencyBonusProductSuccessWithOtherRules() {
        Log.info("Test Frequency [BonusProduct]: Rule 1023, 1024 [Expected: 1023]");

        List<PriceComponentRuleSubjectProduct> products = new ArrayList<>(0);
        products.add(TestUtil.createProductRule48());

        List<PriceComponentRuleOutcomeProduct> bonusProducts = new ArrayList<>(0);
        bonusProducts.add(TestUtil.createProductBonus46(new BigDecimal(7)));

        List<PriceComponentRuleOutcomeProduct> bonusProducts2 = new ArrayList<>(0);
        bonusProducts2.add(TestUtil.createProductBonus47(BigDecimal.ONE));

        PriceComponentRuleOutcomeOutput output = TestUtil.createOutcomeOutputProductBonus(bonusProducts, ScaleTypeEnum.QUANTITY, OutComeModeEnum.FREQUENCY,
                new BigDecimal(5), Collections.emptyList());

        PriceRule frequencyRule1023 = TestUtil.createRule1023RetailRamiroAllFactors(products, false, PriceRuleTypeEnum.GENERAL, TargetEnum.PRODUCT, output, ScaleTypeEnum.QUANTITY,
                OutComeModeEnum.FREQUENCY);

        PriceComponentRuleOutcomeOutput outputFreq = TestUtil.createOutcomeOutputProductBonus(bonusProducts2, ScaleTypeEnum.QUANTITY, OutComeModeEnum.FREQUENCY,
                new BigDecimal(10), Collections.emptyList());

        PriceRule rule1024 = TestUtil.createRuleOnlyProducts1024(products, false, TargetEnum.PRODUCT, PriceRuleTypeEnum.GENERAL,
                ScaleTypeEnum.QUANTITY, outputFreq, OutComeModeEnum.FREQUENCY);

        rules.add(frequencyRule1023);
        rules.add(rule1024);

        componentRule = PriceComponentRule.builder()
                .resolutionMode(PriceRuleResolutionModeEnum.BY_BEST_VALUE_SALE_ORDER)
                .rules(rules)
                .allowSetupLogger(true)
                .build();

        List<PriceComponentRuleInputSaleOrderProduct> results = componentRule.computate(saleOrderRamiro4448WithAllFactors);

        assertThat(results.get(0).getRulesApplied().get(0).getId(), is(1023L));
        assertThat(results.get(1).getRulesApplied().get(0).getId(), is(1023L));
        assertThat(results.get(1).getId(), is(0L));
        assertThat(results.get(1).getUnitPrice(), is(new BigDecimal(15)));
        assertThat(results.get(1).getDiscountPercentage(), is(new BigDecimal(100)));
        assertThat(results.get(1).getTotalPriceGross(), is(new BigDecimal(210)));
        assertThat(results.get(1).getTotalDiscount(), is(new BigDecimal(210)));

        Log.info("RESULT: \n" + ToJsonString.toJson(results));
    }

    @Tag("production")
    @Test
    void testFrequencyBonusProductWithExclusiveOutcome() {
        Log.info("testFrequencyBonusProductWithExclusiveOutcome Test Frequency [BonusProduct]: Rule 1023, 1024 " +
                "[Expected: 1012, 1023]");

        List<PriceComponentRuleSubjectProduct> products = new ArrayList<>(0);
        products.add(TestUtil.createProductRule48());

//        Rule 1
        List<PriceComponentRuleOutcomeProduct> bonusProducts = new ArrayList<>(0);
        bonusProducts.add(TestUtil.createProductBonus46(new BigDecimal(7)));

        PriceComponentRuleOutcomeOutput outputFrequency1023 = TestUtil.createOutcomeOutputProductBonus(
                bonusProducts, ScaleTypeEnum.QUANTITY, OutComeModeEnum.FREQUENCY,
                new BigDecimal(5), Collections.emptyList()
        );

        PriceRule frequencyRule1023 = TestUtil.createRule1023RetailRamiroAllFactors(
                products, false, PriceRuleTypeEnum.GENERAL,
                TargetEnum.PRODUCT, outputFrequency1023, ScaleTypeEnum.QUANTITY,
                OutComeModeEnum.FREQUENCY
        );
        frequencyRule1023.setExclusiveOutcomeEnum(ExclusiveOutcomeEnum.OUTCOME_TYPE);


//        Rule 2
        List<PriceComponentRuleOutcomeProduct> bonusProducts2 = new ArrayList<>(0);
        bonusProducts2.add(TestUtil.createProductBonus47(BigDecimal.ONE));

        PriceComponentRuleOutcomeOutput outputFrequency1024 = TestUtil.createOutcomeOutputProductBonus(
                bonusProducts2, ScaleTypeEnum.QUANTITY, OutComeModeEnum.FREQUENCY,
                new BigDecimal(10), Collections.emptyList()
        );

        PriceRule frequencyRule1024 = TestUtil.createRuleOnlyProducts1024(
                products, false, TargetEnum.PRODUCT, PriceRuleTypeEnum.GENERAL,
                ScaleTypeEnum.QUANTITY, outputFrequency1024, OutComeModeEnum.FREQUENCY
        );

        frequencyRule1024.setExclusiveOutcomeEnum(ExclusiveOutcomeEnum.OUTCOME_TYPE);

//        Rule 3
        PriceComponentRuleOutcomeOutput outcomeDiscountPerc = TestUtil.createOutcomeOutputDiscountPerc(
                new BigDecimal(12), ScaleTypeEnum.AMOUNT
        );


        PriceRule singlePercentageDiscount = TestUtil.createRule1012RetailWithProduct4448(
                false, PriceRuleTypeEnum.RESTRICTED, TargetEnum.PRODUCT,
                outcomeDiscountPerc, ScaleTypeEnum.AMOUNT);

//        Rules
        rules.add(frequencyRule1023);
        rules.add(frequencyRule1024);
        rules.add(singlePercentageDiscount);

        componentRule = PriceComponentRule.builder()
                .resolutionMode(PriceRuleResolutionModeEnum.BY_BEST_VALUE_PRODUCT)
                .rules(rules)
                .allowSetupLogger(true)
                .build();

        List<PriceComponentRuleInputSaleOrderProduct> results = componentRule.computate(saleOrderRamiro4448WithAllFactors);
        assertThat(results, hasSize(3));

        List<Long> ruleIds = Arrays.asList(
                results.get(0).getRulesApplied().get(0).getId(),
                results.get(1).getRulesApplied().get(0).getId(),
                results.get(2).getRulesApplied().get(0).getId()
        );
        assertThat(ruleIds, hasItems(1012L, 1023L));

//        Check the exclusive Bonus Rule has not discount
        results.stream().filter(saleOrderProduct -> saleOrderProduct.getId() != 0L
                        && saleOrderProduct.getDiscountPercentage().compareTo(BigDecimal.ZERO) == 0)
                .forEach(saleOrderProduct -> {
                    assertThat(saleOrderProduct.getRulesApplied(), hasSize(1));
                    assertThat(saleOrderProduct.getRulesApplied().get(0).getId(), is(1023L));
                });

//        Check Product Bonus from Exclusive Bonus Rule
        results.stream().filter(saleOrderProduct -> saleOrderProduct.getId() == 0L).forEach(saleOrderProduct -> {
            assertThat(saleOrderProduct.getId(), is(0L));
            assertThat(saleOrderProduct.getUnitPrice(), is(new BigDecimal(15)));
            assertThat(saleOrderProduct.getDiscountPercentage(), is(new BigDecimal(100)));
            assertThat(saleOrderProduct.getTotalPriceGross(), is(new BigDecimal(210)));
            assertThat(saleOrderProduct.getTotalDiscount(), is(new BigDecimal(210)));
        });

        Log.info("RESULT: \n" + ToJsonString.toJson(results));
    }

    @Tag("production")
    @Test
    void testFrequencyBonusUsingSaleOrderTotalQuantityProductToSelectRule() {
        Log.info("testFrequencyBonusUsingSaleOrderTotalQuantityProductToSelectRule Test Frequency [BonusProduct]: " +
                "Rule 70, 71 " +
                "[Expected: 71]");

        List<PriceComponentRuleSubjectProduct> products = new ArrayList<>(0);
        products.add(TestUtil.createProductRule70());
        products.add(TestUtil.createProductRule71());
        products.add(TestUtil.createProductRule72());

//        Rule 1
        List<PriceComponentRuleOutcomeProduct> bonusProducts1 = new ArrayList<>(0);
        bonusProducts1.add(TestUtil.createProductBonus70(new BigDecimal(1)));

        PriceComponentRuleOutcomeOutput outputFrequency70 = TestUtil.createOutcomeOutputProductBonus(
                bonusProducts1, ScaleTypeEnum.QUANTITY, OutComeModeEnum.FREQUENCY,
                new BigDecimal(2), Collections.emptyList()
        );

        PriceRule frequencyRule70 = TestUtil.createRuleOnlyProducts(
                70, "Regla 70", products, false, TargetEnum.PRODUCT, PriceRuleTypeEnum.GENERAL, ScaleTypeEnum.QUANTITY,
                outputFrequency70, OutComeModeEnum.FREQUENCY
        );

        frequencyRule70.setExclusiveOutcomeEnum(ExclusiveOutcomeEnum.OUTCOME_TYPE);


//        Rule 2
        List<PriceComponentRuleOutcomeProduct> bonusProducts2 = new ArrayList<>(0);
        bonusProducts2.add(TestUtil.createProductBonus71(new BigDecimal(1)));

        PriceComponentRuleOutcomeOutput outputFrequency71 = TestUtil.createOutcomeOutputProductBonus(
                bonusProducts2, ScaleTypeEnum.QUANTITY, OutComeModeEnum.FREQUENCY,
                new BigDecimal(7), Collections.emptyList()
        );

        PriceRule frequencyRule71 = TestUtil.createRuleOnlyProducts(
                71, "Regla 71", products, false, TargetEnum.PRODUCT, PriceRuleTypeEnum.GENERAL, ScaleTypeEnum.QUANTITY,
                outputFrequency71, OutComeModeEnum.FREQUENCY
        );

        frequencyRule71.setExclusiveOutcomeEnum(ExclusiveOutcomeEnum.OUTCOME_TYPE);

//        Rules
        rules.add(frequencyRule70);
        rules.add(frequencyRule71);

        componentRule = PriceComponentRule.builder()
                .resolutionMode(PriceRuleResolutionModeEnum.BY_BEST_VALUE_PRODUCT)
                .rules(rules)
                .allowSetupLogger(true)
                .build();

        List<PriceComponentRuleInputSaleOrderProduct> results = componentRule.computate(TestUtil.createSaleOrderArrieroLtda707172WithAllFactors(DeliveryModeEnum.MOBILE_STORE, PaymentConditionsTypeEnum.CASH));
        assertThat(results, hasSize(4));

        List<Long> ruleIds = Arrays.asList(
                results.get(0).getRulesApplied().get(0).getId(),
                results.get(1).getRulesApplied().get(0).getId(),
                results.get(2).getRulesApplied().get(0).getId(),
                results.get(3).getRulesApplied().get(0).getId()
        );
        assertThat(ruleIds, hasItems(71L));

//        Check Product Bonus from Exclusive Bonus Rule
        results.stream().filter(saleOrderProduct -> saleOrderProduct.getId() == 0L).forEach(saleOrderProduct -> {
            assertThat(saleOrderProduct.getId(), is(0L));
            assertThat(saleOrderProduct.getProduct().getCode(), is("300868"));
            assertThat(saleOrderProduct.getQty(), is(new BigDecimal(1)));
            assertThat(saleOrderProduct.getUnitPrice(), is(new BigDecimal(48)));
            assertThat(saleOrderProduct.getTotalPriceGross(), is(new BigDecimal(48)));
            assertThat(saleOrderProduct.getDiscountPercentage(), is(new BigDecimal(100)));
            assertThat(saleOrderProduct.getTotalDiscount(), is(new BigDecimal(48)));
            assertThat(saleOrderProduct.getTotalPrice(), is(new BigDecimal(48)));
        });

        Log.info("RESULT: \n" + ToJsonString.toJson(results));
    }

    @Tag("production")
    @Test
    void testFrequencyBonusDisplayVsCaja() {
        Log.info("testFrequencyBonusDisplayVsCaja Test Frequency [BonusProduct]: " +
                "Rule 3001070, 3001071 " +
                "[Expected: 3001071]");

        List<PriceComponentRuleSubjectProduct> products = new ArrayList<>(0);
        products.add(TestUtil.createProductRule300175());

//        Rule 1
        List<PriceComponentRuleOutcomeProduct> bonusProducts1 = new ArrayList<>(0);
        bonusProducts1.add(TestUtil.createProductBonus300107Display(new BigDecimal(1)));

        PriceComponentRuleOutcomeOutput outputFrequency300107Display = TestUtil.createOutcomeOutputProductBonus(
                bonusProducts1, ScaleTypeEnum.QUANTITY, OutComeModeEnum.FREQUENCY,
                new BigDecimal(24), Collections.emptyList()
        );

        PriceRule frequencyRule300107Display = TestUtil.createRuleOnlyProducts(
                3001070, "Regla Frec. 24/1 display", products, false, TargetEnum.PRODUCT, PriceRuleTypeEnum.GENERAL,
                ScaleTypeEnum.QUANTITY,
                outputFrequency300107Display, OutComeModeEnum.FREQUENCY
        );

        frequencyRule300107Display.setExclusiveOutcomeEnum(ExclusiveOutcomeEnum.OUTCOME_TYPE);


//        Rule 2
        List<PriceComponentRuleOutcomeProduct> bonusProducts2 = new ArrayList<>(0);
        bonusProducts2.add(TestUtil.createProductBonus300107Caja(new BigDecimal(1)));

        PriceComponentRuleOutcomeOutput outputFrequency300107Caja = TestUtil.createOutcomeOutputProductBonus(
                bonusProducts2, ScaleTypeEnum.QUANTITY, OutComeModeEnum.FREQUENCY,
                new BigDecimal(96), Collections.emptyList()
        );

        PriceRule frequencyRule300107Caja = TestUtil.createRuleOnlyProducts(
                3001071, "Regla Frec. 96/1 Caja", products, false, TargetEnum.PRODUCT, PriceRuleTypeEnum.GENERAL,
                ScaleTypeEnum.QUANTITY,
                outputFrequency300107Caja, OutComeModeEnum.FREQUENCY
        );

        frequencyRule300107Caja.setExclusiveOutcomeEnum(ExclusiveOutcomeEnum.OUTCOME_TYPE);

//        Rules
        rules.add(frequencyRule300107Display);
        rules.add(frequencyRule300107Caja);

        componentRule = PriceComponentRule.builder()
                .resolutionMode(PriceRuleResolutionModeEnum.BY_BEST_VALUE_PRODUCT)
                .rules(rules)
                .allowSetupLogger(true)
                .build();

        List<PriceComponentRuleInputSaleOrderProduct> results = componentRule.computate(TestUtil.createSaleOrderAnais2110142229AllFactors(DeliveryModeEnum.MOBILE_STORE, PaymentConditionsTypeEnum.CASH));
        assertThat(results, hasSize(2));

        List<Long> ruleIds = Arrays.asList(
                results.get(0).getRulesApplied().get(0).getId(),
                results.get(1).getRulesApplied().get(0).getId()
        );
        assertThat(ruleIds, hasItems(3001071L));

//        Check Product Bonus from Exclusive Bonus Rule
        results.stream().filter(saleOrderProduct -> saleOrderProduct.getId() == 0L).forEach(saleOrderProduct -> {
            assertThat(saleOrderProduct.getId(), is(0L));
            assertThat(saleOrderProduct.getProduct().getCode(), is("300107"));
            assertThat(saleOrderProduct.getQty(), is(new BigDecimal(1)));
            assertThat(saleOrderProduct.getUnitPrice(), is(new BigDecimal(224)));
            assertThat(saleOrderProduct.getTotalPriceGross(), is(new BigDecimal(224)));
            assertThat(saleOrderProduct.getDiscountPercentage(), is(new BigDecimal(100)));
            assertThat(saleOrderProduct.getTotalDiscount(), is(new BigDecimal(224)));
            assertThat(saleOrderProduct.getTotalPrice(), is(new BigDecimal(224)));
        });

        Log.info("RESULT: \n" + ToJsonString.toJson(results));
    }


    @Tag("production")
    @Test
    void testFrequencySurchargeProductSuccessWithOtherRules() {
        Log.info("Test Frequency [Surcharge Product]: Rule 1023, 1024 [Expected: 1024]");

        List<PriceComponentRuleSubjectProduct> products = new ArrayList<>(0);
        products.add(TestUtil.createProductRule48());

        List<PriceComponentRuleOutcomeProduct> surchargeProducts1023 = new ArrayList<>(0);
        surchargeProducts1023.add(TestUtil.createProductBonus46(BigDecimal.TEN));

        List<PriceComponentRuleOutcomeProduct> surchargeProducts1024 = new ArrayList<>(0);
        surchargeProducts1024.add(TestUtil.createProductBonus47(new BigDecimal(2)));

        PriceComponentRuleOutcomeOutput output = TestUtil.createOutcomeOutputProductSurcharge(surchargeProducts1023, ScaleTypeEnum.QUANTITY, OutComeModeEnum.FREQUENCY,
                new BigDecimal(5), Collections.emptyList());

        PriceRule frequencyRule1023 = TestUtil.createRule1023RetailRamiroAllFactors(products, false, PriceRuleTypeEnum.GENERAL, TargetEnum.PRODUCT, output, ScaleTypeEnum.QUANTITY,
                OutComeModeEnum.FREQUENCY);

        PriceComponentRuleOutcomeOutput outputFreq = TestUtil.createOutcomeOutputProductSurcharge(surchargeProducts1024, ScaleTypeEnum.QUANTITY, OutComeModeEnum.FREQUENCY,
                new BigDecimal(10), Collections.emptyList());

        PriceRule rule1024 = TestUtil.createRuleOnlyProducts1024(products, false, TargetEnum.PRODUCT, PriceRuleTypeEnum.GENERAL,
                ScaleTypeEnum.QUANTITY, outputFreq, OutComeModeEnum.FREQUENCY);

        rules.add(frequencyRule1023);
        rules.add(rule1024);


        componentRule = PriceComponentRule.builder()
                .resolutionMode(PriceRuleResolutionModeEnum.BY_BEST_VALUE_SALE_ORDER)
                .rules(rules)
                .allowSetupLogger(true)
                .build();

        List<PriceComponentRuleInputSaleOrderProduct> results = componentRule.computate(saleOrderRamiro4448WithAllFactors);

        assertThat(results.get(0).getRulesApplied().get(0).getId(), is(1024L));
        assertThat(results.get(1).getRulesApplied().get(0).getId(), is(1024L));
        assertThat(results.get(1).getId(), is(-1L));
        assertThat(results.get(1).getUnitPrice(), is(new BigDecimal(200)));
        assertThat(results.get(1).getQty(), is(new BigDecimal(2)));
        assertThat(results.get(1).getDiscountPercentage(), is(BigDecimal.ZERO));
        assertThat(results.get(1).getTotalPriceGross(), is(new BigDecimal(400)));
        assertThat(results.get(1).getTotalDiscount(), is(BigDecimal.ZERO));

        Log.info("RESULT: \n" + ToJsonString.toJson(results));
    }

    @Tag("production")
    @Test
    void testFrequencyDsctPerc() {
        Log.info("Test Frequency [Discount Percentage]: Rule 1017, 1023, 1004[S], 1024. [Expected: 1024]");

        List<PriceComponentRuleSubjectProduct> products = new ArrayList<>(0);
        products.add(TestUtil.createProductRule48());

        PriceRule frequencyRule1017 = TestUtil.createRuleOnlyProductFrequencyOutput1017(products, ScaleTypeEnum.QUANTITY, new BigDecimal(2), false, TargetEnum.PRODUCT, PriceRuleTypeEnum.GENERAL);

        PriceComponentRuleOutcomeOutput output = TestUtil.createOutcomeOutputDiscountPercFrequency(new BigDecimal(12), ScaleTypeEnum.QUANTITY, new BigDecimal(5));
        PriceRule frequencyRule1023 = TestUtil.createRule1023RetailRamiroAllFactors(products, false, PriceRuleTypeEnum.GENERAL, TargetEnum.PRODUCT, output, ScaleTypeEnum.QUANTITY,
                OutComeModeEnum.FREQUENCY);

        PriceRule rule1004With10PercDsct = TestUtil.createRuleOnlyBusinessUnit1004(Collections.singletonList(TestUtil.createBusinessUnitRetail()), false, TargetEnum.PRODUCT, PriceRuleTypeEnum.GENERAL);

        PriceComponentRuleOutcomeOutput outputFreq = TestUtil.createOutcomeOutputDiscountPerc(new BigDecimal(51), ScaleTypeEnum.QUANTITY, OutComeModeEnum.FREQUENCY,
                new BigDecimal(10), Collections.emptyList());

        PriceRule rule1024 = TestUtil.createRuleOnlyProducts1024(products, false, TargetEnum.PRODUCT, PriceRuleTypeEnum.GENERAL,
                ScaleTypeEnum.QUANTITY, outputFreq, OutComeModeEnum.FREQUENCY);

        rules.add(frequencyRule1017);
        rules.add(frequencyRule1023);
        rules.add(rule1004With10PercDsct);
        rules.add(rule1024);

        componentRule = PriceComponentRule.builder()
                .resolutionMode(PriceRuleResolutionModeEnum.BY_BEST_VALUE_SALE_ORDER)
                .rules(rules)
                .allowSetupLogger(true)
                .build();

        List<PriceComponentRuleInputSaleOrderProduct> results = componentRule.computate(saleOrderRamiro4448WithAllFactors);

        assertThat(results.get(0).getDiscountPercentage(), is(new BigDecimal(51)));
        assertThat(results.get(0).getRulesApplied().get(0).getId(), is(1024L));

        Log.info("RESULT: \n" + ToJsonString.toJson(results));
    }


    @Tag("production")
    @Test
    void testFrequencyDsctPercP() {
        Log.info("Test Frequency [Discount Percentage]: Rule 1023, 1024. [Expected: 1024]");

        List<PriceComponentRuleSubjectProduct> products = new ArrayList<>(0);
        products.add(TestUtil.createProductRule48());

        PriceComponentRuleOutcomeOutput output = TestUtil.createOutcomeOutputDiscountPercFrequency(new BigDecimal(2), ScaleTypeEnum.QUANTITY, new BigDecimal(2));
        PriceRule frequencyRule1023 = TestUtil.createRule1023RetailRamiroAllFactors(products, false, PriceRuleTypeEnum.GENERAL, TargetEnum.PRODUCT, output, ScaleTypeEnum.QUANTITY,
                OutComeModeEnum.FREQUENCY);

        PriceComponentRuleOutcomeOutput outputFreq = TestUtil.createOutcomeOutputDiscountPerc(new BigDecimal(5), ScaleTypeEnum.QUANTITY, OutComeModeEnum.FREQUENCY,
                new BigDecimal(10), Collections.emptyList());

        PriceRule rule1024 = TestUtil.createRuleOnlyProducts1024(products, false, TargetEnum.PRODUCT, PriceRuleTypeEnum.GENERAL,
                ScaleTypeEnum.QUANTITY, outputFreq, OutComeModeEnum.FREQUENCY);

        rules.add(frequencyRule1023);
        rules.add(rule1024);

        componentRule = PriceComponentRule.builder()
                .resolutionMode(PriceRuleResolutionModeEnum.BY_BEST_VALUE_SALE_ORDER)
                .rules(rules)
                .allowSetupLogger(true)
                .build();
        saleOrderRamiro4448WithAllFactors.getProductDetails().get(1).getProduct().setOriginalPrice(new BigDecimal(250.0000));
        saleOrderRamiro4448WithAllFactors.getProductDetails().get(1).setUnitPrice(new BigDecimal(250.0000));
        saleOrderRamiro4448WithAllFactors.getProductDetails().get(1).setTotalPriceGross(new BigDecimal(2500.0000));
        saleOrderRamiro4448WithAllFactors.getProductDetails().get(1).setTotalPrice(new BigDecimal(2500.0000));

        List<PriceComponentRuleInputSaleOrderProduct> results = componentRule.computate(saleOrderRamiro4448WithAllFactors);

        assertThat(results.get(0).getRulesApplied().get(0).getId(), is(1023L));
        Log.info("RESULT: \n" + ToJsonString.toJson(results));
    }

    @Tag("production")
    @Test
    void testFrequencySurchargeAmount() {
        Log.info("Test Frequency [Surcharge Amount]: Rule 1024. [Expected: 1024]");

        List<PriceComponentRuleSubjectProduct> products = new ArrayList<>(0);
        products.add(TestUtil.createProductRule48());

        PriceComponentRuleOutcomeOutput output = TestUtil.createOutcomeOutputSurchargeAmount(new BigDecimal(5.00), ScaleTypeEnum.QUANTITY, OutComeModeEnum.FREQUENCY, new BigDecimal(5), null);

        PriceRule rule1024 = TestUtil.createRuleOnlyProducts1024(products, false, TargetEnum.PRODUCT, PriceRuleTypeEnum.GENERAL,
                ScaleTypeEnum.QUANTITY, output, OutComeModeEnum.FREQUENCY);

        rules.add(rule1024);

        componentRule = PriceComponentRule.builder()
                .resolutionMode(PriceRuleResolutionModeEnum.BY_BEST_VALUE_SALE_ORDER)
                .rules(rules)
                .allowSetupLogger(true)
                .build();
        saleOrderRamiro4448WithAllFactors.getProductDetails().get(1).getProduct().setOriginalPrice(new BigDecimal(290.0000));
        saleOrderRamiro4448WithAllFactors.getProductDetails().get(1).setUnitPrice(new BigDecimal(290.0000));
        saleOrderRamiro4448WithAllFactors.getProductDetails().get(1).setTotalPriceGross(new BigDecimal(2900.0000));
        saleOrderRamiro4448WithAllFactors.getProductDetails().get(1).setTotalPrice(new BigDecimal(2900.0000));

        List<PriceComponentRuleInputSaleOrderProduct> results = componentRule.computate(saleOrderRamiro4448WithAllFactors);

        assertThat(results.get(0).getRulesApplied().get(0).getId(), is(1024L));
        Log.info("RESULT: \n" + ToJsonString.toJson(results));
    }

    @Tag("production")
    @Test
    void testFrequencyProductFeatureSurchargeAmount() {
        Log.info("Test Frequency Product Features [Surcharge Amount]: Rule 1025. [Expected: 1025]");

        List<PriceComponentRuleSubjectProductFeature> products = new ArrayList<>(0);
        products.add(TestUtil.createProductFeatureBrandHollister());

        PriceComponentRuleOutcomeOutput output = TestUtil.createOutcomeOutputSurchargeAmount(new BigDecimal(5.00), ScaleTypeEnum.QUANTITY, OutComeModeEnum.FREQUENCY, new BigDecimal(5), null);

        PriceRule rule1024 = TestUtil.createRuleOnlyProductFeatures(1025, products, false, TargetEnum.PRODUCT, PriceRuleTypeEnum.RESTRICTED,
                ScaleTypeEnum.QUANTITY, output, OutComeModeEnum.FREQUENCY);

        rules.add(rule1024);

        componentRule = PriceComponentRule.builder()
                .resolutionMode(PriceRuleResolutionModeEnum.BY_BEST_VALUE_SALE_ORDER)
                .rules(rules)
                .allowSetupLogger(true)
                .build();

        saleOrderRamiro4448WithAllFactors.getProductDetails().get(1).getProduct().setOriginalPrice(new BigDecimal(290.0000));
        saleOrderRamiro4448WithAllFactors.getProductDetails().get(1).setUnitPrice(new BigDecimal(290.0000));
        saleOrderRamiro4448WithAllFactors.getProductDetails().get(1).setTotalPriceGross(new BigDecimal(2900.0000));
        saleOrderRamiro4448WithAllFactors.getProductDetails().get(1).setTotalPrice(new BigDecimal(2900.0000));

        List<PriceComponentRuleInputSaleOrderProduct> results = componentRule.computate(saleOrderRamiro4448WithAllFactors);

        assertThat(results.get(0).getRulesApplied().get(0).getId(), is(1025L));
        Log.info("RESULT: \n" + ToJsonString.toJson(results));
    }

    @Tag("production")
    @Test
    void testFrequencyProductClassificationSurchargeAmountFail() {
        Log.info("Test Frequency Product Classification [Surcharge Amount]: Rule 1026. [Expected: Empty ]");

        List<PriceComponentRuleFactorProductClassification> products = new ArrayList<>(0);
        products.add(TestUtil.createProductClassificationQuirofano());

        PriceComponentRuleOutcomeOutput output = TestUtil.createOutcomeOutputSurchargeAmount(new BigDecimal(10.00), ScaleTypeEnum.QUANTITY, OutComeModeEnum.FREQUENCY, new BigDecimal(5), null);

        PriceRule rule1026 = TestUtil.createRuleOnlyProductClassifications(1026, products, false, TargetEnum.PRODUCT, PriceRuleTypeEnum.GENERAL,
                ScaleTypeEnum.QUANTITY, output, OutComeModeEnum.FREQUENCY);

        rules.add(rule1026);

        componentRule = PriceComponentRule.builder()
                .resolutionMode(PriceRuleResolutionModeEnum.BY_BEST_VALUE_SALE_ORDER)
                .rules(rules)
                .allowSetupLogger(true)
                .build();

        List<PriceComponentRuleInputSaleOrderProduct> results = componentRule.computate(saleOrderRamiro4448WithAllFactors);

        assertThat(results, is(Collections.emptyList()));
        Log.info("RESULT: \n" + ToJsonString.toJson(results));
    }

    @Tag("production")
    @Test
    void testFrequencyProductClassificationBonification() {
        Log.info("Test Frequency Product Classification [Bonification]: Rule 1026. [Expected: Empty ]");

        List<PriceComponentRuleFactorProductClassification> products = new ArrayList<>(0);
        products.add(TestUtil.createProductClassificationCaboPBisturi());

        //region products bonifications
        PriceComponentRuleOutcomeProduct bonus1 = TestUtil.createProductBonus47(BigDecimal.ONE);
        PriceComponentRuleOutcomeProduct bonus2 = TestUtil.createProductBonus46(BigDecimal.ONE);
        List<PriceComponentRuleOutcomeProduct> bonus = new ArrayList<>(0);
        bonus.add(bonus1);
        bonus.add(bonus2);
        //endregion

        PriceComponentRuleOutcomeOutput output = TestUtil
                .createOutcomeOutputProductBonus(bonus, ScaleTypeEnum.AMOUNT, OutComeModeEnum.FREQUENCY, new BigDecimal(5), null);

        PriceRule rule1026 = TestUtil.createRuleOnlyProductClassifications(1026, products, false, TargetEnum.PRODUCT, PriceRuleTypeEnum.GENERAL,
                ScaleTypeEnum.QUANTITY, output, OutComeModeEnum.FREQUENCY);

        rules.add(rule1026);

        componentRule = PriceComponentRule.builder()
                .resolutionMode(PriceRuleResolutionModeEnum.BY_BEST_VALUE_SALE_ORDER)
                .rules(rules)
                .allowSetupLogger(true)
                .build();

        PriceComponentRuleInputSaleOrder saleOrderRamiro44With10 = TestUtil.createSaleOrderRamiro48();

        List<PriceComponentRuleInputSaleOrderProduct> results = componentRule.computate(saleOrderRamiro44With10);

        PriceComponentRuleInputSaleOrderProduct product46 = results.stream().filter(product -> product.getProduct().getId() == 46).findFirst().orElse(null);
        PriceComponentRuleInputSaleOrderProduct product47 = results.stream().filter(product -> product.getProduct().getId() == 47).findFirst().orElse(null);
        PriceComponentRuleInputSaleOrderProduct product48 = results.stream().filter(product -> product.getProduct().getId() == 48).findFirst().orElse(null);

        Log.info("RESULT: \n" + ToJsonString.toJson(results));

        assertThat(results, hasItem(product46));
        assertThat(results, hasItem(product47));
        assertThat(results, hasItem(product48));

        List<PriceComponentRuleInputSaleOrderProduct> resultsFail = componentRule.computate(saleOrderRamiro4448WithAllFactors);

        assertThat(resultsFail, is(Collections.emptyList()));
    }
}
