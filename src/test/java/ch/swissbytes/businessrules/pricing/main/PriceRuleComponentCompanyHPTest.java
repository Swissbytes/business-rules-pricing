package ch.swissbytes.businessrules.pricing.main;

import ch.swissbytes.businessrules.pricing.enums.*;
import ch.swissbytes.businessrules.pricing.inputs.PriceComponentRuleInputSaleOrder;
import ch.swissbytes.businessrules.pricing.inputs.PriceComponentRuleInputSaleOrderProduct;
import ch.swissbytes.businessrules.pricing.log.Logger;
import ch.swissbytes.businessrules.pricing.log.LoggerImpl;
import ch.swissbytes.businessrules.pricing.models.PriceComponentRule;
import ch.swissbytes.businessrules.pricing.models.PriceRule;
import ch.swissbytes.businessrules.pricing.utils.HPTestUtil;
import ch.swissbytes.businessrules.pricing.utils.TestUtil;
import ch.swissbytes.businessrules.pricing.utils.ToJsonString;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

/**
 * Price Rule Component Tests based on HP Medical requirements.
 * These test will assure that all requirement rules are working.
 */
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class PriceRuleComponentCompanyHPTest {

    private static final Logger Log = LoggerImpl.getLogger();

    //region Rules
    private PriceRule ruleOstomizadoRetailCustomers;
    private PriceRule rulePublicCreditSale;
    private PriceRule ruleDistribuidoresExclusivos;
    private PriceRule rulePrivadasIncor;
    private PriceRule rulePrivadasFioanini;
    //endregion

    private PriceComponentRule componentRule;

    @BeforeAll
    void setUp() {
        Log.info("Begin: Test HP Company Rules...");
        List<PriceRule> rules = new ArrayList<>();

        ruleOstomizadoRetailCustomers = HPTestUtil.createRuleOstomizadoRetailCustomers(1, PriceRuleTypeEnum.RESTRICTED, ScaleTypeEnum.AMOUNT, OutComeModeEnum.SINGLE);
        rules.add(ruleOstomizadoRetailCustomers);

        rulePublicCreditSale = HPTestUtil.createRulePublicCreditSale(2);
        rules.add(rulePublicCreditSale);

        ruleDistribuidoresExclusivos = HPTestUtil.createRuleDistribuidoresExclusivos(3);
        rules.add(ruleDistribuidoresExclusivos);

        rulePrivadasIncor = HPTestUtil.createRulePrivadasIncor(4);
        rules.add(rulePrivadasIncor);

        rulePrivadasFioanini = HPTestUtil.createRulePrivadasFioanini(5);
        rules.add(rulePrivadasFioanini);

        componentRule = PriceComponentRule.builder()
                .resolutionMode(PriceRuleResolutionModeEnum.BY_BEST_VALUE_PRODUCT)
                .rules(rules)
                .build();
    }

    /**
     * Business Unit: Retail
     * Products Classifications: Only ostomy products
     * Product Features: Brand -> Hollister
     * Party Classification: Oztomizados
     * Outcome: Discount Percentage -> 10%
     */
    @Tag("production")
    @Test
    void testOztomizadoRetailCustomers() {
        Log.info("Executing Test -> Rule:\n" +
                " Business Unit: Retail\n" +
                " Products Classifications: Only ostomy products\n" +
                " Product Features: Brand -> Hollister\n" +
                " Party Classification: Oztomizados\n" +
                " Outcome: Discount Percentage -> 10%");
        PriceComponentRuleInputSaleOrder saleOrderSuccess = HPTestUtil.createSaleOrder(
                HPTestUtil.createRandomCustomer(),
                HPTestUtil.createBusinessUnitRetail(),
                TestUtil.createAgencySC(),
                HPTestUtil.toSaleOrderProductsList(
                        HPTestUtil.createSaleOrderProductBolsaColostomiaUnidad(),
                        TestUtil.createSaleOrderProduct47All(),
                        TestUtil.createSaleOrderProduct48WithAll()),
                PaymentConditionsTypeEnum.CASH,
                DeliveryModeEnum.MOBILE_STORE);

        PriceComponentRuleInputSaleOrder failOrder = TestUtil.createSaleOrderRamiro4448WithAllFactors(DeliveryModeEnum.MOBILE_STORE, PaymentConditionsTypeEnum.CASH);

        List<PriceComponentRuleInputSaleOrderProduct> results = componentRule.computate(saleOrderSuccess);

        PriceComponentRuleInputSaleOrderProduct product83 = results.stream().filter(product -> product.getProduct().getId() == 83).findFirst().orElse(null);

        Log.warn("Results: \n" + ToJsonString.toJson(results));

        PriceComponentRuleInputSaleOrderProduct product47 = results.stream().filter(product -> product.getProduct().getId() == 47).findFirst().orElse(null);
        PriceComponentRuleInputSaleOrderProduct product48 = results.stream().filter(product -> product.getProduct().getId() == 48).findFirst().orElse(null);

        if (Objects.nonNull(product47))
            fail("Product 47 does exists");
        if (Objects.nonNull(product48))
            fail("Product 48 does exists");
        if (Objects.isNull(product83))
            fail("Product 83 does not exists");
        assertEquals(product83.getDiscountPercentage(), new BigDecimal(10));

        results = componentRule.computate(failOrder);
        assertEquals(results.isEmpty(), Boolean.TRUE);
    }


    /**
     * Business Unit: "Instituciones Publicas"
     * Payment Condition: Credit
     * Outcome: Discount Percentage -> 15%
     */
    @Tag("production")
    @Test
    void testPublicCreditSale() {
        PriceComponentRuleInputSaleOrder saleOrderSuccess = HPTestUtil.createSaleOrder(
                HPTestUtil.createRandomCustomer(),
                HPTestUtil.createBusinessUnitPublicas(),
                TestUtil.createAgencySC(),
                HPTestUtil.toSaleOrderProductsList(
                        HPTestUtil.createSaleOrderProductBolsaColostomiaUnidad(),
                        TestUtil.createSaleOrderProduct47All(),
                        TestUtil.createSaleOrderProduct48WithAll()),
                PaymentConditionsTypeEnum.CASH,
                DeliveryModeEnum.MOBILE_STORE);

        PriceComponentRuleInputSaleOrder failOrder = TestUtil.createSaleOrderRamiro4448WithAllFactors(DeliveryModeEnum.MOBILE_STORE, PaymentConditionsTypeEnum.CASH);

        List<PriceComponentRuleInputSaleOrderProduct> results = componentRule.computate(saleOrderSuccess);

        PriceComponentRuleInputSaleOrderProduct product83 = results.stream().filter(product -> product.getProduct().getId() == 83).findFirst().orElse(null);

        Log.warn("Results: \n" + ToJsonString.toJson(results));

        PriceComponentRuleInputSaleOrderProduct product47 = results.stream().filter(product -> product.getProduct().getId() == 47).findFirst().orElse(null);
        PriceComponentRuleInputSaleOrderProduct product48 = results.stream().filter(product -> product.getProduct().getId() == 48).findFirst().orElse(null);

        if (Objects.isNull(product47))
            fail("Product 47 does not exists");
        if (Objects.isNull(product48))
            fail("Product 48 does not exists");
        if (Objects.isNull(product83))
            fail("Product 83 does not exists");
        assertEquals(product47.getDiscountPercentage(), new BigDecimal(15));
        assertEquals(product48.getDiscountPercentage(), new BigDecimal(15));
        assertEquals(product83.getDiscountPercentage(), new BigDecimal(15));

        results = componentRule.computate(failOrder);
        assertEquals(results.isEmpty(), Boolean.TRUE);
    }


    /**
     * Business Unit: "Distribuidores"
     * Party Classification: "Distribuidor Exclusivo" / "Distribuidor Autorizado"
     * FromAmount: 0
     * ThruAmount: 1'500.00 $
     * Outcome: Discount Percentage -> 10%
     */
    @Tag("production")
    @Test
    void testDistribuidoresExclusivos() {
        PriceComponentRuleInputSaleOrder saleOrderSuccess = HPTestUtil.createSaleOrder(
                HPTestUtil.createRandomCustomer(),
                HPTestUtil.createBusinessUnitDistribuidores(),
                TestUtil.createAgencySC(),
                HPTestUtil.toSaleOrderProductsList(
                        HPTestUtil.createSaleOrderProductBolsaColostomiaUnidad(),
                        TestUtil.createSaleOrderProduct47All(),
                        TestUtil.createSaleOrderProduct48WithAll()),
                PaymentConditionsTypeEnum.CASH,
                DeliveryModeEnum.MOBILE_STORE);

        PriceComponentRuleInputSaleOrder failOrder = TestUtil.createSaleOrderRamiro4448WithAllFactors(DeliveryModeEnum.MOBILE_STORE, PaymentConditionsTypeEnum.CASH);

        List<PriceComponentRuleInputSaleOrderProduct> results = componentRule.computate(saleOrderSuccess);

        PriceComponentRuleInputSaleOrderProduct product83 = results.stream().filter(product -> product.getProduct().getId() == 83).findFirst().orElse(null);

        Log.warn("Results: \n" + ToJsonString.toJson(results));

        PriceComponentRuleInputSaleOrderProduct product47 = results.stream().filter(product -> product.getProduct().getId() == 47).findFirst().orElse(null);
        PriceComponentRuleInputSaleOrderProduct product48 = results.stream().filter(product -> product.getProduct().getId() == 48).findFirst().orElse(null);

        if (Objects.isNull(product47))
            fail("Product 47 does not exists");
        if (Objects.isNull(product48))
            fail("Product 48 does not exists");
        if (Objects.isNull(product83))
            fail("Product 83 does not exists");
        assertEquals(product47.getDiscountPercentage(), new BigDecimal(10));
        assertEquals(product48.getDiscountPercentage(), new BigDecimal(10));
        assertEquals(product83.getDiscountPercentage(), new BigDecimal(10));

        results = componentRule.computate(failOrder);
        assertEquals(results.isEmpty(), Boolean.TRUE);
    }

    /**
     * Business Unit: "Distribuidores"
     * Customer: "Distribuidora San Lucas"
     * FromAmount: 1'500.00 $
     * Outcome: Discount Percentage -> 10%
     */
    @Tag("production")
    @Test
    void testDistribuidoresSanLucas() {

    }

    /**
     * Business Unit: "Distribuidores"
     * Customer: "Farmacorp"
     * Party Classification: "Red de Farmacias"
     * ThruAmount: 1'500.00 $
     * FromQty: 1000
     * ThruQty: 0
     * Outcome: Discount Percentage -> 10%
     */
    @Tag("production")
    @Test
    void testDistribuidoresFarmacorp() {

    }

    /**
     * Business Unit: "Distribuidores"
     * Customer: "Chavez"
     * Party Classification: "Red de Farmacias"
     * FromAmount: 0
     * ThruAmount: 1'500.00 $
     * Outcome: Discount Percentage -> 10%
     */
    @Tag("production")
    @Test
    void testDistribuidoresChavez() {

    }

    /**
     * Business Unit: "Distribuidores"
     * Party Classification: "Red de Farmacias"
     * FromAmount: 0
     * ThruAmount: 1'500.00 $
     * Outcome: Discount Percentage -> 5%
     */
    @Tag("production")
    @Test
    void testDistribuidoresFarmacias() {

    }

    /**
     * Business Unit: "Distribuidores"
     * FromAmount: 0
     * ThruAmount: 1'500.00 $
     * Outcome: Discount Percentage -> 5%
     */
    @Tag("production")
    @Test
    void testDistribuidores() {

    }

    /**
     * Business Unit: "Entidades Privadas"
     * Customer: "Clinica Incor"
     * Outcome: Discount Percentage -> 5%
     */
    @Tag("production")
    @Test
    void testPrivadasIncor() {
        PriceComponentRuleInputSaleOrder saleOrderSuccess = HPTestUtil.createSaleOrder(
                HPTestUtil.createClinicaIncorCustomer(999),
                HPTestUtil.createBusinessUnitPrivadas(),
                TestUtil.createAgencySC(),
                HPTestUtil.toSaleOrderProductsList(
                        HPTestUtil.createSaleOrderProductBolsaColostomiaUnidad(),
                        TestUtil.createSaleOrderProduct47All(),
                        TestUtil.createSaleOrderProduct48WithAll()),
                PaymentConditionsTypeEnum.CASH,
                DeliveryModeEnum.MOBILE_STORE);

        PriceComponentRuleInputSaleOrder failOrder = TestUtil.createSaleOrderRamiro4448WithAllFactors(DeliveryModeEnum.MOBILE_STORE, PaymentConditionsTypeEnum.CASH);

        List<PriceComponentRuleInputSaleOrderProduct> results = componentRule.computate(saleOrderSuccess);

        PriceComponentRuleInputSaleOrderProduct product83 = results.stream().filter(product -> product.getProduct().getId() == 83).findFirst().orElse(null);

        Log.warn("Results: \n" + ToJsonString.toJson(results));

        PriceComponentRuleInputSaleOrderProduct product47 = results.stream().filter(product -> product.getProduct().getId() == 47).findFirst().orElse(null);
        PriceComponentRuleInputSaleOrderProduct product48 = results.stream().filter(product -> product.getProduct().getId() == 48).findFirst().orElse(null);

        if (Objects.isNull(product47))
            fail("Product 47 does not exists");
        if (Objects.isNull(product48))
            fail("Product 48 does not exists");
        if (Objects.isNull(product83))
            fail("Product 83 does not exists");
        assertEquals(product47.getDiscountPercentage(), new BigDecimal(5));
        assertEquals(product48.getDiscountPercentage(), new BigDecimal(5));
        assertEquals(product83.getDiscountPercentage(), new BigDecimal(5));

        results = componentRule.computate(failOrder);
        assertEquals(results.isEmpty(), Boolean.TRUE);
    }

    /**
     * Business Unit: "Entidades Privadas"
     * Customer: "Clinica Fionanini"
     * Outcome: Discount Percentage -> 10%
     */
    @Tag("production")
    @Test
    void testPrivadasFionanini() {
        PriceComponentRuleInputSaleOrder saleOrderSuccess = HPTestUtil.createSaleOrder(
                HPTestUtil.createClinicaFioaniniCustomer(1000),
                HPTestUtil.createBusinessUnitPrivadas(),
                TestUtil.createAgencySC(),
                HPTestUtil.toSaleOrderProductsList(
                        HPTestUtil.createSaleOrderProductBolsaColostomiaUnidad(),
                        TestUtil.createSaleOrderProduct47All(),
                        TestUtil.createSaleOrderProduct48WithAll()),
                PaymentConditionsTypeEnum.CASH,
                DeliveryModeEnum.MOBILE_STORE);

        PriceComponentRuleInputSaleOrder failOrder = TestUtil.createSaleOrderRamiro4448WithAllFactors(DeliveryModeEnum.MOBILE_STORE, PaymentConditionsTypeEnum.CASH);

        List<PriceComponentRuleInputSaleOrderProduct> results = componentRule.computate(saleOrderSuccess);

        PriceComponentRuleInputSaleOrderProduct product83 = results.stream().filter(product -> product.getProduct().getId() == 83).findFirst().orElse(null);

        Log.warn("Results: \n" + ToJsonString.toJson(results));

        PriceComponentRuleInputSaleOrderProduct product47 = results.stream().filter(product -> product.getProduct().getId() == 47).findFirst().orElse(null);
        PriceComponentRuleInputSaleOrderProduct product48 = results.stream().filter(product -> product.getProduct().getId() == 48).findFirst().orElse(null);

        if (Objects.isNull(product47))
            fail("Product 47 does not exists");
        if (Objects.isNull(product48))
            fail("Product 48 does not exists");
        if (Objects.isNull(product83))
            fail("Product 83 does not exists");
        assertEquals(product47.getDiscountPercentage(), new BigDecimal(10));
        assertEquals(product48.getDiscountPercentage(), new BigDecimal(10));
        assertEquals(product83.getDiscountPercentage(), new BigDecimal(10));

        results = componentRule.computate(failOrder);
        assertEquals(results.isEmpty(), Boolean.TRUE);
    }

    /**
     * Party Classification: "Pariente de Primer Grado"
     * Outcome: Discount Percentage -> 15%
     */
    @Tag("production")
    @Test
    void testParientes() {

    }

    /**
     * Product Classification: "Productos Dañados"
     * Outcome: Discount Percentage -> 10%
     */
    @Tag("production")
    @Test
    void testDamageProducts() {

    }

}
