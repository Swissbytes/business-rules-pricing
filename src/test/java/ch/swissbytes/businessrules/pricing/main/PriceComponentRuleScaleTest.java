package ch.swissbytes.businessrules.pricing.main;

import ch.swissbytes.businessrules.pricing.enums.*;
import ch.swissbytes.businessrules.pricing.factors.PriceComponentRuleFactorScale;
import ch.swissbytes.businessrules.pricing.inputs.PriceComponentRuleInputSaleOrder;
import ch.swissbytes.businessrules.pricing.inputs.PriceComponentRuleInputSaleOrderProduct;
import ch.swissbytes.businessrules.pricing.log.Logger;
import ch.swissbytes.businessrules.pricing.log.LoggerImpl;
import ch.swissbytes.businessrules.pricing.models.PriceComponentRule;
import ch.swissbytes.businessrules.pricing.models.PriceRule;
import ch.swissbytes.businessrules.pricing.outcomes.PriceComponentRuleOutcome;
import ch.swissbytes.businessrules.pricing.outcomes.PriceComponentRuleOutcomeOutput;
import ch.swissbytes.businessrules.pricing.outcomes.PriceComponentRuleOutcomeProduct;
import ch.swissbytes.businessrules.pricing.subjects.PriceComponentRuleSubjectProduct;
import ch.swissbytes.businessrules.pricing.utils.LogUtil;
import ch.swissbytes.businessrules.pricing.utils.TestUtil;
import ch.swissbytes.businessrules.pricing.utils.ToJsonString;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;


@TestInstance(TestInstance.Lifecycle.PER_METHOD)
public class PriceComponentRuleScaleTest {

    private static final Logger Log = LoggerImpl.getLogger();

    //region Outcomes
    private PriceComponentRuleOutcome outcome1024_1;
    private PriceComponentRuleOutcome outcome1024_2;
    private PriceComponentRuleOutcome outcome1023_1;
    private PriceComponentRuleOutcome outcome1017;
    private PriceComponentRuleOutcome outcome1024_3;
    private PriceComponentRuleOutcome outcome1023_2;
    private PriceComponentRuleOutcome outcome1004;
    //endregion

    //region Scales
    private List<PriceComponentRuleFactorScale> scale1004;
    private List<PriceComponentRuleFactorScale> scale1017;
    private List<PriceComponentRuleFactorScale> scale1023;
    private List<PriceComponentRuleFactorScale> scale1024;
    //endregion

    //region Outputs
    private PriceComponentRuleOutcomeOutput output1004;
    private PriceComponentRuleOutcomeOutput output1017;
    private PriceComponentRuleOutcomeOutput output1023;
    private PriceComponentRuleOutcomeOutput output1024;
    //endregion

    private PriceComponentRule componentRule;
    private List<PriceRule> rules;

    //region SaleOrders
    private PriceComponentRuleInputSaleOrder saleOrderRamiro4448WithAllFactors;
    //endregion

    @BeforeEach
    void setUp() {
        Log.info("Begin: Test Scale Rules...");
        rules = new ArrayList<>(0);
        saleOrderRamiro4448WithAllFactors = TestUtil.createSaleOrderRamiro4448WithAllFactors(DeliveryModeEnum.MOBILE_STORE, PaymentConditionsTypeEnum.CASH);
    }

    @Tag("production")
    @Test
    void testScaleDsctPerc() {
        Log.info("Test Scale [Discount Percentage]: Rule 1017, 1023, 1004, 1024. [Expected: 1023]");

        List<PriceComponentRuleSubjectProduct> products = new ArrayList<>(0);
        products.add(TestUtil.createProductRule48());


        //region Outcomes
        outcome1024_1 = TestUtil.createOutcomeDiscountPerc(new BigDecimal(2));
        outcome1024_2 = TestUtil.createOutcomeDiscountPerc(new BigDecimal(3));
        outcome1023_1 = TestUtil.createOutcomeDiscountPerc(new BigDecimal(5));
        outcome1017 = TestUtil.createOutcomeDiscountPerc(new BigDecimal(7));
        outcome1024_3 = TestUtil.createOutcomeDiscountPerc(new BigDecimal(8));
        outcome1023_2 = TestUtil.createOutcomeDiscountPerc(new BigDecimal(9));
        outcome1004 = TestUtil.createOutcomeDiscountPerc(new BigDecimal(12));
        //endregion

        //region Scales
        scale1004 = Collections.singletonList(TestUtil.createScale(new BigDecimal(11), null, ScaleTypeEnum.QUANTITY, outcome1004));
        scale1017 = Collections.singletonList(TestUtil.createScale(new BigDecimal(10), null, ScaleTypeEnum.QUANTITY, outcome1017));

        scale1023 = new ArrayList<>(0);
        scale1023.add(TestUtil.createScale(new BigDecimal(5), new BigDecimal(7), ScaleTypeEnum.QUANTITY, outcome1023_1));
        scale1023.add(TestUtil.createScale(new BigDecimal(8), null, ScaleTypeEnum.QUANTITY, outcome1023_2));

        scale1024 = new ArrayList<>(0);
        scale1024.add(TestUtil.createScale(BigDecimal.ZERO, new BigDecimal(2), ScaleTypeEnum.QUANTITY, outcome1024_1));
        scale1024.add(TestUtil.createScale(new BigDecimal(3), new BigDecimal(5), ScaleTypeEnum.QUANTITY, outcome1024_2));
        scale1024.add(TestUtil.createScale(new BigDecimal(6), null, ScaleTypeEnum.QUANTITY, outcome1024_3));
        //endregion

        //region outputs
        output1004 = TestUtil.createOutcomeOutput(3213, null, ScaleTypeEnum.QUANTITY, OutComeModeEnum.SCALE, null, scale1004, PriceComponentOutcomeTypeEnum.DISCOUNT_PERCENTAGE);
        output1017 = TestUtil.createOutcomeOutput(3214, null, ScaleTypeEnum.QUANTITY, OutComeModeEnum.SCALE, null, scale1017, PriceComponentOutcomeTypeEnum.DISCOUNT_PERCENTAGE);
        output1023 = TestUtil.createOutcomeOutput(3215, null, ScaleTypeEnum.QUANTITY, OutComeModeEnum.SCALE, null, scale1023, PriceComponentOutcomeTypeEnum.DISCOUNT_PERCENTAGE);
        output1024 = TestUtil.createOutcomeOutput(3216, null, ScaleTypeEnum.QUANTITY, OutComeModeEnum.SCALE, null, scale1024, PriceComponentOutcomeTypeEnum.DISCOUNT_PERCENTAGE);
        //endregion

        //1004
        PriceRule rule1004 = TestUtil.createRuleOnlyProducts(1004L, products, false, TargetEnum.PRODUCT,
                PriceRuleTypeEnum.GENERAL, ScaleTypeEnum.QUANTITY, output1004, OutComeModeEnum.SCALE);
        //1017
        PriceRule rule1017 = TestUtil.createRuleOnlyProducts(1017L, products, false, TargetEnum.PRODUCT,
                PriceRuleTypeEnum.GENERAL, ScaleTypeEnum.QUANTITY, output1017, OutComeModeEnum.SCALE);
        //1023
        PriceRule rule1023 = TestUtil.createRuleOnlyProducts(1023L, products, false, TargetEnum.PRODUCT,
                PriceRuleTypeEnum.GENERAL, ScaleTypeEnum.QUANTITY, output1023, OutComeModeEnum.SCALE);
        //1024
        PriceRule rule1024 = TestUtil.createRuleOnlyProducts(1024L, products, false, TargetEnum.PRODUCT,
                PriceRuleTypeEnum.GENERAL, ScaleTypeEnum.QUANTITY, output1024, OutComeModeEnum.SCALE);


        rules.add(rule1004);//scale not apply
        rules.add(rule1017);
        rules.add(rule1023);
        rules.add(rule1024);

        componentRule = PriceComponentRule.builder()
                .resolutionMode(PriceRuleResolutionModeEnum.BY_BEST_VALUE_SALE_ORDER)
                .rules(rules)
                .allowSetupLogger(true)
                .build();

        List<PriceComponentRuleInputSaleOrderProduct> results = componentRule.computate(saleOrderRamiro4448WithAllFactors);


        PriceComponentRuleInputSaleOrderProduct product44 = results.stream().filter(product -> product.getProduct().getId() == 44).findFirst().orElse(null);
        PriceComponentRuleInputSaleOrderProduct product48 = results.stream().filter(product -> product.getProduct().getId() == 48).findFirst().orElse(null);

        if (Objects.nonNull(product44))
            fail("Product 44 does exists");
        if (Objects.isNull(product48))
            fail("Product 48 does not exists");

//        assertThat(product48.getDiscountPercentage(), is(new BigDecimal(9)));
//        assertThat(product48.getRulesApplied().get(0).getId(), is(1023L));

        Log.info("RESULT: \n" + ToJsonString.toJson(results));
    }


    @Tag("production")
    @Test
    void testScaleDsctAmount() {
        Log.info("Test Scale [Discount Amount]: Rule 1017, 1023, 1004, 1024. [Expected: 1024]");

        List<PriceComponentRuleSubjectProduct> products = new ArrayList<>(0);
        products.add(TestUtil.createProductRule48());


        //region Rule 1024
        outcome1024_1 = TestUtil.createOutcomeDiscountAmount(new BigDecimal(22));
        outcome1024_2 = TestUtil.createOutcomeDiscountAmount(new BigDecimal(33));
        outcome1024_3 = TestUtil.createOutcomeDiscountAmount(new BigDecimal(68));

        scale1024 = new ArrayList<>(0);
        scale1024.add(TestUtil.createScale(BigDecimal.ZERO, new BigDecimal(2), ScaleTypeEnum.QUANTITY, outcome1024_1));
        scale1024.add(TestUtil.createScale(new BigDecimal(3), new BigDecimal(5), ScaleTypeEnum.QUANTITY, outcome1024_2));
        scale1024.add(TestUtil.createScale(new BigDecimal(6), null, ScaleTypeEnum.QUANTITY, outcome1024_3));
        //endregion

        //region Rule 1023
        outcome1023_1 = TestUtil.createOutcomeDiscountAmount(new BigDecimal(45));
        outcome1023_2 = TestUtil.createOutcomeDiscountAmount(new BigDecimal(59));

        scale1023 = new ArrayList<>(0);
        scale1023.add(TestUtil.createScale(new BigDecimal(5), new BigDecimal(7), ScaleTypeEnum.QUANTITY, outcome1023_1));
        scale1023.add(TestUtil.createScale(new BigDecimal(8), null, ScaleTypeEnum.QUANTITY, outcome1023_2));
        //endregion

        //region Rule 1017
        scale1017 = Collections.singletonList(TestUtil.createScale(new BigDecimal(10), null, ScaleTypeEnum.QUANTITY, TestUtil.createOutcomeDiscountAmount(new BigDecimal(17))));
        //endregion

        //region Rule 1004
        outcome1004 = TestUtil.createOutcomeDiscountAmount(new BigDecimal(20));
        scale1004 = Collections.singletonList(TestUtil.createScale(new BigDecimal(11), null, ScaleTypeEnum.QUANTITY, TestUtil.createOutcomeDiscountAmount(new BigDecimal(20))));
        //endregion

        output1004 = TestUtil.createOutcomeOutput(3213, null, ScaleTypeEnum.QUANTITY, OutComeModeEnum.SCALE, null, scale1004, PriceComponentOutcomeTypeEnum.DISCOUNT_AMOUNT);
        output1017 = TestUtil.createOutcomeOutput(3214, null, ScaleTypeEnum.QUANTITY, OutComeModeEnum.SCALE, null, scale1017, PriceComponentOutcomeTypeEnum.DISCOUNT_AMOUNT);
        output1023 = TestUtil.createOutcomeOutput(3215, null, ScaleTypeEnum.QUANTITY, OutComeModeEnum.SCALE, null, scale1023, PriceComponentOutcomeTypeEnum.DISCOUNT_AMOUNT);
        output1024 = TestUtil.createOutcomeOutput(3216, null, ScaleTypeEnum.QUANTITY, OutComeModeEnum.SCALE, null, scale1024, PriceComponentOutcomeTypeEnum.DISCOUNT_AMOUNT);

        //1004
        PriceRule rule1004 = TestUtil.createRuleOnlyProducts(1004L, products, false, TargetEnum.PRODUCT,
                PriceRuleTypeEnum.GENERAL, ScaleTypeEnum.QUANTITY, output1004, OutComeModeEnum.SCALE);
        //1017
        PriceRule rule1017 = TestUtil.createRuleOnlyProducts(1017L, products, false, TargetEnum.PRODUCT,
                PriceRuleTypeEnum.GENERAL, ScaleTypeEnum.QUANTITY, output1017, OutComeModeEnum.SCALE);
        //1023
        PriceRule rule1023 = TestUtil.createRuleOnlyProducts(1023L, products, false, TargetEnum.PRODUCT,
                PriceRuleTypeEnum.GENERAL, ScaleTypeEnum.QUANTITY, output1023, OutComeModeEnum.SCALE);
        //1024
        PriceRule rule1024 = TestUtil.createRuleOnlyProducts(1024L, products, false, TargetEnum.PRODUCT,
                PriceRuleTypeEnum.GENERAL, ScaleTypeEnum.QUANTITY, output1024, OutComeModeEnum.SCALE);


        rules.add(rule1004);//scale not apply
        rules.add(rule1017);
        rules.add(rule1023);
        rules.add(rule1024);

        componentRule = PriceComponentRule.builder()
                .resolutionMode(PriceRuleResolutionModeEnum.BY_BEST_VALUE_SALE_ORDER)
                .rules(rules)
                .allowSetupLogger(true)
                .build();

        List<PriceComponentRuleInputSaleOrderProduct> results = componentRule.computate(saleOrderRamiro4448WithAllFactors);
        PriceComponentRuleInputSaleOrderProduct product44 = results.stream().filter(product -> product.getProduct().getId() == 44).findFirst().orElse(null);
        PriceComponentRuleInputSaleOrderProduct product48 = results.stream().filter(product -> product.getProduct().getId() == 48).findFirst().orElse(null);

        if (Objects.isNull(product48))
            fail("Product 48 does not exists");

        if (Objects.nonNull(product44))
            fail("Product 44 does exists and should not according to the expected result.");
        assertThat(product48.getDiscountAmount(), is(new BigDecimal(68)));
        assertThat(product48.getRulesApplied().get(0).getId(), is(1024L));

        Log.info("RESULT: \n" + ToJsonString.toJson(results));
    }

    @Tag("production")
    @Test
    void testScaleBonusProduct() {
        Log.info("Test Scale [Bonus Product]: Rule 1017, 1023, 1024. [Expected: 1024]");

        List<PriceComponentRuleSubjectProduct> products = new ArrayList<>(0);
        products.add(TestUtil.createProductRule48());


        List<PriceComponentRuleOutcomeProduct> productBonus1024_1 = Collections.singletonList(TestUtil.createProductBonus46(new BigDecimal(12)));
        List<PriceComponentRuleOutcomeProduct> productBonus1024_2 = Collections.singletonList(TestUtil.createProductBonus47(BigDecimal.ONE));
        List<PriceComponentRuleOutcomeProduct> productBonus1024_3 = new ArrayList<>(0);
        productBonus1024_3.add(TestUtil.createProductBonus46(new BigDecimal(6)));
        productBonus1024_3.add(TestUtil.createProductBonus47(new BigDecimal(3)));

        //region Rule 1024
        outcome1024_1 = TestUtil.createOutcomeProductBonus(productBonus1024_1);
        outcome1024_2 = TestUtil.createOutcomeProductBonus(productBonus1024_2);
        outcome1024_3 = TestUtil.createOutcomeProductBonus(productBonus1024_3);

        scale1024 = new ArrayList<>(0);
        scale1024.add(TestUtil.createScale(BigDecimal.ZERO, new BigDecimal(2), ScaleTypeEnum.QUANTITY, outcome1024_1));
        scale1024.add(TestUtil.createScale(new BigDecimal(3), new BigDecimal(5), ScaleTypeEnum.QUANTITY, outcome1024_2));
        scale1024.add(TestUtil.createScale(new BigDecimal(6), null, ScaleTypeEnum.QUANTITY, outcome1024_3));
        //endregion

        //region Rule 1023

        List<PriceComponentRuleOutcomeProduct> productBonus1023_1 = Collections.singletonList(TestUtil.createProductBonus47(BigDecimal.ONE));

        List<PriceComponentRuleOutcomeProduct> productBonus1023_2 = new ArrayList<>(0);
        productBonus1023_2.add(TestUtil.createProductBonus46(new BigDecimal(8)));
        productBonus1023_2.add(TestUtil.createProductBonus47(new BigDecimal(2)));

        outcome1023_1 = TestUtil.createOutcomeProductBonus(productBonus1023_1);
        outcome1023_2 = TestUtil.createOutcomeProductBonus(productBonus1023_2);

        scale1023 = new ArrayList<>(0);
        scale1023.add(TestUtil.createScale(new BigDecimal(5), new BigDecimal(7), ScaleTypeEnum.QUANTITY, outcome1023_1));
        scale1023.add(TestUtil.createScale(new BigDecimal(8), null, ScaleTypeEnum.QUANTITY, outcome1023_2));
        //endregion

        //region Rule 1017
        List<PriceComponentRuleOutcomeProduct> productBonus1017_1 = Collections.singletonList(TestUtil.createProductBonus47(new BigDecimal(3)));
        scale1017 = Collections.singletonList(TestUtil.createScale(new BigDecimal(10), null, ScaleTypeEnum.QUANTITY, TestUtil.createOutcomeProductBonus(productBonus1017_1)));
        //endregion

        output1004 = TestUtil.createOutcomeOutput(3213, null, ScaleTypeEnum.QUANTITY, OutComeModeEnum.SCALE, null, scale1004, PriceComponentOutcomeTypeEnum.PRODUCT);
        output1017 = TestUtil.createOutcomeOutput(3214, null, ScaleTypeEnum.QUANTITY, OutComeModeEnum.SCALE, null, scale1017, PriceComponentOutcomeTypeEnum.PRODUCT);
        output1023 = TestUtil.createOutcomeOutput(3215, null, ScaleTypeEnum.QUANTITY, OutComeModeEnum.SCALE, null, scale1023, PriceComponentOutcomeTypeEnum.PRODUCT);
        output1024 = TestUtil.createOutcomeOutput(3216, null, ScaleTypeEnum.QUANTITY, OutComeModeEnum.SCALE, null, scale1024, PriceComponentOutcomeTypeEnum.PRODUCT);
        //1017
        PriceRule rule1017 = TestUtil.createRuleOnlyProducts(1017L, products, false, TargetEnum.PRODUCT,
                PriceRuleTypeEnum.GENERAL, ScaleTypeEnum.QUANTITY, output1017, OutComeModeEnum.SCALE);
        //1023
        PriceRule rule1023 = TestUtil.createRuleOnlyProducts(1023L, products, false, TargetEnum.PRODUCT,
                PriceRuleTypeEnum.GENERAL, ScaleTypeEnum.QUANTITY, output1023, OutComeModeEnum.SCALE);
        //1024
        PriceRule rule1024 = TestUtil.createRuleOnlyProducts(1024L, products, false, TargetEnum.PRODUCT,
                PriceRuleTypeEnum.GENERAL, ScaleTypeEnum.QUANTITY, output1024, OutComeModeEnum.SCALE);

        rules.add(rule1017);
        rules.add(rule1023);
        rules.add(rule1024);

        componentRule = PriceComponentRule.builder()
                .resolutionMode(PriceRuleResolutionModeEnum.BY_BEST_VALUE_SALE_ORDER)
                .rules(rules)
                .allowSetupLogger(true)
                .build();

        List<PriceComponentRuleInputSaleOrderProduct> results = componentRule.computate(saleOrderRamiro4448WithAllFactors);

        PriceComponentRuleInputSaleOrderProduct product46 = results.stream().filter(product -> product.getProduct().getId() == 46).findFirst().orElse(null);
        PriceComponentRuleInputSaleOrderProduct product47 = results.stream().filter(product -> product.getProduct().getId() == 47).findFirst().orElse(null);
        PriceComponentRuleInputSaleOrderProduct product48 = results.stream().filter(product -> product.getProduct().getId() == 48).findFirst().orElse(null);

        if (Objects.isNull(product48))
            fail("Product 48 does not exists");
        if (Objects.isNull(product46))
            fail("Product 46 does not exists");
        if (Objects.isNull(product47))
            fail("Product 47 does not exists");

        assertThat(product48.getRulesApplied().get(0).getId(), is(1024L));
        assertThat(product46.getRulesApplied().get(0).getId(), is(1024L));
        assertThat(product47.getRulesApplied().get(0).getId(), is(1024L));

        assertThat(product48.getId(), is(not(0L)));
        assertThat(product46.getId(), is(0L));
        assertThat(product47.getId(), is(0L));
        assertThat(product46.getDiscountPercentage(), is(new BigDecimal(100)));
        assertThat(product47.getDiscountPercentage(), is(new BigDecimal(100)));

        Log.info("RESULT: \n" + ToJsonString.toJson(results));
    }

    @Tag("production")
    @Test
    void testScaleSurchargeProduct() {
        Log.info("Test Scale [Surcharge Product]: Rule 1017, 1023, 1024. [Expected: 1024]");

        List<PriceComponentRuleSubjectProduct> products = new ArrayList<>(0);
        products.add(TestUtil.createProductRule48());


        List<PriceComponentRuleOutcomeProduct> productBonus1024_1 = Collections.singletonList(TestUtil.createProductBonus46(new BigDecimal(12)));
        List<PriceComponentRuleOutcomeProduct> productBonus1024_2 = Collections.singletonList(TestUtil.createProductBonus47(BigDecimal.ONE));
        List<PriceComponentRuleOutcomeProduct> productBonus1024_3 = new ArrayList<>(0);
        productBonus1024_3.add(TestUtil.createProductBonus46(new BigDecimal(6)));
        productBonus1024_3.add(TestUtil.createProductBonus47(new BigDecimal(3)));

        //region Rule 1024
        outcome1024_1 = TestUtil.createOutcomeProductSurcharge(productBonus1024_1);
        outcome1024_2 = TestUtil.createOutcomeProductSurcharge(productBonus1024_2);
        outcome1024_3 = TestUtil.createOutcomeProductSurcharge(productBonus1024_3);

        scale1024 = new ArrayList<>(0);
        scale1024.add(TestUtil.createScale(BigDecimal.ZERO, new BigDecimal(2), ScaleTypeEnum.QUANTITY, outcome1024_1));
        scale1024.add(TestUtil.createScale(new BigDecimal(3), new BigDecimal(5), ScaleTypeEnum.QUANTITY, outcome1024_2));
        scale1024.add(TestUtil.createScale(new BigDecimal(6), null, ScaleTypeEnum.QUANTITY, outcome1024_3));
        //endregion

        //region Rule 1023

        List<PriceComponentRuleOutcomeProduct> productBonus1023_1 = Collections.singletonList(TestUtil.createProductBonus47(BigDecimal.ONE));

        List<PriceComponentRuleOutcomeProduct> productBonus1023_2 = new ArrayList<>(0);
        productBonus1023_2.add(TestUtil.createProductBonus46(new BigDecimal(8)));
        productBonus1023_2.add(TestUtil.createProductBonus47(new BigDecimal(2)));

        outcome1023_1 = TestUtil.createOutcomeProductSurcharge(productBonus1023_1);
        outcome1023_2 = TestUtil.createOutcomeProductSurcharge(productBonus1023_2);

        scale1023 = new ArrayList<>(0);
        scale1023.add(TestUtil.createScale(new BigDecimal(5), new BigDecimal(7), ScaleTypeEnum.QUANTITY, outcome1023_1));
        scale1023.add(TestUtil.createScale(new BigDecimal(8), null, ScaleTypeEnum.QUANTITY, outcome1023_2));
        //endregion

        //region Rule 1017
        List<PriceComponentRuleOutcomeProduct> productBonus1017_1 = Collections.singletonList(TestUtil.createProductBonus47(new BigDecimal(3)));
        scale1017 = Collections.singletonList(TestUtil.createScale(new BigDecimal(10), null, ScaleTypeEnum.QUANTITY, TestUtil.createOutcomeProductBonus(productBonus1017_1)));
        //endregion

        output1004 = TestUtil.createOutcomeOutput(3213, null, ScaleTypeEnum.QUANTITY, OutComeModeEnum.SCALE, null, scale1004, PriceComponentOutcomeTypeEnum.PRODUCT_SURCHARGE);
        output1017 = TestUtil.createOutcomeOutput(3214, null, ScaleTypeEnum.QUANTITY, OutComeModeEnum.SCALE, null, scale1017, PriceComponentOutcomeTypeEnum.PRODUCT_SURCHARGE);
        output1023 = TestUtil.createOutcomeOutput(3215, null, ScaleTypeEnum.QUANTITY, OutComeModeEnum.SCALE, null, scale1023, PriceComponentOutcomeTypeEnum.PRODUCT_SURCHARGE);
        output1024 = TestUtil.createOutcomeOutput(3216, null, ScaleTypeEnum.QUANTITY, OutComeModeEnum.SCALE, null, scale1024, PriceComponentOutcomeTypeEnum.PRODUCT_SURCHARGE);
        //1017
        PriceRule rule1017 = TestUtil.createRuleOnlyProducts(1017L, products, false, TargetEnum.PRODUCT,
                PriceRuleTypeEnum.GENERAL, ScaleTypeEnum.QUANTITY, output1017, OutComeModeEnum.SCALE);
        //1023
        PriceRule rule1023 = TestUtil.createRuleOnlyProducts(1023L, products, false, TargetEnum.PRODUCT,
                PriceRuleTypeEnum.GENERAL, ScaleTypeEnum.QUANTITY, output1023, OutComeModeEnum.SCALE);
        //1024
        PriceRule rule1024 = TestUtil.createRuleOnlyProducts(1024L, products, false, TargetEnum.PRODUCT,
                PriceRuleTypeEnum.GENERAL, ScaleTypeEnum.QUANTITY, output1024, OutComeModeEnum.SCALE);

        rules.add(rule1017);
        rules.add(rule1023);
        rules.add(rule1024);

        componentRule = PriceComponentRule.builder()
                .resolutionMode(PriceRuleResolutionModeEnum.BY_BEST_VALUE_SALE_ORDER)
                .rules(rules)
                .allowSetupLogger(true)
                .build();

        List<PriceComponentRuleInputSaleOrderProduct> results = componentRule.computate(saleOrderRamiro4448WithAllFactors);

        assertThat(results.get(0).getRulesApplied().get(0).getId(), is(1024L));
        assertThat(results.get(1).getRulesApplied().get(0).getId(), is(1024L));
        assertThat(results.get(2).getRulesApplied().get(0).getId(), is(1024L));
        assertThat(results.get(0).getId(), is(not(-1L)));
        assertThat(results.get(1).getId(), is(-1L));
        assertThat(results.get(2).getId(), is(-1L));

        Log.info("RESULT: \n" + ToJsonString.toJson(results));
    }

    @Tag("production")
    @Test
    void testScaleBonusProductEmptyProductsRule() {
        Log.info("Test Scale [Bonus Product]: Rule 1017. [Expected: 1017]");

        List<PriceComponentRuleOutcomeProduct> productBonus1017_1 = Collections.singletonList(TestUtil.createProductBonus47(new BigDecimal(3)));
        scale1017 = Collections.singletonList(TestUtil.createScale(new BigDecimal(500), null, ScaleTypeEnum.AMOUNT, TestUtil.createOutcomeProductBonus(productBonus1017_1)));
        output1017 = TestUtil.createOutcomeOutput(3214, null, ScaleTypeEnum.AMOUNT, OutComeModeEnum.SCALE, null, scale1017, PriceComponentOutcomeTypeEnum.PRODUCT);
        //1017
        PriceRule rule1017 = TestUtil.createRuleOnlyProducts(1017L, Collections.emptyList(), false, TargetEnum.PRODUCT,
                PriceRuleTypeEnum.RESTRICTED, ScaleTypeEnum.AMOUNT, output1017, OutComeModeEnum.SCALE);

        rules.add(rule1017);

        componentRule = PriceComponentRule.builder()
                .resolutionMode(PriceRuleResolutionModeEnum.BY_BEST_VALUE_SALE_ORDER)
                .rules(rules)
                .allowSetupLogger(true)
                .build();

        List<PriceComponentRuleInputSaleOrderProduct> results = componentRule.computate(saleOrderRamiro4448WithAllFactors);

        assertThat(results.get(0).getRulesApplied().get(0).getId(), is(1017L));
        assertThat(results.get(1).getRulesApplied().get(0).getId(), is(1017L));
        assertThat(results.get(2).getRulesApplied().get(0).getId(), is(1017L));
        assertThat(results.get(0).getId(), is(not(0L)));
        assertThat(results.get(1).getId(), is(not(0L)));
        assertThat(results.get(2).getId(), is(0L));
        Log.info("RESULT: \n" + ToJsonString.toJson(results));
    }

    @Tag("production")
    @Test
    public void validateTestCaseScale6BestDiscountAmountNonAccumGeneral() {

        List<PriceComponentRuleInputSaleOrderProduct> products = new ArrayList<>(0);
        products.add(TestUtil.createSaleOrderProduct44WithAll());
        products.add(TestUtil.createSaleOrderProduct46All());
        products.add(TestUtil.createSaleOrderProduct47All());
        products.add(TestUtil.createSaleOrderProduct48WithAll());
        //region Sale Order
        PriceComponentRuleInputSaleOrder saleOrder4Products = TestUtil.createSaleOrderRamiroWithAllFactors(products,
                DeliveryModeEnum.MOBILE_STORE, PaymentConditionsTypeEnum.CASH);
        //endregion

        //region Scales
        List<PriceComponentRuleFactorScale> scaleOne = new ArrayList<>(0);
        List<PriceComponentRuleFactorScale> scaleTwo = new ArrayList<>(0);
        List<PriceComponentRuleFactorScale> scaleThree = new ArrayList<>(0);
        List<PriceComponentRuleFactorScale> scaleFour = new ArrayList<>(0);
        List<PriceComponentRuleFactorScale> scaleFive = new ArrayList<>(0);
        List<PriceComponentRuleFactorScale> scaleSix = new ArrayList<>(0);

        scaleOne.add(TestUtil.createScale(new BigDecimal(300), new BigDecimal(500), ScaleTypeEnum.AMOUNT, TestUtil.createOutcomeDiscountPerc(new BigDecimal(1))));
        scaleOne.add(TestUtil.createScale(new BigDecimal(501), new BigDecimal(1000), ScaleTypeEnum.AMOUNT, TestUtil.createOutcomeDiscountPerc(new BigDecimal(2))));
        scaleOne.add(TestUtil.createScale(new BigDecimal(1001), new BigDecimal(2000), ScaleTypeEnum.AMOUNT, TestUtil.createOutcomeDiscountPerc(new BigDecimal(4))));
        scaleOne.add(TestUtil.createScale(new BigDecimal(2001), null, ScaleTypeEnum.AMOUNT, TestUtil.createOutcomeDiscountPerc(new BigDecimal(5))));

        scaleTwo.add(TestUtil.createScale(new BigDecimal(800), new BigDecimal(1000), ScaleTypeEnum.AMOUNT, TestUtil.createOutcomeDiscountPerc(new BigDecimal(9))));
        scaleTwo.add(TestUtil.createScale(new BigDecimal(1001), null, ScaleTypeEnum.AMOUNT, TestUtil.createOutcomeDiscountPerc(new BigDecimal(12))));

        scaleThree.add(TestUtil.createScale(new BigDecimal(100), new BigDecimal(500), ScaleTypeEnum.AMOUNT, TestUtil.createOutcomeDiscountPerc(new BigDecimal(7))));
        scaleThree.add(TestUtil.createScale(new BigDecimal(501), null, ScaleTypeEnum.AMOUNT, TestUtil.createOutcomeDiscountPerc(new BigDecimal(8))));

        scaleFour.add(TestUtil.createScale(new BigDecimal(500), new BigDecimal(1000), ScaleTypeEnum.AMOUNT, TestUtil.createOutcomeDiscountPerc(new BigDecimal(9))));
        scaleFour.add(TestUtil.createScale(new BigDecimal(1001), null, ScaleTypeEnum.AMOUNT, TestUtil.createOutcomeDiscountPerc(new BigDecimal(10))));

        scaleFive.add(TestUtil.createScale(new BigDecimal(100), new BigDecimal(300), ScaleTypeEnum.AMOUNT, TestUtil.createOutcomeDiscountPerc(new BigDecimal(2))));
        scaleFive.add(TestUtil.createScale(new BigDecimal(301), new BigDecimal(1000), ScaleTypeEnum.AMOUNT, TestUtil.createOutcomeDiscountPerc(new BigDecimal(3))));
        scaleFive.add(TestUtil.createScale(new BigDecimal(1001), null, ScaleTypeEnum.AMOUNT, TestUtil.createOutcomeDiscountPerc(new BigDecimal(4))));

        scaleSix.add(TestUtil.createScale(new BigDecimal(300), new BigDecimal(500), ScaleTypeEnum.AMOUNT, TestUtil.createOutcomeDiscountPerc(new BigDecimal(3))));
        scaleSix.add(TestUtil.createScale(new BigDecimal(501), new BigDecimal(1000), ScaleTypeEnum.AMOUNT, TestUtil.createOutcomeDiscountPerc(new BigDecimal(5))));
        scaleSix.add(TestUtil.createScale(new BigDecimal(1001), null, ScaleTypeEnum.AMOUNT, TestUtil.createOutcomeDiscountPerc(new BigDecimal(11))));

//        endregion

        PriceComponentRuleOutcomeOutput outputDiscOne = TestUtil.createOutcomeOutput(1L, null,
                ScaleTypeEnum.AMOUNT, OutComeModeEnum.SCALE, null, scaleOne, PriceComponentOutcomeTypeEnum.DISCOUNT_PERCENTAGE);

        PriceComponentRuleOutcomeOutput outputDiscTwo = TestUtil.createOutcomeOutput(2L, null,
                ScaleTypeEnum.AMOUNT, OutComeModeEnum.SCALE, null, scaleTwo, PriceComponentOutcomeTypeEnum.DISCOUNT_PERCENTAGE);

        PriceComponentRuleOutcomeOutput outputDiscThree = TestUtil.createOutcomeOutput(3L, null,
                ScaleTypeEnum.AMOUNT, OutComeModeEnum.SCALE, null, scaleThree, PriceComponentOutcomeTypeEnum.DISCOUNT_PERCENTAGE);

        PriceComponentRuleOutcomeOutput outputDiscFour = TestUtil.createOutcomeOutput(4L, null,
                ScaleTypeEnum.AMOUNT, OutComeModeEnum.SCALE, null, scaleFour, PriceComponentOutcomeTypeEnum.DISCOUNT_PERCENTAGE);

        PriceComponentRuleOutcomeOutput outputDiscFive = TestUtil.createOutcomeOutput(5L, null,
                ScaleTypeEnum.AMOUNT, OutComeModeEnum.SCALE, null, scaleFive, PriceComponentOutcomeTypeEnum.DISCOUNT_PERCENTAGE);

        PriceComponentRuleOutcomeOutput outputDiscSix = TestUtil.createOutcomeOutput(6L, null,
                ScaleTypeEnum.AMOUNT, OutComeModeEnum.SCALE, null, scaleSix, PriceComponentOutcomeTypeEnum.DISCOUNT_PERCENTAGE);

        List<PriceComponentRuleSubjectProduct> productsOne = new ArrayList<>(0);
        List<PriceComponentRuleSubjectProduct> productsTwo = new ArrayList<>(0);
        List<PriceComponentRuleSubjectProduct> productsThree = new ArrayList<>(0);
        List<PriceComponentRuleSubjectProduct> productsFour = new ArrayList<>(0);
        List<PriceComponentRuleSubjectProduct> productsFive = new ArrayList<>(0);
        List<PriceComponentRuleSubjectProduct> productsSix = new ArrayList<>(0);

        productsOne.add(TestUtil.createProductRule44());
        productsOne.add(TestUtil.createProductRule46());
        productsOne.add(TestUtil.createProductRule47());
        productsOne.add(TestUtil.createProductRule48());

        productsTwo.add(TestUtil.createProductRule48());

        productsThree.add(TestUtil.createProductRule44());
        productsThree.add(TestUtil.createProductRule48());
        productsThree.add(TestUtil.createProductRule46());

        productsFour.add(TestUtil.createProductRule46());
        productsFour.add(TestUtil.createProductRule47());

        productsFive.add(TestUtil.createProductRule44());
        productsFive.add(TestUtil.createProductRule47());

        productsSix.add(TestUtil.createProductRule44());
        productsSix.add(TestUtil.createProductRule46());


        PriceRule ruleOne = TestUtil.createRuleOnlyProducts(1L, productsOne, false, TargetEnum.PRODUCT,
                PriceRuleTypeEnum.GENERAL, ScaleTypeEnum.AMOUNT, outputDiscOne, OutComeModeEnum.SCALE);

        PriceRule ruleTwo = TestUtil.createRuleOnlyProducts(2L, productsTwo, false, TargetEnum.PRODUCT,
                PriceRuleTypeEnum.GENERAL, ScaleTypeEnum.AMOUNT, outputDiscTwo, OutComeModeEnum.SCALE);

        PriceRule ruleThree = TestUtil.createRuleOnlyProducts(3L, productsThree, false, TargetEnum.PRODUCT,
                PriceRuleTypeEnum.GENERAL, ScaleTypeEnum.AMOUNT, outputDiscThree, OutComeModeEnum.SCALE);

        PriceRule ruleFour = TestUtil.createRuleOnlyProducts(4L, productsFour, false, TargetEnum.PRODUCT,
                PriceRuleTypeEnum.GENERAL, ScaleTypeEnum.AMOUNT, outputDiscFour, OutComeModeEnum.SCALE);

        PriceRule ruleFive = TestUtil.createRuleOnlyProducts(5L, productsFive, false, TargetEnum.PRODUCT,
                PriceRuleTypeEnum.GENERAL, ScaleTypeEnum.AMOUNT, outputDiscFive, OutComeModeEnum.SCALE);

        PriceRule ruleSix = TestUtil.createRuleOnlyProducts(6L, productsSix, false, TargetEnum.PRODUCT,
                PriceRuleTypeEnum.GENERAL, ScaleTypeEnum.AMOUNT, outputDiscSix, OutComeModeEnum.SCALE);


        List<PriceRule> rules = new ArrayList<>(0);
        rules.add(ruleOne);
        rules.add(ruleTwo);
        rules.add(ruleThree);
        rules.add(ruleFour);
        rules.add(ruleFive);
        rules.add(ruleSix);

        PriceComponentRule componentRule = PriceComponentRule.builder()
                .rules(rules)
                .resolutionMode(PriceRuleResolutionModeEnum.BY_BEST_VALUE_PRODUCT)
                .allowSetupLogger(false)
                .build();

        List<PriceComponentRuleInputSaleOrderProduct> results = componentRule.computate(saleOrder4Products);

        LogUtil.warn("REGLAS: \n"+ToJsonString.toJson(componentRule.getApplicableRules().stream().map(PriceRule::getId).collect(Collectors.toList())));

        PriceComponentRuleInputSaleOrderProduct product44 = results.stream().filter(product -> product.getProduct().getId() == 44).findFirst().orElse(null);
        PriceComponentRuleInputSaleOrderProduct product46 = results.stream().filter(product -> product.getProduct().getId() == 46).findFirst().orElse(null);
        PriceComponentRuleInputSaleOrderProduct product47 = results.stream().filter(product -> product.getProduct().getId() == 47).findFirst().orElse(null);
        PriceComponentRuleInputSaleOrderProduct product48 = results.stream().filter(product -> product.getProduct().getId() == 48).findFirst().orElse(null);

        if (Objects.isNull(product44))
            fail("Product 44 does not exists");
        assertThat(product44.getRulesApplied().get(0).getId(), is(6L));

        if (Objects.isNull(product46))
            fail("Product 46 does not exists");
        assertThat(product46.getRulesApplied().get(0).getId(), is(6L));

        if (Objects.isNull(product47))
            fail("Product 47 does not exists");
        assertThat(product47.getRulesApplied().get(0).getId(), is(4L));

        if (Objects.isNull(product48))
            fail("Product 48 does not exists.");
        assertThat(product48.getRulesApplied().get(0).getId(), is(2L));

        LogUtil.warn("Results: \n" + ToJsonString.toJson(results));
    }


    @Tag("production")
    @Test
    public void validateTestCaseScale6BestDiscountAmountNonAccumRestricted() {

        List<PriceComponentRuleInputSaleOrderProduct> products = new ArrayList<>(0);
        products.add(TestUtil.createSaleOrderProduct44WithAll());
        products.add(TestUtil.createSaleOrderProduct46All());
        products.add(TestUtil.createSaleOrderProduct47All());
        products.add(TestUtil.createSaleOrderProduct48WithAll());
        products.add(TestUtil.createSaleOrderProduct49All(new BigDecimal(3), new BigDecimal(250)));

        //region Sale Order
        PriceComponentRuleInputSaleOrder saleOrder4Products = TestUtil.createSaleOrderRamiroWithAllFactors(products,
                DeliveryModeEnum.MOBILE_STORE, PaymentConditionsTypeEnum.CASH);
        //endregion

        //region Scales
        List<PriceComponentRuleFactorScale> scaleOne = new ArrayList<>(0);
        List<PriceComponentRuleFactorScale> scaleTwo = new ArrayList<>(0);
        List<PriceComponentRuleFactorScale> scaleThree = new ArrayList<>(0);
        List<PriceComponentRuleFactorScale> scaleFour = new ArrayList<>(0);
        List<PriceComponentRuleFactorScale> scaleFive = new ArrayList<>(0);
        List<PriceComponentRuleFactorScale> scaleSix = new ArrayList<>(0);

        scaleOne.add(TestUtil.createScale(new BigDecimal(300), new BigDecimal(500), ScaleTypeEnum.AMOUNT, TestUtil.createOutcomeDiscountPerc(new BigDecimal(1))));
        scaleOne.add(TestUtil.createScale(new BigDecimal(501), new BigDecimal(1000), ScaleTypeEnum.AMOUNT, TestUtil.createOutcomeDiscountPerc(new BigDecimal(2))));
        scaleOne.add(TestUtil.createScale(new BigDecimal(1001), new BigDecimal(2000), ScaleTypeEnum.AMOUNT, TestUtil.createOutcomeDiscountPerc(new BigDecimal(4))));
        scaleOne.add(TestUtil.createScale(new BigDecimal(2001), null, ScaleTypeEnum.AMOUNT, TestUtil.createOutcomeDiscountPerc(new BigDecimal(5))));

        scaleTwo.add(TestUtil.createScale(new BigDecimal(800), new BigDecimal(1000), ScaleTypeEnum.AMOUNT, TestUtil.createOutcomeDiscountPerc(new BigDecimal(12))));
        scaleTwo.add(TestUtil.createScale(new BigDecimal(1001), null, ScaleTypeEnum.AMOUNT, TestUtil.createOutcomeDiscountPerc(new BigDecimal(23))));

        scaleThree.add(TestUtil.createScale(new BigDecimal(100), new BigDecimal(500), ScaleTypeEnum.AMOUNT, TestUtil.createOutcomeDiscountPerc(new BigDecimal(7))));
        scaleThree.add(TestUtil.createScale(new BigDecimal(501), null, ScaleTypeEnum.AMOUNT, TestUtil.createOutcomeDiscountPerc(new BigDecimal(8))));

        scaleFour.add(TestUtil.createScale(new BigDecimal(500), new BigDecimal(1000), ScaleTypeEnum.AMOUNT, TestUtil.createOutcomeDiscountPerc(new BigDecimal(9))));
        scaleFour.add(TestUtil.createScale(new BigDecimal(1001), new BigDecimal(5000), ScaleTypeEnum.AMOUNT, TestUtil.createOutcomeDiscountPerc(new BigDecimal(10))));
        scaleFour.add(TestUtil.createScale(new BigDecimal(5001), null, ScaleTypeEnum.AMOUNT, TestUtil.createOutcomeDiscountPerc(new BigDecimal(18))));

        scaleFive.add(TestUtil.createScale(new BigDecimal(100), new BigDecimal(300), ScaleTypeEnum.AMOUNT, TestUtil.createOutcomeDiscountPerc(new BigDecimal(2))));
        scaleFive.add(TestUtil.createScale(new BigDecimal(301), new BigDecimal(1000), ScaleTypeEnum.AMOUNT, TestUtil.createOutcomeDiscountPerc(new BigDecimal(3))));
        scaleFive.add(TestUtil.createScale(new BigDecimal(1001), null, ScaleTypeEnum.AMOUNT, TestUtil.createOutcomeDiscountPerc(new BigDecimal(4))));

        scaleSix.add(TestUtil.createScale(new BigDecimal(300), new BigDecimal(500), ScaleTypeEnum.AMOUNT, TestUtil.createOutcomeDiscountPerc(new BigDecimal(3))));
        scaleSix.add(TestUtil.createScale(new BigDecimal(501), new BigDecimal(1000), ScaleTypeEnum.AMOUNT, TestUtil.createOutcomeDiscountPerc(new BigDecimal(5))));
        scaleSix.add(TestUtil.createScale(new BigDecimal(1001), null, ScaleTypeEnum.AMOUNT, TestUtil.createOutcomeDiscountPerc(new BigDecimal(11))));
//        endregion

        PriceComponentRuleOutcomeOutput outputDiscOne = TestUtil.createOutcomeOutput(1L, null,
                ScaleTypeEnum.AMOUNT, OutComeModeEnum.SCALE, null, scaleOne, PriceComponentOutcomeTypeEnum.DISCOUNT_PERCENTAGE);

        PriceComponentRuleOutcomeOutput outputDiscTwo = TestUtil.createOutcomeOutput(2L, null,
                ScaleTypeEnum.AMOUNT, OutComeModeEnum.SCALE, null, scaleTwo, PriceComponentOutcomeTypeEnum.DISCOUNT_PERCENTAGE);

        PriceComponentRuleOutcomeOutput outputDiscThree = TestUtil.createOutcomeOutput(3L, null,
                ScaleTypeEnum.AMOUNT, OutComeModeEnum.SCALE, null, scaleThree, PriceComponentOutcomeTypeEnum.DISCOUNT_PERCENTAGE);

        PriceComponentRuleOutcomeOutput outputDiscFour = TestUtil.createOutcomeOutput(4L, null,
                ScaleTypeEnum.AMOUNT, OutComeModeEnum.SCALE, null, scaleFour, PriceComponentOutcomeTypeEnum.DISCOUNT_PERCENTAGE);

        PriceComponentRuleOutcomeOutput outputDiscFive = TestUtil.createOutcomeOutput(5L, null,
                ScaleTypeEnum.AMOUNT, OutComeModeEnum.SCALE, null, scaleFive, PriceComponentOutcomeTypeEnum.DISCOUNT_PERCENTAGE);

        PriceComponentRuleOutcomeOutput outputDiscSix = TestUtil.createOutcomeOutput(6L, null,
                ScaleTypeEnum.AMOUNT, OutComeModeEnum.SCALE, null, scaleSix, PriceComponentOutcomeTypeEnum.DISCOUNT_PERCENTAGE);

        List<PriceComponentRuleSubjectProduct> productsOne = new ArrayList<>(0);
        List<PriceComponentRuleSubjectProduct> productsTwo = new ArrayList<>(0);
        List<PriceComponentRuleSubjectProduct> productsThree = new ArrayList<>(0);
        List<PriceComponentRuleSubjectProduct> productsFour = new ArrayList<>(0);
        List<PriceComponentRuleSubjectProduct> productsFive = new ArrayList<>(0);
        List<PriceComponentRuleSubjectProduct> productsSix = new ArrayList<>(0);

        productsOne.add(TestUtil.createProductRule44());
        productsOne.add(TestUtil.createProductRule46());
        productsOne.add(TestUtil.createProductRule47());
        productsOne.add(TestUtil.createProductRule48());

        productsTwo.add(TestUtil.createProductRule48());

        productsThree.add(TestUtil.createProductRule44());
        productsThree.add(TestUtil.createProductRule48());
        productsThree.add(TestUtil.createProductRule46());

        productsFour.add(TestUtil.createProductRule46());
        productsFour.add(TestUtil.createProductRule47());

        productsFive.add(TestUtil.createProductRule44());
        productsFive.add(TestUtil.createProductRule47());

        productsSix.add(TestUtil.createProductRule44());
        productsSix.add(TestUtil.createProductRule46());


        PriceRule ruleOne = TestUtil.createRuleOnlyProducts(1L, productsOne, false, TargetEnum.PRODUCT,
                PriceRuleTypeEnum.RESTRICTED, ScaleTypeEnum.AMOUNT, outputDiscOne, OutComeModeEnum.SCALE);

        PriceRule ruleTwo = TestUtil.createRuleOnlyProducts(2L, productsTwo, false, TargetEnum.PRODUCT,
                PriceRuleTypeEnum.RESTRICTED, ScaleTypeEnum.AMOUNT, outputDiscTwo, OutComeModeEnum.SCALE);

        PriceRule ruleThree = TestUtil.createRuleOnlyProducts(3L, productsThree, false, TargetEnum.PRODUCT,
                PriceRuleTypeEnum.RESTRICTED, ScaleTypeEnum.AMOUNT, outputDiscThree, OutComeModeEnum.SCALE);

        PriceRule ruleFour = TestUtil.createRuleOnlyProducts(4L, productsFour, false, TargetEnum.PRODUCT,
                PriceRuleTypeEnum.RESTRICTED, ScaleTypeEnum.AMOUNT, outputDiscFour, OutComeModeEnum.SCALE);

        PriceRule ruleFive = TestUtil.createRuleOnlyProducts(5L, productsFive, false, TargetEnum.PRODUCT,
                PriceRuleTypeEnum.RESTRICTED, ScaleTypeEnum.AMOUNT, outputDiscFive, OutComeModeEnum.SCALE);

        PriceRule ruleSix = TestUtil.createRuleOnlyProducts(6L, productsSix, false, TargetEnum.PRODUCT,
                PriceRuleTypeEnum.RESTRICTED, ScaleTypeEnum.AMOUNT, outputDiscSix, OutComeModeEnum.SCALE);

        List<PriceRule> rules = new ArrayList<>(0);
        rules.add(ruleOne);
        rules.add(ruleTwo);
        rules.add(ruleThree);
        rules.add(ruleFour);
        rules.add(ruleFive);
        rules.add(ruleSix);

        PriceComponentRule componentRule = PriceComponentRule.builder()
                .rules(rules)
                .resolutionMode(PriceRuleResolutionModeEnum.BY_BEST_VALUE_PRODUCT)
                .allowSetupLogger(false)
                .build();

        List<PriceComponentRuleInputSaleOrderProduct> results = componentRule.computate(saleOrder4Products);
        PriceComponentRuleInputSaleOrderProduct product44 = results.stream().filter(product -> product.getProduct().getId() == 44).findFirst().orElse(null);
        PriceComponentRuleInputSaleOrderProduct product46 = results.stream().filter(product -> product.getProduct().getId() == 46).findFirst().orElse(null);
        PriceComponentRuleInputSaleOrderProduct product47 = results.stream().filter(product -> product.getProduct().getId() == 47).findFirst().orElse(null);
        PriceComponentRuleInputSaleOrderProduct product48 = results.stream().filter(product -> product.getProduct().getId() == 48).findFirst().orElse(null);

        if (Objects.isNull(product44))
            fail("Product 44 does not exists");
        assertThat(product44.getRulesApplied().get(0).getId(), is(6L));

        if (Objects.isNull(product46))
            fail("Product 46 does not exists");
        assertThat(product46.getRulesApplied().get(0).getId(), is(6L));

        if (Objects.isNull(product47))
            fail("Product 47 does not exists");
        assertThat(product47.getRulesApplied().get(0).getId(), is(4L));

        if (Objects.isNull(product48))
            fail("Product 48 does not exists.");
        assertThat(product48.getRulesApplied().get(0).getId(), is(2L));

        LogUtil.warn("Results [useTotalSaleOrderAmount = Off]: \n" + ToJsonString.toJson(results));
    }


    @Tag("production")
    @Test
    public void validateTestCaseScale6BestDiscountAmountNonAccumRestrictedUsingSaleOrder() {

        List<PriceComponentRuleInputSaleOrderProduct> products = new ArrayList<>(0);
        products.add(TestUtil.createSaleOrderProduct44WithAll());
        products.add(TestUtil.createSaleOrderProduct46All());
        products.add(TestUtil.createSaleOrderProduct47All());
        products.add(TestUtil.createSaleOrderProduct48WithAll());
        products.add(TestUtil.createSaleOrderProduct49All(new BigDecimal(3), new BigDecimal(250)));

        //region Sale Order
        PriceComponentRuleInputSaleOrder saleOrder4Products = TestUtil.createSaleOrderRamiroWithAllFactors(products,
                DeliveryModeEnum.MOBILE_STORE, PaymentConditionsTypeEnum.CASH);
        //endregion

        //region Scales
        List<PriceComponentRuleFactorScale> scaleOne = new ArrayList<>(0);
        List<PriceComponentRuleFactorScale> scaleTwo = new ArrayList<>(0);
        List<PriceComponentRuleFactorScale> scaleThree = new ArrayList<>(0);
        List<PriceComponentRuleFactorScale> scaleFour = new ArrayList<>(0);
        List<PriceComponentRuleFactorScale> scaleFive = new ArrayList<>(0);
        List<PriceComponentRuleFactorScale> scaleSix = new ArrayList<>(0);

        scaleOne.add(TestUtil.createScale(new BigDecimal(300), new BigDecimal(500), ScaleTypeEnum.AMOUNT, TestUtil.createOutcomeDiscountPerc(new BigDecimal(1))));
        scaleOne.add(TestUtil.createScale(new BigDecimal(501), new BigDecimal(1000), ScaleTypeEnum.AMOUNT, TestUtil.createOutcomeDiscountPerc(new BigDecimal(2))));
        scaleOne.add(TestUtil.createScale(new BigDecimal(1001), new BigDecimal(2000), ScaleTypeEnum.AMOUNT, TestUtil.createOutcomeDiscountPerc(new BigDecimal(4))));
        scaleOne.add(TestUtil.createScale(new BigDecimal(2001), null, ScaleTypeEnum.AMOUNT, TestUtil.createOutcomeDiscountPerc(new BigDecimal(5))));

        scaleTwo.add(TestUtil.createScale(new BigDecimal(800), new BigDecimal(1000), ScaleTypeEnum.AMOUNT, TestUtil.createOutcomeDiscountPerc(new BigDecimal(12))));
        scaleTwo.add(TestUtil.createScale(new BigDecimal(1001), null, ScaleTypeEnum.AMOUNT, TestUtil.createOutcomeDiscountPerc(new BigDecimal(23))));

        scaleThree.add(TestUtil.createScale(new BigDecimal(100), new BigDecimal(500), ScaleTypeEnum.AMOUNT, TestUtil.createOutcomeDiscountPerc(new BigDecimal(7))));
        scaleThree.add(TestUtil.createScale(new BigDecimal(501), null, ScaleTypeEnum.AMOUNT, TestUtil.createOutcomeDiscountPerc(new BigDecimal(8))));

        scaleFour.add(TestUtil.createScale(new BigDecimal(500), new BigDecimal(1000), ScaleTypeEnum.AMOUNT, TestUtil.createOutcomeDiscountPerc(new BigDecimal(9))));
        scaleFour.add(TestUtil.createScale(new BigDecimal(1001), new BigDecimal(5000), ScaleTypeEnum.AMOUNT, TestUtil.createOutcomeDiscountPerc(new BigDecimal(10))));
        scaleFour.add(TestUtil.createScale(new BigDecimal(5001), null, ScaleTypeEnum.AMOUNT, TestUtil.createOutcomeDiscountPerc(new BigDecimal(18))));

        scaleFive.add(TestUtil.createScale(new BigDecimal(100), new BigDecimal(300), ScaleTypeEnum.AMOUNT, TestUtil.createOutcomeDiscountPerc(new BigDecimal(2))));
        scaleFive.add(TestUtil.createScale(new BigDecimal(301), new BigDecimal(1000), ScaleTypeEnum.AMOUNT, TestUtil.createOutcomeDiscountPerc(new BigDecimal(3))));
        scaleFive.add(TestUtil.createScale(new BigDecimal(1001), null, ScaleTypeEnum.AMOUNT, TestUtil.createOutcomeDiscountPerc(new BigDecimal(4))));

        scaleSix.add(TestUtil.createScale(new BigDecimal(300), new BigDecimal(500), ScaleTypeEnum.AMOUNT, TestUtil.createOutcomeDiscountPerc(new BigDecimal(3))));
        scaleSix.add(TestUtil.createScale(new BigDecimal(501), new BigDecimal(1000), ScaleTypeEnum.AMOUNT, TestUtil.createOutcomeDiscountPerc(new BigDecimal(5))));
        scaleSix.add(TestUtil.createScale(new BigDecimal(1001), null, ScaleTypeEnum.AMOUNT, TestUtil.createOutcomeDiscountPerc(new BigDecimal(11))));
//        endregion

        PriceComponentRuleOutcomeOutput outputDiscOne = TestUtil.createOutcomeOutput(1L, null,
                ScaleTypeEnum.AMOUNT, OutComeModeEnum.SCALE, null, scaleOne, PriceComponentOutcomeTypeEnum.DISCOUNT_PERCENTAGE);

        PriceComponentRuleOutcomeOutput outputDiscTwo = TestUtil.createOutcomeOutput(2L, null,
                ScaleTypeEnum.AMOUNT, OutComeModeEnum.SCALE, null, scaleTwo, PriceComponentOutcomeTypeEnum.DISCOUNT_PERCENTAGE);

        PriceComponentRuleOutcomeOutput outputDiscThree = TestUtil.createOutcomeOutput(3L, null,
                ScaleTypeEnum.AMOUNT, OutComeModeEnum.SCALE, null, scaleThree, PriceComponentOutcomeTypeEnum.DISCOUNT_PERCENTAGE);

        PriceComponentRuleOutcomeOutput outputDiscFour = TestUtil.createOutcomeOutput(4L, null,
                ScaleTypeEnum.AMOUNT, OutComeModeEnum.SCALE, null, scaleFour, PriceComponentOutcomeTypeEnum.DISCOUNT_PERCENTAGE);

        PriceComponentRuleOutcomeOutput outputDiscFive = TestUtil.createOutcomeOutput(5L, null,
                ScaleTypeEnum.AMOUNT, OutComeModeEnum.SCALE, null, scaleFive, PriceComponentOutcomeTypeEnum.DISCOUNT_PERCENTAGE);

        PriceComponentRuleOutcomeOutput outputDiscSix = TestUtil.createOutcomeOutput(6L, null,
                ScaleTypeEnum.AMOUNT, OutComeModeEnum.SCALE, null, scaleSix, PriceComponentOutcomeTypeEnum.DISCOUNT_PERCENTAGE);

        List<PriceComponentRuleSubjectProduct> productsOne = new ArrayList<>(0);
        List<PriceComponentRuleSubjectProduct> productsTwo = new ArrayList<>(0);
        List<PriceComponentRuleSubjectProduct> productsThree = new ArrayList<>(0);
        List<PriceComponentRuleSubjectProduct> productsFour = new ArrayList<>(0);
        List<PriceComponentRuleSubjectProduct> productsFive = new ArrayList<>(0);
        List<PriceComponentRuleSubjectProduct> productsSix = new ArrayList<>(0);

        productsOne.add(TestUtil.createProductRule44());
        productsOne.add(TestUtil.createProductRule46());
        productsOne.add(TestUtil.createProductRule47());
        productsOne.add(TestUtil.createProductRule48());

        productsTwo.add(TestUtil.createProductRule48());

        productsThree.add(TestUtil.createProductRule44());
        productsThree.add(TestUtil.createProductRule48());
        productsThree.add(TestUtil.createProductRule46());

        productsFour.add(TestUtil.createProductRule46());
        productsFour.add(TestUtil.createProductRule47());

        productsFive.add(TestUtil.createProductRule44());
        productsFive.add(TestUtil.createProductRule47());

        productsSix.add(TestUtil.createProductRule44());
        productsSix.add(TestUtil.createProductRule46());


        PriceRule ruleOne = TestUtil.createRuleOnlyProducts(1L, productsOne, false, TargetEnum.PRODUCT,
                PriceRuleTypeEnum.RESTRICTED, ScaleTypeEnum.AMOUNT, outputDiscOne, OutComeModeEnum.SCALE);
        ruleOne.setUseSaleOrderTotalForOutcome(true);

        PriceRule ruleTwo = TestUtil.createRuleOnlyProducts(2L, productsTwo, false, TargetEnum.PRODUCT,
                PriceRuleTypeEnum.RESTRICTED, ScaleTypeEnum.AMOUNT, outputDiscTwo, OutComeModeEnum.SCALE);
        ruleTwo.setUseSaleOrderTotalForOutcome(true);

        PriceRule ruleThree = TestUtil.createRuleOnlyProducts(3L, productsThree, false, TargetEnum.PRODUCT,
                PriceRuleTypeEnum.RESTRICTED, ScaleTypeEnum.AMOUNT, outputDiscThree, OutComeModeEnum.SCALE);
        ruleThree.setUseSaleOrderTotalForOutcome(true);

        PriceRule ruleFour = TestUtil.createRuleOnlyProducts(4L, productsFour, false, TargetEnum.PRODUCT,
                PriceRuleTypeEnum.RESTRICTED, ScaleTypeEnum.AMOUNT, outputDiscFour, OutComeModeEnum.SCALE);
        ruleFour.setUseSaleOrderTotalForOutcome(true);

        PriceRule ruleFive = TestUtil.createRuleOnlyProducts(5L, productsFive, false, TargetEnum.PRODUCT,
                PriceRuleTypeEnum.RESTRICTED, ScaleTypeEnum.AMOUNT, outputDiscFive, OutComeModeEnum.SCALE);
        ruleFive.setUseSaleOrderTotalForOutcome(true);

        PriceRule ruleSix = TestUtil.createRuleOnlyProducts(6L, productsSix, false, TargetEnum.PRODUCT,
                PriceRuleTypeEnum.RESTRICTED, ScaleTypeEnum.AMOUNT, outputDiscSix, OutComeModeEnum.SCALE);
        ruleSix.setUseSaleOrderTotalForOutcome(true);

        List<PriceRule> rules = new ArrayList<>(0);
        rules.add(ruleOne);
        rules.add(ruleTwo);
        rules.add(ruleThree);
        rules.add(ruleFour);
        rules.add(ruleFive);
        rules.add(ruleSix);

        PriceComponentRule componentRule = PriceComponentRule.builder()
                .rules(rules)
                .resolutionMode(PriceRuleResolutionModeEnum.BY_BEST_VALUE_PRODUCT)
                .allowSetupLogger(false)
                .build();

        List<PriceComponentRuleInputSaleOrderProduct> results = componentRule.computate(saleOrder4Products);
        PriceComponentRuleInputSaleOrderProduct product44 = results.stream().filter(product -> product.getProduct().getId() == 44).findFirst().orElse(null);
        PriceComponentRuleInputSaleOrderProduct product46 = results.stream().filter(product -> product.getProduct().getId() == 46).findFirst().orElse(null);
        PriceComponentRuleInputSaleOrderProduct product47 = results.stream().filter(product -> product.getProduct().getId() == 47).findFirst().orElse(null);
        PriceComponentRuleInputSaleOrderProduct product48 = results.stream().filter(product -> product.getProduct().getId() == 48).findFirst().orElse(null);

        if (Objects.isNull(product44))
            fail("Product 44 does not exists");
        assertThat(product44.getRulesApplied().get(0).getId(), is(6L));

        if (Objects.isNull(product46))
            fail("Product 46 does not exists");
        assertThat(product46.getRulesApplied().get(0).getId(), is(4L));

        if (Objects.isNull(product47))
            fail("Product 47 does not exists");
        assertThat(product47.getRulesApplied().get(0).getId(), is(4L));

        if (Objects.isNull(product48))
            fail("Product 48 does not exists.");
        assertThat(product48.getRulesApplied().get(0).getId(), is(2L));

        LogUtil.warn("Results: \n" + ToJsonString.toJson(results));
    }
}
