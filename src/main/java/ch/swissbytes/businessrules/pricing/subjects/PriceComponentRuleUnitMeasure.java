package ch.swissbytes.businessrules.pricing.subjects;

import ch.swissbytes.businessrules.pricing.models.BaseModel;

import java.util.Objects;


public class PriceComponentRuleUnitMeasure extends BaseModel {
    private long id;
    private String name;
    private String abbreviation;

    public PriceComponentRuleUnitMeasure(long id, String name, String abbreviation) {
        this.id = id;
        this.name = name;
        this.abbreviation = abbreviation;
    }

    public PriceComponentRuleUnitMeasure() {
    }

    public static PriceComponentRuleUnitMeasureBuilder builder() {
        return new PriceComponentRuleUnitMeasureBuilder();
    }

    public boolean isValid(PriceComponentRuleSubjectProduct product){
        if (Objects.isNull(product.getUnitMeasure()))
            return false;
        return product.getUnitMeasure().getId() == this.id;
    }

    public long getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public String getAbbreviation() {
        return this.abbreviation;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAbbreviation(String abbreviation) {
        this.abbreviation = abbreviation;
    }

    public String toString() {
        return "PriceComponentRuleUnitMeasure(id=" + this.getId() + ", name=" + this.getName() + ", abbreviation=" + this.getAbbreviation() + ")";
    }

    public boolean equals(final Object o) {
        if (o == this) return true;
        if (!(o instanceof PriceComponentRuleUnitMeasure)) return false;
        final PriceComponentRuleUnitMeasure other = (PriceComponentRuleUnitMeasure) o;
        if (!other.canEqual((Object) this)) return false;
        if (this.getId() != other.getId()) return false;
        final Object this$name = this.getName();
        final Object other$name = other.getName();
        if (this$name == null ? other$name != null : !this$name.equals(other$name)) return false;
        final Object this$abbreviation = this.getAbbreviation();
        final Object other$abbreviation = other.getAbbreviation();
        if (this$abbreviation == null ? other$abbreviation != null : !this$abbreviation.equals(other$abbreviation))
            return false;
        return true;
    }

    protected boolean canEqual(final Object other) {
        return other instanceof PriceComponentRuleUnitMeasure;
    }

    public int hashCode() {
        final int PRIME = 59;
        int result = 1;
        final long $id = this.getId();
        result = result * PRIME + (int) ($id >>> 32 ^ $id);
        final Object $name = this.getName();
        result = result * PRIME + ($name == null ? 43 : $name.hashCode());
        final Object $abbreviation = this.getAbbreviation();
        result = result * PRIME + ($abbreviation == null ? 43 : $abbreviation.hashCode());
        return result;
    }

    public static class PriceComponentRuleUnitMeasureBuilder {
        private long id;
        private String name;
        private String abbreviation;

        PriceComponentRuleUnitMeasureBuilder() {
        }

        public PriceComponentRuleUnitMeasure.PriceComponentRuleUnitMeasureBuilder id(long id) {
            this.id = id;
            return this;
        }

        public PriceComponentRuleUnitMeasure.PriceComponentRuleUnitMeasureBuilder name(String name) {
            this.name = name;
            return this;
        }

        public PriceComponentRuleUnitMeasure.PriceComponentRuleUnitMeasureBuilder abbreviation(String abbreviation) {
            this.abbreviation = abbreviation;
            return this;
        }

        public PriceComponentRuleUnitMeasure build() {
            return new PriceComponentRuleUnitMeasure(id, name, abbreviation);
        }

        public String toString() {
            return "PriceComponentRuleUnitMeasure.PriceComponentRuleUnitMeasureBuilder(id=" + this.id + ", name=" + this.name + ", abbreviation=" + this.abbreviation + ")";
        }
    }
}
