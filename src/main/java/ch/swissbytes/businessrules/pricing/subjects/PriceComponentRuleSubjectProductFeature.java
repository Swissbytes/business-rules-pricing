package ch.swissbytes.businessrules.pricing.subjects;

import ch.swissbytes.businessrules.pricing.inputs.PriceComponentRuleInputSaleOrder;
import ch.swissbytes.businessrules.pricing.inputs.PriceComponentRuleInputSaleOrderProduct;
import ch.swissbytes.businessrules.pricing.interfaces.IPriceRuleProductValidator;
import ch.swissbytes.businessrules.pricing.interfaces.ISimplePriceRuleValidator;
import ch.swissbytes.businessrules.pricing.models.BaseModel;


public class PriceComponentRuleSubjectProductFeature extends BaseModel implements ISimplePriceRuleValidator, IPriceRuleProductValidator {
    private long id;
    private String name;
    private String value;

    public PriceComponentRuleSubjectProductFeature(long id, String name, String value) {
        this.id = id;
        this.name = name;
        this.value = value;
    }

    public PriceComponentRuleSubjectProductFeature() {
    }

    public static PriceComponentRuleSubjectProductFeatureBuilder builder() {
        return new PriceComponentRuleSubjectProductFeatureBuilder();
    }

    @Override
    public boolean isValid(PriceComponentRuleInputSaleOrder saleOrder) {
        return saleOrder.getProductDetails().stream().anyMatch(saleOrderProduct ->
                saleOrderProduct.getFeatures().stream().anyMatch(feature -> feature.getId() == this.id) );
    }

    @Override
    public boolean isValidProduct(PriceComponentRuleInputSaleOrderProduct product) {
        return product.getFeatures().stream().anyMatch(feature -> feature.getId() == this.id);
    }

    public long getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public String getValue() {
        return this.value;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String toString() {
        return "PriceComponentRuleSubjectProductFeature(id=" + this.getId() + ", name=" + this.getName() + ", value=" + this.getValue() + ")";
    }

    public boolean equals(final Object o) {
        if (o == this) return true;
        if (!(o instanceof PriceComponentRuleSubjectProductFeature))
            return false;
        final PriceComponentRuleSubjectProductFeature other = (PriceComponentRuleSubjectProductFeature) o;
        if (!other.canEqual((Object) this)) return false;
        if (this.getId() != other.getId()) return false;
        final Object this$name = this.getName();
        final Object other$name = other.getName();
        if (this$name == null ? other$name != null : !this$name.equals(other$name)) return false;
        final Object this$value = this.getValue();
        final Object other$value = other.getValue();
        if (this$value == null ? other$value != null : !this$value.equals(other$value)) return false;
        return true;
    }

    protected boolean canEqual(final Object other) {
        return other instanceof PriceComponentRuleSubjectProductFeature;
    }

    public int hashCode() {
        final int PRIME = 59;
        int result = 1;
        final long $id = this.getId();
        result = result * PRIME + (int) ($id >>> 32 ^ $id);
        final Object $name = this.getName();
        result = result * PRIME + ($name == null ? 43 : $name.hashCode());
        final Object $value = this.getValue();
        result = result * PRIME + ($value == null ? 43 : $value.hashCode());
        return result;
    }

    public static class PriceComponentRuleSubjectProductFeatureBuilder {
        private long id;
        private String name;
        private String value;

        PriceComponentRuleSubjectProductFeatureBuilder() {
        }

        public PriceComponentRuleSubjectProductFeature.PriceComponentRuleSubjectProductFeatureBuilder id(long id) {
            this.id = id;
            return this;
        }

        public PriceComponentRuleSubjectProductFeature.PriceComponentRuleSubjectProductFeatureBuilder name(String name) {
            this.name = name;
            return this;
        }

        public PriceComponentRuleSubjectProductFeature.PriceComponentRuleSubjectProductFeatureBuilder value(String value) {
            this.value = value;
            return this;
        }

        public PriceComponentRuleSubjectProductFeature build() {
            return new PriceComponentRuleSubjectProductFeature(id, name, value);
        }

        public String toString() {
            return "PriceComponentRuleSubjectProductFeature.PriceComponentRuleSubjectProductFeatureBuilder(id=" + this.id + ", name=" + this.name + ", value=" + this.value + ")";
        }
    }
}
