package ch.swissbytes.businessrules.pricing.subjects;

import ch.swissbytes.businessrules.pricing.inputs.PriceComponentRuleInputSaleOrder;
import ch.swissbytes.businessrules.pricing.inputs.PriceComponentRuleInputSaleOrderProduct;
import ch.swissbytes.businessrules.pricing.interfaces.IPriceRuleProductValidator;
import ch.swissbytes.businessrules.pricing.outcomes.PriceComponentRuleOutcomeProduct;

import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;


public class PriceComponentRuleSubjectProduct implements IPriceRuleProductValidator {
    private long id;
    private String code;
    private String name;
    private BigDecimal originalPrice;//base price in most case scenario
    private PriceComponentRuleUnitMeasure unitMeasure;
    private List<PriceComponentRuleOutcomeProduct> optionalProducts;

    private PriceComponentRuleSubjectProduct(long id, String code, String name, BigDecimal originalPrice,
            PriceComponentRuleUnitMeasure unitMeasure, List<PriceComponentRuleOutcomeProduct> optionalProducts) {
        this.id = id;
        this.code = code;
        this.name = name;
        this.originalPrice = originalPrice;
        this.unitMeasure = unitMeasure;
        this.optionalProducts = optionalProducts;
    }

    public PriceComponentRuleSubjectProduct() {
    }

    public static PriceComponentRuleSubjectProductBuilder builder() {
        return new PriceComponentRuleSubjectProductBuilder();
    }

    public boolean isValid(PriceComponentRuleInputSaleOrder saleOrder) {
        return saleOrder.getProductDetails().stream().anyMatch(saleOrderProduct ->
                saleOrderProduct.getProduct().getId() == this.id && this.unitMeasure.isValid(saleOrderProduct.getProduct()));
    }

    @Override
    public boolean isValidProduct(PriceComponentRuleInputSaleOrderProduct product) {
        return product.getProduct().getId() == this.id && this.unitMeasure.isValid(product.getProduct());
    }

    public long getId() {
        return this.id;
    }

    public String getCode() {
        return this.code;
    }

    public String getName() {
        return this.name;
    }

    public BigDecimal getOriginalPrice() {
        return this.originalPrice;
    }

    public PriceComponentRuleUnitMeasure getUnitMeasure() {
        return this.unitMeasure;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setOriginalPrice(BigDecimal originalPrice) {
        this.originalPrice = originalPrice;
    }

    public void setUnitMeasure(PriceComponentRuleUnitMeasure unitMeasure) {
        this.unitMeasure = unitMeasure;
    }

    public List<PriceComponentRuleOutcomeProduct> getOptionalProducts() {
        return optionalProducts;
    }

    public void setOptionalProducts(List<PriceComponentRuleOutcomeProduct> optionalProducts) {
        this.optionalProducts = optionalProducts;
    }

    public String toString() {
        return "PriceComponentRuleSubjectProduct(id=" + this.getId() + ", code=" + this.getCode() + ", name=" + this.getName() + ", originalPrice=" + this.getOriginalPrice() + ", unitMeasure=" + this.getUnitMeasure() + ")";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PriceComponentRuleSubjectProduct that = (PriceComponentRuleSubjectProduct) o;
        return id == that.id &&
                Objects.equals(code, that.code) &&
                Objects.equals(name, that.name) &&
                Objects.equals(originalPrice, that.originalPrice) &&
                Objects.equals(unitMeasure, that.unitMeasure) &&
                Objects.equals(optionalProducts, that.optionalProducts);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, code, name, originalPrice, unitMeasure, optionalProducts);
    }

    public static class PriceComponentRuleSubjectProductBuilder {
        private long id;
        private String code;
        private String name;
        private BigDecimal originalPrice;
        private PriceComponentRuleUnitMeasure unitMeasure;
        private List<PriceComponentRuleOutcomeProduct> optionalProducts;

        PriceComponentRuleSubjectProductBuilder() {
        }

        public PriceComponentRuleSubjectProduct.PriceComponentRuleSubjectProductBuilder id(long id) {
            this.id = id;
            return this;
        }

        public PriceComponentRuleSubjectProduct.PriceComponentRuleSubjectProductBuilder code(String code) {
            this.code = code;
            return this;
        }

        public PriceComponentRuleSubjectProduct.PriceComponentRuleSubjectProductBuilder name(String name) {
            this.name = name;
            return this;
        }

        public PriceComponentRuleSubjectProduct.PriceComponentRuleSubjectProductBuilder originalPrice(BigDecimal originalPrice) {
            this.originalPrice = originalPrice;
            return this;
        }

        public PriceComponentRuleSubjectProduct.PriceComponentRuleSubjectProductBuilder unitMeasure(PriceComponentRuleUnitMeasure unitMeasure) {
            this.unitMeasure = unitMeasure;
            return this;
        }

        public PriceComponentRuleSubjectProduct.PriceComponentRuleSubjectProductBuilder optionalProducts(List<PriceComponentRuleOutcomeProduct> optionalProducts) {
            this.optionalProducts = optionalProducts;
            return this;
        }

        public PriceComponentRuleSubjectProduct build() {
            return new PriceComponentRuleSubjectProduct(id, code, name, originalPrice, unitMeasure, optionalProducts);
        }

        public String toString() {
            return "PriceComponentRuleSubjectProduct.PriceComponentRuleSubjectProductBuilder(id=" + this.id + ", code=" + this.code + ", name=" + this.name + ", originalPrice=" + this.originalPrice + ", unitMeasure=" + this.unitMeasure + ")";
        }
    }
}
