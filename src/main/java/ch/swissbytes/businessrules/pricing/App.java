package ch.swissbytes.businessrules.pricing;

import ch.swissbytes.businessrules.pricing.enums.PriceRuleResolutionModeEnum;
import ch.swissbytes.businessrules.pricing.log.Logger;
import ch.swissbytes.businessrules.pricing.log.LoggerImpl;
import ch.swissbytes.businessrules.pricing.models.PriceComponentRule;
import ch.swissbytes.businessrules.pricing.utils.BundleProvider;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Collections;


public final class App {

    private static final Logger Log = LoggerImpl.getLogger();

    public static void main(String[] args) {
        Log.info("----------------------------------------------------------------");
        Log.info(BundleProvider.getString("appName"));
        Log.info(BundleProvider.getString("appVersion"));
        Log.info(BundleProvider.getString("appInfo"));
        Log.info(BundleProvider.getString("logWrittenBy"));
        Log.info("----------------------------------------------------------------");

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        Log.info(BundleProvider.getString("appTestConfigurations"));
        try {
            String input = br.readLine();
            if (input.equalsIgnoreCase("Y") || input.equalsIgnoreCase("S")){
                Log.info(BundleProvider.getString("appInputConfigFile"));
                String path = br.readLine();
                testConfigurations(path);
            }
        } catch (Exception e){
            Log.error("Error :"+e);
        }
    }

    public static void testConfigurations(String path){
        PriceComponentRule priceComponentRule = PriceComponentRule.builder()
                .rules(Collections.emptyList())
                .resolutionMode(PriceRuleResolutionModeEnum.BY_BEST_VALUE_SALE_ORDER)
                .allowSetupLogger(false)
                .build();
        priceComponentRule.loadConfig(path, true);

    }

}
