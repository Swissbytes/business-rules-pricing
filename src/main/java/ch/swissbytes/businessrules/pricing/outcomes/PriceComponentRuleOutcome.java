package ch.swissbytes.businessrules.pricing.outcomes;

import ch.swissbytes.businessrules.pricing.config.Keys;
import ch.swissbytes.businessrules.pricing.enums.PriceComponentOutcomeTypeEnum;
import ch.swissbytes.businessrules.pricing.inputs.PriceComponentRuleInputSaleOrderProduct;
import ch.swissbytes.businessrules.pricing.models.BaseModel;
import ch.swissbytes.businessrules.pricing.models.PriceComponentRule;
import ch.swissbytes.businessrules.pricing.utils.LogUtil;
import ch.swissbytes.businessrules.pricing.utils.PriceRuleLog;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicReference;


public class PriceComponentRuleOutcome extends BaseModel {

    private long id;
    private PriceComponentOutcomeTypeEnum outcomeType;
    private BigDecimal discountAmount;
    private BigDecimal discountPercentage;
    private BigDecimal surchargeAmount;
    private BigDecimal surchargePercentage;
    private BigDecimal fixedPrice;
    private List<PriceComponentRuleOutcomeProduct> products;

    public PriceComponentRuleOutcome(long id, PriceComponentOutcomeTypeEnum outcomeType, BigDecimal discountAmount, BigDecimal discountPercentage, BigDecimal surchargeAmount, BigDecimal surchargePercentage, BigDecimal fixedPrice, List<PriceComponentRuleOutcomeProduct> products) {
        this.id = id;
        this.outcomeType = outcomeType;
        this.discountAmount = discountAmount;
        this.discountPercentage = discountPercentage;
        this.surchargeAmount = surchargeAmount;
        this.surchargePercentage = surchargePercentage;
        this.fixedPrice = fixedPrice;
        this.products = products;
    }

    public PriceComponentRuleOutcome() {
    }

    public static PriceComponentRuleOutcomeBuilder builder() {
        return new PriceComponentRuleOutcomeBuilder();
    }

    public BigDecimal computateOutcome(BigDecimal totalProducts, List<PriceComponentRuleInputSaleOrderProduct> products) {
        switch (this.outcomeType) {
            case DISCOUNT_AMOUNT:
                return computateOutcomeDiscountAmount(totalProducts);
            case SURCHARGE_AMOUNT:
                return computateOutcomeSurchargeAmount(totalProducts);
            case DISCOUNT_PERCENTAGE:
                return computateOutcomeDiscountPercentage(totalProducts);
            case SURCHARGE_PERCENTAGE:
                return computateOutcomeSurchargePercentage(totalProducts);
            case FIXED_PRICE:
                return computateOutcomeFixedPrice(products);
            case PRODUCT:
            case PRODUCT_SURCHARGE:
                return computateOutcomeProductBonus();
        }
        return BigDecimal.ZERO;
    }


    /**
     * We deprecated this function because it was not useful in the current environment and only be used in some case scenario.
     */
    @Deprecated
    private BigDecimal computateOutcomeDiscountAmount(List<PriceComponentRuleInputSaleOrderProduct> products) {
        AtomicReference<BigDecimal> total = new AtomicReference<>(BigDecimal.ZERO);
        AtomicReference<BigDecimal> totalAmountProducts = new AtomicReference<>(BigDecimal.ZERO);

        products.forEach(product -> {
            BigDecimal subtract = product.getUnitPrice().subtract(this.discountAmount);
            if(subtract.compareTo(BigDecimal.ZERO) < 0){
                subtract = BigDecimal.ZERO;
            }
            total.set(total.get().add(subtract.multiply(product.getQty())));
            totalAmountProducts.set(totalAmountProducts.get().add(product.calculateTotalPriceGross()));
        });

        BigDecimal totalAfterDiscount = (totalAmountProducts.get().subtract(total.get()));
        if (totalAfterDiscount.compareTo(BigDecimal.ZERO) < 0) {
            LogUtil.log(PriceRuleLog.createWarningComputateOutcomeIsSmallerThanZero(totalAfterDiscount, id, outcomeType));
            if (PriceComponentRule.getConfigurations().getBoolean(Keys.OUTCOME_FORCE_ZERO_IF_NEGATIVE))
                return BigDecimal.ZERO;
        }
        return totalAfterDiscount;
    }

    private BigDecimal computateOutcomeDiscountAmount(BigDecimal totalProducts) {
        BigDecimal totalAfterDiscount = (totalProducts.subtract(this.discountAmount));
        if (totalAfterDiscount.compareTo(BigDecimal.ZERO) < 0) {
            LogUtil.log(PriceRuleLog.createWarningComputateOutcomeIsSmallerThanZero(totalAfterDiscount, id, outcomeType));
            if (PriceComponentRule.getConfigurations().getBoolean(Keys.OUTCOME_FORCE_ZERO_IF_NEGATIVE))
                return BigDecimal.ZERO;
        }
        return totalAfterDiscount;
    }

    private BigDecimal computateOutcomeDiscountPercentage(BigDecimal totalProducts) {
        return totalProducts.multiply(this.discountPercentage.divide(new BigDecimal(100), 2, BigDecimal.ROUND_CEILING));
    }

    private BigDecimal computateOutcomeSurchargePercentage(BigDecimal totalProducts) {
        return totalProducts.add(this.surchargePercentage.divide(new BigDecimal(100), 2, BigDecimal.ROUND_CEILING));
    }

    /**
     * We deprecated this function because it was not useful in the current environment and only be used in some case scenario.
     */
    @Deprecated
    private BigDecimal computateOutcomeSurchargeAmount(List<PriceComponentRuleInputSaleOrderProduct> products) {
        AtomicReference<BigDecimal> total = new AtomicReference<>(BigDecimal.ZERO);
        products.forEach(product -> total.set(total.get().add(product.getUnitPrice().add(this.surchargeAmount).multiply(product.getQty()))));
        return total.get();
    }

    private BigDecimal computateOutcomeSurchargeAmount(BigDecimal totalProducts) {
        return totalProducts.add(this.surchargeAmount);
    }

    private BigDecimal computateOutcomeFixedPrice(List<PriceComponentRuleInputSaleOrderProduct> ruleProducts) {
        return ruleProducts.stream().map(product -> this.fixedPrice.multiply(product.getQty())).reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    private BigDecimal computateOutcomeProductBonus() {
        return products.stream().map(PriceComponentRuleOutcomeProduct::calculateTotalPriceGross).reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    /**
     * Clones the outcome. It multiplies the "times" by the outcome value.
     * Fixed price should not have frequency.
     */
    public PriceComponentRuleOutcome copy(BigDecimal times) {
        PriceComponentRuleOutcome clone = this.copy();
        switch (this.outcomeType) {
            case DISCOUNT_AMOUNT:
                clone.setDiscountAmount(this.discountAmount.multiply(times));
                break;
            case SURCHARGE_AMOUNT:
                clone.setSurchargeAmount(this.surchargeAmount.multiply(times));
                break;
            case DISCOUNT_PERCENTAGE:
                clone.setDiscountPercentage(this.discountPercentage.multiply(times));
                break;
            case SURCHARGE_PERCENTAGE:
                clone.setSurchargePercentage(this.surchargePercentage.multiply(times));
                break;
            case FIXED_PRICE:
                LogUtil.log(PriceRuleLog.createWarningFixedPriceTimes());
                break;
            case PRODUCT:
            case PRODUCT_SURCHARGE:
                clone.getProducts().forEach(product-> {
                    LogUtil.warn("Times "+times+" * "+product.getQty());
                    BigDecimal qty = times.multiply(product.getQty());
                    LogUtil.warn("Product: "+product.getId()+" -> "+qty);
                    product.setQty(qty);
                    if (product.getOptionals() != null) {
                        product.getOptionals().forEach(optional -> {
                            optional.setQty(times.multiply(optional.getQty()));
                        });
                    }
                });
                break;
        }
        return clone;
    }

    protected PriceComponentRuleOutcome copy() {
        return PriceComponentRuleOutcome.builder()
                .id(this.id)
                .outcomeType(this.outcomeType)
                .products(cloneProducts(this.products))
                .discountAmount(this.discountAmount)
                .discountPercentage(this.discountPercentage)
                .surchargeAmount(this.surchargeAmount)
                .surchargePercentage(this.surchargePercentage)
                .fixedPrice(this.fixedPrice)
                .build();
    }

    private List<PriceComponentRuleOutcomeProduct> cloneProducts(List<PriceComponentRuleOutcomeProduct> products) {
        if (Objects.isNull(products) || products.isEmpty())
            return products;
        List<PriceComponentRuleOutcomeProduct> productsList = new ArrayList<>(0);
        products.forEach(product -> productsList.add(product.cloneProduct()));
        return  productsList;
    }

    public long getId() {
        return this.id;
    }

    public PriceComponentOutcomeTypeEnum getOutcomeType() {
        return this.outcomeType;
    }

    public BigDecimal getDiscountAmount() {
        return this.discountAmount;
    }

    public BigDecimal getDiscountPercentage() {
        return this.discountPercentage;
    }

    public BigDecimal getSurchargeAmount() {
        return this.surchargeAmount;
    }

    public BigDecimal getSurchargePercentage() {
        return this.surchargePercentage;
    }

    public BigDecimal getFixedPrice() {
        return this.fixedPrice;
    }

    public List<PriceComponentRuleOutcomeProduct> getProducts() {
        return this.products;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setOutcomeType(PriceComponentOutcomeTypeEnum outcomeType) {
        this.outcomeType = outcomeType;
    }

    public void setDiscountAmount(BigDecimal discountAmount) {
        this.discountAmount = discountAmount;
    }

    public void setDiscountPercentage(BigDecimal discountPercentage) {
        this.discountPercentage = discountPercentage;
    }

    public void setSurchargeAmount(BigDecimal surchargeAmount) {
        this.surchargeAmount = surchargeAmount;
    }

    public void setSurchargePercentage(BigDecimal surchargePercentage) {
        this.surchargePercentage = surchargePercentage;
    }

    public void setFixedPrice(BigDecimal fixedPrice) {
        this.fixedPrice = fixedPrice;
    }

    public void setProducts(List<PriceComponentRuleOutcomeProduct> products) {
        this.products = products;
    }

    public String toString() {
        return "PriceComponentRuleOutcome(id=" + this.getId() + ", outcomeType=" + this.getOutcomeType() + ", discountAmount=" + this.getDiscountAmount() + ", discountPercentage=" + this.getDiscountPercentage() + ", surchargeAmount=" + this.getSurchargeAmount() + ", surchargePercentage=" + this.getSurchargePercentage() + ", fixedPrice=" + this.getFixedPrice() + ", products=" + this.getProducts() + ")";
    }

    public boolean equals(final Object o) {
        if (o == this) return true;
        if (!(o instanceof PriceComponentRuleOutcome)) return false;
        final PriceComponentRuleOutcome other = (PriceComponentRuleOutcome) o;
        if (!other.canEqual((Object) this)) return false;
        if (this.getId() != other.getId()) return false;
        final Object this$outcomeType = this.getOutcomeType();
        final Object other$outcomeType = other.getOutcomeType();
        if (this$outcomeType == null ? other$outcomeType != null : !this$outcomeType.equals(other$outcomeType))
            return false;
        final Object this$discountAmount = this.getDiscountAmount();
        final Object other$discountAmount = other.getDiscountAmount();
        if (this$discountAmount == null ? other$discountAmount != null : !this$discountAmount.equals(other$discountAmount))
            return false;
        final Object this$discountPercentage = this.getDiscountPercentage();
        final Object other$discountPercentage = other.getDiscountPercentage();
        if (this$discountPercentage == null ? other$discountPercentage != null : !this$discountPercentage.equals(other$discountPercentage))
            return false;
        final Object this$surchargeAmount = this.getSurchargeAmount();
        final Object other$surchargeAmount = other.getSurchargeAmount();
        if (this$surchargeAmount == null ? other$surchargeAmount != null : !this$surchargeAmount.equals(other$surchargeAmount))
            return false;
        final Object this$surchargePercentage = this.getSurchargePercentage();
        final Object other$surchargePercentage = other.getSurchargePercentage();
        if (this$surchargePercentage == null ? other$surchargePercentage != null : !this$surchargePercentage.equals(other$surchargePercentage))
            return false;
        final Object this$fixedPrice = this.getFixedPrice();
        final Object other$fixedPrice = other.getFixedPrice();
        if (this$fixedPrice == null ? other$fixedPrice != null : !this$fixedPrice.equals(other$fixedPrice))
            return false;
        final Object this$products = this.getProducts();
        final Object other$products = other.getProducts();
        if (this$products == null ? other$products != null : !this$products.equals(other$products)) return false;
        return true;
    }

    protected boolean canEqual(final Object other) {
        return other instanceof PriceComponentRuleOutcome;
    }

    public int hashCode() {
        final int PRIME = 59;
        int result = 1;
        final long $id = this.getId();
        result = result * PRIME + (int) ($id >>> 32 ^ $id);
        final Object $outcomeType = this.getOutcomeType();
        result = result * PRIME + ($outcomeType == null ? 43 : $outcomeType.hashCode());
        final Object $discountAmount = this.getDiscountAmount();
        result = result * PRIME + ($discountAmount == null ? 43 : $discountAmount.hashCode());
        final Object $discountPercentage = this.getDiscountPercentage();
        result = result * PRIME + ($discountPercentage == null ? 43 : $discountPercentage.hashCode());
        final Object $surchargeAmount = this.getSurchargeAmount();
        result = result * PRIME + ($surchargeAmount == null ? 43 : $surchargeAmount.hashCode());
        final Object $surchargePercentage = this.getSurchargePercentage();
        result = result * PRIME + ($surchargePercentage == null ? 43 : $surchargePercentage.hashCode());
        final Object $fixedPrice = this.getFixedPrice();
        result = result * PRIME + ($fixedPrice == null ? 43 : $fixedPrice.hashCode());
        final Object $products = this.getProducts();
        result = result * PRIME + ($products == null ? 43 : $products.hashCode());
        return result;
    }

    public static class PriceComponentRuleOutcomeBuilder {
        private long id;
        private PriceComponentOutcomeTypeEnum outcomeType;
        private BigDecimal discountAmount;
        private BigDecimal discountPercentage;
        private BigDecimal surchargeAmount;
        private BigDecimal surchargePercentage;
        private BigDecimal fixedPrice;
        private List<PriceComponentRuleOutcomeProduct> products;

        PriceComponentRuleOutcomeBuilder() {
        }

        public PriceComponentRuleOutcome.PriceComponentRuleOutcomeBuilder id(long id) {
            this.id = id;
            return this;
        }

        public PriceComponentRuleOutcome.PriceComponentRuleOutcomeBuilder outcomeType(PriceComponentOutcomeTypeEnum outcomeType) {
            this.outcomeType = outcomeType;
            return this;
        }

        public PriceComponentRuleOutcome.PriceComponentRuleOutcomeBuilder discountAmount(BigDecimal discountAmount) {
            this.discountAmount = discountAmount;
            return this;
        }

        public PriceComponentRuleOutcome.PriceComponentRuleOutcomeBuilder discountPercentage(BigDecimal discountPercentage) {
            this.discountPercentage = discountPercentage;
            return this;
        }

        public PriceComponentRuleOutcome.PriceComponentRuleOutcomeBuilder surchargeAmount(BigDecimal surchargeAmount) {
            this.surchargeAmount = surchargeAmount;
            return this;
        }

        public PriceComponentRuleOutcome.PriceComponentRuleOutcomeBuilder surchargePercentage(BigDecimal surchargePercentage) {
            this.surchargePercentage = surchargePercentage;
            return this;
        }

        public PriceComponentRuleOutcome.PriceComponentRuleOutcomeBuilder fixedPrice(BigDecimal fixedPrice) {
            this.fixedPrice = fixedPrice;
            return this;
        }

        public PriceComponentRuleOutcome.PriceComponentRuleOutcomeBuilder products(List<PriceComponentRuleOutcomeProduct> products) {
            this.products = products;
            return this;
        }

        public PriceComponentRuleOutcome build() {
            return new PriceComponentRuleOutcome(id, outcomeType, discountAmount, discountPercentage, surchargeAmount, surchargePercentage, fixedPrice, products);
        }

        public String toString() {
            return "PriceComponentRuleOutcome.PriceComponentRuleOutcomeBuilder(id=" + this.id + ", outcomeType=" + this.outcomeType + ", discountAmount=" + this.discountAmount + ", discountPercentage=" + this.discountPercentage + ", surchargeAmount=" + this.surchargeAmount + ", surchargePercentage=" + this.surchargePercentage + ", fixedPrice=" + this.fixedPrice + ", products=" + this.products + ")";
        }
    }
}
