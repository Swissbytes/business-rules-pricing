package ch.swissbytes.businessrules.pricing.outcomes;

import ch.swissbytes.businessrules.pricing.enums.OutComeModeEnum;
import ch.swissbytes.businessrules.pricing.enums.PriceComponentOutcomeTypeEnum;
import ch.swissbytes.businessrules.pricing.enums.ScaleTypeEnum;
import ch.swissbytes.businessrules.pricing.factors.PriceComponentRuleFactorScale;
import ch.swissbytes.businessrules.pricing.models.BaseModel;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;


public class PriceComponentRuleOutcomeOutput extends BaseModel {

    private long id;

    private OutComeModeEnum outComeModeEnum = OutComeModeEnum.SCALE;

    private ScaleTypeEnum validationType = ScaleTypeEnum.AMOUNT;

    private List<PriceComponentRuleFactorScale> scales = new ArrayList<>(0);

    // it indicates that each time the value is reached, the outcomeType will be triggered.
    private BigDecimal frequency = BigDecimal.ZERO;

    private PriceComponentOutcomeTypeEnum outcomeType = PriceComponentOutcomeTypeEnum.DISCOUNT_PERCENTAGE;

    private PriceComponentRuleOutcome outcome;

    public PriceComponentRuleOutcomeOutput(long id, OutComeModeEnum outComeModeEnum, ScaleTypeEnum validationType, List<PriceComponentRuleFactorScale> scales, BigDecimal frequency, PriceComponentOutcomeTypeEnum outcomeType, PriceComponentRuleOutcome outcome) {
        this.id = id;
        this.outComeModeEnum = outComeModeEnum;
        this.validationType = validationType;
        this.scales = scales;
        this.frequency = frequency;
        this.outcomeType = outcomeType;
        this.outcome = outcome;
    }

    public PriceComponentRuleOutcomeOutput() {
    }

    public static PriceComponentRuleOutcomeOutputBuilder builder() {
        return new PriceComponentRuleOutcomeOutputBuilder();
    }


    public PriceComponentRuleOutcome getOutcomeScale(PriceComponentRuleFactorScale scale) {
        if (Objects.isNull(scale)) {
            return null;
        }
        Optional<PriceComponentRuleFactorScale> optional = scales.stream().filter(priceComponentRuleFactorScale -> priceComponentRuleFactorScale.getId() == scale.getId()).findFirst();
        return optional.map(PriceComponentRuleFactorScale::getOutcome).orElse(null);
    }

    public PriceComponentRuleOutcome getOutcomeFrequency(BigDecimal times) {
        return this.outcome.copy(times);
    }

    public PriceComponentRuleOutcome getOutcome() {
        return outcome;
    }

    public boolean equals(final Object o) {
        if (o == this) return true;
        if (!(o instanceof PriceComponentRuleOutcomeOutput)) return false;
        final PriceComponentRuleOutcomeOutput other = (PriceComponentRuleOutcomeOutput) o;
        if (!other.canEqual((Object) this)) return false;
        if (this.id != other.id) return false;
        final Object this$outComeModeEnum = this.outComeModeEnum;
        final Object other$outComeModeEnum = other.outComeModeEnum;
        if (this$outComeModeEnum == null ? other$outComeModeEnum != null : !this$outComeModeEnum.equals(other$outComeModeEnum))
            return false;
        final Object this$validationType = this.validationType;
        final Object other$validationType = other.validationType;
        if (this$validationType == null ? other$validationType != null : !this$validationType.equals(other$validationType))
            return false;
        final Object this$scales = this.scales;
        final Object other$scales = other.scales;
        if (this$scales == null ? other$scales != null : !this$scales.equals(other$scales)) return false;
        final Object this$frequency = this.frequency;
        final Object other$frequency = other.frequency;
        if (this$frequency == null ? other$frequency != null : !this$frequency.equals(other$frequency)) return false;
        final Object this$outcomeType = this.outcomeType;
        final Object other$outcomeType = other.outcomeType;
        if (this$outcomeType == null ? other$outcomeType != null : !this$outcomeType.equals(other$outcomeType))
            return false;
        final Object this$outcome = this.getOutcome();
        final Object other$outcome = other.getOutcome();
        if (this$outcome == null ? other$outcome != null : !this$outcome.equals(other$outcome)) return false;
        return true;
    }

    protected boolean canEqual(final Object other) {
        return other instanceof PriceComponentRuleOutcomeOutput;
    }

    public int hashCode() {
        final int PRIME = 59;
        int result = 1;
        final long $id = this.id;
        result = result * PRIME + (int) ($id >>> 32 ^ $id);
        final Object $outComeModeEnum = this.outComeModeEnum;
        result = result * PRIME + ($outComeModeEnum == null ? 43 : $outComeModeEnum.hashCode());
        final Object $validationType = this.validationType;
        result = result * PRIME + ($validationType == null ? 43 : $validationType.hashCode());
        final Object $scales = this.scales;
        result = result * PRIME + ($scales == null ? 43 : $scales.hashCode());
        final Object $frequency = this.frequency;
        result = result * PRIME + ($frequency == null ? 43 : $frequency.hashCode());
        final Object $outcomeType = this.outcomeType;
        result = result * PRIME + ($outcomeType == null ? 43 : $outcomeType.hashCode());
        final Object $outcome = this.getOutcome();
        result = result * PRIME + ($outcome == null ? 43 : $outcome.hashCode());
        return result;
    }

    public long getId() {
        return this.id;
    }

    public OutComeModeEnum getOutComeModeEnum() {
        return this.outComeModeEnum;
    }

    public ScaleTypeEnum getValidationType() {
        return this.validationType;
    }

    public List<PriceComponentRuleFactorScale> getScales() {
        return this.scales;
    }

    public BigDecimal getFrequency() {
        return this.frequency;
    }

    public PriceComponentOutcomeTypeEnum getOutcomeType() {
        return this.outcomeType;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setOutComeModeEnum(OutComeModeEnum outComeModeEnum) {
        this.outComeModeEnum = outComeModeEnum;
    }

    public void setValidationType(ScaleTypeEnum validationType) {
        this.validationType = validationType;
    }

    public void setScales(List<PriceComponentRuleFactorScale> scales) {
        this.scales = scales;
    }

    public void setFrequency(BigDecimal frequency) {
        this.frequency = frequency;
    }

    public void setOutcomeType(PriceComponentOutcomeTypeEnum outcomeType) {
        this.outcomeType = outcomeType;
    }

    public void setOutcome(PriceComponentRuleOutcome outcome) {
        this.outcome = outcome;
    }

    public static class PriceComponentRuleOutcomeOutputBuilder {
        private long id;
        private OutComeModeEnum outComeModeEnum = OutComeModeEnum.SCALE;
        private ScaleTypeEnum validationType = ScaleTypeEnum.AMOUNT;
        private List<PriceComponentRuleFactorScale> scales = new ArrayList<>(0);
        private BigDecimal frequency = BigDecimal.ZERO;
        private PriceComponentOutcomeTypeEnum outcomeType = PriceComponentOutcomeTypeEnum.DISCOUNT_PERCENTAGE;
        private PriceComponentRuleOutcome outcome;

        PriceComponentRuleOutcomeOutputBuilder() {
        }

        public PriceComponentRuleOutcomeOutput.PriceComponentRuleOutcomeOutputBuilder id(long id) {
            this.id = id;
            return this;
        }

        public PriceComponentRuleOutcomeOutput.PriceComponentRuleOutcomeOutputBuilder outComeModeEnum(OutComeModeEnum outComeModeEnum) {
            this.outComeModeEnum = outComeModeEnum;
            return this;
        }

        public PriceComponentRuleOutcomeOutput.PriceComponentRuleOutcomeOutputBuilder validationType(ScaleTypeEnum validationType) {
            this.validationType = validationType;
            return this;
        }

        public PriceComponentRuleOutcomeOutput.PriceComponentRuleOutcomeOutputBuilder scales(List<PriceComponentRuleFactorScale> scales) {
            this.scales = scales;
            return this;
        }

        public PriceComponentRuleOutcomeOutput.PriceComponentRuleOutcomeOutputBuilder frequency(BigDecimal frequency) {
            this.frequency = frequency;
            return this;
        }

        public PriceComponentRuleOutcomeOutput.PriceComponentRuleOutcomeOutputBuilder outcomeType(PriceComponentOutcomeTypeEnum outcomeType) {
            this.outcomeType = outcomeType;
            return this;
        }

        public PriceComponentRuleOutcomeOutput.PriceComponentRuleOutcomeOutputBuilder outcome(PriceComponentRuleOutcome outcome) {
            this.outcome = outcome;
            return this;
        }

        public PriceComponentRuleOutcomeOutput build() {
            return new PriceComponentRuleOutcomeOutput(id, outComeModeEnum, validationType, scales, frequency, outcomeType, outcome);
        }

        public String toString() {
            return "PriceComponentRuleOutcomeOutput.PriceComponentRuleOutcomeOutputBuilder(id=" + this.id + ", outComeModeEnum=" + this.outComeModeEnum + ", validationType=" + this.validationType + ", scales=" + this.scales + ", frequency=" + this.frequency + ", outcomeType=" + this.outcomeType + ", outcome=" + this.outcome + ")";
        }
    }
}
