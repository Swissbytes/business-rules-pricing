package ch.swissbytes.businessrules.pricing.outcomes;

import ch.swissbytes.businessrules.pricing.models.BaseModel;
import ch.swissbytes.businessrules.pricing.subjects.PriceComponentRuleSubjectProduct;
import ch.swissbytes.businessrules.pricing.subjects.PriceComponentRuleUnitMeasure;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;


public class PriceComponentRuleOutcomeProduct extends BaseModel {
    private long id;
    private String code;
    private String name;
    private BigDecimal originalPrice;
    private PriceComponentRuleUnitMeasure unitMeasure;
    private BigDecimal qty;
    private List<PriceComponentRuleOutcomeProduct> optionals;

    private PriceComponentRuleOutcomeProduct(long id, String code, String name, BigDecimal originalPrice,
            PriceComponentRuleUnitMeasure unitMeasure, BigDecimal qty, List<PriceComponentRuleOutcomeProduct> optionals) {
        this.id = id;
        this.code = code;
        this.name = name;
        this.originalPrice = originalPrice;
        this.unitMeasure = unitMeasure;
        this.qty = qty;
        this.optionals = optionals;
    }

    public PriceComponentRuleOutcomeProduct() {
    }

    public static PriceComponentRuleOutcomeProductBuilder builder() {
        return new PriceComponentRuleOutcomeProductBuilder();
    }

    public PriceComponentRuleSubjectProduct toSaleOrderProduct() {
        return PriceComponentRuleSubjectProduct.builder()
                .id(id)
                .code(code)
                .name(name)
                .unitMeasure(unitMeasure)
                .originalPrice(originalPrice)
                .optionalProducts(optionals)
                .build();
    }

    public BigDecimal calculateTotalPriceGross() {
        if (Objects.isNull(this.originalPrice))
            return BigDecimal.ZERO;
        else
            return originalPrice.multiply(qty);
    }

//    public PriceComponentRuleOutcomeProduct cloneProduct() {
//        return PriceComponentRuleOutcomeProduct.builder()
//                .id(this.id)
//                .code(this.code)
//                .name(this.name)
//                .unitMeasure(this.unitMeasure)
//                .originalPrice(this.originalPrice)
//                .qty(this.qty)
//                .optionals(this.optionals)
//                .build();
//    }

    public PriceComponentRuleOutcomeProduct cloneProduct() {
        PriceComponentRuleOutcomeProduct clonedProduct = cloneOutcomeProduct(this);
        if (this.optionals != null) {
            clonedProduct.setOptionals(this.optionals.stream().map(this::cloneOutcomeProduct).collect(Collectors.toList()));
        }
        return clonedProduct;
    }
//
    public PriceComponentRuleOutcomeProduct cloneOutcomeProduct(PriceComponentRuleOutcomeProduct outcomeProduct) {
        return PriceComponentRuleOutcomeProduct.builder()
                .id(outcomeProduct.id)
                .code(outcomeProduct.code)
                .name(outcomeProduct.name)
                .unitMeasure(outcomeProduct.unitMeasure)
                .originalPrice(outcomeProduct.originalPrice)
                .qty(outcomeProduct.qty)
                .optionals(outcomeProduct.optionals)
                .build();
    }

    public long getId() {
        return this.id;
    }

    public String getCode() {
        return this.code;
    }

    public String getName() {
        return this.name;
    }

    public BigDecimal getOriginalPrice() {
        return this.originalPrice;
    }

    public PriceComponentRuleUnitMeasure getUnitMeasure() {
        return this.unitMeasure;
    }

    public BigDecimal getQty() {
        return this.qty;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setOriginalPrice(BigDecimal originalPrice) {
        this.originalPrice = originalPrice;
    }

    public void setUnitMeasure(PriceComponentRuleUnitMeasure unitMeasure) {
        this.unitMeasure = unitMeasure;
    }

    public void setQty(BigDecimal qty) {
        this.qty = qty;
    }

    public List<PriceComponentRuleOutcomeProduct> getOptionals() {
        return optionals;
    }

    public void setOptionals(List<PriceComponentRuleOutcomeProduct> optionals) {
        this.optionals = optionals;
    }

    public String toString() {
        return "PriceComponentRuleOutcomeProduct(id=" + this.getId() + ", code=" + this.getCode() + ", name=" + this.getName() + ", originalPrice=" + this.getOriginalPrice() + ", unitMeasure=" + this.getUnitMeasure() + ", qty=" + this.getQty() + ")";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PriceComponentRuleOutcomeProduct that = (PriceComponentRuleOutcomeProduct) o;
        return id == that.id &&
                Objects.equals(code, that.code) &&
                Objects.equals(name, that.name) &&
                Objects.equals(originalPrice, that.originalPrice) &&
                Objects.equals(unitMeasure, that.unitMeasure) &&
                Objects.equals(qty, that.qty) &&
                Objects.equals(optionals, that.optionals);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, code, name, originalPrice, unitMeasure, qty, optionals);
    }

    public static class PriceComponentRuleOutcomeProductBuilder {
        private long id;
        private String code;
        private String name;
        private BigDecimal originalPrice;
        private PriceComponentRuleUnitMeasure unitMeasure;
        private BigDecimal qty;
        private List<PriceComponentRuleOutcomeProduct> optionals;

        PriceComponentRuleOutcomeProductBuilder() {
        }

        public PriceComponentRuleOutcomeProduct.PriceComponentRuleOutcomeProductBuilder id(long id) {
            this.id = id;
            return this;
        }

        public PriceComponentRuleOutcomeProduct.PriceComponentRuleOutcomeProductBuilder code(String code) {
            this.code = code;
            return this;
        }

        public PriceComponentRuleOutcomeProduct.PriceComponentRuleOutcomeProductBuilder name(String name) {
            this.name = name;
            return this;
        }

        public PriceComponentRuleOutcomeProduct.PriceComponentRuleOutcomeProductBuilder originalPrice(BigDecimal originalPrice) {
            this.originalPrice = originalPrice;
            return this;
        }

        public PriceComponentRuleOutcomeProduct.PriceComponentRuleOutcomeProductBuilder unitMeasure(PriceComponentRuleUnitMeasure unitMeasure) {
            this.unitMeasure = unitMeasure;
            return this;
        }

        public PriceComponentRuleOutcomeProduct.PriceComponentRuleOutcomeProductBuilder qty(BigDecimal qty) {
            this.qty = qty;
            return this;
        }

        public PriceComponentRuleOutcomeProduct.PriceComponentRuleOutcomeProductBuilder optionals(List<PriceComponentRuleOutcomeProduct> optionals) {
            this.optionals = optionals;
            return this;
        }

        public PriceComponentRuleOutcomeProduct build() {
            return new PriceComponentRuleOutcomeProduct(id, code, name, originalPrice, unitMeasure, qty, optionals);
        }

        public String toString() {
            return "PriceComponentRuleOutcomeProduct.PriceComponentRuleOutcomeProductBuilder(id=" + this.id + ", code=" +
                    this.code + ", name=" + this.name + ", originalPrice=" + this.originalPrice + ", unitMeasure=" +
                    this.unitMeasure + ", qty=" + this.qty + ")";
        }
    }
}
