package ch.swissbytes.businessrules.pricing.models;

import ch.swissbytes.businessrules.pricing.config.Config;
import ch.swissbytes.businessrules.pricing.config.ConfigKey;
import ch.swissbytes.businessrules.pricing.config.Keys;
import ch.swissbytes.businessrules.pricing.enums.ExclusiveOutcomeEnum;
import ch.swissbytes.businessrules.pricing.enums.OutComeModeEnum;
import ch.swissbytes.businessrules.pricing.enums.PriceComponentOutcomeTypeEnum;
import ch.swissbytes.businessrules.pricing.enums.PriceRuleResolutionModeEnum;
import ch.swissbytes.businessrules.pricing.factors.PriceComponentRuleFactorScale;
import ch.swissbytes.businessrules.pricing.inputs.PriceComponentRuleInputSaleOrder;
import ch.swissbytes.businessrules.pricing.inputs.PriceComponentRuleInputSaleOrderProduct;
import ch.swissbytes.businessrules.pricing.outcomes.PriceComponentRuleOutcome;
import ch.swissbytes.businessrules.pricing.utils.BigDecimalUtil;
import ch.swissbytes.businessrules.pricing.utils.BundleProvider;
import ch.swissbytes.businessrules.pricing.utils.LogUtil;
import ch.swissbytes.businessrules.pricing.utils.PriceRuleLog;
import org.jeasy.rules.api.Facts;

import java.io.IOException;
import java.math.BigDecimal;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Function;
import java.util.stream.Collectors;

import static ch.swissbytes.businessrules.pricing.utils.BigDecimalUtil.isGreaterThanZero;


public class PriceComponentRule extends BaseModel {

    private static Config config;
    //the only input
    private List<PriceRule> rules;

    private PriceRuleResolutionModeEnum resolutionMode;

    private boolean allowSetupLogger;

    //resolution
    private List<PriceRule> applicableRules;

    private List<PriceRule> bestRules;

    private List<PriceComponentRuleInputSaleOrderProduct> results;

    private List<PriceRuleLog> logHistory;

    public PriceComponentRule(List<PriceRule> rules,
                              PriceRuleResolutionModeEnum resolutionMode,
                              boolean allowSetupLogger) {
        this.allowSetupLogger = allowSetupLogger;
        this.loadConfig();
        this.rules = rules;
        this.validateRules();

        if (Objects.isNull(resolutionMode))
            this.resolutionMode = PriceRuleResolutionModeEnum.BY_BEST_VALUE_SALE_ORDER;
        this.resolutionMode = resolutionMode;

        this.applicableRules = new ArrayList<>(0);
        this.bestRules = new ArrayList<>(0);
        this.results = new ArrayList<>(0);
        this.logHistory = new ArrayList<>(0);
    }

    public static Config getConfigurations() {
        if (Objects.isNull(config)) {
            config = Config.builder().build();
        }
        return config;
    }

    public static PriceComponentRuleBuilder builder() {
        return new PriceComponentRuleBuilder();
    }

    public void setAllowSetupLogger(boolean allowSetupLogger) {
        this.allowSetupLogger = allowSetupLogger;
        if (this.allowSetupLogger)
            LogUtil.setupLogger(config);
    }

    private void loadConfig() {
        config = Config.builder().build();
        if (this.allowSetupLogger)
            LogUtil.setupLogger(config);
    }

    public void loadConfig(String path, boolean allowSetupLogger) {
        config.loadConfigurations(path);
        if (allowSetupLogger)
            LogUtil.setupLogger(config);
    }


    public void validateRules() {
        this.rules.forEach(this::validateRule);
    }

    private void validateRule(PriceRule priceRule) {
        if (Objects.isNull(priceRule.getName()))
            throw new RuntimeException("No name defined in the rule: " + priceRule.getId());
        if (Objects.isNull(priceRule.getOutput()))
            throw new RuntimeException("No output defined in the rule: " + priceRule.getId());
        if (Objects.isNull(priceRule.getTarget()))
            throw new RuntimeException("No target defined in the rule: " + priceRule.getId());
        if (Objects.isNull(priceRule.getOutComeMode()))
            throw new RuntimeException("Outcome mode not found in the rule: " + priceRule.getId());
        if (Objects.isNull(priceRule.getValidationType()))
            LogUtil.warn("No validation type defined in the rule: " + priceRule.getId());
        if (priceRule.getOutComeMode() == OutComeModeEnum.SINGLE && Objects.isNull(priceRule.getOutput().getOutcome())) {
            throw new RuntimeException(BundleProvider.getString("logNoOutcomeDefined"));
        }
    }

    /**
     * return the list of elements modified or added to the detail of the saleOrder.
     */
    public List<PriceComponentRuleInputSaleOrderProduct> computate(PriceComponentRuleInputSaleOrder saleOrder) {
        if (config.getBoolean(Keys.CLEAR_CACHE_ON_COMPUTATE_ENABLE))
            this.clearCache();
        this.addToHistoryLog(PriceRuleLog.createBeginComputate(rules));
        this.addToHistoryLog(PriceRuleLog.createInputSaleOrder(saleOrder));
        applicableRules = new ArrayList<>();
        rules.stream().filter(priceRule -> priceRule.isApplicable(saleOrder))
                .forEach(priceRule -> applicableRules.add(priceRule));
        this.addToHistoryLog(PriceRuleLog.createApplicableRules(applicableRules));
        switch (resolutionMode) {
            case BY_PRIORITY:
                return computateByPriority(saleOrder);
            case BY_BEST_VALUE_PRODUCT:
                return compateByBestValueProduct(saleOrder);
            case BY_BEST_VALUE_SALE_ORDER:
            default:
                return computateByBestValueSaleOrder(saleOrder);
        }
    }

    @Deprecated
    private List<PriceComponentRuleInputSaleOrderProduct> computateByPriority(PriceComponentRuleInputSaleOrder saleOrder) {
        Facts factors = new Facts();
        factors.put("saleOrder", saleOrder);
        PriceRule bestRule = applicableRules.stream().max(Comparator.comparing(PriceRule::getPriority)).orElseThrow(() -> new RuntimeException("could not find the bestRule"));
        this.addToHistoryLog(PriceRuleLog.createBestRuleFound(bestRule));
        applicableRules = applicableRules.stream().filter(PriceRule::isAccumulative).collect(Collectors.toList());
        if (!applicableRules.contains(bestRule)) {
            applicableRules.add(bestRule);
        }
        List<PriceComponentRuleInputSaleOrderProduct> detailAfterComputateRules = new ArrayList<>();
        applicableRules.stream().sorted((o1, o2) -> (int) (o1.getPriority() > o2.getPriority() ? o1.getPriority() : o2.getPriority())).forEach(priceRule -> {
            List<PriceComponentRuleInputSaleOrderProduct> detail = priceRule.apply(saleOrder);
            detailAfterComputateRules.addAll(detail);
        });
        return detailAfterComputateRules.stream()
                .distinct()
                .collect(Collectors.toList());
    }

    private List<PriceComponentRuleInputSaleOrderProduct> computateByBestValueSaleOrder(PriceComponentRuleInputSaleOrder saleOrder) {
        Facts factors = new Facts();
        factors.put("saleOrder", saleOrder);
        results = new ArrayList<>(0);
        bestRules = this.findBestRulesByBestValueBySaleOrder(saleOrder);
        bestRules.removeAll(Collections.singleton(null));
        this.addToHistoryLog(PriceRuleLog.createFoundBestRulesByBestValue(bestRules));
        bestRules.forEach(priceRule -> results.addAll(priceRule.apply(saleOrder)));
        return results.stream().distinct().collect(Collectors.toList());
    }

    private List<PriceComponentRuleInputSaleOrderProduct> compateByBestValueProduct(PriceComponentRuleInputSaleOrder saleOrder) {
        results = this.findBestRulesByBestValueByProductAndApply(saleOrder);
        return results;
    }

    /**
     * Finds the best rules by best value in the whole Sale Order. It returns All Best Rules according to their type:
     * If you want to return the greatest value or the smallest you can configure this in the xml configurations file.
     * Discounts in Percentage: Returns the rule with the max or min value (depending of the configurations, by default is max).
     * Discounts in Amount: Returns the rule with the max or min value (depending of the configurations, by default is max).
     * Bonus Product: after calculate the total amount of each product, returns the rule with the max or min value or min value (depending of the configurations, by default is max).
     * Surcharge Product: after calculate the total amount of each product, returns the rule with the max or min value or min value (depending of the configurations, by default is max).
     * Fixed Price: returns the rule with the min or max value in fixed price (depending of the configurations, by default is min).
     * Surcharge Percentage: Returns the rule with the min or max value (depending of the configurations, by default is max).
     * Surcharge Amount: Returns the rule with the min or max value (depending of the configurations, by default is max).
     */
    private List<PriceRule> findBestRulesByBestValueBySaleOrder(PriceComponentRuleInputSaleOrder saleOrder) {
        List<PriceRule> bestRulesFound = new ArrayList<>();

        Comparator<PriceRule> bestValueComparator = Comparator.comparing(priceRule -> computateOutcome(priceRule, saleOrder));

        //region Discount percentage
        bestRulesFound.addAll(findBestDiscountPercentageRule(bestValueComparator, saleOrder));
        //endregion

        //region Discount Amount
        bestRulesFound.addAll(findBestDiscountAmountRule(bestValueComparator, saleOrder));
        //endregion

        //region Surcharge Percentage
        bestRulesFound.addAll(findBestSurchargePercentageRule(bestValueComparator, saleOrder));
        //endregion

        //region Surcharge Amount
        bestRulesFound.addAll(findBestSurchargeAmountRule(bestValueComparator, saleOrder));
        //endregion

        //region fixed Price
        bestRulesFound.add(findBestFixedPriceRule(bestValueComparator));
        //endregion

        //region product bonus
        bestRulesFound.addAll(findBestBonusProductPriceRule(bestValueComparator, saleOrder));
        //endregion

        //region product surcharge
        bestRulesFound.addAll(findBestSurchargeProductPriceRule(bestValueComparator, saleOrder));
        //endregion

        return bestRulesFound;
    }

    /**
     * It Finds and applies the best rules by best value by product in the sale Order. It returns All Best Rules according to their type.
     * Like we said before, If you want to return the greatest value or the smallest you can configure this in the xml configurations file.
     */
    private List<PriceComponentRuleInputSaleOrderProduct> findBestRulesByBestValueByProductAndApply(PriceComponentRuleInputSaleOrder saleOrder) {

        List<PriceComponentRuleInputSaleOrderProduct> results = new ArrayList<>(0);
        HashMap<PriceComponentRuleInputSaleOrderProduct, List<PriceRule>> bestRulesByProductFound = new HashMap<>();

        saleOrder.getProductDetails().forEach(
                product -> {
                    Comparator<PriceRule> bestValueComparator = Comparator.comparing(priceRule -> computateOutcomeByProduct(priceRule, saleOrder, product));
                    Function<PriceRule, BigDecimal> computateOutcomeByProductFn = priceRule -> computateOutcomeByProduct(priceRule, saleOrder, product);
                    List<PriceRule> rules = new ArrayList<>(0);

                    rules.addAll(findBestDiscountPercentageRuleByProduct(bestValueComparator, saleOrder, product, computateOutcomeByProductFn));
                    rules.addAll(findBestDiscountAmountRuleByProduct(bestValueComparator, saleOrder, product, computateOutcomeByProductFn));
                    rules.addAll(findBestSurchargePercentageRuleByProduct(bestValueComparator, saleOrder, product, computateOutcomeByProductFn));
                    rules.addAll(findBestSurchargeAmountRuleByProduct(bestValueComparator, saleOrder, product, computateOutcomeByProductFn));
                    rules.add(findBestFixedPriceRuleByProduct(bestValueComparator, saleOrder, product, computateOutcomeByProductFn));
                    rules.addAll(findBestBonusProductPriceRuleByProduct(bestValueComparator, saleOrder, product, computateOutcomeByProductFn));
                    rules.addAll(findBestSurchargeProductPriceRuleByProduct(bestValueComparator, saleOrder, product, computateOutcomeByProductFn));

                    bestRulesByProductFound.put(product, filterByExclusiveOutcomeEnum(rules));

                });
        List<PriceRule> finalBestRulesFound = bestRules;
        bestRulesByProductFound.forEach((productId, priceRules) -> priceRules.forEach(priceRule -> {
            if (Objects.nonNull(priceRule)) {
                if (priceRule.getOutput().getOutcomeType().isBonusOrSurchargeProduct()) {
                    priceRule.applyBonusOrSurchargeProduct(results, saleOrder);
                }
                results.add(priceRule.applyToProductByProduct(productId, saleOrder));
                finalBestRulesFound.add(priceRule);
            }
        }));
        bestRules = finalBestRulesFound.stream().distinct().collect(Collectors.toList());
        this.addToHistoryLog(PriceRuleLog.createFoundBestRulesByBestValueProduct(bestRulesByProductFound));
        return results.stream().distinct().collect(Collectors.toList());
    }

    private List<PriceRule> filterByExclusiveOutcomeEnum(List<PriceRule> rules) {
        HashMap<ExclusiveOutcomeEnum, Object> exclusiveOutcomeFilter = getExclusiveOutcomeFilter(rules);

        if (exclusiveOutcomeFilter.containsKey(ExclusiveOutcomeEnum.OUTCOME_TYPE)) {
            PriceComponentOutcomeTypeEnum priceComponentOutcomeTypeEnum =
                    (PriceComponentOutcomeTypeEnum)exclusiveOutcomeFilter.get(ExclusiveOutcomeEnum.OUTCOME_TYPE);
            return rules.stream().filter(priceRule ->
                            Objects.nonNull(priceRule)
                            && priceRule.getOutput().getOutcomeType() == priceComponentOutcomeTypeEnum)
                    .collect(Collectors.toList());
        }

        return rules;
    }

    private HashMap<ExclusiveOutcomeEnum, Object> getExclusiveOutcomeFilter(List<PriceRule> priceRules) {
        HashMap<ExclusiveOutcomeEnum, Object> result = new HashMap<>();

        for (PriceRule priceRule: priceRules) {
            if ( Objects.nonNull(priceRule)
                    && Objects.nonNull(priceRule.getExclusiveOutcomeEnum())
                    && priceRule.getExclusiveOutcomeEnum().isOutcomeType()
            ) {
                result.put(priceRule.getExclusiveOutcomeEnum(), priceRule.getOutput().getOutcomeType());
                return result;
            }
        };

        return result;
    }

    /**
     * All accumulative rules calculate according to the amount.
     * It does not matter the validationType because all the rules in this point has already pass the validation if its applicable.
     */
    private BigDecimal calculateOutcomeAccumulative(List<PriceRule> priceRules, PriceComponentRuleInputSaleOrder saleOrder) {
        AtomicReference<BigDecimal> valueDiscountAccumulatives = new AtomicReference<>(BigDecimal.ZERO);

        priceRules.forEach(priceRule ->
        {
            BigDecimal value = computateOutcome(priceRule, saleOrder);
            valueDiscountAccumulatives.set(valueDiscountAccumulatives.get().add(value));
        });

        return valueDiscountAccumulatives.get();
    }

    /**
     * All accumulative rules calculate by Product
     */
    private BigDecimal calculateOutcomeAccumulativeByProduct(List<PriceRule> priceRules, PriceComponentRuleInputSaleOrder saleOrder, PriceComponentRuleInputSaleOrderProduct product) {
        AtomicReference<BigDecimal> valueDiscountAccumulatives = new AtomicReference<>(BigDecimal.ZERO);

        priceRules.forEach(priceRule ->
        {
            BigDecimal value = computateOutcomeByProduct(priceRule, saleOrder, product);
            valueDiscountAccumulatives.set(valueDiscountAccumulatives.get().add(value));
        });

        return valueDiscountAccumulatives.get();
    }


    private BigDecimal computateOutcomeByProduct(PriceRule priceRule, PriceComponentRuleInputSaleOrder saleOrder, PriceComponentRuleInputSaleOrderProduct product) {
        switch (priceRule.getOutComeMode()) {
            case SCALE:
                BigDecimal productTotal = BigDecimal.ZERO;
                switch (priceRule.getOutput().getValidationType()) {
                    case AMOUNT:
                        productTotal = priceRule.isUseSaleOrderTotalForOutcome() ?
                                saleOrder.getTotalAmount() :
                                priceRule.calculateProductsTotalFromSaleOrderByPriorityAmount(saleOrder.getProductDetails());
                        break;
                    case QUANTITY:
                        productTotal = priceRule.isUseSaleOrderTotalForOutcome() ?
                                saleOrder.getProductDetails().stream().map(PriceComponentRuleInputSaleOrderProduct::getQty)
                                        .reduce(BigDecimal.ZERO, BigDecimal::add) :
                                priceRule.calculateProductsTotalByQuantity(saleOrder.getProductDetails());
                        break;
                }
                if (productTotal.equals(BigDecimal.ZERO)) {
                    return BigDecimal.ZERO;
                }
                PriceComponentRuleFactorScale outcomeScale = priceRule.getApplicableScaleByTotal(productTotal);
                PriceComponentRuleOutcome output = priceRule.getOutput().getOutcomeScale(outcomeScale);
                if (Objects.isNull(priceRule.getOutput()) || Objects.isNull(outcomeScale) || Objects.isNull(output)) {
                    return BigDecimal.ZERO;
                }
                return output.computateOutcome(product.calculateTotalPriceGross(), priceRule.getSubjectProducts(saleOrder));
            case FREQUENCY:
                return priceRule.getOutput().getOutcomeFrequency(priceRule.getApplicableFrequencyByProduct(saleOrder, product))
                        .computateOutcome(product.calculateTotalPriceGross(),priceRule.getSubjectProducts(saleOrder));
            case SINGLE:
            default:
                return priceRule.getOutput().getOutcome().computateOutcome(product.calculateTotalPriceGross(),
                        priceRule.getSubjectProducts(saleOrder));
        }
    }


    private BigDecimal computateOutcome(PriceRule priceRule, PriceComponentRuleInputSaleOrder saleOrder) {
        switch (priceRule.getOutComeMode()) {
            case SCALE:
                return priceRule.getOutput().getOutcomeScale(priceRule.getApplicableScale(saleOrder)).computateOutcome(priceRule.calculateSubjectsTotalAmount(saleOrder),
                        priceRule.getSubjectProducts(saleOrder));
            case FREQUENCY:
                return priceRule.getOutput().getOutcomeFrequency(priceRule.getApplicableFrequency(saleOrder)).computateOutcome(priceRule.calculateSubjectsTotalAmount(saleOrder),
                        priceRule.getSubjectProducts(saleOrder));
            case SINGLE:
            default:
                return priceRule.getOutput().getOutcome().computateOutcome(priceRule.calculateSubjectsTotalAmount(saleOrder),
                        priceRule.getSubjectProducts(saleOrder));
        }
    }

    //region Find Rules
    private List<PriceRule> findRules(PriceComponentOutcomeTypeEnum outcomeType, boolean accumulative) {
        return this.applicableRules.stream()
                .filter(priceRule ->
                        priceRule.getOutput().getOutcomeType() == outcomeType &&
                                priceRule.isAccumulative() == accumulative)
                .collect(Collectors.toList());
    }

    private List<PriceRule> findAllRules(PriceComponentOutcomeTypeEnum outcomeType) {
        return this.applicableRules.stream()
                .filter(priceRule ->
                        priceRule.getOutput().getOutcomeType() == outcomeType)
                .collect(Collectors.toList());
    }

    private List<PriceRule> findRulesByProduct(PriceComponentOutcomeTypeEnum outcomeType,
                                               PriceComponentRuleInputSaleOrderProduct product,
                                               boolean accumulative, PriceComponentRuleInputSaleOrder saleOrder) {
        return this.applicableRules.stream()
                .filter(priceRule ->
                        priceRule.getOutput().getOutcomeType() == outcomeType &&
                                priceRule.isAccumulative() == accumulative && priceRule.isApplicableByProduct(product, saleOrder))
                .collect(Collectors.toList());
    }

    private List<PriceRule> findAllRulesByProduct(PriceComponentOutcomeTypeEnum outcomeType,
                                                  PriceComponentRuleInputSaleOrderProduct product,
                                                  PriceComponentRuleInputSaleOrder saleOrder) {
        return this.applicableRules.stream()
                .filter(priceRule ->
                        priceRule.getOutput().getOutcomeType() == outcomeType &&
                                priceRule.isApplicableByProduct(product, saleOrder))
                .collect(Collectors.toList());
    }

    private List<PriceRule> findBestRulesByGreaterOutcome(PriceComponentOutcomeTypeEnum outcomeType,
                                                          PriceComponentRuleInputSaleOrder saleOrder,
                                                          List<PriceRule> accumulativeRules,
                                                          BigDecimal valueAccumulativeRules,
                                                          PriceRule bestRule) {
        if (Objects.nonNull(bestRule)) {
            BigDecimal bestNoAccum = computateOutcome(bestRule, saleOrder);

            return findBestRulesCompare(outcomeType, accumulativeRules, valueAccumulativeRules, bestRule, bestNoAccum);
        } else {
            LogUtil.log(PriceRuleLog.createBestAccumulativeRulesAndValues(outcomeType, accumulativeRules, valueAccumulativeRules));
            return accumulativeRules;
        }
    }

    private List<PriceRule> findBestRulesBySmallerOrEqualsOutcome(PriceComponentOutcomeTypeEnum outcomeType,
                                                                  PriceComponentRuleInputSaleOrder saleOrder,
                                                                  List<PriceRule> accumulativeRules,
                                                                  BigDecimal valueAccumulativeRules,
                                                                  PriceRule bestRule) {
        if (Objects.nonNull(bestRule)) {
            BigDecimal bestRuleNoAccum = computateOutcome(bestRule, saleOrder);

            if (valueAccumulativeRules.compareTo(BigDecimal.ZERO) == 0) {
                this.addToHistoryLog(PriceRuleLog.createFoundBestRule(outcomeType, bestRule, bestRuleNoAccum));
                return Collections.singletonList(bestRule);
            }

            if (valueAccumulativeRules.compareTo(bestRuleNoAccum) < 0) {
                LogUtil.log(PriceRuleLog.createBestAccumulativeRulesAndValues(outcomeType, accumulativeRules, valueAccumulativeRules));
                return accumulativeRules;
            }

            this.addToHistoryLog(PriceRuleLog.createFoundBestRule(outcomeType, bestRule, bestRuleNoAccum));
            return Collections.singletonList(bestRule);

        } else {
            LogUtil.log(PriceRuleLog.createBestAccumulativeRulesAndValues(outcomeType, accumulativeRules, valueAccumulativeRules));
            return accumulativeRules;
        }
    }

    private List<PriceRule> findBestRulesByGreaterOutcomeByProduct(PriceComponentOutcomeTypeEnum outcomeType,
                                                                   PriceComponentRuleInputSaleOrder saleOrder,
                                                                   PriceComponentRuleInputSaleOrderProduct product,
                                                                   List<PriceRule> accumulativeRules,
                                                                   BigDecimal valueAccumulativeRules,
                                                                   PriceRule bestRule) {
        if (Objects.nonNull(bestRule)) {
            BigDecimal bestNoAccum = computateOutcomeByProduct(bestRule, saleOrder, product);
            return findBestRulesCompare(outcomeType, accumulativeRules, valueAccumulativeRules, bestRule, bestNoAccum);
        } else {
            LogUtil.log(PriceRuleLog.createBestAccumulativeRulesAndValues(outcomeType, accumulativeRules, valueAccumulativeRules));
            return accumulativeRules;
        }
    }

    private List<PriceRule> findBestRulesCompare(PriceComponentOutcomeTypeEnum outcomeType, List<PriceRule> accumulativeRules, BigDecimal valueAccumulativeRules, PriceRule bestRule, BigDecimal bestNoAccum) {
        if (valueAccumulativeRules.compareTo(bestNoAccum) > 0) {
            LogUtil.log(PriceRuleLog.createBestAccumulativeRulesAndValues(outcomeType, accumulativeRules, valueAccumulativeRules));
            return accumulativeRules;
        } else {
            this.addToHistoryLog(PriceRuleLog.createFoundBestRule(outcomeType, bestRule, bestNoAccum));
            return Collections.singletonList(bestRule);
        }
    }

    private List<PriceRule> findBestRulesBySmallerOrEqualsOutcomeByProduct(PriceComponentOutcomeTypeEnum outcomeType,
                                                                           PriceComponentRuleInputSaleOrder saleOrder,
                                                                           PriceComponentRuleInputSaleOrderProduct product,
                                                                           List<PriceRule> accumulativeRules,
                                                                           BigDecimal valueAccumulativeRules,
                                                                           PriceRule bestRule) {
        if (Objects.nonNull(bestRule)) {
            BigDecimal bestRuleNoAccum = computateOutcomeByProduct(bestRule, saleOrder, product);

            if (valueAccumulativeRules.compareTo(BigDecimal.ZERO) == 0) {
                this.addToHistoryLog(PriceRuleLog.createFoundBestRule(outcomeType, bestRule, bestRuleNoAccum));
                return Collections.singletonList(bestRule);
            }

            if (valueAccumulativeRules.compareTo(bestRuleNoAccum) < 0) {
                LogUtil.log(PriceRuleLog.createBestAccumulativeRulesAndValues(outcomeType, accumulativeRules, valueAccumulativeRules));
                return accumulativeRules;
            }

            this.addToHistoryLog(PriceRuleLog.createFoundBestRule(outcomeType, bestRule, bestRuleNoAccum));
            return Collections.singletonList(bestRule);

        } else {
            LogUtil.log(PriceRuleLog.createBestAccumulativeRulesAndValues(outcomeType, accumulativeRules, valueAccumulativeRules));
            return accumulativeRules;
        }
    }

    private Optional<PriceRule> findBestRuleByMinValue(Comparator<PriceRule> comparator, PriceComponentOutcomeTypeEnum outcomeType, boolean accumulative) {
        return this.applicableRules.stream()
                .filter(priceRule ->
                        priceRule.getOutput().getOutcomeType() == outcomeType &&
                                priceRule.isAccumulative() == accumulative)
                .min(comparator);
    }

    private Optional<PriceRule> findBestRuleByMaxValue(Comparator<PriceRule> comparator, PriceComponentOutcomeTypeEnum outcomeType, boolean accumulative) {
        return this.applicableRules.stream()
                .filter(priceRule ->
                        priceRule.getOutput().getOutcomeType() == outcomeType &&
                                priceRule.isAccumulative() == accumulative)
                .max(comparator);
    }

    private Optional<PriceRule> findBestRuleByMaxValue(Comparator<PriceRule> comparator, PriceComponentOutcomeTypeEnum outcomeType) {
        return this.applicableRules.stream()
                .filter(priceRule ->
                        priceRule.getOutput().getOutcomeType() == outcomeType)
                .max(comparator);
    }


    private Optional<PriceRule> findBestRuleByMaxValue(List<PriceRule> rules,
                                                       Comparator<PriceRule> comparator,
                                                       PriceComponentOutcomeTypeEnum outcomeType) {
        return rules.stream()
                .filter(priceRule -> priceRule.getOutput().getOutcomeType() == outcomeType)
                .max(comparator);
    }

    private Optional<PriceRule> findBestRuleByMaxValue(List<PriceRule> rules,
                                                       Comparator<PriceRule> comparator,
                                                       PriceComponentOutcomeTypeEnum outcomeType,
                                                       Function<PriceRule, BigDecimal> computeOutcomeByProduct) {
        return rules.stream()
                .filter(priceRule ->
                        priceRule.getOutput().getOutcomeType() == outcomeType
                        && isGreaterThanZero(computeOutcomeByProduct.apply(priceRule))
                )
                .max(comparator);
    }

    private Optional<PriceRule> findBestRuleByMinValue(List<PriceRule> rules,
                                                       Comparator<PriceRule> comparator,
                                                       PriceComponentOutcomeTypeEnum outcomeType) {
        return rules.stream()
                .filter(priceRule -> priceRule.getOutput().getOutcomeType() == outcomeType)
                .min(comparator);
    }

    private Optional<PriceRule> findBestRuleByMinValue(List<PriceRule> rules,
                                                       Comparator<PriceRule> comparator,
                                                       PriceComponentOutcomeTypeEnum outcomeType,
                                                       Function<PriceRule, BigDecimal> computeOutcomeByProduct) {
        return rules.stream()
                .filter(priceRule ->
                        priceRule.getOutput().getOutcomeType() == outcomeType
                        && isGreaterThanZero(computeOutcomeByProduct.apply(priceRule))
                )
                .min(comparator);
    }

    private Optional<PriceRule> findBestRuleByMaxValue(List<PriceRule> rules,
                                                       Comparator<PriceRule> comparator,
                                                       PriceComponentOutcomeTypeEnum outcomeType,
                                                       boolean accumulative) {
        return rules.stream()
                .filter(priceRule ->
                        priceRule.getOutput().getOutcomeType() == outcomeType &&
                                priceRule.isAccumulative() == accumulative)
                .max(comparator);
    }

    private Optional<PriceRule> findBestRuleByMaxValue(List<PriceRule> rules,
                                                       Comparator<PriceRule> comparator,
                                                       PriceComponentOutcomeTypeEnum outcomeType,
                                                       boolean accumulative,
                                                       Function<PriceRule, BigDecimal> computeOutcomeByProduct) {
        return rules.stream()
                .filter(priceRule ->
                        priceRule.getOutput().getOutcomeType() == outcomeType
                                && priceRule.isAccumulative() == accumulative
                                && isGreaterThanZero(computeOutcomeByProduct.apply(priceRule))
                        )
                .sorted(comparator)
                .max(comparator);
    }

    private Optional<PriceRule> findBestRuleByMinValue(List<PriceRule> rules,
                                                       Comparator<PriceRule> comparator,
                                                       PriceComponentOutcomeTypeEnum outcomeType,
                                                       boolean accumulative,
                                                       Function<PriceRule, BigDecimal> computeOutcomeByProduct) {
        return rules.stream()
                .filter(priceRule ->
                        priceRule.getOutput().getOutcomeType() == outcomeType
                                && priceRule.isAccumulative() == accumulative
                                && isGreaterThanZero(computeOutcomeByProduct.apply(priceRule))
                )
                .min(comparator);
    }

    private Optional<PriceRule> findBestRuleByMinValue(List<PriceRule> rules,
                                                       Comparator<PriceRule> comparator,
                                                       PriceComponentOutcomeTypeEnum outcomeType,
                                                       boolean accumulative) {
        return rules.stream()
                .filter(priceRule ->
                        priceRule.getOutput().getOutcomeType() == outcomeType &&
                                priceRule.isAccumulative() == accumulative)
                .min(comparator);
    }

    //endregion

    //region Find Best Rule By Product
    private List<PriceRule> findBestDiscountPercentageRuleByProduct(Comparator<PriceRule> comparator,
                                                                    PriceComponentRuleInputSaleOrder saleOrder,
                                                                    PriceComponentRuleInputSaleOrderProduct product,
                                                                    Function<PriceRule, BigDecimal> computeOutcomeByProduct) {

        List<PriceRule> productApplicableRules = findAllRulesByProduct(PriceComponentOutcomeTypeEnum.DISCOUNT_PERCENTAGE, product, saleOrder);

        List<PriceRule> discountAccumulatives = findRulesByProduct(PriceComponentOutcomeTypeEnum.DISCOUNT_PERCENTAGE, product, true, saleOrder);
        BigDecimal valueDiscountAccumulatives = calculateOutcomeAccumulativeByProduct(discountAccumulatives, saleOrder, product);

        LogUtil.log(PriceRuleLog.createFoundAccumulativeRulesAndValues(PriceComponentOutcomeTypeEnum.DISCOUNT_PERCENTAGE,
                discountAccumulatives, valueDiscountAccumulatives, product));

        Optional<PriceRule> discountPercentageBestRule =
                config.getBoolean(Keys.GIVE_THE_GREATER_DISCOUNT) ?
                        findBestRuleByMaxValue(productApplicableRules, comparator,
                                PriceComponentOutcomeTypeEnum.DISCOUNT_PERCENTAGE, false, computeOutcomeByProduct) :
                        findBestRuleByMinValue(productApplicableRules, comparator,
                                PriceComponentOutcomeTypeEnum.DISCOUNT_PERCENTAGE, false, computeOutcomeByProduct);
        return config.getBoolean(Keys.GIVE_THE_GREATER_DISCOUNT) ?
                findBestRulesByGreaterOutcomeByProduct(PriceComponentOutcomeTypeEnum.DISCOUNT_PERCENTAGE,
                        saleOrder, product, discountAccumulatives, valueDiscountAccumulatives, discountPercentageBestRule.orElse(null)) :
                findBestRulesBySmallerOrEqualsOutcomeByProduct(PriceComponentOutcomeTypeEnum.DISCOUNT_PERCENTAGE,
                        saleOrder, product, discountAccumulatives, valueDiscountAccumulatives, discountPercentageBestRule.orElse(null));
    }

    private List<PriceRule> findBestDiscountAmountRuleByProduct(Comparator<PriceRule> comparator,
                                                                PriceComponentRuleInputSaleOrder saleOrder,
                                                                PriceComponentRuleInputSaleOrderProduct product,
                                                                Function<PriceRule, BigDecimal> computeOutcomeByProduct) {

        List<PriceRule> productApplicableRules = findAllRulesByProduct(PriceComponentOutcomeTypeEnum.DISCOUNT_AMOUNT, product, saleOrder);

        List<PriceRule> discountAccumulatives = findRulesByProduct(PriceComponentOutcomeTypeEnum.DISCOUNT_AMOUNT, product, true, saleOrder);
        BigDecimal valueDiscountAccumulatives = calculateOutcomeAccumulativeByProduct(discountAccumulatives, saleOrder, product);

        LogUtil.log(PriceRuleLog.createFoundAccumulativeRulesAndValues(PriceComponentOutcomeTypeEnum.DISCOUNT_AMOUNT,
                discountAccumulatives, valueDiscountAccumulatives, product));

        Optional<PriceRule> bestRule = config.getBoolean(Keys.GIVE_THE_GREATER_DISCOUNT) ?
                findBestRuleByMinValue(productApplicableRules, comparator,
                        PriceComponentOutcomeTypeEnum.DISCOUNT_AMOUNT, false, computeOutcomeByProduct) :
                findBestRuleByMaxValue(productApplicableRules, comparator,
                        PriceComponentOutcomeTypeEnum.DISCOUNT_AMOUNT, false, computeOutcomeByProduct);

        return config.getBoolean(Keys.GIVE_THE_GREATER_DISCOUNT) ?
                findBestRulesByGreaterOutcomeByProduct(PriceComponentOutcomeTypeEnum.DISCOUNT_AMOUNT,
                        saleOrder, product, discountAccumulatives, valueDiscountAccumulatives, bestRule.orElse(null)) :
                findBestRulesBySmallerOrEqualsOutcomeByProduct(PriceComponentOutcomeTypeEnum.DISCOUNT_AMOUNT,
                        saleOrder, product, discountAccumulatives, valueDiscountAccumulatives, bestRule.orElse(null));
    }

    private List<PriceRule> findBestSurchargePercentageRuleByProduct(Comparator<PriceRule> comparator,
                                                                     PriceComponentRuleInputSaleOrder saleOrder,
                                                                     PriceComponentRuleInputSaleOrderProduct product,
                                                                     Function<PriceRule, BigDecimal> computeOutcomeByProduct) {

        List<PriceRule> productApplicableRules = findAllRulesByProduct(PriceComponentOutcomeTypeEnum.SURCHARGE_PERCENTAGE, product, saleOrder);

        List<PriceRule> surchargeAccumulatives = findRulesByProduct(PriceComponentOutcomeTypeEnum.SURCHARGE_PERCENTAGE, product, true, saleOrder);
        BigDecimal valueSurchargeAccumulatives = calculateOutcomeAccumulativeByProduct(surchargeAccumulatives, saleOrder, product);

        LogUtil.log(PriceRuleLog.createFoundAccumulativeRulesAndValues(PriceComponentOutcomeTypeEnum.SURCHARGE_PERCENTAGE,
                surchargeAccumulatives, valueSurchargeAccumulatives, product));

        Optional<PriceRule> bestRule = config.getBoolean(Keys.GIVE_THE_GREATER_SURCHARGE) ?
                findBestRuleByMaxValue(productApplicableRules, comparator,
                        PriceComponentOutcomeTypeEnum.SURCHARGE_PERCENTAGE, false, computeOutcomeByProduct) :
                findBestRuleByMinValue(productApplicableRules, comparator,
                        PriceComponentOutcomeTypeEnum.SURCHARGE_PERCENTAGE, false, computeOutcomeByProduct);

        return config.getBoolean(Keys.GIVE_THE_GREATER_SURCHARGE) ?
                findBestRulesByGreaterOutcomeByProduct(PriceComponentOutcomeTypeEnum.SURCHARGE_PERCENTAGE,
                        saleOrder, product, surchargeAccumulatives, valueSurchargeAccumulatives, bestRule.orElse(null)) :
                findBestRulesBySmallerOrEqualsOutcomeByProduct(PriceComponentOutcomeTypeEnum.SURCHARGE_PERCENTAGE,
                        saleOrder, product, surchargeAccumulatives, valueSurchargeAccumulatives, bestRule.orElse(null));
    }

    private List<PriceRule> findBestSurchargeAmountRuleByProduct(Comparator<PriceRule> comparator,
                                                                 PriceComponentRuleInputSaleOrder saleOrder,
                                                                 PriceComponentRuleInputSaleOrderProduct product,
                                                                 Function<PriceRule, BigDecimal> computeOutcomeByProduct) {

        List<PriceRule> productApplicableRules = findAllRulesByProduct(PriceComponentOutcomeTypeEnum.SURCHARGE_AMOUNT, product, saleOrder);

        List<PriceRule> surchargeAccumulatives = findRulesByProduct(PriceComponentOutcomeTypeEnum.SURCHARGE_AMOUNT, product, true, saleOrder);
        BigDecimal valueSurchargeAccumulatives = calculateOutcomeAccumulativeByProduct(surchargeAccumulatives, saleOrder, product);

        LogUtil.log(PriceRuleLog.createFoundAccumulativeRulesAndValues(PriceComponentOutcomeTypeEnum.SURCHARGE_AMOUNT,
                surchargeAccumulatives, valueSurchargeAccumulatives, product));

        Optional<PriceRule> bestRule = config.getBoolean(Keys.GIVE_THE_GREATER_SURCHARGE) ?
                findBestRuleByMaxValue(productApplicableRules, comparator,
                        PriceComponentOutcomeTypeEnum.SURCHARGE_AMOUNT, false, computeOutcomeByProduct) :
                findBestRuleByMinValue(productApplicableRules, comparator,
                        PriceComponentOutcomeTypeEnum.SURCHARGE_AMOUNT, false, computeOutcomeByProduct);

        return config.getBoolean(Keys.GIVE_THE_GREATER_SURCHARGE) ?
                findBestRulesByGreaterOutcomeByProduct(PriceComponentOutcomeTypeEnum.SURCHARGE_AMOUNT,
                        saleOrder, product, surchargeAccumulatives, valueSurchargeAccumulatives, bestRule.orElse(null)) :
                findBestRulesBySmallerOrEqualsOutcomeByProduct(PriceComponentOutcomeTypeEnum.SURCHARGE_AMOUNT,
                        saleOrder, product, surchargeAccumulatives, valueSurchargeAccumulatives, bestRule.orElse(null));
    }


    private PriceRule findBestFixedPriceRuleByProduct(Comparator<PriceRule> comparator,
                                                      PriceComponentRuleInputSaleOrder saleOrder,
                                                      PriceComponentRuleInputSaleOrderProduct product,
                                                      Function<PriceRule, BigDecimal> computeOutcomeByProduct) {
        List<PriceRule> fixedPriceRules = findAllRulesByProduct(PriceComponentOutcomeTypeEnum.FIXED_PRICE, product, saleOrder);

        Optional<PriceRule> bestRule = config.getBoolean(Keys.GIVE_THE_GREATER_FIXED_PRICE) ?
                findBestRuleByMaxValue(fixedPriceRules, comparator, PriceComponentOutcomeTypeEnum.FIXED_PRICE, computeOutcomeByProduct) :
                findBestRuleByMinValue(fixedPriceRules, comparator, PriceComponentOutcomeTypeEnum.FIXED_PRICE, computeOutcomeByProduct);

        return bestRule.orElse(null);
    }

    private List<PriceRule> findBestBonusProductPriceRuleByProduct(Comparator<PriceRule> comparator,
                                                                   PriceComponentRuleInputSaleOrder saleOrder,
                                                                   PriceComponentRuleInputSaleOrderProduct product,
                                                                   Function<PriceRule, BigDecimal> computeOutcomeByProduct) {

        List<PriceRule> productApplicableRules = findAllRulesByProduct(PriceComponentOutcomeTypeEnum.PRODUCT, product, saleOrder);

        List<PriceRule> bonusProductAccumulativeRules = findRulesByProduct(PriceComponentOutcomeTypeEnum.PRODUCT, product, true, saleOrder);
        BigDecimal valueBonusProductAccumulativeRules = calculateOutcomeAccumulativeByProduct(bonusProductAccumulativeRules, saleOrder, product);

        LogUtil.log(PriceRuleLog.createFoundAccumulativeRulesAndValues(PriceComponentOutcomeTypeEnum.PRODUCT,
                bonusProductAccumulativeRules, valueBonusProductAccumulativeRules, product));

        Optional<PriceRule> bestRule = config.getBoolean(Keys.GIVE_THE_GREATER_BONUS_PRODUCT) ?
                findBestRuleByMaxValue(productApplicableRules, comparator, PriceComponentOutcomeTypeEnum.PRODUCT, false, computeOutcomeByProduct) :
                findBestRuleByMinValue(productApplicableRules, comparator, PriceComponentOutcomeTypeEnum.PRODUCT, false, computeOutcomeByProduct);

        return config.getBoolean(Keys.GIVE_THE_GREATER_BONUS_PRODUCT) ?
                findBestRulesByGreaterOutcomeByProduct(PriceComponentOutcomeTypeEnum.PRODUCT,
                        saleOrder, product, bonusProductAccumulativeRules, valueBonusProductAccumulativeRules, bestRule.orElse(null)) :
                findBestRulesBySmallerOrEqualsOutcomeByProduct(PriceComponentOutcomeTypeEnum.PRODUCT,
                        saleOrder, product, bonusProductAccumulativeRules, valueBonusProductAccumulativeRules, bestRule.orElse(null));
    }

    private List<PriceRule> findBestSurchargeProductPriceRuleByProduct(Comparator<PriceRule> comparator,
                                                                       PriceComponentRuleInputSaleOrder saleOrder,
                                                                       PriceComponentRuleInputSaleOrderProduct product,
                                                                       Function<PriceRule, BigDecimal> computeOutcomeByProduct) {

        List<PriceRule> productApplicableRules = findAllRulesByProduct(PriceComponentOutcomeTypeEnum.PRODUCT_SURCHARGE, product, saleOrder);

        List<PriceRule> surchargeProductAccumulativeRules = findRulesByProduct(PriceComponentOutcomeTypeEnum.PRODUCT_SURCHARGE, product, true, saleOrder);
        BigDecimal valueSurchargeProductAccumulativeRules = calculateOutcomeAccumulativeByProduct(surchargeProductAccumulativeRules, saleOrder, product);

        LogUtil.log(PriceRuleLog.createFoundAccumulativeRulesAndValues(PriceComponentOutcomeTypeEnum.PRODUCT_SURCHARGE,
                surchargeProductAccumulativeRules, valueSurchargeProductAccumulativeRules, product));

        Optional<PriceRule> bestRule = config.getBoolean(Keys.GIVE_THE_GREATER_SURCHARGE_PRODUCT) ?
                findBestRuleByMaxValue(productApplicableRules, comparator,
                        PriceComponentOutcomeTypeEnum.PRODUCT_SURCHARGE, false, computeOutcomeByProduct) :
                findBestRuleByMinValue(productApplicableRules, comparator,
                        PriceComponentOutcomeTypeEnum.PRODUCT_SURCHARGE, false, computeOutcomeByProduct);

        return config.getBoolean(Keys.GIVE_THE_GREATER_SURCHARGE_PRODUCT) ?
                findBestRulesByGreaterOutcomeByProduct(PriceComponentOutcomeTypeEnum.PRODUCT_SURCHARGE,
                        saleOrder, product, surchargeProductAccumulativeRules, valueSurchargeProductAccumulativeRules, bestRule.orElse(null)) :
                findBestRulesBySmallerOrEqualsOutcomeByProduct(PriceComponentOutcomeTypeEnum.PRODUCT_SURCHARGE,
                        saleOrder, product, surchargeProductAccumulativeRules, valueSurchargeProductAccumulativeRules, bestRule.orElse(null));
    }
    //endregion

    //region find Best Rules By Sale Order
    private List<PriceRule> findBestDiscountPercentageRule(Comparator<PriceRule> comparator, PriceComponentRuleInputSaleOrder saleOrder) {
        List<PriceRule> discountAccumulatives = findRules(PriceComponentOutcomeTypeEnum.DISCOUNT_PERCENTAGE, true);
        BigDecimal valueDiscountAccumulatives = calculateOutcomeAccumulative(discountAccumulatives, saleOrder);

        LogUtil.log(PriceRuleLog.createFoundAccumulativeRulesAndValues(PriceComponentOutcomeTypeEnum.DISCOUNT_PERCENTAGE, discountAccumulatives, valueDiscountAccumulatives));

        Optional<PriceRule> discountPercentageBestRule =
                config.getBoolean(Keys.GIVE_THE_GREATER_DISCOUNT) ?
                        findBestRuleByMaxValue(comparator, PriceComponentOutcomeTypeEnum.DISCOUNT_PERCENTAGE, false) :
                        findBestRuleByMinValue(comparator, PriceComponentOutcomeTypeEnum.DISCOUNT_PERCENTAGE, false);

        return config.getBoolean(Keys.GIVE_THE_GREATER_DISCOUNT) ?
                findBestRulesByGreaterOutcome(PriceComponentOutcomeTypeEnum.DISCOUNT_PERCENTAGE,
                        saleOrder, discountAccumulatives, valueDiscountAccumulatives, discountPercentageBestRule.orElse(null)) :
                findBestRulesBySmallerOrEqualsOutcome(PriceComponentOutcomeTypeEnum.DISCOUNT_PERCENTAGE,
                        saleOrder, discountAccumulatives, valueDiscountAccumulatives, discountPercentageBestRule.orElse(null));
    }

    private List<PriceRule> findBestDiscountAmountRule(Comparator<PriceRule> comparator, PriceComponentRuleInputSaleOrder saleOrder) {
        List<PriceRule> discountAccumulatives = findRules(PriceComponentOutcomeTypeEnum.DISCOUNT_AMOUNT, true);
        BigDecimal valueDiscountAccumulatives = calculateOutcomeAccumulative(discountAccumulatives, saleOrder);

        LogUtil.log(PriceRuleLog.createFoundAccumulativeRulesAndValues(PriceComponentOutcomeTypeEnum.DISCOUNT_AMOUNT, discountAccumulatives, valueDiscountAccumulatives));

        Optional<PriceRule> bestRule = config.getBoolean(Keys.GIVE_THE_GREATER_DISCOUNT) ?
                findBestRuleByMinValue(comparator, PriceComponentOutcomeTypeEnum.DISCOUNT_AMOUNT, false) :
                findBestRuleByMaxValue(comparator, PriceComponentOutcomeTypeEnum.DISCOUNT_AMOUNT, false);

        return config.getBoolean(Keys.GIVE_THE_GREATER_DISCOUNT) ?
                findBestRulesByGreaterOutcome(PriceComponentOutcomeTypeEnum.DISCOUNT_AMOUNT,
                        saleOrder, discountAccumulatives, valueDiscountAccumulatives, bestRule.orElse(null)) :
                findBestRulesBySmallerOrEqualsOutcome(PriceComponentOutcomeTypeEnum.DISCOUNT_AMOUNT,
                        saleOrder, discountAccumulatives, valueDiscountAccumulatives, bestRule.orElse(null));
    }

    private List<PriceRule> findBestSurchargePercentageRule(Comparator<PriceRule> comparator, PriceComponentRuleInputSaleOrder saleOrder) {
        List<PriceRule> surchargeAccumulatives = findRules(PriceComponentOutcomeTypeEnum.SURCHARGE_PERCENTAGE, true);
        BigDecimal valueSurchargeAccumulatives = calculateOutcomeAccumulative(surchargeAccumulatives, saleOrder);

        LogUtil.log(PriceRuleLog.createFoundAccumulativeRulesAndValues(PriceComponentOutcomeTypeEnum.SURCHARGE_PERCENTAGE, surchargeAccumulatives, valueSurchargeAccumulatives));

        Optional<PriceRule> bestRule = config.getBoolean(Keys.GIVE_THE_GREATER_SURCHARGE) ?
                findBestRuleByMaxValue(comparator, PriceComponentOutcomeTypeEnum.SURCHARGE_PERCENTAGE, false) :
                findBestRuleByMinValue(comparator, PriceComponentOutcomeTypeEnum.SURCHARGE_PERCENTAGE, false);

        return config.getBoolean(Keys.GIVE_THE_GREATER_SURCHARGE) ?
                findBestRulesByGreaterOutcome(PriceComponentOutcomeTypeEnum.SURCHARGE_PERCENTAGE,
                        saleOrder, surchargeAccumulatives, valueSurchargeAccumulatives, bestRule.orElse(null)) :
                findBestRulesBySmallerOrEqualsOutcome(PriceComponentOutcomeTypeEnum.SURCHARGE_PERCENTAGE,
                        saleOrder, surchargeAccumulatives, valueSurchargeAccumulatives, bestRule.orElse(null));
    }

    private List<PriceRule> findBestSurchargeAmountRule(Comparator<PriceRule> comparator, PriceComponentRuleInputSaleOrder saleOrder) {
        List<PriceRule> surchargeAccumulatives = findRules(PriceComponentOutcomeTypeEnum.SURCHARGE_AMOUNT, true);
        BigDecimal valueSurchargeAccumulatives = calculateOutcomeAccumulative(surchargeAccumulatives, saleOrder);

        LogUtil.log(PriceRuleLog.createFoundAccumulativeRulesAndValues(PriceComponentOutcomeTypeEnum.SURCHARGE_AMOUNT, surchargeAccumulatives, valueSurchargeAccumulatives));

        Optional<PriceRule> bestRule = config.getBoolean(Keys.GIVE_THE_GREATER_SURCHARGE) ?
                findBestRuleByMaxValue(comparator, PriceComponentOutcomeTypeEnum.SURCHARGE_AMOUNT, false) :
                findBestRuleByMinValue(comparator, PriceComponentOutcomeTypeEnum.SURCHARGE_AMOUNT, false);

        return config.getBoolean(Keys.GIVE_THE_GREATER_SURCHARGE) ?
                findBestRulesByGreaterOutcome(PriceComponentOutcomeTypeEnum.SURCHARGE_AMOUNT,
                        saleOrder, surchargeAccumulatives, valueSurchargeAccumulatives, bestRule.orElse(null)) :
                findBestRulesBySmallerOrEqualsOutcome(PriceComponentOutcomeTypeEnum.SURCHARGE_AMOUNT,
                        saleOrder, surchargeAccumulatives, valueSurchargeAccumulatives, bestRule.orElse(null));
    }

    private PriceRule findBestFixedPriceRule(Comparator<PriceRule> comparator) {
        List<PriceRule> fixedPriceRules = findAllRules(PriceComponentOutcomeTypeEnum.FIXED_PRICE);

        Optional<PriceRule> bestRule = config.getBoolean(Keys.GIVE_THE_GREATER_FIXED_PRICE) ?
                findBestRuleByMaxValue(fixedPriceRules, comparator, PriceComponentOutcomeTypeEnum.FIXED_PRICE) :
                findBestRuleByMinValue(fixedPriceRules, comparator, PriceComponentOutcomeTypeEnum.FIXED_PRICE);

        return bestRule.orElse(null);
    }

    private List<PriceRule> findBestBonusProductPriceRule(Comparator<PriceRule> comparator, PriceComponentRuleInputSaleOrder saleOrder) {
        List<PriceRule> bonusProductAccumulativeRules = findRules(PriceComponentOutcomeTypeEnum.PRODUCT, true);
        BigDecimal valueBonusProductAccumulativeRules = calculateOutcomeAccumulative(bonusProductAccumulativeRules, saleOrder);

        LogUtil.log(PriceRuleLog.createFoundAccumulativeRulesAndValues(PriceComponentOutcomeTypeEnum.PRODUCT, bonusProductAccumulativeRules, valueBonusProductAccumulativeRules));

        Optional<PriceRule> bestRule = config.getBoolean(Keys.GIVE_THE_GREATER_BONUS_PRODUCT) ?
                findBestRuleByMaxValue(comparator, PriceComponentOutcomeTypeEnum.PRODUCT, false) :
                findBestRuleByMinValue(comparator, PriceComponentOutcomeTypeEnum.PRODUCT, false);

        return config.getBoolean(Keys.GIVE_THE_GREATER_BONUS_PRODUCT) ?
                findBestRulesByGreaterOutcome(PriceComponentOutcomeTypeEnum.PRODUCT,
                        saleOrder, bonusProductAccumulativeRules, valueBonusProductAccumulativeRules, bestRule.orElse(null)) :
                findBestRulesBySmallerOrEqualsOutcome(PriceComponentOutcomeTypeEnum.PRODUCT,
                        saleOrder, bonusProductAccumulativeRules, valueBonusProductAccumulativeRules, bestRule.orElse(null));
    }

    private List<PriceRule> findBestSurchargeProductPriceRule(Comparator<PriceRule> comparator, PriceComponentRuleInputSaleOrder saleOrder) {
        List<PriceRule> surchargeProductAccumulativeRules = findRules(PriceComponentOutcomeTypeEnum.PRODUCT_SURCHARGE, true);
        BigDecimal valueSurchargeProductAccumulativeRules = calculateOutcomeAccumulative(surchargeProductAccumulativeRules, saleOrder);

        LogUtil.log(PriceRuleLog.createFoundAccumulativeRulesAndValues(PriceComponentOutcomeTypeEnum.PRODUCT_SURCHARGE, surchargeProductAccumulativeRules, valueSurchargeProductAccumulativeRules));

        Optional<PriceRule> bestRule = config.getBoolean(Keys.GIVE_THE_GREATER_SURCHARGE_PRODUCT) ?
                findBestRuleByMaxValue(comparator, PriceComponentOutcomeTypeEnum.PRODUCT_SURCHARGE, false) :
                findBestRuleByMinValue(comparator, PriceComponentOutcomeTypeEnum.PRODUCT_SURCHARGE, false);

        return config.getBoolean(Keys.GIVE_THE_GREATER_SURCHARGE_PRODUCT) ?
                findBestRulesByGreaterOutcome(PriceComponentOutcomeTypeEnum.PRODUCT_SURCHARGE,
                        saleOrder, surchargeProductAccumulativeRules, valueSurchargeProductAccumulativeRules, bestRule.orElse(null)) :
                findBestRulesBySmallerOrEqualsOutcome(PriceComponentOutcomeTypeEnum.PRODUCT_SURCHARGE,
                        saleOrder, surchargeProductAccumulativeRules, valueSurchargeProductAccumulativeRules, bestRule.orElse(null));
    }
    //endregion

    public void addRule(PriceRule rule) {
        validateRule(rule);
        this.rules.add(rule);
    }

    public void removeRule(int index) {
        this.rules.remove(index);
    }

    public void removeRule(long id) {
        Optional<PriceRule> rule = this.rules.stream().filter(priceRule -> priceRule.getId() == id).findFirst();
        rule.ifPresent(priceRule -> this.rules.remove(priceRule));
    }

    public void clearCache() {
        this.applicableRules.clear();
        this.bestRules.clear();
        this.results.clear();
    }

    private void addToHistoryLog(PriceRuleLog logg) {
        this.logHistory.add(logg);
        LogUtil.log(logg);
    }

    public void exportLogs() {
        Path out = Paths.get(config.getString(Keys.EXPORT_LOG_HISTORY_FILE));
        if (Objects.isNull(out)) {
            LogUtil.log(PriceRuleLog.createExporLogHistoryFileNotFound());
            return;
        }
        List<String> arrayList = new ArrayList<>();
        try {
            this.logHistory.forEach(priceRuleLog -> arrayList.add(String.format("%s %s : %s",
                    priceRuleLog.getDate(), priceRuleLog.getType(), priceRuleLog.getDetail())));
            Files.write(out, arrayList, Charset.defaultCharset());
        } catch (IOException e) {
            LogUtil.error(e.getMessage());
        }
    }

    public void clearLogHistory() {
        this.logHistory.clear();
    }

    public void updateConfigParamBoolean(ConfigKey key, boolean value) {
        config.setBoolean(key, value);
    }

    public void updateConfigParam(ConfigKey key, String value) {
        config.setString(key, value);
        if (key.getKey().equalsIgnoreCase(Keys.SILENT_MODE_ENABLE.getKey())) {
            LogUtil.setSilentMode(Boolean.parseBoolean(value));
        }
    }

    public boolean equals(final Object o) {
        if (o == this) return true;
        if (!(o instanceof PriceComponentRule)) return false;
        final PriceComponentRule other = (PriceComponentRule) o;
        if (!other.canEqual((Object) this)) return false;
        final Object this$rules = this.rules;
        final Object other$rules = other.rules;
        if (this$rules == null ? other$rules != null : !this$rules.equals(other$rules)) return false;
        final Object this$resolutionMode = this.resolutionMode;
        final Object other$resolutionMode = other.resolutionMode;
        if (this$resolutionMode == null ? other$resolutionMode != null : !this$resolutionMode.equals(other$resolutionMode))
            return false;
        if (this.allowSetupLogger != other.allowSetupLogger) return false;
        final Object this$applicableRules = this.applicableRules;
        final Object other$applicableRules = other.applicableRules;
        if (this$applicableRules == null ? other$applicableRules != null : !this$applicableRules.equals(other$applicableRules))
            return false;
        final Object this$bestRules = this.bestRules;
        final Object other$bestRules = other.bestRules;
        if (this$bestRules == null ? other$bestRules != null : !this$bestRules.equals(other$bestRules)) return false;
        final Object this$results = this.results;
        final Object other$results = other.results;
        if (this$results == null ? other$results != null : !this$results.equals(other$results)) return false;
        final Object this$logHistory = this.logHistory;
        final Object other$logHistory = other.logHistory;
        if (this$logHistory == null ? other$logHistory != null : !this$logHistory.equals(other$logHistory))
            return false;
        return true;
    }

    protected boolean canEqual(final Object other) {
        return other instanceof PriceComponentRule;
    }

    public int hashCode() {
        final int PRIME = 59;
        int result = 1;
        final Object $rules = this.rules;
        result = result * PRIME + ($rules == null ? 43 : $rules.hashCode());
        final Object $resolutionMode = this.resolutionMode;
        result = result * PRIME + ($resolutionMode == null ? 43 : $resolutionMode.hashCode());
        result = result * PRIME + (this.allowSetupLogger ? 79 : 97);
        final Object $applicableRules = this.applicableRules;
        result = result * PRIME + ($applicableRules == null ? 43 : $applicableRules.hashCode());
        final Object $bestRules = this.bestRules;
        result = result * PRIME + ($bestRules == null ? 43 : $bestRules.hashCode());
        final Object $results = this.results;
        result = result * PRIME + ($results == null ? 43 : $results.hashCode());
        final Object $logHistory = this.logHistory;
        result = result * PRIME + ($logHistory == null ? 43 : $logHistory.hashCode());
        return result;
    }

    public List<PriceRule> getRules() {
        return this.rules;
    }

    public PriceRuleResolutionModeEnum getResolutionMode() {
        return this.resolutionMode;
    }

    public boolean isAllowSetupLogger() {
        return this.allowSetupLogger;
    }

    public List<PriceRule> getApplicableRules() {
        return this.applicableRules;
    }

    public List<PriceRule> getBestRules() {
        return this.bestRules;
    }

    public List<PriceComponentRuleInputSaleOrderProduct> getResults() {
        return this.results;
    }

    public List<PriceRuleLog> getLogHistory() {
        return this.logHistory;
    }

    public void setRules(List<PriceRule> rules) {
        this.rules = rules;
    }

    public void setResolutionMode(PriceRuleResolutionModeEnum resolutionMode) {
        this.resolutionMode = resolutionMode;
    }

    public static class PriceComponentRuleBuilder {
        private List<PriceRule> rules;
        private PriceRuleResolutionModeEnum resolutionMode;
        private boolean allowSetupLogger;

        PriceComponentRuleBuilder() {
        }

        public PriceComponentRule.PriceComponentRuleBuilder rules(List<PriceRule> rules) {
            this.rules = rules;
            return this;
        }

        public PriceComponentRule.PriceComponentRuleBuilder resolutionMode(PriceRuleResolutionModeEnum resolutionMode) {
            this.resolutionMode = resolutionMode;
            return this;
        }

        public PriceComponentRule.PriceComponentRuleBuilder allowSetupLogger(boolean allowSetupLogger) {
            this.allowSetupLogger = allowSetupLogger;
            return this;
        }

        public PriceComponentRule build() {
            return new PriceComponentRule(rules, resolutionMode, allowSetupLogger);
        }

        public String toString() {
            return "PriceComponentRule.PriceComponentRuleBuilder(rules=" + this.rules + ", resolutionMode=" + this.resolutionMode + ", allowSetupLogger=" + this.allowSetupLogger + ")";
        }
    }
}
