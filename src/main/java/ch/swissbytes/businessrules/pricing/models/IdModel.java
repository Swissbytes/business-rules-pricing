package ch.swissbytes.businessrules.pricing.models;


public class IdModel {
    private long id;

    public IdModel(long id) {
        this.id = id;
    }

    public IdModel() {
    }

    public static IdModelBuilder builder() {
        return new IdModelBuilder();
    }

    public long getId() {
        return this.id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public boolean equals(final Object o) {
        if (o == this) return true;
        if (!(o instanceof IdModel)) return false;
        final IdModel other = (IdModel) o;
        if (!other.canEqual((Object) this)) return false;
        if (this.getId() != other.getId()) return false;
        return true;
    }

    protected boolean canEqual(final Object other) {
        return other instanceof IdModel;
    }

    public int hashCode() {
        final int PRIME = 59;
        int result = 1;
        final long $id = this.getId();
        result = result * PRIME + (int) ($id >>> 32 ^ $id);
        return result;
    }

    public String toString() {
        return "IdModel(id=" + this.getId() + ")";
    }

    public static class IdModelBuilder {
        private long id;

        IdModelBuilder() {
        }

        public IdModel.IdModelBuilder id(long id) {
            this.id = id;
            return this;
        }

        public IdModel build() {
            return new IdModel(id);
        }

        public String toString() {
            return "IdModel.IdModelBuilder(id=" + this.id + ")";
        }
    }
}
