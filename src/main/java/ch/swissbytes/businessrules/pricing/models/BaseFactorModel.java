package ch.swissbytes.businessrules.pricing.models;

import ch.swissbytes.businessrules.pricing.inputs.PriceComponentRuleInputSaleOrderProduct;
import ch.swissbytes.businessrules.pricing.utils.BigDecimalUtil;

import java.math.BigDecimal;


public abstract class BaseFactorModel extends BaseModel {

    public boolean validateSaleOrderProductQtyAndAmount(PriceComponentRuleInputSaleOrderProduct product,
                                                        BigDecimal fromAmount, BigDecimal thruAmount,
                                                        BigDecimal fromQty, BigDecimal thruQty) {
        return BigDecimalUtil.between(product.getQty(), fromQty, thruQty) &&
                BigDecimalUtil.between(product.getTotalPriceGross(), fromAmount, thruAmount);
    }

    public boolean equals(final Object o) {
        if (o == this) return true;
        if (!(o instanceof BaseFactorModel)) return false;
        final BaseFactorModel other = (BaseFactorModel) o;
        if (!other.canEqual((Object) this)) return false;
        return true;
    }

    protected boolean canEqual(final Object other) {
        return other instanceof BaseFactorModel;
    }

    public int hashCode() {
        int result = 1;
        return result;
    }
}
