package ch.swissbytes.businessrules.pricing.models;

import java.math.BigDecimal;

/**
 * @project: business-rules-pricing
 *
 * This class adds context to all the priceRule to be applied.
 * Ex:
 * if someone picks saleOrderHistory in ranges,
 * the PriceRuleComponent will search in this Object for amounts.
 */
public class PriceRuleContext extends BaseModel{

    private BigDecimal historicAmount;

    public PriceRuleContext(BigDecimal historicAmount) {
        this.historicAmount = historicAmount;
    }

    public PriceRuleContext() {
    }

    public static PriceRuleContextBuilder builder() {
        return new PriceRuleContextBuilder();
    }

    public BigDecimal getHistoricAmount() {
        return this.historicAmount;
    }

    public void setHistoricAmount(BigDecimal historicAmount) {
        this.historicAmount = historicAmount;
    }

    public String toString() {
        return "PriceRuleContext(historicAmount=" + this.getHistoricAmount() + ")";
    }

    public boolean equals(final Object o) {
        if (o == this) return true;
        if (!(o instanceof PriceRuleContext)) return false;
        final PriceRuleContext other = (PriceRuleContext) o;
        if (!other.canEqual((Object) this)) return false;
        final Object this$historicAmount = this.getHistoricAmount();
        final Object other$historicAmount = other.getHistoricAmount();
        if (this$historicAmount == null ? other$historicAmount != null : !this$historicAmount.equals(other$historicAmount))
            return false;
        return true;
    }

    protected boolean canEqual(final Object other) {
        return other instanceof PriceRuleContext;
    }

    public int hashCode() {
        final int PRIME = 59;
        int result = 1;
        final Object $historicAmount = this.getHistoricAmount();
        result = result * PRIME + ($historicAmount == null ? 43 : $historicAmount.hashCode());
        return result;
    }

    public static class PriceRuleContextBuilder {
        private BigDecimal historicAmount;

        PriceRuleContextBuilder() {
        }

        public PriceRuleContext.PriceRuleContextBuilder historicAmount(BigDecimal historicAmount) {
            this.historicAmount = historicAmount;
            return this;
        }

        public PriceRuleContext build() {
            return new PriceRuleContext(historicAmount);
        }

        public String toString() {
            return "PriceRuleContext.PriceRuleContextBuilder(historicAmount=" + this.historicAmount + ")";
        }
    }
}
