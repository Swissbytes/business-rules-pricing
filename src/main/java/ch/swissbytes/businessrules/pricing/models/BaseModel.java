package ch.swissbytes.businessrules.pricing.models;

import ch.swissbytes.businessrules.pricing.utils.ToJsonString;


public class BaseModel {
    public String toJson(){
        return ToJsonString.toJson(this);
    }
}
