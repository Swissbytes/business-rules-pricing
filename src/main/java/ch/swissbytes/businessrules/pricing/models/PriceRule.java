package ch.swissbytes.businessrules.pricing.models;

import ch.swissbytes.businessrules.pricing.enums.*;
import ch.swissbytes.businessrules.pricing.factors.*;
import ch.swissbytes.businessrules.pricing.inputs.PriceComponentRuleInputSaleOrder;
import ch.swissbytes.businessrules.pricing.inputs.PriceComponentRuleInputSaleOrderProduct;
import ch.swissbytes.businessrules.pricing.outcomes.PriceComponentRuleOutcome;
import ch.swissbytes.businessrules.pricing.outcomes.PriceComponentRuleOutcomeOutput;
import ch.swissbytes.businessrules.pricing.outcomes.PriceComponentRuleOutcomeProduct;
import ch.swissbytes.businessrules.pricing.subjects.PriceComponentRuleSubjectProduct;
import ch.swissbytes.businessrules.pricing.subjects.PriceComponentRuleSubjectProductFeature;
import ch.swissbytes.businessrules.pricing.utils.BundleProvider;
import ch.swissbytes.businessrules.pricing.utils.LogUtil;
import ch.swissbytes.businessrules.pricing.utils.PriceRuleLog;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.tomgibara.bits.BitStore;
import com.tomgibara.bits.BitVector;
import org.jeasy.rules.api.Facts;
import org.jeasy.rules.api.Rule;
import org.jeasy.rules.api.Rules;
import org.jeasy.rules.api.RulesEngine;
import org.jeasy.rules.core.DefaultRulesEngine;
import org.jeasy.rules.core.RuleBuilder;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;
import java.util.stream.Collectors;


/**
 * We hacked this library in order to make it work for deploy, but we need to fix
 * all the commented code. This code may be need it for next iteration, that's why
 * we are not gonna remove it until the next release.
 */
@JsonIgnoreProperties(value = {"rulesEngine"})
public class PriceRule extends BaseModel {

    private static final int PRICE_RULE_FACTORS_SIZE = 13;

    //region Priorities of Inner Rules
    private static final int PRIORITY_DELIVERY_MODE_RULE = 1;
    private static final int PRIORITY_PAYMENT_CONDITION_RULE = 2;
    private static final int PRIORITY_BUSINESS_UNIT_RULE = 3;
    private static final int PRIORITY_AGENCY_RULE = 4;
    private static final int PRIORITY_ROLE_TYPE_RULE = 5;
    private static final int PRIORITY_CUSTOMER_RULE = 6;
    private static final int PRIORITY_PARTY_CLASSIFICATION_RULE = 7;
    private static final int PRIORITY_SALE_ORDER_ORIGIN_RULE = 8;
    private static final int PRIORITY_PRICELIST_RULE = 9;
    private static final int PRIORITY_GEOGRAPHIC_BOUNDARY_RULE = 10;
    private static final int PRIORITY_PRODUCT_RULE = 11;
    private static final int PRIORITY_PRODUCT_FEATURE_RULE = 12;
    private static final int PRIORITY_PRODUCT_CLASSIFICATION_RULE = 13;

    //endregion

    //region Index of Inner Rules in order to validate
    private static final int INDEX_DELIVERY_MODE_RULE = 0;
    private static final int INDEX_PAYMENT_CONDITION_RULE = 1;
    private static final int INDEX_BUSINESS_UNIT_RULE = 2;
    private static final int INDEX_AGENCY_RULE = 3;
    private static final int INDEX_ROLE_TYPE_RULE = 4;
    private static final int INDEX_CUSTOMER_RULE = 5;
    private static final int INDEX_PARTY_CLASSIFICATION_RULE = 6;
    private static final int INDEX_SALE_ORDER_ORIGIN_RULE = 7;
    private static final int INDEX_PRICELIST_RULE = 8;
    private static final int INDEX_GEOGRAPHIC_BOUNDARY_RULE = 9;
    private static final int INDEX_PRODUCT_RULE = 10;
    private static final int INDEX_PRODUCT_FEATURE_RULE = 11;
    private static final int INDEX_PRODUCT_CLASSIFICATION_RULE = 12;
    //endregion

    //region Rule single properties
    private long id;
    private String name;
    private String description;
    private Date fromDate;
    private Date thruDate;
    private boolean accumulative;
    //endregion

    private long priority = 1L;

    private PriceRuleTypeEnum ruleType = PriceRuleTypeEnum.RESTRICTED;

    private TargetEnum target = TargetEnum.PRODUCT;
    private OutComeModeEnum outComeMode = OutComeModeEnum.SINGLE;
    private ScaleTypeEnum validationType = ScaleTypeEnum.AMOUNT; //indicates what type of scale will be using in case the price rule is Rank Mode -> Scale

    private ExclusiveOutcomeEnum exclusiveOutcomeEnum;
    /**
     * Only will be use when outcomeMode is SCALE or FREQUENCY.
     * You can choose if you want to use the total Sale Order Amount in order to validate the scales or
     * use the total sum of the products defined in the subjects.
     */
    private boolean useSaleOrderTotalForOutcome = false;

    private PriceComponentRuleOutcomeOutput output;

    //region Subject to apply Rule
    private List<PriceComponentRuleSubjectProduct> products = new ArrayList<>(0);
    private List<PriceComponentRuleSubjectProductFeature> productFeatures = new ArrayList<>(0);
    private List<PriceComponentRuleFactorProductClassification> productClassifications = new ArrayList<>(0);
    //endregion

    //region General Factors
    private PaymentConditionsTypeEnum paymentCondition;

    private List<PriceComponentRuleFactorDeliveryMode> deliveryModes = new ArrayList<>(0);
    private List<PriceComponentRuleFactorPartyClassification> partyClassifications = new ArrayList<>(0);
    private List<PriceComponentRuleFactorGeographicBoundary> geographicBoundaries = new ArrayList<>(0);
    private List<PriceComponentRuleFactorCustomer> customers = new ArrayList<>(0);
    private List<PriceComponentRuleFactorAgency> agencies = new ArrayList<>(0);
    private List<PriceComponentRuleFactorBusinessUnit> businessUnits = new ArrayList<>(0);
    private List<PriceComponentRuleFactorRoleType> roleTypes = new ArrayList<>(0);
    private List<PriceComponentRuleFactorSaleOrderOrigin> saleOrderOrigins = new ArrayList<>(0);
    private List<PriceComponentRuleFactorPricelist> pricelists = new ArrayList<>(0);
    //endregion

    private RulesEngine rulesEngine = new DefaultRulesEngine();

    public PriceRule(long id,
                     String name,
                     String description,
                     Date fromDate,
                     Date thruDate,
                     boolean accumulative,
                     long priority,
                     PriceRuleTypeEnum ruleType,
                     TargetEnum target,
                     OutComeModeEnum outComeMode,
                     ScaleTypeEnum validationType,
                     boolean useSaleOrderTotalForOutcome,
                     PriceComponentRuleOutcomeOutput output,
                     List<PriceComponentRuleSubjectProduct> products,
                     List<PriceComponentRuleSubjectProductFeature> productFeatures,
                     List<PriceComponentRuleFactorProductClassification> productClassifications,
                     PaymentConditionsTypeEnum paymentCondition,
                     List<PriceComponentRuleFactorDeliveryMode> deliveryModes,
                     List<PriceComponentRuleFactorPartyClassification> partyClassifications,
                     List<PriceComponentRuleFactorGeographicBoundary> geographicBoundaries,
                     List<PriceComponentRuleFactorCustomer> customers,
                     List<PriceComponentRuleFactorAgency> agencies,
                     List<PriceComponentRuleFactorBusinessUnit> businessUnits,
                     List<PriceComponentRuleFactorRoleType> roleTypes,
                     RulesEngine rulesEngine,
                     List<PriceComponentRuleFactorSaleOrderOrigin> saleOrderOrigins,
                     List<PriceComponentRuleFactorPricelist> pricelists) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.fromDate = fromDate;
        this.thruDate = thruDate;
        this.accumulative = accumulative;
        this.priority = priority;
        this.ruleType = ruleType;
        this.target = target;
        this.outComeMode = outComeMode;
        this.validationType = validationType;
        this.useSaleOrderTotalForOutcome = useSaleOrderTotalForOutcome;
        this.output = output;
        this.products = products;
        this.productFeatures = productFeatures;
        this.productClassifications = productClassifications;
        this.paymentCondition = paymentCondition;
        this.deliveryModes = deliveryModes;
        this.partyClassifications = partyClassifications;
        this.geographicBoundaries = geographicBoundaries;
        this.customers = customers;
        this.agencies = agencies;
        this.businessUnits = businessUnits;
        this.roleTypes = roleTypes;
        this.rulesEngine = rulesEngine;
        this.exclusiveOutcomeEnum = ExclusiveOutcomeEnum.NONE;
        this.saleOrderOrigins = saleOrderOrigins;
        this.pricelists = pricelists;
    }

    public PriceRule(long id, String name, String description, Date fromDate, Date thruDate, boolean accumulative,
                     long priority, PriceRuleTypeEnum ruleType, TargetEnum target, OutComeModeEnum outComeMode,
                     ScaleTypeEnum validationType, boolean useSaleOrderTotalForOutcome, PriceComponentRuleOutcomeOutput output,
                     List<PriceComponentRuleSubjectProduct> products, List<PriceComponentRuleSubjectProductFeature> productFeatures,
                     List<PriceComponentRuleFactorProductClassification> productClassifications,
                     PaymentConditionsTypeEnum paymentCondition, List<PriceComponentRuleFactorDeliveryMode> deliveryModes,
                     List<PriceComponentRuleFactorPartyClassification> partyClassifications,
                     List<PriceComponentRuleFactorGeographicBoundary> geographicBoundaries,
                     List<PriceComponentRuleFactorCustomer> customers, List<PriceComponentRuleFactorAgency> agencies,
                     List<PriceComponentRuleFactorBusinessUnit> businessUnits, List<PriceComponentRuleFactorRoleType> roleTypes,
                     RulesEngine rulesEngine, ExclusiveOutcomeEnum exclusiveOutcomeEnum,
                     List<PriceComponentRuleFactorSaleOrderOrigin> saleOrderOrigins,
                     List<PriceComponentRuleFactorPricelist> pricelists) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.fromDate = fromDate;
        this.thruDate = thruDate;
        this.accumulative = accumulative;
        this.priority = priority;
        this.ruleType = ruleType;
        this.target = target;
        this.outComeMode = outComeMode;
        this.validationType = validationType;
        this.useSaleOrderTotalForOutcome = useSaleOrderTotalForOutcome;
        this.output = output;
        this.products = products;
        this.productFeatures = productFeatures;
        this.productClassifications = productClassifications;
        this.paymentCondition = paymentCondition;
        this.deliveryModes = deliveryModes;
        this.partyClassifications = partyClassifications;
        this.geographicBoundaries = geographicBoundaries;
        this.customers = customers;
        this.agencies = agencies;
        this.businessUnits = businessUnits;
        this.roleTypes = roleTypes;
        this.rulesEngine = rulesEngine;
        this.exclusiveOutcomeEnum = exclusiveOutcomeEnum;
        this.saleOrderOrigins = saleOrderOrigins;
        this.pricelists = pricelists;
    }

    public PriceRule() {
    }

    public static PriceRuleBuilder builder() {
        return new PriceRuleBuilder();
    }

    //region Validations
    private boolean validateSaleOrderDeliveryMode(PriceComponentRuleInputSaleOrder saleOrder) {
        if (Objects.isNull(this.deliveryModes) || this.deliveryModes.isEmpty()) {
            return Boolean.TRUE;
        }
        return Objects.nonNull(saleOrder.getDeliveryMode()) && this.deliveryModes.stream().anyMatch(deliveryMode -> deliveryMode.isValid(saleOrder));
    }

    private boolean validateSaleOrderPaymentCondition(PriceComponentRuleInputSaleOrder saleOrder) {
        if (Objects.isNull(this.paymentCondition))
            return Boolean.TRUE;
        return Objects.nonNull(saleOrder.getPaymentCondition()) && saleOrder.getPaymentCondition().equals(this.paymentCondition);
    }

    private boolean validateBusinessUnitByRestriction(PriceComponentRuleInputSaleOrder saleOrder) {
        if (Objects.isNull(this.businessUnits) || this.businessUnits.isEmpty())
            return Boolean.TRUE;
        return this.businessUnits.stream().anyMatch(bu -> bu.isValid(saleOrder));
    }

    /**
     * Returns the scale applicable to the sale Order.
     * It is useful, to get the outcome of the rule
     * return PriceComponentRuleFactorScale
     */
    public PriceComponentRuleFactorScale getApplicableScale(PriceComponentRuleInputSaleOrder saleOrder) {
        if (Objects.isNull(this.output.getScales()) || this.output.getScales().isEmpty()) {
            throw new RuntimeException(PriceRuleLog.createScalesInvalid().getDetail());
        }
        Optional<PriceComponentRuleFactorScale> optional = Optional.empty();
        if (this.canUseAllSaleOrder() || this.useSaleOrderTotalForOutcome) {
            optional = this.output.getScales().stream().filter(scale -> scale.isValid(calculateAllProductsTotal(saleOrder))).findFirst();
        } else {
            if (Objects.nonNull(products) && !products.isEmpty())
                optional = this.output.getScales().stream().filter(scale -> scale.isValid(calculateProductsTotal(saleOrder))).findFirst();
            else if (Objects.nonNull(productFeatures) && !productFeatures.isEmpty())
                optional = this.output.getScales().stream().filter(scale -> scale.isValid(calculateProductFeaturesTotal(saleOrder))).findFirst();
            else if (Objects.nonNull(productClassifications) && !productClassifications.isEmpty())
                optional = this.output.getScales().stream().filter(scale -> scale.isValid(calculateProductClassificationsTotal(saleOrder))).findFirst();
        }
        return optional.orElse(null);
    }

    public PriceComponentRuleFactorScale getApplicableScaleByTotal(BigDecimal total) {
        if (Objects.isNull(this.output.getScales()) || this.output.getScales().isEmpty()) {
            throw new RuntimeException(PriceRuleLog.createScalesInvalid().getDetail());
        }
        Optional<PriceComponentRuleFactorScale> optional = Optional.empty();
        if (this.canUseAllSaleOrder() || this.useSaleOrderTotalForOutcome) {
            optional = this.output.getScales().stream().filter(scale -> scale.isValid(total)).findFirst();
        } else {
            if (Objects.nonNull(products) && !products.isEmpty())
                optional = this.output.getScales().stream().filter(scale -> scale.isValid(total)).findFirst();
            else if (Objects.nonNull(productFeatures) && !productFeatures.isEmpty())
                optional = this.output.getScales().stream().filter(scale -> scale.isValid(total)).findFirst();
            else if (Objects.nonNull(productClassifications) && !productClassifications.isEmpty())
                optional = this.output.getScales().stream().filter(scale -> scale.isValid(total)).findFirst();
        }
        return optional.orElse(null);
    }

    /**
     * Returns how many times the frequency of the outcome is applied in the saleOrder.
     * It is useful, to get the outcome of the rule
     * return int
     */
    public BigDecimal getApplicableFrequency(PriceComponentRuleInputSaleOrder saleOrder) {
        if (Objects.isNull(this.output.getFrequency())) {
            throw new RuntimeException(PriceRuleLog.createFrequencyInvalid().getDetail());
        }
        BigDecimal total = BigDecimal.ZERO;
        if (canUseAllSaleOrder() || this.useSaleOrderTotalForOutcome)
            total = calculateAllProductsTotal(saleOrder);
        else if (Objects.nonNull(products) && !products.isEmpty())
            total = calculateProductsTotal(saleOrder);
        else if (Objects.nonNull(productFeatures) && !productFeatures.isEmpty())
            total = calculateProductFeaturesTotal(saleOrder);
        else if (Objects.nonNull(productClassifications) && !productClassifications.isEmpty())
            total = calculateProductClassificationsTotal(saleOrder);
        return total.divide(this.output.getFrequency(), 0, RoundingMode.FLOOR);
    }

    public BigDecimal getApplicableFrequencyByProduct(PriceComponentRuleInputSaleOrder saleOrder,
                                                      PriceComponentRuleInputSaleOrderProduct product) {
        if (Objects.isNull(this.output.getFrequency())) {
            throw new RuntimeException(PriceRuleLog.createFrequencyInvalid().getDetail());
        }
        BigDecimal total;
        if (this.useSaleOrderTotalForOutcome)
            total = calculateAllProductsTotal(saleOrder);
        else if (Objects.nonNull(products) && !products.isEmpty())
            total = calculateProductsTotalByProduct(saleOrder);
        else if (Objects.nonNull(productFeatures) && !productFeatures.isEmpty())
            total = calculateProductFeaturesTotalByProduct(saleOrder);
        else if (Objects.nonNull(productClassifications) && !productClassifications.isEmpty())
            total = calculateProductClassificationsTotalByProduct(saleOrder);
        else
            total = calculateProductsTotal(product);

        return total.divide(this.output.getFrequency(), 0, RoundingMode.FLOOR);
    }

    public PriceComponentRuleOutcome getOutcome(PriceComponentRuleInputSaleOrder saleOrder) {
        switch (this.output.getOutComeModeEnum()) {
            case SCALE:
                return this.getOutput().getOutcomeScale(getApplicableScale(saleOrder));
            case FREQUENCY:
                return this.getOutput().getOutcomeFrequency(getApplicableFrequency(saleOrder));
            case SINGLE:
            default:
                return this.output.getOutcome();
        }
    }

    public PriceComponentRuleOutcome getOutcomeByProduct(PriceComponentRuleInputSaleOrder saleOrder,
                                                         PriceComponentRuleInputSaleOrderProduct product) {
        switch (this.output.getOutComeModeEnum()) {
            case SCALE:
                return this.getOutput().getOutcomeScale(getApplicableScale(saleOrder));
            case FREQUENCY:
                return this.getOutput().getOutcomeFrequency(getApplicableFrequencyByProduct(saleOrder, product));
            case SINGLE:
            default:
                return this.output.getOutcome();
        }
    }

    /**
     * Steps:
     * 0.- Check outcomeMode, if its single, do step 1 and finish.
     * 1.- It takes all the products defined in the rule and all must match.
     * 2.- Sum all the matches products
     * 3.1 - (Scales) Check if sum is between the ranges in the scale
     * 3.2 - (Frequency) Check if sum is higher or equals to the frequency
     */
    //region Validate Products
    public boolean validateProducts(PriceComponentRuleInputSaleOrder saleOrder) {
        switch (this.ruleType) {
            default:
            case GENERAL:
                return validateProductsByActivation(saleOrder);
            case RESTRICTED:
                return validateProductsByRestriction(saleOrder);
        }
    }

    private boolean validateProductsScalesByRestriction(PriceComponentRuleInputSaleOrder saleOrder, BigDecimal total) {
        if (Objects.isNull(this.output.getScales()) || this.output.getScales().isEmpty()) {
            throw new RuntimeException(PriceRuleLog.createScalesInvalid().getDetail());
        }
        return this.validateProductsSingleByRestriction(saleOrder) && this.output.getScales().stream().anyMatch(scale -> scale.isValid(total));
    }

    private boolean validateProductsFrequencyByRestriction(PriceComponentRuleInputSaleOrder saleOrder, BigDecimal total) {
        if (Objects.isNull(this.output.getFrequency())) {
            throw new RuntimeException(PriceRuleLog.createFrequencyInvalid().getDetail());
        }
        return this.validateProductsSingleByRestriction(saleOrder) && total.compareTo(this.output.getFrequency()) >= 0;
    }

    private boolean validateProductsSingleByRestriction(PriceComponentRuleInputSaleOrder saleOrder) {
        return this.products.stream().allMatch(product -> product.isValid(saleOrder));
    }

    private boolean validateProductsScalesByActivation(PriceComponentRuleInputSaleOrder saleOrder, BigDecimal total) {
        if (Objects.isNull(this.output.getScales()) || this.output.getScales().isEmpty()) {
            throw new RuntimeException(PriceRuleLog.createScalesInvalid().getDetail());
        }
        return this.validateProductsSingleByActivation(saleOrder) && this.output.getScales().stream().anyMatch(scale -> scale.isValid(total));
    }

    private boolean validateProductsFrequencyByActivation(PriceComponentRuleInputSaleOrder saleOrder, BigDecimal total) {
        if (Objects.isNull(this.output.getFrequency())) {
            throw new RuntimeException(PriceRuleLog.createFrequencyInvalid().getDetail());
        }
        return this.validateProductsSingleByActivation(saleOrder) && total.compareTo(this.output.getFrequency()) >= 0;
    }

    private boolean validateProductsSingleByActivation(PriceComponentRuleInputSaleOrder saleOrder) {
        return this.products.stream().anyMatch(product -> product.isValid(saleOrder));
    }
    //endregion

    public BigDecimal calculateSaleOrderProductsTotal(PriceComponentRuleInputSaleOrder saleOrder) {
        switch (this.validationType) {
            case QUANTITY:
                return saleOrder.getTotalQuantities();
            case AMOUNT:
            default:
                return saleOrder.getTotalAmount();
        }
    }

    public BigDecimal calculateProductsTotal(PriceComponentRuleInputSaleOrder saleOrder) {
        BigDecimal sum = saleOrder.getProductDetails().stream()
                .filter(product -> this.products.stream().anyMatch(ruleProduct -> ruleProduct.isValidProduct(product)))
                .map(this.validationType == ScaleTypeEnum.QUANTITY
                        ? PriceComponentRuleInputSaleOrderProduct::getQty
                        : PriceComponentRuleInputSaleOrderProduct::getTotalPriceGross)
                .reduce(BigDecimal::add).orElse(BigDecimal.ZERO);
        if (this.ruleType.isRestricted()) {
            if (!this.products.stream().allMatch(ruleProduct -> ruleProduct.isValid(saleOrder))) {
                sum = BigDecimal.ZERO;
            }
        }
        if (sum.equals(BigDecimal.ZERO)) {
            LogUtil.log(PriceRuleLog.createProductTotalZero(this.validationType));
        }
        return sum;
    }

    public BigDecimal calculateProductsTotalByProduct(PriceComponentRuleInputSaleOrder saleOrder) {

        BigDecimal sum = saleOrder.getProductDetails().stream()
                .filter(sol -> this.products.stream().anyMatch(ruleProduct -> ruleProduct.isValidProduct(sol)))
                .map(this.validationType == ScaleTypeEnum.QUANTITY
                        ? PriceComponentRuleInputSaleOrderProduct::getQty
                        : PriceComponentRuleInputSaleOrderProduct::getTotalPriceGross)
                .reduce(BigDecimal::add).orElse(BigDecimal.ZERO);

        if (sum.equals(BigDecimal.ZERO))
            LogUtil.log(PriceRuleLog.createProductTotalZero(this.validationType));

        return sum;
    }

    public BigDecimal calculateAllProductsTotal(PriceComponentRuleInputSaleOrder saleOrder) {
        BigDecimal sum;
        switch (this.validationType) {
            case QUANTITY:
                sum = saleOrder.getProductDetails().stream()
                        .map(PriceComponentRuleInputSaleOrderProduct::getQty).reduce(BigDecimal::add).orElse(BigDecimal.ZERO);
                break;
            case AMOUNT:
            default:
                sum = saleOrder.getProductDetails().stream()
                        .map(PriceComponentRuleInputSaleOrderProduct::getTotalPriceGross).reduce(BigDecimal::add).orElse(BigDecimal.ZERO);
                break;
        }
        if (sum.equals(BigDecimal.ZERO)) {
            LogUtil.log(PriceRuleLog.createProductTotalZero(this.validationType));
        }
        return sum;
    }

    public BigDecimal calculateProductFeaturesTotal(PriceComponentRuleInputSaleOrder saleOrder) {
        BigDecimal sum = saleOrder.getProductDetails().stream()
                .filter(product -> this.ruleType.isRestricted()
                        ? this.productFeatures.stream().allMatch(ruleProduct -> ruleProduct.isValidProduct(product))
                        : this.productFeatures.stream().anyMatch(ruleProduct -> ruleProduct.isValidProduct(product)))
                .map(this.validationType == ScaleTypeEnum.QUANTITY
                        ? PriceComponentRuleInputSaleOrderProduct::getQty
                        : PriceComponentRuleInputSaleOrderProduct::getTotalPriceGross)
                .reduce(BigDecimal::add).orElse(BigDecimal.ZERO);
        ;
        if (sum.equals(BigDecimal.ZERO)) {
            LogUtil.log(PriceRuleLog.createProductFeaturesTotalZero(this.validationType));
        }
        return sum;
    }

    public BigDecimal calculateProductFeaturesTotalByProduct(PriceComponentRuleInputSaleOrder saleOrder) {

        BigDecimal sum = saleOrder.getProductDetails().stream()
                .filter(sol -> this.productFeatures.stream().anyMatch(ruleProduct -> ruleProduct.isValidProduct(sol)))
                .map(this.validationType == ScaleTypeEnum.QUANTITY
                        ? PriceComponentRuleInputSaleOrderProduct::getQty
                        : PriceComponentRuleInputSaleOrderProduct::getTotalPriceGross)
                .reduce(BigDecimal::add).orElse(BigDecimal.ZERO);

        if (sum.equals(BigDecimal.ZERO)) {
            LogUtil.log(PriceRuleLog.createProductFeaturesTotalZero(this.validationType));
        }
        return sum;
    }


    public BigDecimal calculateProductClassificationsTotal(PriceComponentRuleInputSaleOrder saleOrder) {
        BigDecimal sum = saleOrder.getProductDetails().stream()
                .filter(product -> this.ruleType.isRestricted()
                        ? this.productClassifications.stream().allMatch(ruleProduct -> ruleProduct.isValidProduct(product))
                        : this.productClassifications.stream().anyMatch(ruleProduct -> ruleProduct.isValidProduct(product)))
                .map(this.validationType == ScaleTypeEnum.QUANTITY
                        ? PriceComponentRuleInputSaleOrderProduct::getQty
                        : PriceComponentRuleInputSaleOrderProduct::getTotalPriceGross)
                .reduce(BigDecimal::add).orElse(BigDecimal.ZERO);
        if (sum.equals(BigDecimal.ZERO)) {
            LogUtil.log(PriceRuleLog.createProductClassificationTotalZero(this.validationType));
        }
        return sum;
    }

    public BigDecimal calculateProductClassificationsTotalByProduct(PriceComponentRuleInputSaleOrder saleOrder) {

        BigDecimal sum = saleOrder.getProductDetails().stream()
                .filter(sol -> this.productClassifications.stream().anyMatch(ruleProduct -> ruleProduct.isValidProduct(sol)))
                .map(this.validationType == ScaleTypeEnum.QUANTITY
                        ? PriceComponentRuleInputSaleOrderProduct::getQty
                        : PriceComponentRuleInputSaleOrderProduct::getTotalPriceGross)
                .reduce(BigDecimal::add).orElse(BigDecimal.ZERO);

        if (sum.equals(BigDecimal.ZERO))
            LogUtil.log(PriceRuleLog.createProductClassificationTotalZero(this.validationType));

        return sum;
    }

    public BigDecimal calculateProductsTotal(PriceComponentRuleInputSaleOrderProduct product) {
        BigDecimal sum;
        switch (this.validationType) {
            case QUANTITY:
                sum = product.getQty();
                break;
            case AMOUNT:
            default:
                sum = product.calculateTotalPriceGross();
                break;
        }
        if (sum.equals(BigDecimal.ZERO)) {
            LogUtil.log(PriceRuleLog.createProductTotalZero(this.validationType));
        }
        return sum;
    }

    private boolean validateProductsByRestriction(PriceComponentRuleInputSaleOrder saleOrder) {
        if (Objects.nonNull(products) && !products.isEmpty()) {
            switch (this.outComeMode) {
                case SCALE:
                    return validateProductsScalesByRestriction(saleOrder, this.useSaleOrderTotalForOutcome
                            ? calculateAllProductsTotal(saleOrder) : calculateProductsTotal(saleOrder));
                case FREQUENCY:
                    return validateProductsFrequencyByRestriction(saleOrder, this.useSaleOrderTotalForOutcome
                            ? calculateAllProductsTotal(saleOrder) : calculateProductsTotal(saleOrder));
                case SINGLE:
                default:
                    return validateProductsSingleByRestriction(saleOrder);
            }
        } else {
            return true;
        }
    }

    private boolean validateProductsByActivation(PriceComponentRuleInputSaleOrder saleOrder) {
        if (Objects.nonNull(products) && !products.isEmpty()) {
            switch (this.outComeMode) {
                case SCALE:
                    return validateProductsScalesByActivation(saleOrder, this.useSaleOrderTotalForOutcome ?
                            calculateAllProductsTotal(saleOrder) :
                            calculateProductsTotal(saleOrder));
                case FREQUENCY:
                    return validateProductsFrequencyByActivation(saleOrder, this.useSaleOrderTotalForOutcome ?
                            calculateAllProductsTotal(saleOrder) :
                            calculateProductsTotal(saleOrder));
                case SINGLE:
                default:
                    return validateProductsSingleByActivation(saleOrder);
            }
        } else {
            return true;
        }
    }

    private boolean validateProductFeatures(PriceComponentRuleInputSaleOrder saleOrder) {
        switch (this.ruleType) {
            default:
            case GENERAL:
                return validateProductFeaturesByActivation(saleOrder);
            case RESTRICTED:
                return validateProductFeaturesByRestriction(saleOrder);
        }
    }

    private boolean validateProductFeaturesByActivation(PriceComponentRuleInputSaleOrder saleOrder) {
        if (Objects.nonNull(productFeatures) && !productFeatures.isEmpty()) {
            switch (this.outComeMode) {
                case SCALE:
                    return validateScale(this.useSaleOrderTotalForOutcome ?
                                    calculateAllProductsTotal(saleOrder) :
                                    calculateProductFeaturesTotal(saleOrder),
                            validateProductFeaturesSingleByActivation(saleOrder));
                case FREQUENCY:
                    return validateFrequency(this.useSaleOrderTotalForOutcome ?
                                    calculateAllProductsTotal(saleOrder) :
                                    calculateProductFeaturesTotal(saleOrder),
                            validateProductFeaturesSingleByActivation(saleOrder));
                case SINGLE:
                default:
                    return validateProductFeaturesSingleByActivation(saleOrder);
            }
        } else {
            return true;
        }
    }

    private boolean validateProductFeaturesByRestriction(PriceComponentRuleInputSaleOrder saleOrder) {
        if (Objects.nonNull(productFeatures) && !productFeatures.isEmpty()) {
            switch (this.outComeMode) {
                case SCALE:
                    return validateScale(this.useSaleOrderTotalForOutcome ?
                                    calculateAllProductsTotal(saleOrder) :
                                    calculateProductFeaturesTotal(saleOrder),
                            validateProductFeaturesSingleByRestriction(saleOrder));
                case FREQUENCY:
                    return validateFrequency(this.useSaleOrderTotalForOutcome ?
                                    calculateAllProductsTotal(saleOrder) :
                                    calculateProductFeaturesTotal(saleOrder),
                            validateProductFeaturesSingleByRestriction(saleOrder));
                case SINGLE:
                default:
                    return validateProductFeaturesSingleByRestriction(saleOrder);
            }
        } else {
            return true;
        }
    }

    private boolean validateAllProducts(PriceComponentRuleInputSaleOrder saleOrder) {
        switch (this.outComeMode) {
            case SCALE:
                return validateScale(calculateAllProductsTotal(saleOrder), Boolean.TRUE);
            case FREQUENCY:
                return validateFrequency(calculateAllProductsTotal(saleOrder), Boolean.TRUE);
            case SINGLE:
            default:
                return Boolean.TRUE;
        }
    }

    private boolean validateScale(BigDecimal total, boolean singleValidation) {
        if (Objects.isNull(this.output.getScales()) || this.output.getScales().isEmpty()) {
            throw new RuntimeException(PriceRuleLog.createScalesInvalid().getDetail());
        }
        return singleValidation && this.output.getScales().stream().anyMatch(scale -> scale.isValid(total));
    }

    private boolean validateFrequency(BigDecimal total, boolean singleValidation) {
        if (Objects.isNull(this.output.getFrequency())) {
            throw new RuntimeException(PriceRuleLog.createFrequencyInvalid().getDetail());
        }
        return singleValidation && total.compareTo(this.output.getFrequency()) >= 0;
    }

    private boolean validateProductFeaturesSingleByRestriction(PriceComponentRuleInputSaleOrder saleOrder) {
        return this.productFeatures.stream().allMatch(feature -> feature.isValid(saleOrder));
    }

    private boolean validateProductFeaturesSingleByActivation(PriceComponentRuleInputSaleOrder saleOrder) {
        return this.productFeatures.stream().anyMatch(feature -> feature.isValid(saleOrder));
    }

    public boolean validateProductClassifications(PriceComponentRuleInputSaleOrder saleOrder) {
        switch (this.ruleType) {
            default:
            case GENERAL:
                return validateProductClassificationByActivation(saleOrder);
            case RESTRICTED:
                return validateProductClassificationByRestriction(saleOrder);
        }
    }

    public boolean validateProductClassificationByActivation(PriceComponentRuleInputSaleOrder saleOrder) {
        if (Objects.nonNull(productClassifications) && !productClassifications.isEmpty()) {
            switch (this.outComeMode) {
                case SCALE:
                    return validateScale(calculateProductClassificationsTotal(saleOrder), validateProductClassificationSingleByActivation(saleOrder));
                case FREQUENCY:
                    return validateFrequency(calculateProductClassificationsTotal(saleOrder), validateProductClassificationSingleByActivation(saleOrder));
                case SINGLE:
                default:
                    return validateProductClassificationSingleByActivation(saleOrder);
            }
        } else {
            return true;
        }
    }

    private boolean validateProductClassificationByRestriction(PriceComponentRuleInputSaleOrder saleOrder) {
        if (Objects.nonNull(productClassifications) && !productClassifications.isEmpty()) {
            switch (this.outComeMode) {
                case SCALE:
                    return validateScale(calculateProductClassificationsTotal(saleOrder),
                            validateProductClassificationSingleByRestriction(saleOrder));
                case FREQUENCY:
                    return validateFrequency(calculateProductClassificationsTotal(saleOrder),
                            validateProductClassificationSingleByRestriction(saleOrder));
                case SINGLE:
                default:
                    return validateProductClassificationSingleByRestriction(saleOrder);
            }
        } else {
            return true;
        }
    }

    private boolean validateProductClassificationSingleByRestriction(PriceComponentRuleInputSaleOrder saleOrder) {
        return this.productClassifications.stream().allMatch(classification -> classification.isValid(saleOrder));
    }

    private boolean validateProductClassificationSingleByActivation(PriceComponentRuleInputSaleOrder saleOrder) {
        return this.productClassifications.stream().anyMatch(classification -> classification.isValid(saleOrder));
    }

    private boolean validatePartyClassificationByRestriction(PriceComponentRuleInputSaleOrder saleOrder) {
        if (Objects.isNull(partyClassifications) || partyClassifications.isEmpty())
            return Boolean.TRUE;
        return partyClassifications.stream()
                .anyMatch(partyClassification -> partyClassification.isValid(saleOrder));
    }

    private boolean validateGeographicBoundaryByRestriction(PriceComponentRuleInputSaleOrder saleOrder) {
        if (Objects.isNull(geographicBoundaries) || geographicBoundaries.isEmpty())
            return Boolean.TRUE;
        return geographicBoundaries.stream()
                .anyMatch(geographicBoundary -> geographicBoundary.isValid(saleOrder));
    }

    private boolean validateCustomerByRestriction(PriceComponentRuleInputSaleOrder saleOrder) {
        if (Objects.isNull(customers) || customers.isEmpty())
            return Boolean.TRUE;
        return customers.stream().anyMatch(party -> party.isValid(saleOrder));
    }

    private boolean validateAgencyByRestriction(PriceComponentRuleInputSaleOrder saleOrder) {
        if (Objects.isNull(agencies) || agencies.isEmpty())
            return Boolean.TRUE;
        return agencies.stream().anyMatch(agency -> agency.isValid(saleOrder));
    }

    public boolean validateRoleTypeByRestriction(PriceComponentRuleInputSaleOrder saleOrder) {
        if (Objects.isNull(roleTypes) || roleTypes.isEmpty())
            return Boolean.TRUE;
        return roleTypes.stream().anyMatch(roleType -> roleType.isValid(saleOrder));
    }

    public boolean validateSaleOrderOriginsByRestriction(PriceComponentRuleInputSaleOrder saleOrder) {
        if (Objects.isNull(saleOrderOrigins) || saleOrderOrigins.isEmpty())
            return Boolean.TRUE;
        return saleOrderOrigins.stream().anyMatch(saleOrderOrigin -> saleOrderOrigin.isValid(saleOrder));
    }

    public boolean validatePricelistByRestriction(PriceComponentRuleInputSaleOrder saleOrder) {
        if (Objects.isNull(pricelists) || pricelists.isEmpty())
            return Boolean.TRUE;
        return pricelists.stream().anyMatch(pricelist -> pricelist.isValid(saleOrder));
    }
    //endregion

    private boolean isRuleValid(BitStore bitStore, boolean isRestricted) {
        LogUtil.log(PriceRuleLog.createRuleValidation(this));

        logEvaluatedFactors(bitStore, isRestricted);

        boolean ans = bitStore.asList().stream().allMatch(val -> val);
        LogUtil.log(ans ? PriceRuleLog.createPriceRuleValid(this) : PriceRuleLog.createPriceRuleFail(this));
        return ans;
    }

    private void logEvaluatedFactors(BitStore bitStore, boolean isRestricted) {
        LogUtil.log(bitStore.getBit(INDEX_DELIVERY_MODE_RULE) ?
                PriceRuleLog.createDeliveryModeRuleSuccess() : PriceRuleLog.createDeliveryModeRuleFail());

        LogUtil.log(bitStore.getBit(INDEX_PAYMENT_CONDITION_RULE) ?
                PriceRuleLog.createPaymentConditionRuleSuccess() : PriceRuleLog.createPaymentConditionRuleFail());

        LogUtil.log(bitStore.getBit(INDEX_BUSINESS_UNIT_RULE) ?
                PriceRuleLog.createBusinessUnitRuleSuccess() : PriceRuleLog.createBusinessUnitRuleFail());

        LogUtil.log(bitStore.getBit(INDEX_AGENCY_RULE) ?
                PriceRuleLog.createAgencyRuleSuccess() : PriceRuleLog.createAgencyRuleFail());

        LogUtil.log(bitStore.getBit(INDEX_ROLE_TYPE_RULE) ?
                PriceRuleLog.createRoleTypeRuleSuccess() : PriceRuleLog.createRoleTypeRuleFail());

        LogUtil.log(bitStore.getBit(INDEX_CUSTOMER_RULE) ?
                PriceRuleLog.createCustomerRuleSuccess() : PriceRuleLog.createCustomerRuleFail());

        LogUtil.log(bitStore.getBit(INDEX_PARTY_CLASSIFICATION_RULE) ?
                PriceRuleLog.createPartyClassificationRuleSuccess() : PriceRuleLog.createPartyClassificationRuleFail());

        LogUtil.log(bitStore.getBit(INDEX_GEOGRAPHIC_BOUNDARY_RULE) ?
                PriceRuleLog.createGeographicBoundaryRuleSuccess() : PriceRuleLog.createGeographicBoundaryRuleFail());

        LogUtil.log(bitStore.getBit(INDEX_PRODUCT_RULE)
                ? PriceRuleLog.createProductRuleSuccess(isRestricted)
                : logRuleFail(PriceRuleLog.createProductRuleFail(isRestricted)));

        LogUtil.log(bitStore.getBit(INDEX_PRODUCT_FEATURE_RULE)
                ? PriceRuleLog.createProductFeatureRuleSuccess(isRestricted)
                : logRuleFail(PriceRuleLog.createProductFeatureRuleFail(isRestricted)));

        LogUtil.log(bitStore.getBit(INDEX_PRODUCT_CLASSIFICATION_RULE)
                ? PriceRuleLog.createProductClassificationRuleSuccess(isRestricted)
                : logRuleFail(PriceRuleLog.createProductClassificationRuleFail(isRestricted)));

        LogUtil.log(bitStore.getBit(INDEX_SALE_ORDER_ORIGIN_RULE)
                ? PriceRuleLog.createSaleOrderOriginRuleSuccess()
                : PriceRuleLog.createSaleOrderOriginRuleFail());

        LogUtil.log(bitStore.getBit(INDEX_PRICELIST_RULE)
                ? PriceRuleLog.createPricelistRuleSuccess()
                : PriceRuleLog.createPricelistRuleSuccess());
    }

    private PriceRuleLog logRuleFail(PriceRuleLog ruleFailLog) {
        switch (this.outComeMode) {
            case SCALE:
                LogUtil.log(PriceRuleLog.createRuleFailCheckScale(this.validationType));
                break;
            case FREQUENCY:
                LogUtil.log(PriceRuleLog.createRuleFailCheckFrequency(this.validationType));
                break;
        }
        return ruleFailLog;
    }

    public boolean isApplicable(PriceComponentRuleInputSaleOrder saleOrder) {
        BitStore isRuleValid = new BitVector(PRICE_RULE_FACTORS_SIZE);
        Facts facts = new Facts();
        facts.put("saleOrder", saleOrder);

        //Activations Rules-> if one of the components in the all segments of the rule match the conditions, the rule applies.
        Rules rules = new Rules();

        //region 1. Delivery Mode Rule
        Rule deliveryModeRule = this.createDeliveryModeRule(isRuleValid);
        rules.register(deliveryModeRule);
        //endregion

        //region 2. Payment Condition Rule
        Rule paymentConditionsRule = createPaymentConditionRule(isRuleValid);
        rules.register(paymentConditionsRule);
        //endregion

        //region 3. Business Unit Rule
        Rule businessUnitRule = createBusinessRule(isRuleValid);
        rules.register(businessUnitRule);
        //endregion

        //region 4. Agency Rule
        Rule agencyRule = createAgencyRule(isRuleValid);
        rules.register(agencyRule);
        //endregion

        //region 5. Role Type Rule
        Rule roleTypeRule = createRoleTypeRule(isRuleValid);
        rules.register(roleTypeRule);
        //endregion

        //region 6. Customer Rule
        Rule customerRule = createCustomerRule(isRuleValid);
        rules.register(customerRule);
        //endregion

        //region 7. Party Classification Rule
        Rule partyClassificationsRule = createPartyClassificationRule(isRuleValid);
        rules.register(partyClassificationsRule);
        //endregion

        //region 8. Geographic Boundary Rule
        Rule geographicBoundariesRule = createGeographicBoundaryRule(isRuleValid);
        rules.register(geographicBoundariesRule);
        //endregion

        //region 9. Product Rule
        Rule productRule = createProductRule(isRuleValid);
        rules.register(productRule);
        //endregion

        //region 10. Product Feature Rule
        Rule featureRule = createProductFeatureRule(isRuleValid);
        rules.register(featureRule);
        //endregion

        //region 11. Product Classification Rule
        Rule classificationRule = createProductClassificationRule(isRuleValid);
        rules.register(classificationRule);
        //endregion

        //region 12. Sale Order Origin Rule
        Rule saleOrderOriginRule = createSaleOrderOriginRule(isRuleValid);
        rules.register(saleOrderOriginRule);
        //endregion

        //region 13. Pricelist Rule
        Rule pricelistRule = createPricelistRule(isRuleValid);
        rules.register(pricelistRule);
        //endregion

        rulesEngine.fire(rules, facts);

        return this.isRuleValid(isRuleValid, this.ruleType.isRestricted());
    }

    //region Factor Rules
    private Rule createDeliveryModeRule(BitStore isRuleValid) {
        return new RuleBuilder()
                .priority(PRIORITY_DELIVERY_MODE_RULE)
                .name(BundleProvider.getString("deliveryModeRuleName"))
                .description(BundleProvider.getString("deliveryModeRuleDescription"))
                .when(factDeliveryMode -> validateSaleOrderDeliveryMode(factDeliveryMode.get("saleOrder")))
                .then(factors -> isRuleValid.setBit(INDEX_DELIVERY_MODE_RULE, true))
                .build();
    }

    private Rule createPaymentConditionRule(BitStore isRuleValid) {
        return new RuleBuilder()
                .priority(PRIORITY_PAYMENT_CONDITION_RULE)
                .name(BundleProvider.getString("paymentConditionsRuleName"))
                .description(BundleProvider.getString("paymentConditionsRuleDescription"))
                .when(factPaymentConditions -> validateSaleOrderPaymentCondition(factPaymentConditions.get("saleOrder")))
                .then(factors -> isRuleValid.setBit(INDEX_PAYMENT_CONDITION_RULE, true))
                .build();
    }

    private Rule createBusinessRule(BitStore isRuleValid) {
        return new RuleBuilder()
                .priority(PRIORITY_BUSINESS_UNIT_RULE)
                .name(BundleProvider.getString("businessUnitRuleName"))
                .description(BundleProvider.getString("businessUnitRuleDescription"))
                .when(factBu -> validateBusinessUnitByRestriction(factBu.get("saleOrder")))
                .then(factors -> isRuleValid.setBit(INDEX_BUSINESS_UNIT_RULE, true))
                .build();
    }

    private Rule createAgencyRule(BitStore isRuleValid) {
        return new RuleBuilder()
                .priority(PRIORITY_AGENCY_RULE)
                .name(BundleProvider.getString("agencyRuleName"))
                .description(BundleProvider.getString("agencyRuleDescription"))
                .when(factParty -> validateAgencyByRestriction(factParty.get("saleOrder")))
                .then(factors -> isRuleValid.setBit(INDEX_AGENCY_RULE, true))
                .build();
    }

    private Rule createRoleTypeRule(BitStore isRuleValid) {
        return new RuleBuilder()
                .priority(PRIORITY_ROLE_TYPE_RULE)
                .name(BundleProvider.getString("roleTypeRuleName"))
                .description(BundleProvider.getString("roleTypeRuleDescription"))
                .when(factParty -> validateRoleTypeByRestriction(factParty.get("saleOrder")))
                .then(factors -> isRuleValid.setBit(INDEX_ROLE_TYPE_RULE, true))
                .build();
    }

    private Rule createCustomerRule(BitStore isRuleValid) {
        return new RuleBuilder()
                .priority(PRIORITY_CUSTOMER_RULE)
                .name(BundleProvider.getString("customerRuleName"))
                .description(BundleProvider.getString("customerRuleDescription"))
                .when(factParty -> validateCustomerByRestriction(factParty.get("saleOrder")))
                .then(factors -> isRuleValid.setBit(INDEX_CUSTOMER_RULE, true))
                .build();
    }

    private Rule createPartyClassificationRule(BitStore isRuleValid) {
        return new RuleBuilder()
                .priority(PRIORITY_PARTY_CLASSIFICATION_RULE)
                .name(BundleProvider.getString("partyClassificationRuleName"))
                .description(BundleProvider.getString("partyClassificationRuleDescription"))
                .when(factPartyClassification -> validatePartyClassificationByRestriction(factPartyClassification.get("saleOrder")))
                .then(factors -> isRuleValid.setBit(INDEX_PARTY_CLASSIFICATION_RULE, true))
                .build();
    }

    private Rule createGeographicBoundaryRule(BitStore isRuleValid) {
        return new RuleBuilder()
                .priority(PRIORITY_GEOGRAPHIC_BOUNDARY_RULE)
                .name(BundleProvider.getString("geographicBoundaryRuleName"))
                .description(BundleProvider.getString("geographicBoundaryRuleDescription"))
                .when(factGeographicBoundary -> validateGeographicBoundaryByRestriction(factGeographicBoundary.get("saleOrder")))
                .then(factors -> isRuleValid.setBit(INDEX_GEOGRAPHIC_BOUNDARY_RULE, true))
                .build();
    }

    private Rule createProductRule(BitStore isRuleValid) {
        return new RuleBuilder()
                .priority(PRIORITY_PRODUCT_RULE)
                .name(BundleProvider.getString("productRuleName"))
                .description(ruleType == PriceRuleTypeEnum.GENERAL
                        ? BundleProvider.getString("productRuleActivationDescription")
                        : BundleProvider.getString("productRuleDescription"))
                .when(factProduct -> validateProducts(factProduct.get("saleOrder")))
                .then(factors -> isRuleValid.setBit(INDEX_PRODUCT_RULE, true))
                .build();
    }

    private Rule createProductFeatureRule(BitStore isRuleValid) {
        return new RuleBuilder()
                .priority(PRIORITY_PRODUCT_FEATURE_RULE)
                .name(BundleProvider.getString("productFeatureRuleName"))
                .description(ruleType == PriceRuleTypeEnum.GENERAL
                        ? BundleProvider.getString("productFeatureRuleActivationDescription")
                        : BundleProvider.getString("productFeatureRuleDescription"))
                .when(factFeature -> validateProductFeatures(factFeature.get("saleOrder")))
                .then(factors -> isRuleValid.setBit(INDEX_PRODUCT_FEATURE_RULE, true))
                .build();
    }

    private Rule createProductClassificationRule(BitStore isRuleValid) {
        return new RuleBuilder()
                .priority(PRIORITY_PRODUCT_CLASSIFICATION_RULE)
                .name(BundleProvider.getString("productClassificationRuleName"))
                .description(ruleType == PriceRuleTypeEnum.GENERAL
                        ? BundleProvider.getString("productClassificationRuleActivationDescription")
                        : BundleProvider.getString("productClassificationRuleDescription"))
                .when(factClassification -> validateProductClassifications(factClassification.get("saleOrder")))
                .then(factors -> isRuleValid.setBit(INDEX_PRODUCT_CLASSIFICATION_RULE, true))
                .build();
    }

    private Rule createSaleOrderOriginRule(BitStore isRuleValid) {
        return new RuleBuilder()
                .priority(PRIORITY_SALE_ORDER_ORIGIN_RULE)
                .name(BundleProvider.getString("saleOrderOriginRuleName"))
                .description(BundleProvider.getString("saleOrderOriginRuleNameDescription"))
                .when(saleOrderOrigins -> validateSaleOrderOriginsByRestriction(saleOrderOrigins.get("saleOrder")))
                .then(factors -> isRuleValid.setBit(INDEX_SALE_ORDER_ORIGIN_RULE, true))
                .build();
    }

    private Rule createPricelistRule(BitStore isRuleValid) {
        return new RuleBuilder()
                .priority(PRIORITY_PRICELIST_RULE)
                .name(BundleProvider.getString("pricelistRuleName"))
                .description(BundleProvider.getString("pricelistRuleNameDescription"))
                .when(pricelists -> validatePricelistByRestriction(pricelists.get("saleOrder")))
                .then(factors -> isRuleValid.setBit(INDEX_PRICELIST_RULE, true))
                .build();
    }
    //endregion

    //region Apply To Sale Order

    /**
     * It returns the all products applied by the best rules found.
     * Bonus Products have their id set to: 0 (ZERO) and discount percentage set to: 100
     * Surcharge Products have their id set to: -1
     */
    public List<PriceComponentRuleInputSaleOrderProduct> apply(PriceComponentRuleInputSaleOrder saleOrder) {
        List<PriceComponentRuleInputSaleOrderProduct> updateDetails = new ArrayList<>(0);
        if (target == TargetEnum.SALE_ORDER || this.canUseAllSaleOrder()) {
            saleOrder.getProductDetails().forEach(saleOrderProduct -> updateDetails.add(this.applyToProduct(saleOrderProduct, saleOrder)));
        } else {
            saleOrder.getProductDetails().forEach(saleOrderProduct -> {
                if (this.canApplyToProduct(saleOrderProduct, saleOrder)) {
                    updateDetails.add(this.applyToProduct(saleOrderProduct, saleOrder));
                }
            });
        }
        applyBonusOrSurchargeProduct(updateDetails, saleOrder);
        return updateDetails;
    }

    private PriceComponentRuleInputSaleOrderProduct createBonusProduct(PriceComponentRuleOutcomeProduct product, List<IdNameModel> rulesApplied, BigDecimal total) {
        return PriceComponentRuleInputSaleOrderProduct.builder()
                .id(0L)
                .product(product.toSaleOrderProduct())
                .qty(product.getQty())
                .unitPrice(product.getOriginalPrice())
                .rulesApplied(rulesApplied)
                .totalPrice(total)
                .totalPriceGross(total)
                .totalDiscount(total)
                .discountAmount(BigDecimal.ZERO)
                .discountPercentage(new BigDecimal(100))
                .surchargeAmount(BigDecimal.ZERO)
                .surchargePercentage(BigDecimal.ZERO)
                .build();
    }

    private PriceComponentRuleInputSaleOrderProduct createSurchargeProduct(PriceComponentRuleOutcomeProduct product, List<IdNameModel> rulesApplied, BigDecimal total) {
        return PriceComponentRuleInputSaleOrderProduct.builder()
                .id(-1L)
                .product(product.toSaleOrderProduct())
                .qty(product.getQty())
                .unitPrice(product.getOriginalPrice())
                .rulesApplied(rulesApplied)
                .totalPrice(total)
                .totalPriceGross(total)
                .totalDiscount(BigDecimal.ZERO)
                .discountAmount(BigDecimal.ZERO)
                .discountPercentage(BigDecimal.ZERO)
                .surchargeAmount(BigDecimal.ZERO)
                .surchargePercentage(BigDecimal.ZERO)
                .build();
    }

    @Deprecated
    private void applyProductBonification(List<PriceComponentRuleInputSaleOrderProduct> updateDetails, PriceComponentRuleInputSaleOrder saleOrder) {
        PriceComponentRuleOutcome outcome = getOutcome(saleOrder);
        if (Objects.nonNull(outcome) && outcome.getOutcomeType().isBonusProduct()) {
            List<IdNameModel> rulesApplied = createRulesAppliedByThis();
            for (PriceComponentRuleOutcomeProduct product : outcome.getProducts()) {
                updateDetails.add(createBonusProduct(product, rulesApplied, product.calculateTotalPriceGross()));
            }
        }
    }

    @Deprecated
    private void applyProductSurcharge(List<PriceComponentRuleInputSaleOrderProduct> updateDetails, PriceComponentRuleInputSaleOrder saleOrder) {
        PriceComponentRuleOutcome outcome = getOutcome(saleOrder);
        if (Objects.nonNull(outcome) && outcome.getOutcomeType().isProductSurcharge()) {
            List<IdNameModel> rulesApplied = createRulesAppliedByThis();
            for (PriceComponentRuleOutcomeProduct product : outcome.getProducts()) {
                updateDetails.add(createSurchargeProduct(product, rulesApplied, product.calculateTotalPriceGross()));
            }
        }
    }

    private List<IdNameModel> createRulesAppliedByThis() {
        return Collections.singletonList(IdNameModel.builder()
                .id(this.id)
                .name(this.name)
                .build());
    }

    public void applyBonusOrSurchargeProduct(List<PriceComponentRuleInputSaleOrderProduct> updateDetails, PriceComponentRuleInputSaleOrder saleOrder) {
        PriceComponentRuleOutcome outcome = getOutcome(saleOrder);
        if (Objects.nonNull(outcome)) {
            List<IdNameModel> rulesApplied = createRulesAppliedByThis();
            if (outcome.getOutcomeType().isBonusProduct()) {
                for (PriceComponentRuleOutcomeProduct product : outcome.getProducts()) {
                    updateDetails.add(createBonusProduct(product, rulesApplied, product.calculateTotalPriceGross()));
                }
            } else if (outcome.getOutcomeType().isProductSurcharge()) {
                for (PriceComponentRuleOutcomeProduct product : outcome.getProducts()) {
                    updateDetails.add(createSurchargeProduct(product, rulesApplied, product.calculateTotalPriceGross()));
                }
            }
        }
    }

    public PriceComponentRuleInputSaleOrderProduct applyToProduct(PriceComponentRuleInputSaleOrderProduct saleOrderProduct,
                                                                  PriceComponentRuleInputSaleOrder saleOrder) {
        saleOrderProduct.applyOutcome(getOutcome(saleOrder));
        List<IdNameModel> a = saleOrderProduct.getRulesApplied();
        if (Objects.isNull(a)) {
            a = new ArrayList<>(0);
        }
        a.add(IdNameModel.builder()
                .id(this.id)
                .name(this.name)
                .build());
        saleOrderProduct.setRulesApplied(a);
        return saleOrderProduct;
    }

    public PriceComponentRuleInputSaleOrderProduct applyToProductByProduct(PriceComponentRuleInputSaleOrderProduct saleOrderProduct,
                                                                           PriceComponentRuleInputSaleOrder saleOrder) {
        saleOrderProduct.applyOutcome(getOutcomeByProduct(saleOrder, saleOrderProduct));
        List<IdNameModel> a = saleOrderProduct.getRulesApplied();
        if (Objects.isNull(a)) {
            a = new ArrayList<>(0);
        }
        a.add(IdNameModel.builder()
                .id(this.id)
                .name(this.name)
                .build());
        saleOrderProduct.setRulesApplied(a);
        return saleOrderProduct;
    }
    //endregion

    //region Before apply rule: Validations
    private boolean canUseAllSaleOrder() {
        return (Objects.isNull(this.products) || this.products.isEmpty())
                && (Objects.isNull(this.productFeatures) || this.productFeatures.isEmpty())
                && (Objects.isNull(this.productClassifications) || this.productClassifications.isEmpty());
    }

    private boolean canApplyToProduct(PriceComponentRuleInputSaleOrderProduct saleOrderProduct, PriceComponentRuleInputSaleOrder saleOrder) {
        return this.existsProduct(saleOrderProduct, saleOrder) ||
                this.existsProductFeature(saleOrderProduct) ||
                this.existsProductClassification(saleOrderProduct);
    }
    //endregion

    //region Validation after found best rule in order to apply

    private boolean existsProduct(PriceComponentRuleInputSaleOrderProduct saleOrderProduct, PriceComponentRuleInputSaleOrder saleOrder) {
        boolean exists = Objects.nonNull(this.products) && !this.products.isEmpty() && this.products.stream()
                .anyMatch(subjectProduct -> subjectProduct.isValidProduct(saleOrderProduct));
        if (this.ruleType.isRestricted()) {
            if (!this.products.stream().allMatch(ruleProduct -> ruleProduct.isValid(saleOrder))) {
                exists = false;
            }
        }
        return exists;
    }

    private boolean existsProductFeature(PriceComponentRuleInputSaleOrderProduct saleOrderProduct) {
        return Objects.nonNull(this.productFeatures)
                && !this.productFeatures.isEmpty()
                && (this.ruleType.isRestricted()
                ? this.productFeatures.stream().allMatch(subjectProduct -> subjectProduct.isValidProduct(saleOrderProduct))
                : this.productFeatures.stream().anyMatch(subjectProduct -> subjectProduct.isValidProduct(saleOrderProduct)));
    }

    private boolean existsProductClassification(PriceComponentRuleInputSaleOrderProduct saleOrderProduct) {
        return Objects.nonNull(this.productClassifications)
                && !this.productClassifications.isEmpty()
                && (this.ruleType.isRestricted()
                ? this.productClassifications.stream().allMatch(subjectProduct -> subjectProduct.isValidProduct(saleOrderProduct))
                : this.productClassifications.stream().anyMatch(subjectProduct -> subjectProduct.isValidProduct(saleOrderProduct)));
    }

    public BigDecimal calculateSubjectsTotal(PriceComponentRuleInputSaleOrder saleOrder) {
        if (target == TargetEnum.PRODUCT) {
            if (Objects.nonNull(productFeatures) && !productFeatures.isEmpty()) {
                return calculateProductFeaturesTotal(saleOrder);
            } else if (Objects.nonNull(productClassifications) && !productClassifications.isEmpty()) {
                return calculateProductClassificationsTotal(saleOrder);
            } else if (Objects.nonNull(products) && !products.isEmpty()) {
                return calculateProductsTotal(saleOrder);
            } else {
                LogUtil.log(PriceRuleLog.createWarningNoProductsOnTargetProductRule(this));
                return calculateSaleOrderProductsTotal(saleOrder);
            }
        } else {
            return calculateSaleOrderProductsTotal(saleOrder);
        }
    }

    public BigDecimal calculateSubjectsTotalAmount(PriceComponentRuleInputSaleOrder saleOrder) {
        if (target == TargetEnum.PRODUCT) {
            if (Objects.nonNull(productFeatures) && !productFeatures.isEmpty()) {
                return saleOrder.getProductDetails().stream()
                        .filter(product -> this.productFeatures.stream().anyMatch(ruleProduct -> ruleProduct.isValidProduct(product)))
                        .map(PriceComponentRuleInputSaleOrderProduct::getTotalPriceGross).reduce(BigDecimal::add).orElse(BigDecimal.ZERO);
            } else if (Objects.nonNull(productClassifications) && !productClassifications.isEmpty()) {
                return saleOrder.getProductDetails().stream()
                        .filter(product -> this.productClassifications.stream().anyMatch(ruleProduct -> ruleProduct.isValidProduct(product)))
                        .map(PriceComponentRuleInputSaleOrderProduct::getTotalPriceGross).reduce(BigDecimal::add).orElse(BigDecimal.ZERO);
            } else if (Objects.nonNull(products) && !products.isEmpty()) {
                return saleOrder.getProductDetails().stream()
                        .filter(product -> this.products.stream().anyMatch(ruleProduct -> ruleProduct.isValidProduct(product)))
                        .map(PriceComponentRuleInputSaleOrderProduct::getTotalPriceGross).reduce(BigDecimal::add).orElse(BigDecimal.ZERO);
            } else {
                LogUtil.log(PriceRuleLog.createWarningNoProductsOnTargetProductRule(this));
                return saleOrder.getTotalAmount();
            }
        } else {
            return saleOrder.getTotalAmount();
        }
    }

    public List<PriceComponentRuleInputSaleOrderProduct> getSubjectProducts(PriceComponentRuleInputSaleOrder saleOrder) {
        if (target == TargetEnum.PRODUCT) {
            if (Objects.nonNull(productFeatures) && !productFeatures.isEmpty()) {
                return saleOrder.getProductDetails().stream()
                        .filter(product -> this.productFeatures.stream().anyMatch(ruleProduct -> ruleProduct.isValidProduct(product))).collect(Collectors.toList());
            } else if (Objects.nonNull(productClassifications) && !productClassifications.isEmpty()) {
                return saleOrder.getProductDetails().stream()
                        .filter(product -> this.productClassifications.stream().anyMatch(ruleProduct -> ruleProduct.isValidProduct(product))).collect(Collectors.toList());
            } else if (Objects.nonNull(products) && !products.isEmpty()) {
                return saleOrder.getProductDetails().stream()
                        .filter(product -> this.products.stream().anyMatch(ruleProduct -> ruleProduct.isValidProduct(product))).collect(Collectors.toList());
            } else {
                LogUtil.log(PriceRuleLog.createWarningNoProductsOnTargetProductRule(this));
                return saleOrder.getProductDetails();
            }
        } else {
            return saleOrder.getProductDetails();
        }
    }
    //endregion

    public boolean isApplicableByProduct(PriceComponentRuleInputSaleOrderProduct product, PriceComponentRuleInputSaleOrder saleOrder) {
        if (target == TargetEnum.SALE_ORDER || this.canUseAllSaleOrder())
            return true;
        return this.existsProduct(product, saleOrder) ||
                this.existsProductFeature(product) ||
                this.existsProductClassification(product);
    }

    private BigDecimal calculateProductsTotalFromSaleOrder(List<PriceComponentRuleInputSaleOrderProduct> products) {
        return products.stream().filter(product -> this.products
                        .stream().anyMatch(ruleProduct -> ruleProduct.isValidProduct(product)))
                .map(PriceComponentRuleInputSaleOrderProduct::calculateTotalPriceGross).reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    private BigDecimal calculateTotalFromSaleOrder(List<PriceComponentRuleInputSaleOrderProduct> products) {
        return products.stream()
                .map(PriceComponentRuleInputSaleOrderProduct::calculateTotalPriceGross)
                .reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    private BigDecimal calculateTotalQuantityFromSaleOrder(List<PriceComponentRuleInputSaleOrderProduct> products) {
        return products.stream()
                .map(PriceComponentRuleInputSaleOrderProduct::getQty)
                .reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    private BigDecimal calculateProductFeaturesTotalFromSaleOrder(List<PriceComponentRuleInputSaleOrderProduct> products) {
        return products.stream().filter(product -> this.productFeatures.stream().anyMatch(ruleProduct -> ruleProduct.isValidProduct(product)))
                .map(PriceComponentRuleInputSaleOrderProduct::calculateTotalPriceGross).reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    private BigDecimal calculateProductClassificationsTotalFromSaleOrder(List<PriceComponentRuleInputSaleOrderProduct> products) {
        return products.stream().filter(product -> this.productClassifications.stream()
                        .anyMatch(ruleProduct -> ruleProduct.isValidProduct(product)))
                .map(PriceComponentRuleInputSaleOrderProduct::calculateTotalPriceGross)
                .reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    public BigDecimal calculateProductsTotalByQuantity(List<PriceComponentRuleInputSaleOrderProduct> products) {
        if (Objects.nonNull(this.products) && !this.products.isEmpty())
            return products.stream().filter(product -> this.products
                            .stream().anyMatch(ruleProduct -> ruleProduct.isValidProduct(product)))
                    .map(PriceComponentRuleInputSaleOrderProduct::getQty).reduce(BigDecimal.ZERO, BigDecimal::add);
        else if (Objects.nonNull(productFeatures) && !productFeatures.isEmpty())
            return products.stream().filter(product -> this.productFeatures.stream()
                            .anyMatch(ruleProduct -> ruleProduct.isValidProduct(product)))
                    .map(PriceComponentRuleInputSaleOrderProduct::getQty).reduce(BigDecimal.ZERO, BigDecimal::add);
        else if (Objects.nonNull(productClassifications) && !productClassifications.isEmpty())
            return products.stream().filter(product -> this.productClassifications.stream()
                            .anyMatch(ruleProduct -> ruleProduct.isValidProduct(product)))
                    .map(PriceComponentRuleInputSaleOrderProduct::getQty).reduce(BigDecimal.ZERO, BigDecimal::add);
        else
            return calculateTotalQuantityFromSaleOrder(products);
    }

    public BigDecimal calculateProductsTotalFromSaleOrderByPriorityAmount(List<PriceComponentRuleInputSaleOrderProduct> products) {
        if (Objects.nonNull(this.products) && !this.products.isEmpty())
            return calculateProductsTotalFromSaleOrder(products);
        else if (Objects.nonNull(productFeatures) && !productFeatures.isEmpty())
            return calculateProductFeaturesTotalFromSaleOrder(products);
        else if (Objects.nonNull(productClassifications) && !productClassifications.isEmpty())
            return calculateProductClassificationsTotalFromSaleOrder(products);
        else
            return calculateTotalFromSaleOrder(products);
    }

    public long getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public String getDescription() {
        return this.description;
    }

    public Date getFromDate() {
        return this.fromDate;
    }

    public Date getThruDate() {
        return this.thruDate;
    }

    public boolean isAccumulative() {
        return this.accumulative;
    }

    public long getPriority() {
        return this.priority;
    }

    public PriceRuleTypeEnum getRuleType() {
        return this.ruleType;
    }

    public TargetEnum getTarget() {
        return this.target;
    }

    public OutComeModeEnum getOutComeMode() {
        return this.outComeMode;
    }

    public ScaleTypeEnum getValidationType() {
        return this.validationType;
    }

    public boolean isUseSaleOrderTotalForOutcome() {
        return this.useSaleOrderTotalForOutcome;
    }

    public PriceComponentRuleOutcomeOutput getOutput() {
        return this.output;
    }

    public List<PriceComponentRuleSubjectProduct> getProducts() {
        return this.products;
    }

    public List<PriceComponentRuleSubjectProductFeature> getProductFeatures() {
        return this.productFeatures;
    }

    public List<PriceComponentRuleFactorProductClassification> getProductClassifications() {
        return this.productClassifications;
    }

    public PaymentConditionsTypeEnum getPaymentCondition() {
        return this.paymentCondition;
    }

    public List<PriceComponentRuleFactorDeliveryMode> getDeliveryModes() {
        return this.deliveryModes;
    }

    public List<PriceComponentRuleFactorPartyClassification> getPartyClassifications() {
        return this.partyClassifications;
    }

    public List<PriceComponentRuleFactorGeographicBoundary> getGeographicBoundaries() {
        return this.geographicBoundaries;
    }

    public List<PriceComponentRuleFactorCustomer> getCustomers() {
        return this.customers;
    }

    public List<PriceComponentRuleFactorAgency> getAgencies() {
        return this.agencies;
    }

    public List<PriceComponentRuleFactorBusinessUnit> getBusinessUnits() {
        return this.businessUnits;
    }

    public List<PriceComponentRuleFactorRoleType> getRoleTypes() {
        return this.roleTypes;
    }

    public RulesEngine getRulesEngine() {
        return this.rulesEngine;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    public void setThruDate(Date thruDate) {
        this.thruDate = thruDate;
    }

    public void setAccumulative(boolean accumulative) {
        this.accumulative = accumulative;
    }

    public void setPriority(long priority) {
        this.priority = priority;
    }

    public void setRuleType(PriceRuleTypeEnum ruleType) {
        this.ruleType = ruleType;
    }

    public void setTarget(TargetEnum target) {
        this.target = target;
    }

    public void setOutComeMode(OutComeModeEnum outComeMode) {
        this.outComeMode = outComeMode;
    }

    public void setValidationType(ScaleTypeEnum validationType) {
        this.validationType = validationType;
    }

    public void setUseSaleOrderTotalForOutcome(boolean useSaleOrderTotalForOutcome) {
        this.useSaleOrderTotalForOutcome = useSaleOrderTotalForOutcome;
    }

    public void setOutput(PriceComponentRuleOutcomeOutput output) {
        this.output = output;
    }

    public void setProducts(List<PriceComponentRuleSubjectProduct> products) {
        this.products = products;
    }

    public void setProductFeatures(List<PriceComponentRuleSubjectProductFeature> productFeatures) {
        this.productFeatures = productFeatures;
    }

    public void setProductClassifications(List<PriceComponentRuleFactorProductClassification> productClassifications) {
        this.productClassifications = productClassifications;
    }

    public void setPaymentCondition(PaymentConditionsTypeEnum paymentCondition) {
        this.paymentCondition = paymentCondition;
    }

    public void setDeliveryModes(List<PriceComponentRuleFactorDeliveryMode> deliveryModes) {
        this.deliveryModes = deliveryModes;
    }

    public void setPartyClassifications(List<PriceComponentRuleFactorPartyClassification> partyClassifications) {
        this.partyClassifications = partyClassifications;
    }

    public void setGeographicBoundaries(List<PriceComponentRuleFactorGeographicBoundary> geographicBoundaries) {
        this.geographicBoundaries = geographicBoundaries;
    }

    public void setCustomers(List<PriceComponentRuleFactorCustomer> customers) {
        this.customers = customers;
    }

    public void setAgencies(List<PriceComponentRuleFactorAgency> agencies) {
        this.agencies = agencies;
    }

    public void setBusinessUnits(List<PriceComponentRuleFactorBusinessUnit> businessUnits) {
        this.businessUnits = businessUnits;
    }

    public void setRoleTypes(List<PriceComponentRuleFactorRoleType> roleTypes) {
        this.roleTypes = roleTypes;
    }

    public void setRulesEngine(RulesEngine rulesEngine) {
        this.rulesEngine = rulesEngine;
    }

    public ExclusiveOutcomeEnum getExclusiveOutcomeEnum() {
        return exclusiveOutcomeEnum;
    }

    public void setExclusiveOutcomeEnum(ExclusiveOutcomeEnum exclusiveOutcomeEnum) {
        this.exclusiveOutcomeEnum = exclusiveOutcomeEnum;
    }

    public List<PriceComponentRuleFactorSaleOrderOrigin> getSaleOrderOrigins() {
        return saleOrderOrigins;
    }

    public void setSaleOrderOrigins(List<PriceComponentRuleFactorSaleOrderOrigin> saleOrderOrigins) {
        this.saleOrderOrigins = saleOrderOrigins;
    }

    public List<PriceComponentRuleFactorPricelist> getPricelists() {
        return pricelists;
    }

    public void setPricelists(List<PriceComponentRuleFactorPricelist> pricelists) {
        this.pricelists = pricelists;
    }

    public String toString() {
        return "PriceRule(id=" + this.getId()
                + ", name=" + this.getName()
                + ", description=" + this.getDescription()
                + ", fromDate=" + this.getFromDate()
                + ", thruDate=" + this.getThruDate()
                + ", accumulative=" + this.isAccumulative()
                + ", priority=" + this.getPriority()
                + ", ruleType=" + this.getRuleType()
                + ", target=" + this.getTarget()
                + ", outComeMode=" + this.getOutComeMode()
                + ", validationType=" + this.getValidationType()
                + ", useSaleOrderTotalForOutcome=" + this.isUseSaleOrderTotalForOutcome()
                + ", output=" + this.getOutput()
                + ", products=" + this.getProducts()
                + ", productFeatures=" + this.getProductFeatures()
                + ", productClassifications=" + this.getProductClassifications()
                + ", paymentCondition=" + this.getPaymentCondition()
                + ", deliveryModes=" + this.getDeliveryModes()
                + ", partyClassifications=" + this.getPartyClassifications()
                + ", geographicBoundaries=" + this.getGeographicBoundaries()
                + ", customers=" + this.getCustomers()
                + ", agencies=" + this.getAgencies()
                + ", businessUnits=" + this.getBusinessUnits()
                + ", roleTypes=" + this.getRoleTypes()
                + ", rulesEngine=" + this.getRulesEngine()
                + ", saleOrderOrigins=" + this.getSaleOrderOrigins()
                + ", pricelists=" + this.getPricelists()
                + ")";
    }

    public boolean equals(final Object o) {
        if (o == this) return true;
        if (!(o instanceof PriceRule)) return false;
        final PriceRule other = (PriceRule) o;
        if (!other.canEqual((Object) this)) return false;
        if (this.getId() != other.getId()) return false;
        final Object this$name = this.getName();
        final Object other$name = other.getName();
        if (this$name == null ? other$name != null : !this$name.equals(other$name)) return false;
        final Object this$description = this.getDescription();
        final Object other$description = other.getDescription();
        if (this$description == null ? other$description != null : !this$description.equals(other$description))
            return false;
        final Object this$fromDate = this.getFromDate();
        final Object other$fromDate = other.getFromDate();
        if (this$fromDate == null ? other$fromDate != null : !this$fromDate.equals(other$fromDate)) return false;
        final Object this$thruDate = this.getThruDate();
        final Object other$thruDate = other.getThruDate();
        if (this$thruDate == null ? other$thruDate != null : !this$thruDate.equals(other$thruDate)) return false;
        if (this.isAccumulative() != other.isAccumulative()) return false;
        if (this.getPriority() != other.getPriority()) return false;
        final Object this$ruleType = this.getRuleType();
        final Object other$ruleType = other.getRuleType();
        if (this$ruleType == null ? other$ruleType != null : !this$ruleType.equals(other$ruleType)) return false;
        final Object this$target = this.getTarget();
        final Object other$target = other.getTarget();
        if (this$target == null ? other$target != null : !this$target.equals(other$target)) return false;
        final Object this$outComeMode = this.getOutComeMode();
        final Object other$outComeMode = other.getOutComeMode();
        if (this$outComeMode == null ? other$outComeMode != null : !this$outComeMode.equals(other$outComeMode))
            return false;
        final Object this$validationType = this.getValidationType();
        final Object other$validationType = other.getValidationType();
        if (this$validationType == null ? other$validationType != null : !this$validationType.equals(other$validationType))
            return false;
        if (this.isUseSaleOrderTotalForOutcome() != other.isUseSaleOrderTotalForOutcome()) return false;
        final Object this$output = this.getOutput();
        final Object other$output = other.getOutput();
        if (this$output == null ? other$output != null : !this$output.equals(other$output)) return false;
        final Object this$products = this.getProducts();
        final Object other$products = other.getProducts();
        if (this$products == null ? other$products != null : !this$products.equals(other$products)) return false;
        final Object this$productFeatures = this.getProductFeatures();
        final Object other$productFeatures = other.getProductFeatures();
        if (this$productFeatures == null ? other$productFeatures != null : !this$productFeatures.equals(other$productFeatures))
            return false;
        final Object this$productClassifications = this.getProductClassifications();
        final Object other$productClassifications = other.getProductClassifications();
        if (this$productClassifications == null ? other$productClassifications != null : !this$productClassifications.equals(other$productClassifications))
            return false;
        final Object this$paymentCondition = this.getPaymentCondition();
        final Object other$paymentCondition = other.getPaymentCondition();
        if (this$paymentCondition == null ? other$paymentCondition != null : !this$paymentCondition.equals(other$paymentCondition))
            return false;
        final Object this$deliveryModes = this.getDeliveryModes();
        final Object other$deliveryModes = other.getDeliveryModes();
        if (this$deliveryModes == null ? other$deliveryModes != null : !this$deliveryModes.equals(other$deliveryModes))
            return false;
        final Object this$partyClassifications = this.getPartyClassifications();
        final Object other$partyClassifications = other.getPartyClassifications();
        if (this$partyClassifications == null ? other$partyClassifications != null : !this$partyClassifications.equals(other$partyClassifications))
            return false;
        final Object this$geographicBoundaries = this.getGeographicBoundaries();
        final Object other$geographicBoundaries = other.getGeographicBoundaries();
        if (this$geographicBoundaries == null ? other$geographicBoundaries != null : !this$geographicBoundaries.equals(other$geographicBoundaries))
            return false;
        final Object this$customers = this.getCustomers();
        final Object other$customers = other.getCustomers();
        if (this$customers == null ? other$customers != null : !this$customers.equals(other$customers)) return false;
        final Object this$agencies = this.getAgencies();
        final Object other$agencies = other.getAgencies();
        if (this$agencies == null ? other$agencies != null : !this$agencies.equals(other$agencies)) return false;
        final Object this$businessUnits = this.getBusinessUnits();
        final Object other$businessUnits = other.getBusinessUnits();
        if (this$businessUnits == null ? other$businessUnits != null : !this$businessUnits.equals(other$businessUnits))
            return false;
        final Object this$roleTypes = this.getRoleTypes();
        final Object other$roleTypes = other.getRoleTypes();
        if (this$roleTypes == null ? other$roleTypes != null : !this$roleTypes.equals(other$roleTypes)) return false;
        final Object this$rulesEngine = this.getRulesEngine();
        final Object other$rulesEngine = other.getRulesEngine();
        if (this$rulesEngine == null ? other$rulesEngine != null : !this$rulesEngine.equals(other$rulesEngine))
            return false;

        final Object this$saleOrderOrigin = this.getSaleOrderOrigins();
        final Object other$saleOrderOrigin = other.getRulesEngine();
        if (this$saleOrderOrigin == null ? other$saleOrderOrigin != null : !this$saleOrderOrigin.equals(other$saleOrderOrigin))
            return false;

        final Object this$pricelists = this.getPricelists();
        final Object other$pricelists = other.getRulesEngine();
        if (this$pricelists == null ? other$pricelists != null : !this$pricelists.equals(other$pricelists))
            return false;

        return true;
    }

    protected boolean canEqual(final Object other) {
        return other instanceof PriceRule;
    }

    public int hashCode() {
        final int PRIME = 59;
        int result = 1;
        final long $id = this.getId();
        result = result * PRIME + (int) ($id >>> 32 ^ $id);
        final Object $name = this.getName();
        result = result * PRIME + ($name == null ? 43 : $name.hashCode());
        final Object $description = this.getDescription();
        result = result * PRIME + ($description == null ? 43 : $description.hashCode());
        final Object $fromDate = this.getFromDate();
        result = result * PRIME + ($fromDate == null ? 43 : $fromDate.hashCode());
        final Object $thruDate = this.getThruDate();
        result = result * PRIME + ($thruDate == null ? 43 : $thruDate.hashCode());
        result = result * PRIME + (this.isAccumulative() ? 79 : 97);
        final long $priority = this.getPriority();
        result = result * PRIME + (int) ($priority >>> 32 ^ $priority);
        final Object $ruleType = this.getRuleType();
        result = result * PRIME + ($ruleType == null ? 43 : $ruleType.hashCode());
        final Object $target = this.getTarget();
        result = result * PRIME + ($target == null ? 43 : $target.hashCode());
        final Object $outComeMode = this.getOutComeMode();
        result = result * PRIME + ($outComeMode == null ? 43 : $outComeMode.hashCode());
        final Object $validationType = this.getValidationType();
        result = result * PRIME + ($validationType == null ? 43 : $validationType.hashCode());
        result = result * PRIME + (this.isUseSaleOrderTotalForOutcome() ? 79 : 97);
        final Object $output = this.getOutput();
        result = result * PRIME + ($output == null ? 43 : $output.hashCode());
        final Object $products = this.getProducts();
        result = result * PRIME + ($products == null ? 43 : $products.hashCode());
        final Object $productFeatures = this.getProductFeatures();
        result = result * PRIME + ($productFeatures == null ? 43 : $productFeatures.hashCode());
        final Object $productClassifications = this.getProductClassifications();
        result = result * PRIME + ($productClassifications == null ? 43 : $productClassifications.hashCode());
        final Object $paymentCondition = this.getPaymentCondition();
        result = result * PRIME + ($paymentCondition == null ? 43 : $paymentCondition.hashCode());
        final Object $deliveryModes = this.getDeliveryModes();
        result = result * PRIME + ($deliveryModes == null ? 43 : $deliveryModes.hashCode());
        final Object $partyClassifications = this.getPartyClassifications();
        result = result * PRIME + ($partyClassifications == null ? 43 : $partyClassifications.hashCode());
        final Object $geographicBoundaries = this.getGeographicBoundaries();
        result = result * PRIME + ($geographicBoundaries == null ? 43 : $geographicBoundaries.hashCode());
        final Object $customers = this.getCustomers();
        result = result * PRIME + ($customers == null ? 43 : $customers.hashCode());
        final Object $agencies = this.getAgencies();
        result = result * PRIME + ($agencies == null ? 43 : $agencies.hashCode());
        final Object $businessUnits = this.getBusinessUnits();
        result = result * PRIME + ($businessUnits == null ? 43 : $businessUnits.hashCode());
        final Object $roleTypes = this.getRoleTypes();
        result = result * PRIME + ($roleTypes == null ? 43 : $roleTypes.hashCode());
        final Object $rulesEngine = this.getRulesEngine();
        result = result * PRIME + ($rulesEngine == null ? 43 : $rulesEngine.hashCode());

        final Object $saleOrderOrigins = this.getSaleOrderOrigins();
        result = result * PRIME + ($saleOrderOrigins == null ? 43 : $saleOrderOrigins.hashCode());

        final Object $pricelists = this.getPricelists();
        result = result * PRIME + ($pricelists == null ? 43 : $pricelists.hashCode());

        return result;
    }

    public static class PriceRuleBuilder {

        private long id;
        private String name;
        private String description;
        private Date fromDate;
        private Date thruDate;
        private boolean accumulative;
        private long priority = 1L;
        private PriceRuleTypeEnum ruleType = PriceRuleTypeEnum.RESTRICTED;
        private TargetEnum target = TargetEnum.PRODUCT;
        private OutComeModeEnum outComeMode = OutComeModeEnum.SINGLE;
        private ScaleTypeEnum validationType = ScaleTypeEnum.AMOUNT;
        private boolean useSaleOrderTotalForOutcome = false;
        private PriceComponentRuleOutcomeOutput output;
        private List<PriceComponentRuleSubjectProduct> products = new ArrayList<>(0);
        private List<PriceComponentRuleSubjectProductFeature> productFeatures = new ArrayList<>(0);
        private List<PriceComponentRuleFactorProductClassification> productClassifications = new ArrayList<>(0);
        private PaymentConditionsTypeEnum paymentCondition;
        private List<PriceComponentRuleFactorDeliveryMode> deliveryModes = new ArrayList<>(0);
        private List<PriceComponentRuleFactorPartyClassification> partyClassifications = new ArrayList<>(0);
        private List<PriceComponentRuleFactorGeographicBoundary> geographicBoundaries = new ArrayList<>(0);
        private List<PriceComponentRuleFactorCustomer> customers = new ArrayList<>(0);
        private List<PriceComponentRuleFactorAgency> agencies = new ArrayList<>(0);
        private List<PriceComponentRuleFactorBusinessUnit> businessUnits = new ArrayList<>(0);
        private List<PriceComponentRuleFactorRoleType> roleTypes = new ArrayList<>(0);
        private List<PriceComponentRuleFactorSaleOrderOrigin> saleOrderOrigins = new ArrayList<>(0);
        private List<PriceComponentRuleFactorPricelist> pricelists = new ArrayList<>(0);
        private RulesEngine rulesEngine = new DefaultRulesEngine();

        private ExclusiveOutcomeEnum exclusiveOutcomeEnum = ExclusiveOutcomeEnum.NONE;

        PriceRuleBuilder() {
        }

        public PriceRule.PriceRuleBuilder id(long id) {
            this.id = id;
            return this;
        }

        public PriceRule.PriceRuleBuilder name(String name) {
            this.name = name;
            return this;
        }

        public PriceRule.PriceRuleBuilder description(String description) {
            this.description = description;
            return this;
        }

        public PriceRule.PriceRuleBuilder fromDate(Date fromDate) {
            this.fromDate = fromDate;
            return this;
        }

        public PriceRule.PriceRuleBuilder thruDate(Date thruDate) {
            this.thruDate = thruDate;
            return this;
        }

        public PriceRule.PriceRuleBuilder accumulative(boolean accumulative) {
            this.accumulative = accumulative;
            return this;
        }

        public PriceRule.PriceRuleBuilder priority(long priority) {
            this.priority = priority;
            return this;
        }

        public PriceRule.PriceRuleBuilder ruleType(PriceRuleTypeEnum ruleType) {
            this.ruleType = ruleType;
            return this;
        }

        public PriceRule.PriceRuleBuilder target(TargetEnum target) {
            this.target = target;
            return this;
        }

        public PriceRule.PriceRuleBuilder outComeMode(OutComeModeEnum outComeMode) {
            this.outComeMode = outComeMode;
            return this;
        }

        public PriceRule.PriceRuleBuilder validationType(ScaleTypeEnum validationType) {
            this.validationType = validationType;
            return this;
        }

        public PriceRule.PriceRuleBuilder useSaleOrderTotalForOutcome(boolean useSaleOrderTotalForOutcome) {
            this.useSaleOrderTotalForOutcome = useSaleOrderTotalForOutcome;
            return this;
        }

        public PriceRule.PriceRuleBuilder output(PriceComponentRuleOutcomeOutput output) {
            this.output = output;
            return this;
        }

        public PriceRule.PriceRuleBuilder products(List<PriceComponentRuleSubjectProduct> products) {
            this.products = products;
            return this;
        }

        public PriceRule.PriceRuleBuilder productFeatures(List<PriceComponentRuleSubjectProductFeature> productFeatures) {
            this.productFeatures = productFeatures;
            return this;
        }

        public PriceRule.PriceRuleBuilder productClassifications(List<PriceComponentRuleFactorProductClassification> productClassifications) {
            this.productClassifications = productClassifications;
            return this;
        }

        public PriceRule.PriceRuleBuilder paymentCondition(PaymentConditionsTypeEnum paymentCondition) {
            this.paymentCondition = paymentCondition;
            return this;
        }

        public PriceRule.PriceRuleBuilder deliveryModes(List<PriceComponentRuleFactorDeliveryMode> deliveryModes) {
            this.deliveryModes = deliveryModes;
            return this;
        }

        public PriceRule.PriceRuleBuilder partyClassifications(List<PriceComponentRuleFactorPartyClassification> partyClassifications) {
            this.partyClassifications = partyClassifications;
            return this;
        }

        public PriceRule.PriceRuleBuilder geographicBoundaries(List<PriceComponentRuleFactorGeographicBoundary> geographicBoundaries) {
            this.geographicBoundaries = geographicBoundaries;
            return this;
        }

        public PriceRule.PriceRuleBuilder customers(List<PriceComponentRuleFactorCustomer> customers) {
            this.customers = customers;
            return this;
        }

        public PriceRule.PriceRuleBuilder agencies(List<PriceComponentRuleFactorAgency> agencies) {
            this.agencies = agencies;
            return this;
        }

        public PriceRule.PriceRuleBuilder businessUnits(List<PriceComponentRuleFactorBusinessUnit> businessUnits) {
            this.businessUnits = businessUnits;
            return this;
        }

        public PriceRule.PriceRuleBuilder roleTypes(List<PriceComponentRuleFactorRoleType> roleTypes) {
            this.roleTypes = roleTypes;
            return this;
        }

        public PriceRule.PriceRuleBuilder rulesEngine(RulesEngine rulesEngine) {
            this.rulesEngine = rulesEngine;
            return this;
        }

        public PriceRule.PriceRuleBuilder exclusiveOutcome(ExclusiveOutcomeEnum exclusiveOutcomeEnum) {
            this.exclusiveOutcomeEnum = exclusiveOutcomeEnum;
            return this;
        }

        public PriceRule.PriceRuleBuilder saleOrderOrigins(List<PriceComponentRuleFactorSaleOrderOrigin> saleOrderOrigins) {
            this.saleOrderOrigins = saleOrderOrigins;
            return this;
        }

        public PriceRule.PriceRuleBuilder pricelists(List<PriceComponentRuleFactorPricelist> pricelists) {
            this.pricelists = pricelists;
            return this;
        }

        public PriceRule build() {
            return new PriceRule(id, name, description, fromDate, thruDate, accumulative, priority, ruleType, target,
                    outComeMode, validationType, useSaleOrderTotalForOutcome, output, products, productFeatures,
                    productClassifications, paymentCondition, deliveryModes, partyClassifications, geographicBoundaries,
                    customers, agencies, businessUnits, roleTypes, rulesEngine, exclusiveOutcomeEnum, saleOrderOrigins, pricelists);
        }

        public String toString() {
            return "PriceRule.PriceRuleBuilder(id=" + this.id + ", name=" + this.name + ", description=" + this.description + ", fromDate=" + this.fromDate + ", thruDate=" + this.thruDate + ", accumulative=" + this.accumulative + ", priority=" + this.priority + ", ruleType=" + this.ruleType + ", target=" + this.target + ", outComeMode=" + this.outComeMode + ", validationType=" + this.validationType + ", useSaleOrderTotalForOutcome=" + this.useSaleOrderTotalForOutcome + ", output=" + this.output + ", products=" + this.products + ", productFeatures=" + this.productFeatures + ", productClassifications=" + this.productClassifications + ", paymentCondition=" + this.paymentCondition + ", deliveryModes=" + this.deliveryModes + ", partyClassifications=" + this.partyClassifications + ", geographicBoundaries=" + this.geographicBoundaries + ", customers=" + this.customers + ", agencies=" + this.agencies + ", businessUnits=" + this.businessUnits + ", roleTypes=" + this.roleTypes + ", rulesEngine=" + this.rulesEngine + ", saleOrderOrigins=" + this.saleOrderOrigins + ")";
        }
    }
}
