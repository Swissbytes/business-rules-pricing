package ch.swissbytes.businessrules.pricing.models;

import ch.swissbytes.businessrules.pricing.factors.PriceComponentRuleFactorGeographicBoundary;


public class Coordinate {
    public double lon;
    public double lat;

    public Coordinate(double lon, double lat) {
        this.lon = lon;
        this.lat = lat;
    }

    public Coordinate() {
    }

    public static Coordinate toCoordinate(PriceComponentRuleFactorGeographicBoundary deliveryAdress) {
        return Coordinate.builder()
                .lat(deliveryAdress.getLatitude().doubleValue())
                .lon(deliveryAdress.getLongitude().doubleValue())
                .build();
    }

    public static CoordinateBuilder builder() {
        return new CoordinateBuilder();
    }

    public double getLon() {
        return this.lon;
    }

    public double getLat() {
        return this.lat;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public String toString() {
        return "Coordinate(lon=" + this.getLon() + ", lat=" + this.getLat() + ")";
    }

    public boolean equals(final Object o) {
        if (o == this) return true;
        if (!(o instanceof Coordinate)) return false;
        final Coordinate other = (Coordinate) o;
        if (!other.canEqual((Object) this)) return false;
        if (Double.compare(this.getLon(), other.getLon()) != 0) return false;
        if (Double.compare(this.getLat(), other.getLat()) != 0) return false;
        return true;
    }

    protected boolean canEqual(final Object other) {
        return other instanceof Coordinate;
    }

    public int hashCode() {
        final int PRIME = 59;
        int result = 1;
        final long $lon = Double.doubleToLongBits(this.getLon());
        result = result * PRIME + (int) ($lon >>> 32 ^ $lon);
        final long $lat = Double.doubleToLongBits(this.getLat());
        result = result * PRIME + (int) ($lat >>> 32 ^ $lat);
        return result;
    }

    public static class CoordinateBuilder {
        private double lon;
        private double lat;

        CoordinateBuilder() {
        }

        public Coordinate.CoordinateBuilder lon(double lon) {
            this.lon = lon;
            return this;
        }

        public Coordinate.CoordinateBuilder lat(double lat) {
            this.lat = lat;
            return this;
        }

        public Coordinate build() {
            return new Coordinate(lon, lat);
        }

        public String toString() {
            return "Coordinate.CoordinateBuilder(lon=" + this.lon + ", lat=" + this.lat + ")";
        }
    }
}
