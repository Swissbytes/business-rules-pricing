package ch.swissbytes.businessrules.pricing.models;


public class IdNameModel {
    private long id;
    private String name;

    public IdNameModel(long id, String name) {
        this.id = id;
        this.name = name;
    }

    public IdNameModel() {
    }

    public static IdNameModelBuilder builder() {
        return new IdNameModelBuilder();
    }

    public long getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean equals(final Object o) {
        if (o == this) return true;
        if (!(o instanceof IdNameModel)) return false;
        final IdNameModel other = (IdNameModel) o;
        if (!other.canEqual((Object) this)) return false;
        if (this.getId() != other.getId()) return false;
        final Object this$name = this.getName();
        final Object other$name = other.getName();
        if (this$name == null ? other$name != null : !this$name.equals(other$name)) return false;
        return true;
    }

    protected boolean canEqual(final Object other) {
        return other instanceof IdNameModel;
    }

    public int hashCode() {
        final int PRIME = 59;
        int result = 1;
        final long $id = this.getId();
        result = result * PRIME + (int) ($id >>> 32 ^ $id);
        final Object $name = this.getName();
        result = result * PRIME + ($name == null ? 43 : $name.hashCode());
        return result;
    }

    public String toString() {
        return "IdNameModel(id=" + this.getId() + ", name=" + this.getName() + ")";
    }

    public static class IdNameModelBuilder {
        private long id;
        private String name;

        IdNameModelBuilder() {
        }

        public IdNameModel.IdNameModelBuilder id(long id) {
            this.id = id;
            return this;
        }

        public IdNameModel.IdNameModelBuilder name(String name) {
            this.name = name;
            return this;
        }

        public IdNameModel build() {
            return new IdNameModel(id, name);
        }

        public String toString() {
            return "IdNameModel.IdNameModelBuilder(id=" + this.id + ", name=" + this.name + ")";
        }
    }
}
