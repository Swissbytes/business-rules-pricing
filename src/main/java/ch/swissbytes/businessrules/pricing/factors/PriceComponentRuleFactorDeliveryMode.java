package ch.swissbytes.businessrules.pricing.factors;

import ch.swissbytes.businessrules.pricing.enums.DeliveryModeEnum;
import ch.swissbytes.businessrules.pricing.inputs.PriceComponentRuleInputSaleOrder;
import ch.swissbytes.businessrules.pricing.interfaces.ISimplePriceRuleValidator;
import ch.swissbytes.businessrules.pricing.models.BaseFactorModel;

import java.util.Objects;


public class PriceComponentRuleFactorDeliveryMode  extends BaseFactorModel implements ISimplePriceRuleValidator {

    private long id;
    private DeliveryModeEnum deliveryMode;

    public PriceComponentRuleFactorDeliveryMode(long id, DeliveryModeEnum deliveryMode) {
        this.id = id;
        this.deliveryMode = deliveryMode;
    }

    public PriceComponentRuleFactorDeliveryMode() {
    }

    public static PriceComponentRuleFactorDeliveryModeBuilder builder() {
        return new PriceComponentRuleFactorDeliveryModeBuilder();
    }

    @Override
    public boolean isValid(PriceComponentRuleInputSaleOrder saleOrder) {
        return Objects.nonNull(saleOrder.getDeliveryMode()) && saleOrder.getDeliveryMode().equals(deliveryMode);
    }

    public long getId() {
        return this.id;
    }

    public DeliveryModeEnum getDeliveryMode() {
        return this.deliveryMode;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setDeliveryMode(DeliveryModeEnum deliveryMode) {
        this.deliveryMode = deliveryMode;
    }

    public String toString() {
        return "PriceComponentRuleFactorDeliveryMode(id=" + this.getId() + ", deliveryMode=" + this.getDeliveryMode() + ")";
    }

    public boolean equals(final Object o) {
        if (o == this) return true;
        if (!(o instanceof PriceComponentRuleFactorDeliveryMode))
            return false;
        final PriceComponentRuleFactorDeliveryMode other = (PriceComponentRuleFactorDeliveryMode) o;
        if (!other.canEqual((Object) this)) return false;
        if (this.getId() != other.getId()) return false;
        final Object this$deliveryMode = this.getDeliveryMode();
        final Object other$deliveryMode = other.getDeliveryMode();
        if (this$deliveryMode == null ? other$deliveryMode != null : !this$deliveryMode.equals(other$deliveryMode))
            return false;
        return true;
    }

    protected boolean canEqual(final Object other) {
        return other instanceof PriceComponentRuleFactorDeliveryMode;
    }

    public int hashCode() {
        final int PRIME = 59;
        int result = 1;
        final long $id = this.getId();
        result = result * PRIME + (int) ($id >>> 32 ^ $id);
        final Object $deliveryMode = this.getDeliveryMode();
        result = result * PRIME + ($deliveryMode == null ? 43 : $deliveryMode.hashCode());
        return result;
    }

    public static class PriceComponentRuleFactorDeliveryModeBuilder {
        private long id;
        private DeliveryModeEnum deliveryMode;

        PriceComponentRuleFactorDeliveryModeBuilder() {
        }

        public PriceComponentRuleFactorDeliveryMode.PriceComponentRuleFactorDeliveryModeBuilder id(long id) {
            this.id = id;
            return this;
        }

        public PriceComponentRuleFactorDeliveryMode.PriceComponentRuleFactorDeliveryModeBuilder deliveryMode(DeliveryModeEnum deliveryMode) {
            this.deliveryMode = deliveryMode;
            return this;
        }

        public PriceComponentRuleFactorDeliveryMode build() {
            return new PriceComponentRuleFactorDeliveryMode(id, deliveryMode);
        }

        public String toString() {
            return "PriceComponentRuleFactorDeliveryMode.PriceComponentRuleFactorDeliveryModeBuilder(id=" + this.id + ", deliveryMode=" + this.deliveryMode + ")";
        }
    }
}
