package ch.swissbytes.businessrules.pricing.factors;

import ch.swissbytes.businessrules.pricing.inputs.PriceComponentRuleInputSaleOrder;
import ch.swissbytes.businessrules.pricing.interfaces.ISimplePriceRuleValidator;
import ch.swissbytes.businessrules.pricing.models.BaseFactorModel;

import java.util.Objects;


public class PriceComponentRuleFactorBusinessUnit extends BaseFactorModel implements ISimplePriceRuleValidator {
    private long id;
    private String name;

    public PriceComponentRuleFactorBusinessUnit(long id, String name) {
        this.id = id;
        this.name = name;
    }

    public PriceComponentRuleFactorBusinessUnit() {
    }

    public static PriceComponentRuleFactorBusinessUnitBuilder builder() {
        return new PriceComponentRuleFactorBusinessUnitBuilder();
    }

    @Override
    public boolean isValid(PriceComponentRuleInputSaleOrder saleOrder) {
        return Objects.nonNull(saleOrder.getBusinessUnit()) && saleOrder.getBusinessUnit().id == this.id;
    }

    public long getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String toString() {
        return "PriceComponentRuleFactorBusinessUnit(id=" + this.getId() + ", name=" + this.getName() + ")";
    }

    public boolean equals(final Object o) {
        if (o == this) return true;
        if (!(o instanceof PriceComponentRuleFactorBusinessUnit))
            return false;
        final PriceComponentRuleFactorBusinessUnit other = (PriceComponentRuleFactorBusinessUnit) o;
        if (!other.canEqual((Object) this)) return false;
        if (this.getId() != other.getId()) return false;
        final Object this$name = this.getName();
        final Object other$name = other.getName();
        if (this$name == null ? other$name != null : !this$name.equals(other$name)) return false;
        return true;
    }

    protected boolean canEqual(final Object other) {
        return other instanceof PriceComponentRuleFactorBusinessUnit;
    }

    public int hashCode() {
        final int PRIME = 59;
        int result = 1;
        final long $id = this.getId();
        result = result * PRIME + (int) ($id >>> 32 ^ $id);
        final Object $name = this.getName();
        result = result * PRIME + ($name == null ? 43 : $name.hashCode());
        return result;
    }

    public static class PriceComponentRuleFactorBusinessUnitBuilder {
        private long id;
        private String name;

        PriceComponentRuleFactorBusinessUnitBuilder() {
        }

        public PriceComponentRuleFactorBusinessUnit.PriceComponentRuleFactorBusinessUnitBuilder id(long id) {
            this.id = id;
            return this;
        }

        public PriceComponentRuleFactorBusinessUnit.PriceComponentRuleFactorBusinessUnitBuilder name(String name) {
            this.name = name;
            return this;
        }

        public PriceComponentRuleFactorBusinessUnit build() {
            return new PriceComponentRuleFactorBusinessUnit(id, name);
        }

        public String toString() {
            return "PriceComponentRuleFactorBusinessUnit.PriceComponentRuleFactorBusinessUnitBuilder(id=" + this.id + ", name=" + this.name + ")";
        }
    }
}
