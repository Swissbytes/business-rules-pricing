package ch.swissbytes.businessrules.pricing.factors;

import ch.swissbytes.businessrules.pricing.enums.RoleTypeEnum;
import ch.swissbytes.businessrules.pricing.inputs.PriceComponentRuleInputSaleOrder;
import ch.swissbytes.businessrules.pricing.interfaces.ISimplePriceRuleValidator;
import ch.swissbytes.businessrules.pricing.models.BaseModel;

import java.util.Objects;


public class PriceComponentRuleFactorRoleType extends BaseModel implements ISimplePriceRuleValidator {
    private long id;
    private RoleTypeEnum roleType;

    public PriceComponentRuleFactorRoleType(long id, RoleTypeEnum roleType) {
        this.id = id;
        this.roleType = roleType;
    }

    public PriceComponentRuleFactorRoleType() {
    }

    public static PriceComponentRuleFactorRoleTypeBuilder builder() {
        return new PriceComponentRuleFactorRoleTypeBuilder();
    }

    @Override
    public boolean isValid(PriceComponentRuleInputSaleOrder saleOrder) {
        return (Objects.nonNull(saleOrder.getCustomer().getRoles()) &&
                !saleOrder.getCustomer().getRoles().isEmpty()) &&
                saleOrder.getCustomer().getRoles().stream().anyMatch(roleType -> roleType.equals(this.roleType));
    }

    public long getId() {
        return this.id;
    }

    public RoleTypeEnum getRoleType() {
        return this.roleType;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setRoleType(RoleTypeEnum roleType) {
        this.roleType = roleType;
    }

    public String toString() {
        return "PriceComponentRuleFactorRoleType(id=" + this.getId() + ", roleType=" + this.getRoleType() + ")";
    }

    public boolean equals(final Object o) {
        if (o == this) return true;
        if (!(o instanceof PriceComponentRuleFactorRoleType)) return false;
        final PriceComponentRuleFactorRoleType other = (PriceComponentRuleFactorRoleType) o;
        if (!other.canEqual((Object) this)) return false;
        if (this.getId() != other.getId()) return false;
        final Object this$roleType = this.getRoleType();
        final Object other$roleType = other.getRoleType();
        if (this$roleType == null ? other$roleType != null : !this$roleType.equals(other$roleType)) return false;
        return true;
    }

    protected boolean canEqual(final Object other) {
        return other instanceof PriceComponentRuleFactorRoleType;
    }

    public int hashCode() {
        final int PRIME = 59;
        int result = 1;
        final long $id = this.getId();
        result = result * PRIME + (int) ($id >>> 32 ^ $id);
        final Object $roleType = this.getRoleType();
        result = result * PRIME + ($roleType == null ? 43 : $roleType.hashCode());
        return result;
    }

    public static class PriceComponentRuleFactorRoleTypeBuilder {
        private long id;
        private RoleTypeEnum roleType;

        PriceComponentRuleFactorRoleTypeBuilder() {
        }

        public PriceComponentRuleFactorRoleType.PriceComponentRuleFactorRoleTypeBuilder id(long id) {
            this.id = id;
            return this;
        }

        public PriceComponentRuleFactorRoleType.PriceComponentRuleFactorRoleTypeBuilder roleType(RoleTypeEnum roleType) {
            this.roleType = roleType;
            return this;
        }

        public PriceComponentRuleFactorRoleType build() {
            return new PriceComponentRuleFactorRoleType(id, roleType);
        }

        public String toString() {
            return "PriceComponentRuleFactorRoleType.PriceComponentRuleFactorRoleTypeBuilder(id=" + this.id + ", roleType=" + this.roleType + ")";
        }
    }
}
