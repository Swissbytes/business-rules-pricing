package ch.swissbytes.businessrules.pricing.factors;

import ch.swissbytes.businessrules.pricing.inputs.PriceComponentRuleInputSaleOrder;
import ch.swissbytes.businessrules.pricing.interfaces.ISimplePriceRuleValidator;
import ch.swissbytes.businessrules.pricing.models.BaseFactorModel;

import java.math.BigDecimal;
import java.util.Objects;


public class PriceComponentRuleFactorGeographicBoundary extends BaseFactorModel implements ISimplePriceRuleValidator {

    private long id;
    private String name;
    private BigDecimal latitude;
    private BigDecimal longitude;

    public PriceComponentRuleFactorGeographicBoundary(long id, String name, BigDecimal latitude, BigDecimal longitude) {
        this.id = id;
        this.name = name;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public PriceComponentRuleFactorGeographicBoundary() {
    }

    public static PriceComponentRuleFactorGeographicBoundaryBuilder builder() {
        return new PriceComponentRuleFactorGeographicBoundaryBuilder();
    }

    @Override
    public boolean isValid(PriceComponentRuleInputSaleOrder saleOrder) {
        return (Objects.nonNull(saleOrder.getCustomer().getGeographicBoundaries()) &&
                !saleOrder.getCustomer().getGeographicBoundaries().isEmpty()) &&
                saleOrder.getCustomer().getGeographicBoundaries().stream().anyMatch(geographicBoundary -> geographicBoundary.getId() == this.id);
    }

    public long getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public BigDecimal getLatitude() {
        return this.latitude;
    }

    public BigDecimal getLongitude() {
        return this.longitude;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setLatitude(BigDecimal latitude) {
        this.latitude = latitude;
    }

    public void setLongitude(BigDecimal longitude) {
        this.longitude = longitude;
    }

    public String toString() {
        return "PriceComponentRuleFactorGeographicBoundary(id=" + this.getId() + ", name=" + this.getName() + ", latitude=" + this.getLatitude() + ", longitude=" + this.getLongitude() + ")";
    }

    public boolean equals(final Object o) {
        if (o == this) return true;
        if (!(o instanceof PriceComponentRuleFactorGeographicBoundary))
            return false;
        final PriceComponentRuleFactorGeographicBoundary other = (PriceComponentRuleFactorGeographicBoundary) o;
        if (!other.canEqual((Object) this)) return false;
        if (this.getId() != other.getId()) return false;
        final Object this$name = this.getName();
        final Object other$name = other.getName();
        if (this$name == null ? other$name != null : !this$name.equals(other$name)) return false;
        final Object this$latitude = this.getLatitude();
        final Object other$latitude = other.getLatitude();
        if (this$latitude == null ? other$latitude != null : !this$latitude.equals(other$latitude)) return false;
        final Object this$longitude = this.getLongitude();
        final Object other$longitude = other.getLongitude();
        if (this$longitude == null ? other$longitude != null : !this$longitude.equals(other$longitude)) return false;
        return true;
    }

    protected boolean canEqual(final Object other) {
        return other instanceof PriceComponentRuleFactorGeographicBoundary;
    }

    public int hashCode() {
        final int PRIME = 59;
        int result = 1;
        final long $id = this.getId();
        result = result * PRIME + (int) ($id >>> 32 ^ $id);
        final Object $name = this.getName();
        result = result * PRIME + ($name == null ? 43 : $name.hashCode());
        final Object $latitude = this.getLatitude();
        result = result * PRIME + ($latitude == null ? 43 : $latitude.hashCode());
        final Object $longitude = this.getLongitude();
        result = result * PRIME + ($longitude == null ? 43 : $longitude.hashCode());
        return result;
    }

    public static class PriceComponentRuleFactorGeographicBoundaryBuilder {
        private long id;
        private String name;
        private BigDecimal latitude;
        private BigDecimal longitude;

        PriceComponentRuleFactorGeographicBoundaryBuilder() {
        }

        public PriceComponentRuleFactorGeographicBoundary.PriceComponentRuleFactorGeographicBoundaryBuilder id(long id) {
            this.id = id;
            return this;
        }

        public PriceComponentRuleFactorGeographicBoundary.PriceComponentRuleFactorGeographicBoundaryBuilder name(String name) {
            this.name = name;
            return this;
        }

        public PriceComponentRuleFactorGeographicBoundary.PriceComponentRuleFactorGeographicBoundaryBuilder latitude(BigDecimal latitude) {
            this.latitude = latitude;
            return this;
        }

        public PriceComponentRuleFactorGeographicBoundary.PriceComponentRuleFactorGeographicBoundaryBuilder longitude(BigDecimal longitude) {
            this.longitude = longitude;
            return this;
        }

        public PriceComponentRuleFactorGeographicBoundary build() {
            return new PriceComponentRuleFactorGeographicBoundary(id, name, latitude, longitude);
        }

        public String toString() {
            return "PriceComponentRuleFactorGeographicBoundary.PriceComponentRuleFactorGeographicBoundaryBuilder(id=" + this.id + ", name=" + this.name + ", latitude=" + this.latitude + ", longitude=" + this.longitude + ")";
        }
    }
}
