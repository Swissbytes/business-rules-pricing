package ch.swissbytes.businessrules.pricing.factors;

import ch.swissbytes.businessrules.pricing.enums.RoleTypeEnum;
import ch.swissbytes.businessrules.pricing.inputs.PriceComponentRuleInputSaleOrder;
import ch.swissbytes.businessrules.pricing.interfaces.ISimplePriceRuleValidator;

import java.util.List;
import java.util.Objects;


public class PriceComponentRuleFactorCustomer extends PriceComponentRuleFactorPartyRole implements ISimplePriceRuleValidator {

    private List<RoleTypeEnum> roles;
    private List<PriceComponentRuleFactorPartyClassification> partyClassifications;
    private List<PriceComponentRuleFactorGeographicBoundary> geographicBoundaries;

    public PriceComponentRuleFactorCustomer(long id, String name, List<RoleTypeEnum> roles,
                                            List<PriceComponentRuleFactorPartyClassification> partyClassifications,
                                            List<PriceComponentRuleFactorGeographicBoundary> geographicBoundaries) {
        super(id, name);
        this.roles = roles;
        this.partyClassifications = partyClassifications;
        this.geographicBoundaries = geographicBoundaries;
    }

    public static PriceComponentRuleFactorCustomerBuilder builder() {
        return new PriceComponentRuleFactorCustomerBuilder();
    }


    @Override
    public boolean isValid(PriceComponentRuleInputSaleOrder saleOrder) {
        return Objects.nonNull(saleOrder.getCustomer()) && (saleOrder.getCustomer().getId() == this.getId());
    }

    public List<RoleTypeEnum> getRoles() {
        return this.roles;
    }

    public List<PriceComponentRuleFactorPartyClassification> getPartyClassifications() {
        return this.partyClassifications;
    }

    public List<PriceComponentRuleFactorGeographicBoundary> getGeographicBoundaries() {
        return this.geographicBoundaries;
    }

    public void setRoles(List<RoleTypeEnum> roles) {
        this.roles = roles;
    }

    public void setPartyClassifications(List<PriceComponentRuleFactorPartyClassification> partyClassifications) {
        this.partyClassifications = partyClassifications;
    }

    public void setGeographicBoundaries(List<PriceComponentRuleFactorGeographicBoundary> geographicBoundaries) {
        this.geographicBoundaries = geographicBoundaries;
    }

    public String toString() {
        return "PriceComponentRuleFactorCustomer(roles=" + this.getRoles() + ", partyClassifications=" + this.getPartyClassifications() + ", geographicBoundaries=" + this.getGeographicBoundaries() + ")";
    }

    public boolean equals(final Object o) {
        if (o == this) return true;
        if (!(o instanceof PriceComponentRuleFactorCustomer)) return false;
        final PriceComponentRuleFactorCustomer other = (PriceComponentRuleFactorCustomer) o;
        if (!other.canEqual((Object) this)) return false;
        final Object this$roles = this.getRoles();
        final Object other$roles = other.getRoles();
        if (this$roles == null ? other$roles != null : !this$roles.equals(other$roles)) return false;
        final Object this$partyClassifications = this.getPartyClassifications();
        final Object other$partyClassifications = other.getPartyClassifications();
        if (this$partyClassifications == null ? other$partyClassifications != null : !this$partyClassifications.equals(other$partyClassifications))
            return false;
        final Object this$geographicBoundaries = this.getGeographicBoundaries();
        final Object other$geographicBoundaries = other.getGeographicBoundaries();
        if (this$geographicBoundaries == null ? other$geographicBoundaries != null : !this$geographicBoundaries.equals(other$geographicBoundaries))
            return false;
        return true;
    }

    protected boolean canEqual(final Object other) {
        return other instanceof PriceComponentRuleFactorCustomer;
    }

    public int hashCode() {
        final int PRIME = 59;
        int result = 1;
        final Object $roles = this.getRoles();
        result = result * PRIME + ($roles == null ? 43 : $roles.hashCode());
        final Object $partyClassifications = this.getPartyClassifications();
        result = result * PRIME + ($partyClassifications == null ? 43 : $partyClassifications.hashCode());
        final Object $geographicBoundaries = this.getGeographicBoundaries();
        result = result * PRIME + ($geographicBoundaries == null ? 43 : $geographicBoundaries.hashCode());
        return result;
    }

    public static class PriceComponentRuleFactorCustomerBuilder {
        private long id;
        private String name;
        private List<RoleTypeEnum> roles;
        private List<PriceComponentRuleFactorPartyClassification> partyClassifications;
        private List<PriceComponentRuleFactorGeographicBoundary> geographicBoundaries;

        PriceComponentRuleFactorCustomerBuilder() {
        }

        public PriceComponentRuleFactorCustomer.PriceComponentRuleFactorCustomerBuilder id(long id) {
            this.id = id;
            return this;
        }

        public PriceComponentRuleFactorCustomer.PriceComponentRuleFactorCustomerBuilder name(String name) {
            this.name = name;
            return this;
        }

        public PriceComponentRuleFactorCustomer.PriceComponentRuleFactorCustomerBuilder roles(List<RoleTypeEnum> roles) {
            this.roles = roles;
            return this;
        }

        public PriceComponentRuleFactorCustomer.PriceComponentRuleFactorCustomerBuilder partyClassifications(List<PriceComponentRuleFactorPartyClassification> partyClassifications) {
            this.partyClassifications = partyClassifications;
            return this;
        }

        public PriceComponentRuleFactorCustomer.PriceComponentRuleFactorCustomerBuilder geographicBoundaries(List<PriceComponentRuleFactorGeographicBoundary> geographicBoundaries) {
            this.geographicBoundaries = geographicBoundaries;
            return this;
        }

        public PriceComponentRuleFactorCustomer build() {
            return new PriceComponentRuleFactorCustomer(id, name, roles, partyClassifications, geographicBoundaries);
        }

        public String toString() {
            return "PriceComponentRuleFactorCustomer.PriceComponentRuleFactorCustomerBuilder(id=" + this.id + ", name=" + this.name + ", roles=" + this.roles + ", partyClassifications=" + this.partyClassifications + ", geographicBoundaries=" + this.geographicBoundaries + ")";
        }
    }
}
