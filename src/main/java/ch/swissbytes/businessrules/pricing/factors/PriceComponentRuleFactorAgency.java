package ch.swissbytes.businessrules.pricing.factors;

import ch.swissbytes.businessrules.pricing.inputs.PriceComponentRuleInputSaleOrder;
import ch.swissbytes.businessrules.pricing.interfaces.ISimplePriceRuleValidator;

import java.util.Objects;


public class PriceComponentRuleFactorAgency extends PriceComponentRuleFactorPartyRole implements ISimplePriceRuleValidator {

    public PriceComponentRuleFactorAgency(long id, String name) {
        super(id, name);
    }

    public static PriceComponentRuleFactorAgencyBuilder builder() {
        return new PriceComponentRuleFactorAgencyBuilder();
    }

    @Override
    public boolean isValid(PriceComponentRuleInputSaleOrder saleOrder) {
        return Objects.nonNull(saleOrder.getAgency()) && (saleOrder.getAgency().getId() == this.getId());
    }

    public boolean equals(final Object o) {
        if (o == this) return true;
        if (!(o instanceof PriceComponentRuleFactorAgency)) return false;
        final PriceComponentRuleFactorAgency other = (PriceComponentRuleFactorAgency) o;
        if (!other.canEqual((Object) this)) return false;
        return true;
    }

    protected boolean canEqual(final Object other) {
        return other instanceof PriceComponentRuleFactorAgency;
    }

    public int hashCode() {
        int result = 1;
        return result;
    }

    public static class PriceComponentRuleFactorAgencyBuilder {
        private long id;
        private String name;

        PriceComponentRuleFactorAgencyBuilder() {
        }

        public PriceComponentRuleFactorAgency.PriceComponentRuleFactorAgencyBuilder id(long id) {
            this.id = id;
            return this;
        }

        public PriceComponentRuleFactorAgency.PriceComponentRuleFactorAgencyBuilder name(String name) {
            this.name = name;
            return this;
        }

        public PriceComponentRuleFactorAgency build() {
            return new PriceComponentRuleFactorAgency(id, name);
        }

        public String toString() {
            return "PriceComponentRuleFactorAgency.PriceComponentRuleFactorAgencyBuilder(id=" + this.id + ", name=" + this.name + ")";
        }
    }
}
