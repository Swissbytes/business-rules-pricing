package ch.swissbytes.businessrules.pricing.factors;


import ch.swissbytes.businessrules.pricing.inputs.PriceComponentRuleInputSaleOrder;
import ch.swissbytes.businessrules.pricing.interfaces.ISimplePriceRuleValidator;
import ch.swissbytes.businessrules.pricing.models.BaseModel;
import ch.swissbytes.businessrules.pricing.enums.PriceRuleFactorSaleOrderOrigin;

import java.util.Objects;


public class PriceComponentRuleFactorSaleOrderOrigin extends BaseModel implements ISimplePriceRuleValidator {
    private long id;
    private PriceRuleFactorSaleOrderOrigin saleOrderOrigin;

    public PriceComponentRuleFactorSaleOrderOrigin(long id, PriceRuleFactorSaleOrderOrigin saleOrderOrigin) {
        this.id = id;
        this.saleOrderOrigin = saleOrderOrigin;
    }

    public PriceComponentRuleFactorSaleOrderOrigin() {
    }

    public static PriceComponentRuleFactorSaleOrderOriginBuilder builder() {
        return new PriceComponentRuleFactorSaleOrderOriginBuilder();
    }

    @Override
    public boolean isValid(PriceComponentRuleInputSaleOrder saleOrder) {
        return (Objects.nonNull(saleOrder.getSaleOrderOrigin()) && saleOrder.getSaleOrderOrigin().equals(this.saleOrderOrigin));
    }

    public long getId() {
        return this.id;
    }

    public PriceRuleFactorSaleOrderOrigin getSaleOrderOrigin() {
        return this.saleOrderOrigin;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setSaleOrderOrigin(PriceRuleFactorSaleOrderOrigin saleOrderOrigin) {
        this.saleOrderOrigin = saleOrderOrigin;
    }

    public String toString() {
        return "PriceComponentRuleFactorSaleOrderOrigin(id=" + this.getId() + ", saleOrderOrigin=" + this.getSaleOrderOrigin() + ")";
    }

    public boolean equals(final Object o) {
        if (o == this) return true;
        if (!(o instanceof PriceComponentRuleFactorSaleOrderOrigin)) return false;
        final PriceComponentRuleFactorSaleOrderOrigin other = (PriceComponentRuleFactorSaleOrderOrigin) o;
        if (!other.canEqual((Object) this)) return false;
        if (this.getId() != other.getId()) return false;

        final Object this$saleOrderOrigin = this.getSaleOrderOrigin();
        final Object other$saleOrderOrigin = other.getSaleOrderOrigin();

        if (this$saleOrderOrigin == null ? other$saleOrderOrigin != null : !this$saleOrderOrigin.equals(other$saleOrderOrigin)) return false;
        return true;
    }

    protected boolean canEqual(final Object other) {
        return other instanceof PriceComponentRuleFactorSaleOrderOrigin;
    }

    public int hashCode() {
        final int PRIME = 59;
        int result = 1;
        final long $id = this.getId();
        result = result * PRIME + (int) ($id >>> 32 ^ $id);
        final Object $saleOrderOrigin = this.getSaleOrderOrigin();
        result = result * PRIME + ($saleOrderOrigin == null ? 43 : $saleOrderOrigin.hashCode());
        return result;
    }

    public static class PriceComponentRuleFactorSaleOrderOriginBuilder {
        private long id;
        private PriceRuleFactorSaleOrderOrigin saleOrderOrigin;

        PriceComponentRuleFactorSaleOrderOriginBuilder() {
        }

        public PriceComponentRuleFactorSaleOrderOriginBuilder id(long id) {
            this.id = id;
            return this;
        }

        public PriceComponentRuleFactorSaleOrderOriginBuilder saleOrderOrigin(PriceRuleFactorSaleOrderOrigin saleOrderOrigin) {
            this.saleOrderOrigin = saleOrderOrigin;
            return this;
        }

        public PriceComponentRuleFactorSaleOrderOrigin build() {
            return new PriceComponentRuleFactorSaleOrderOrigin(id, saleOrderOrigin);
        }

        public String toString() {
            return "PriceComponentRuleFactorSaleOrderOrigin.PriceComponentRuleFactorSaleOrderOriginBuilder(id=" + this.id + ", saleOrderOrigin=" + this.saleOrderOrigin + ")";
        }
    }
}
