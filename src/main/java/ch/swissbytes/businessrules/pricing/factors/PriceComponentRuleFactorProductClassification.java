package ch.swissbytes.businessrules.pricing.factors;

import ch.swissbytes.businessrules.pricing.inputs.PriceComponentRuleInputSaleOrder;
import ch.swissbytes.businessrules.pricing.inputs.PriceComponentRuleInputSaleOrderProduct;
import ch.swissbytes.businessrules.pricing.interfaces.IPriceRuleProductValidator;
import ch.swissbytes.businessrules.pricing.interfaces.ISimplePriceRuleValidator;
import ch.swissbytes.businessrules.pricing.models.BaseFactorModel;

import java.util.List;
import java.util.Objects;


public class PriceComponentRuleFactorProductClassification extends BaseFactorModel implements ISimplePriceRuleValidator, IPriceRuleProductValidator {
    private long id;
    private String name;
    private String path;

    public PriceComponentRuleFactorProductClassification(long id, String name, String path) {
        this.id = id;
        this.name = name;
        this.path = path;
    }

    public PriceComponentRuleFactorProductClassification() {
    }

    public static PriceComponentRuleFactorProductClassificationBuilder builder() {
        return new PriceComponentRuleFactorProductClassificationBuilder();
    }

    private boolean checkIfExistsOnPath(List<PriceComponentRuleFactorProductClassification> productClassifications) {
        return (Objects.nonNull(productClassifications)) &&
                !productClassifications.isEmpty() &&
                productClassifications.stream().anyMatch(classification -> checkIfExistsInLastItemInPath(classification.getPath()));
    }

    private boolean checkIfExistsInLastItemInPath(String pathSaleOrder) {
        if (Objects.isNull(path) || path.isEmpty() || Objects.isNull(pathSaleOrder) || pathSaleOrder.isEmpty()) {
            return false;
        }
        return pathSaleOrder.toLowerCase().startsWith(path.toLowerCase());
    }

    @Override
    public boolean isValidProduct(PriceComponentRuleInputSaleOrderProduct saleOrderProduct){
        return this.checkIfExistsOnPath(saleOrderProduct.getClassifications());
    }

    @Override
    public boolean isValid(PriceComponentRuleInputSaleOrder saleOrder){
        return saleOrder.getProductDetails().stream().anyMatch(saleOrderProduct -> checkIfExistsOnPath(saleOrderProduct.getClassifications()));
    }

    public long getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public String getPath() {
        return this.path;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String toString() {
        return "PriceComponentRuleFactorProductClassification(id=" + this.getId() + ", name=" + this.getName() + ", path=" + this.getPath() + ")";
    }

    public boolean equals(final Object o) {
        if (o == this) return true;
        if (!(o instanceof PriceComponentRuleFactorProductClassification))
            return false;
        final PriceComponentRuleFactorProductClassification other = (PriceComponentRuleFactorProductClassification) o;
        if (!other.canEqual((Object) this)) return false;
        if (this.getId() != other.getId()) return false;
        final Object this$name = this.getName();
        final Object other$name = other.getName();
        if (this$name == null ? other$name != null : !this$name.equals(other$name)) return false;
        final Object this$path = this.getPath();
        final Object other$path = other.getPath();
        if (this$path == null ? other$path != null : !this$path.equals(other$path)) return false;
        return true;
    }

    protected boolean canEqual(final Object other) {
        return other instanceof PriceComponentRuleFactorProductClassification;
    }

    public int hashCode() {
        final int PRIME = 59;
        int result = 1;
        final long $id = this.getId();
        result = result * PRIME + (int) ($id >>> 32 ^ $id);
        final Object $name = this.getName();
        result = result * PRIME + ($name == null ? 43 : $name.hashCode());
        final Object $path = this.getPath();
        result = result * PRIME + ($path == null ? 43 : $path.hashCode());
        return result;
    }

    public static class PriceComponentRuleFactorProductClassificationBuilder {
        private long id;
        private String name;
        private String path;

        PriceComponentRuleFactorProductClassificationBuilder() {
        }

        public PriceComponentRuleFactorProductClassification.PriceComponentRuleFactorProductClassificationBuilder id(long id) {
            this.id = id;
            return this;
        }

        public PriceComponentRuleFactorProductClassification.PriceComponentRuleFactorProductClassificationBuilder name(String name) {
            this.name = name;
            return this;
        }

        public PriceComponentRuleFactorProductClassification.PriceComponentRuleFactorProductClassificationBuilder path(String path) {
            this.path = path;
            return this;
        }

        public PriceComponentRuleFactorProductClassification build() {
            return new PriceComponentRuleFactorProductClassification(id, name, path);
        }

        public String toString() {
            return "PriceComponentRuleFactorProductClassification.PriceComponentRuleFactorProductClassificationBuilder(id=" + this.id + ", name=" + this.name + ", path=" + this.path + ")";
        }
    }
}
