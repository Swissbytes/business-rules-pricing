package ch.swissbytes.businessrules.pricing.factors;

import ch.swissbytes.businessrules.pricing.inputs.PriceComponentRuleInputSaleOrder;
import ch.swissbytes.businessrules.pricing.interfaces.ISimplePriceRuleValidator;
import ch.swissbytes.businessrules.pricing.models.BaseFactorModel;

import java.util.Objects;


public class PriceComponentRuleFactorPartyClassification extends BaseFactorModel implements ISimplePriceRuleValidator {

    private long id;
    private String name;

    public PriceComponentRuleFactorPartyClassification(long id, String name) {
        this.id = id;
        this.name = name;
    }

    public PriceComponentRuleFactorPartyClassification() {
    }

    public static PriceComponentRuleFactorPartyClassificationBuilder builder() {
        return new PriceComponentRuleFactorPartyClassificationBuilder();
    }
//    private long partyCategoryType;

    @Override
    public boolean isValid(PriceComponentRuleInputSaleOrder saleOrder) {
            return Objects.nonNull(saleOrder.getCustomer().getPartyClassifications()) &&
                    !saleOrder.getCustomer().getPartyClassifications().isEmpty() &&
                    saleOrder.getCustomer().getPartyClassifications().stream()
                    .anyMatch(partyClassification -> partyClassification.getId() == this.id);
    }

    public long getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String toString() {
        return "PriceComponentRuleFactorPartyClassification(id=" + this.getId() + ", name=" + this.getName() + ")";
    }

    public boolean equals(final Object o) {
        if (o == this) return true;
        if (!(o instanceof PriceComponentRuleFactorPartyClassification))
            return false;
        final PriceComponentRuleFactorPartyClassification other = (PriceComponentRuleFactorPartyClassification) o;
        if (!other.canEqual((Object) this)) return false;
        if (this.getId() != other.getId()) return false;
        final Object this$name = this.getName();
        final Object other$name = other.getName();
        if (this$name == null ? other$name != null : !this$name.equals(other$name)) return false;
        return true;
    }

    protected boolean canEqual(final Object other) {
        return other instanceof PriceComponentRuleFactorPartyClassification;
    }

    public int hashCode() {
        final int PRIME = 59;
        int result = 1;
        final long $id = this.getId();
        result = result * PRIME + (int) ($id >>> 32 ^ $id);
        final Object $name = this.getName();
        result = result * PRIME + ($name == null ? 43 : $name.hashCode());
        return result;
    }

    public static class PriceComponentRuleFactorPartyClassificationBuilder {
        private long id;
        private String name;

        PriceComponentRuleFactorPartyClassificationBuilder() {
        }

        public PriceComponentRuleFactorPartyClassification.PriceComponentRuleFactorPartyClassificationBuilder id(long id) {
            this.id = id;
            return this;
        }

        public PriceComponentRuleFactorPartyClassification.PriceComponentRuleFactorPartyClassificationBuilder name(String name) {
            this.name = name;
            return this;
        }

        public PriceComponentRuleFactorPartyClassification build() {
            return new PriceComponentRuleFactorPartyClassification(id, name);
        }

        public String toString() {
            return "PriceComponentRuleFactorPartyClassification.PriceComponentRuleFactorPartyClassificationBuilder(id=" + this.id + ", name=" + this.name + ")";
        }
    }
}
