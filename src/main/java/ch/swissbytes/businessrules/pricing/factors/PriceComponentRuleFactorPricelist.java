package ch.swissbytes.businessrules.pricing.factors;

import ch.swissbytes.businessrules.pricing.inputs.PriceComponentRuleInputSaleOrder;
import ch.swissbytes.businessrules.pricing.interfaces.ISimplePriceRuleValidator;

import java.util.Objects;


public class PriceComponentRuleFactorPricelist extends PriceComponentRuleFactorPartyRole implements ISimplePriceRuleValidator {

    public PriceComponentRuleFactorPricelist(long id, String name) {
        super(id, name);
    }

    public static PriceComponentRuleFactorPricelistBuilder builder() {
        return new PriceComponentRuleFactorPricelistBuilder();
    }

    @Override
    public boolean isValid(PriceComponentRuleInputSaleOrder saleOrder) {
        return Objects.nonNull(saleOrder.getPricelist()) && (saleOrder.getPricelist().getId() == this.getId());
    }

    public boolean equals(final Object o) {
        if (o == this) return true;
        if (!(o instanceof PriceComponentRuleFactorPricelist)) return false;
        final PriceComponentRuleFactorPricelist other = (PriceComponentRuleFactorPricelist) o;
        if (!other.canEqual((Object) this)) return false;
        return true;
    }

    protected boolean canEqual(final Object other) {
        return other instanceof PriceComponentRuleFactorPricelist;
    }

    public int hashCode() {
        final int PRIME = 59;
        int result = 1;
        final long $id = this.getId();
        result = result * PRIME + (int) ($id >>> 32 ^ $id);
        final Object $name = this.getName();
        result = result * PRIME + ($name == null ? 43 : $name.hashCode());
        return result;
    }

    public static class PriceComponentRuleFactorPricelistBuilder {
        private long id;
        private String name;

        PriceComponentRuleFactorPricelistBuilder() {
        }

        public PriceComponentRuleFactorPricelist.PriceComponentRuleFactorPricelistBuilder id(long id) {
            this.id = id;
            return this;
        }

        public PriceComponentRuleFactorPricelist.PriceComponentRuleFactorPricelistBuilder name(String name) {
            this.name = name;
            return this;
        }

        public PriceComponentRuleFactorPricelist build() {
            return new PriceComponentRuleFactorPricelist(id, name);
        }

        public String toString() {
            return "PriceComponentRuleFactorPricelist.PriceComponentRuleFactorPricelistBuilder(id=" + this.id
                    + ", name=" + this.name
                    + ")";
        }
    }
}
