package ch.swissbytes.businessrules.pricing.factors;

import ch.swissbytes.businessrules.pricing.inputs.PriceComponentRuleInputSaleOrder;
import ch.swissbytes.businessrules.pricing.interfaces.ISimplePriceRuleValidator;
import ch.swissbytes.businessrules.pricing.models.BaseFactorModel;
import ch.swissbytes.businessrules.pricing.models.Coordinate;
import com.tomgibara.bits.BitStore;
import com.tomgibara.bits.BitVector;

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicInteger;


public class PriceComponentRuleFactorGeoPolygon extends BaseFactorModel implements ISimplePriceRuleValidator {

    private ArrayList<Coordinate> coordinates;

    private double[] constant;
    private double[] multiple;

    private boolean needNormalize = false;

    public PriceComponentRuleFactorGeoPolygon(ArrayList<Coordinate> coordinates, double[] constant, double[] multiple, boolean needNormalize) {
        this.coordinates = coordinates;
        this.constant = constant;
        this.multiple = multiple;
        this.needNormalize = needNormalize;
    }

    public PriceComponentRuleFactorGeoPolygon() {
    }

    public static PriceComponentRuleFactorGeoPolygonBuilder builder() {
        return new PriceComponentRuleFactorGeoPolygonBuilder();
    }

    private void precalc() {
        if (coordinates == null) {
            return;
        }

        int polyCorners = coordinates.size();
        int i;
        int j = polyCorners - 1;

        if (constant != null) {
            constant = null;
        }
        if (multiple != null) {
            multiple = null;
        }

        constant = new double[polyCorners];
        multiple = new double[polyCorners];

        boolean hasNegative = false;
        boolean hasPositive = false;
        for (i = 0; i < polyCorners; i++) {
            if (coordinates.get(i).getLon() > 90) {
                hasPositive = true;
            } else if (coordinates.get(i).getLon() < -90) {
                hasNegative = true;
            }
        }
        needNormalize = hasPositive && hasNegative;

        for (i = 0; i < polyCorners; j = i++) {
            if (normalizeLon(coordinates.get(j).getLon()) == normalizeLon(coordinates.get(i).getLon())) {
                constant[i] = coordinates.get(i).getLat();
                multiple[i] = 0;
            } else {
                constant[i] = coordinates.get(i).getLat()
                        - (normalizeLon(coordinates.get(i).getLon()) * coordinates.get(j).getLat())
                        / (normalizeLon(coordinates.get(j).getLon()) - normalizeLon(coordinates.get(i).getLon()))
                        + (normalizeLon(coordinates.get(i).getLon()) * coordinates.get(i).getLat())
                        / (normalizeLon(coordinates.get(j).getLon()) - normalizeLon(coordinates.get(i).getLon()));
                multiple[i] = (coordinates.get(j).getLat() - coordinates.get(i).getLat())
                        / (normalizeLon(coordinates.get(j).getLon()) - normalizeLon(coordinates.get(i).getLon()));
            }
        }
    }

    public PriceComponentRuleFactorGeoPolygon(ArrayList<Coordinate> coordinates) {
        this.coordinates = coordinates;
        precalc();
    }

    private double normalizeLon(double lon) {
        if (needNormalize && lon < -90) {
            return lon + 360;
        }
        return lon;
    }

    public boolean containsPoint(double latitude, double longitude) {
        int polyCorners = coordinates.size();
        int i;
        int j = polyCorners - 1;
        double longitudeNorm = normalizeLon(longitude);
        boolean oddNodes = false;

        for (i = 0; i < polyCorners; j = i++) {
            if (normalizeLon(coordinates.get(i).getLon()) < longitudeNorm
                    && normalizeLon(coordinates.get(j).getLon()) >= longitudeNorm
                    || normalizeLon(coordinates.get(j).getLon()) < longitudeNorm
                    && normalizeLon(coordinates.get(i).getLon()) >= longitudeNorm) {
                oddNodes ^= longitudeNorm * multiple[i] + constant[i] < latitude;
            }
        }
        return oddNodes;
    }


    @Override
    public boolean isValid(PriceComponentRuleInputSaleOrder saleOrder) {
        BitStore validation = new BitVector(saleOrder.getCustomer().getGeographicBoundaries().size());
        AtomicInteger index = new AtomicInteger();
        index.set(0);
        saleOrder.getCustomer().getGeographicBoundaries().forEach(factorGeographicBoundary -> {
            Coordinate point = Coordinate.toCoordinate(factorGeographicBoundary);
            validation.setBit(index.getAndIncrement(), containsPoint(point.lat, point.lon));
        });
        return validation.asList().stream().allMatch(val -> val);
    }

    public ArrayList<Coordinate> getCoordinates() {
        return this.coordinates;
    }

    public double[] getConstant() {
        return this.constant;
    }

    public double[] getMultiple() {
        return this.multiple;
    }

    public boolean isNeedNormalize() {
        return this.needNormalize;
    }

    public void setCoordinates(ArrayList<Coordinate> coordinates) {
        this.coordinates = coordinates;
    }

    public void setConstant(double[] constant) {
        this.constant = constant;
    }

    public void setMultiple(double[] multiple) {
        this.multiple = multiple;
    }

    public void setNeedNormalize(boolean needNormalize) {
        this.needNormalize = needNormalize;
    }

    public String toString() {
        return "PriceComponentRuleFactorGeoPolygon(coordinates=" + this.getCoordinates() + ", constant=" + java.util.Arrays.toString(this.getConstant()) + ", multiple=" + java.util.Arrays.toString(this.getMultiple()) + ", needNormalize=" + this.isNeedNormalize() + ")";
    }

    public boolean equals(final Object o) {
        if (o == this) return true;
        if (!(o instanceof PriceComponentRuleFactorGeoPolygon))
            return false;
        final PriceComponentRuleFactorGeoPolygon other = (PriceComponentRuleFactorGeoPolygon) o;
        if (!other.canEqual((Object) this)) return false;
        final Object this$coordinates = this.getCoordinates();
        final Object other$coordinates = other.getCoordinates();
        if (this$coordinates == null ? other$coordinates != null : !this$coordinates.equals(other$coordinates))
            return false;
        if (!java.util.Arrays.equals(this.getConstant(), other.getConstant())) return false;
        if (!java.util.Arrays.equals(this.getMultiple(), other.getMultiple())) return false;
        if (this.isNeedNormalize() != other.isNeedNormalize()) return false;
        return true;
    }

    protected boolean canEqual(final Object other) {
        return other instanceof PriceComponentRuleFactorGeoPolygon;
    }

    public int hashCode() {
        final int PRIME = 59;
        int result = 1;
        final Object $coordinates = this.getCoordinates();
        result = result * PRIME + ($coordinates == null ? 43 : $coordinates.hashCode());
        result = result * PRIME + java.util.Arrays.hashCode(this.getConstant());
        result = result * PRIME + java.util.Arrays.hashCode(this.getMultiple());
        result = result * PRIME + (this.isNeedNormalize() ? 79 : 97);
        return result;
    }

    public static class PriceComponentRuleFactorGeoPolygonBuilder {
        private ArrayList<Coordinate> coordinates;
        private double[] constant;
        private double[] multiple;
        private boolean needNormalize = false;

        PriceComponentRuleFactorGeoPolygonBuilder() {
        }

        public PriceComponentRuleFactorGeoPolygon.PriceComponentRuleFactorGeoPolygonBuilder coordinates(ArrayList<Coordinate> coordinates) {
            this.coordinates = coordinates;
            return this;
        }

        public PriceComponentRuleFactorGeoPolygon.PriceComponentRuleFactorGeoPolygonBuilder constant(double[] constant) {
            this.constant = constant;
            return this;
        }

        public PriceComponentRuleFactorGeoPolygon.PriceComponentRuleFactorGeoPolygonBuilder multiple(double[] multiple) {
            this.multiple = multiple;
            return this;
        }

        public PriceComponentRuleFactorGeoPolygon.PriceComponentRuleFactorGeoPolygonBuilder needNormalize(boolean needNormalize) {
            this.needNormalize = needNormalize;
            return this;
        }

        public PriceComponentRuleFactorGeoPolygon build() {
            return new PriceComponentRuleFactorGeoPolygon(coordinates, constant, multiple, needNormalize);
        }

        public String toString() {
            return "PriceComponentRuleFactorGeoPolygon.PriceComponentRuleFactorGeoPolygonBuilder(coordinates=" + this.coordinates + ", constant=" + java.util.Arrays.toString(this.constant) + ", multiple=" + java.util.Arrays.toString(this.multiple) + ", needNormalize=" + this.needNormalize + ")";
        }
    }
}
