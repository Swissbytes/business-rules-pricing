package ch.swissbytes.businessrules.pricing.factors;

import ch.swissbytes.businessrules.pricing.enums.ScaleTypeEnum;
import ch.swissbytes.businessrules.pricing.models.BaseFactorModel;
import ch.swissbytes.businessrules.pricing.outcomes.PriceComponentRuleOutcome;
import ch.swissbytes.businessrules.pricing.utils.BigDecimalUtil;

import java.math.BigDecimal;


public class PriceComponentRuleFactorScale extends BaseFactorModel {

    private long id;
    private BigDecimal from;
    private BigDecimal thru;
    private ScaleTypeEnum type;
    private PriceComponentRuleOutcome outcome;

    public PriceComponentRuleFactorScale(long id, BigDecimal from, BigDecimal thru, ScaleTypeEnum type, PriceComponentRuleOutcome outcome) {
        this.id = id;
        this.from = from;
        this.thru = thru;
        this.type = type;
        this.outcome = outcome;
    }

    public PriceComponentRuleFactorScale() {
    }

    public static PriceComponentRuleFactorScaleBuilder builder() {
        return new PriceComponentRuleFactorScaleBuilder();
    }

    public boolean isValid(BigDecimal total){
        switch (type){
            case QUANTITY:
                return isValidByQty(total);
            case AMOUNT:
                return isValidByAmount(total);
            default:
                return isValidByAmount(total);
        }
    }

    private boolean isValidByAmount(BigDecimal totalAmount) {
        return BigDecimalUtil.between(totalAmount, from, thru);
    }

    private boolean isValidByQty(BigDecimal totalQty) {
        return BigDecimalUtil.between(totalQty, from, thru);
    }

    public long getId() {
        return this.id;
    }

    public BigDecimal getFrom() {
        return this.from;
    }

    public BigDecimal getThru() {
        return this.thru;
    }

    public ScaleTypeEnum getType() {
        return this.type;
    }

    public PriceComponentRuleOutcome getOutcome() {
        return this.outcome;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setFrom(BigDecimal from) {
        this.from = from;
    }

    public void setThru(BigDecimal thru) {
        this.thru = thru;
    }

    public void setType(ScaleTypeEnum type) {
        this.type = type;
    }

    public void setOutcome(PriceComponentRuleOutcome outcome) {
        this.outcome = outcome;
    }

    public String toString() {
        return "PriceComponentRuleFactorScale(id=" + this.getId() + ", from=" + this.getFrom() + ", thru=" + this.getThru() + ", type=" + this.getType() + ", outcome=" + this.getOutcome() + ")";
    }

    public boolean equals(final Object o) {
        if (o == this) return true;
        if (!(o instanceof PriceComponentRuleFactorScale)) return false;
        final PriceComponentRuleFactorScale other = (PriceComponentRuleFactorScale) o;
        if (!other.canEqual((Object) this)) return false;
        if (this.getId() != other.getId()) return false;
        final Object this$from = this.getFrom();
        final Object other$from = other.getFrom();
        if (this$from == null ? other$from != null : !this$from.equals(other$from)) return false;
        final Object this$thru = this.getThru();
        final Object other$thru = other.getThru();
        if (this$thru == null ? other$thru != null : !this$thru.equals(other$thru)) return false;
        final Object this$type = this.getType();
        final Object other$type = other.getType();
        if (this$type == null ? other$type != null : !this$type.equals(other$type)) return false;
        final Object this$outcome = this.getOutcome();
        final Object other$outcome = other.getOutcome();
        if (this$outcome == null ? other$outcome != null : !this$outcome.equals(other$outcome)) return false;
        return true;
    }

    protected boolean canEqual(final Object other) {
        return other instanceof PriceComponentRuleFactorScale;
    }

    public int hashCode() {
        final int PRIME = 59;
        int result = 1;
        final long $id = this.getId();
        result = result * PRIME + (int) ($id >>> 32 ^ $id);
        final Object $from = this.getFrom();
        result = result * PRIME + ($from == null ? 43 : $from.hashCode());
        final Object $thru = this.getThru();
        result = result * PRIME + ($thru == null ? 43 : $thru.hashCode());
        final Object $type = this.getType();
        result = result * PRIME + ($type == null ? 43 : $type.hashCode());
        final Object $outcome = this.getOutcome();
        result = result * PRIME + ($outcome == null ? 43 : $outcome.hashCode());
        return result;
    }

    public static class PriceComponentRuleFactorScaleBuilder {
        private long id;
        private BigDecimal from;
        private BigDecimal thru;
        private ScaleTypeEnum type;
        private PriceComponentRuleOutcome outcome;

        PriceComponentRuleFactorScaleBuilder() {
        }

        public PriceComponentRuleFactorScale.PriceComponentRuleFactorScaleBuilder id(long id) {
            this.id = id;
            return this;
        }

        public PriceComponentRuleFactorScale.PriceComponentRuleFactorScaleBuilder from(BigDecimal from) {
            this.from = from;
            return this;
        }

        public PriceComponentRuleFactorScale.PriceComponentRuleFactorScaleBuilder thru(BigDecimal thru) {
            this.thru = thru;
            return this;
        }

        public PriceComponentRuleFactorScale.PriceComponentRuleFactorScaleBuilder type(ScaleTypeEnum type) {
            this.type = type;
            return this;
        }

        public PriceComponentRuleFactorScale.PriceComponentRuleFactorScaleBuilder outcome(PriceComponentRuleOutcome outcome) {
            this.outcome = outcome;
            return this;
        }

        public PriceComponentRuleFactorScale build() {
            return new PriceComponentRuleFactorScale(id, from, thru, type, outcome);
        }

        public String toString() {
            return "PriceComponentRuleFactorScale.PriceComponentRuleFactorScaleBuilder(id=" + this.id + ", from=" + this.from + ", thru=" + this.thru + ", type=" + this.type + ", outcome=" + this.outcome + ")";
        }
    }
}
