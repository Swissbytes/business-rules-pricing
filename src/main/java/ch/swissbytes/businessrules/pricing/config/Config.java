package ch.swissbytes.businessrules.pricing.config;

import ch.swissbytes.businessrules.pricing.utils.LogUtil;
import ch.swissbytes.businessrules.pricing.utils.PriceRuleLog;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Objects;
import java.util.Properties;


public class Config {

    private final Properties properties = new Properties();

    private final String defaultConfigFile = "default.xml";

    private boolean successfullyLoaded;

    public Config() {
        try (InputStream inputStream =
                     Objects.requireNonNull(getClass().getClassLoader().getResourceAsStream(defaultConfigFile))) {
            properties.loadFromXML(inputStream);
            this.successfullyLoaded = true;
        } catch (IOException e) {
            LogUtil.error("Configuration file error: " + e);
            LogUtil.warn("Configurations will be loaded by default");
            loadDefaultConfigurations();
            this.successfullyLoaded = false;
        }
    }

    public static ConfigBuilder builder() {
        return new ConfigBuilder();
    }

    /**
     * We removed the Log system configurations in order to not produce errors on platforms.
     * */
    private void loadDefaultConfigurations() {
        properties.setProperty(Keys.LOG_FILE_PATH.getKey(), "logs/business-rules-pricing.log");
        properties.setProperty(Keys.SILENT_MODE_ENABLE.getKey(), "false");
        properties.setProperty(Keys.CLEAR_CACHE_ON_COMPUTATE_ENABLE.getKey(), "true");
        properties.setProperty(Keys.EXPORT_LOG_HISTORY_FILE.getKey(), "logs/price-rule-component-history.log");
        properties.setProperty(Keys.OUTCOME_FORCE_ZERO_IF_NEGATIVE.getKey(), "false");
        properties.setProperty(Keys.GIVE_THE_GREATER_DISCOUNT.getKey(), "true");
        properties.setProperty(Keys.GIVE_THE_GREATER_SURCHARGE.getKey(), "true");
        properties.setProperty(Keys.GIVE_THE_GREATER_FIXED_PRICE.getKey(), "false");
        properties.setProperty(Keys.GIVE_THE_GREATER_BONUS_PRODUCT.getKey(), "true");
        properties.setProperty(Keys.GIVE_THE_GREATER_SURCHARGE_PRODUCT.getKey(), "true");
    }

    public void loadConfigurations(String path) {
        try {
            Properties mainProperties = new Properties();
            if (Objects.nonNull(path)) {
                try (InputStream inputStream = new FileInputStream(path)) {
                    mainProperties.loadFromXML(inputStream);
                    this.successfullyLoaded = true;
                }
            }
            properties.putAll(mainProperties); // override defaults
            LogUtil.log(PriceRuleLog.createInfoConfigurationsLoaded(path));
        } catch (IOException e) {
            LogUtil.error("Configuration file error: " + e);
            this.successfullyLoaded = false;
        }
    }

    public boolean hasKey(ConfigKey key) {
        return properties.containsKey(key.getKey());
    }

    public String getString(ConfigKey key) {
        return properties.getProperty(key.getKey());
    }

    public String getString(ConfigKey key, String defaultValue) {
        return hasKey(key) ? getString(key) : defaultValue;
    }

    public boolean getBoolean(ConfigKey key) {
        return Boolean.parseBoolean(getString(key));
    }

    public int getInteger(ConfigKey key, int defaultValue) {
        return hasKey(key) ? Integer.parseInt(getString(key)) : defaultValue;
    }

    public void setBoolean(ConfigKey key, boolean value) {
        properties.setProperty(key.getKey(), String.valueOf(value));
    }

    public void setString(ConfigKey key, String value) {
        properties.setProperty(key.getKey(), value);
    }

    public Properties getProperties() {
        return this.properties;
    }

    public String getDefaultConfigFile() {
        return this.defaultConfigFile;
    }

    public boolean isSuccessfullyLoaded() {
        return this.successfullyLoaded;
    }

    public void setSuccessfullyLoaded(boolean successfullyLoaded) {
        this.successfullyLoaded = successfullyLoaded;
    }

    public boolean equals(final Object o) {
        if (o == this) return true;
        if (!(o instanceof Config)) return false;
        final Config other = (Config) o;
        if (!other.canEqual((Object) this)) return false;
        final Object this$properties = this.getProperties();
        final Object other$properties = other.getProperties();
        if (this$properties == null ? other$properties != null : !this$properties.equals(other$properties))
            return false;
        final Object this$defaultConfigFile = this.getDefaultConfigFile();
        final Object other$defaultConfigFile = other.getDefaultConfigFile();
        if (this$defaultConfigFile == null ? other$defaultConfigFile != null : !this$defaultConfigFile.equals(other$defaultConfigFile))
            return false;
        if (this.isSuccessfullyLoaded() != other.isSuccessfullyLoaded()) return false;
        return true;
    }

    protected boolean canEqual(final Object other) {
        return other instanceof Config;
    }

    public int hashCode() {
        final int PRIME = 59;
        int result = 1;
        final Object $properties = this.getProperties();
        result = result * PRIME + ($properties == null ? 43 : $properties.hashCode());
        final Object $defaultConfigFile = this.getDefaultConfigFile();
        result = result * PRIME + ($defaultConfigFile == null ? 43 : $defaultConfigFile.hashCode());
        result = result * PRIME + (this.isSuccessfullyLoaded() ? 79 : 97);
        return result;
    }

    public String toString() {
        return "Config(properties=" + this.getProperties() + ", defaultConfigFile=" + this.getDefaultConfigFile() + ", successfullyLoaded=" + this.isSuccessfullyLoaded() + ")";
    }

    public static class ConfigBuilder {
        ConfigBuilder() {
        }

        public Config build() {
            return new Config();
        }

        public String toString() {
            return "Config.ConfigBuilder()";
        }
    }
}
