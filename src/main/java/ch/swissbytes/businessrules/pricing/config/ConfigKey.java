package ch.swissbytes.businessrules.pricing.config;


public class ConfigKey {
    private final String key;
    private final Class clazz;

    public ConfigKey(String key, Class clazz) {
        this.key = key;
        this.clazz = clazz;
    }

    public static ConfigKeyBuilder builder() {
        return new ConfigKeyBuilder();
    }

    public String getKey() {
        return this.key;
    }

    public Class getClazz() {
        return this.clazz;
    }

    public static class ConfigKeyBuilder {
        private String key;
        private Class clazz;

        ConfigKeyBuilder() {
        }

        public ConfigKey.ConfigKeyBuilder key(String key) {
            this.key = key;
            return this;
        }

        public ConfigKey.ConfigKeyBuilder clazz(Class clazz) {
            this.clazz = clazz;
            return this;
        }

        public ConfigKey build() {
            return new ConfigKey(key, clazz);
        }

        public String toString() {
            return "ConfigKey.ConfigKeyBuilder(key=" + this.key + ", clazz=" + this.clazz + ")";
        }
    }
}
