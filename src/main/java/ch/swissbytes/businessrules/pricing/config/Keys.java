package ch.swissbytes.businessrules.pricing.config;


public final class Keys {

    /**
     * Sets the path for the default config file.
     */
    public static final ConfigKey CONFIG_DEFAULT_PATH = ConfigKey.builder()
            .key("config.default")
            .clazz(String.class)
            .build();

    /**
     * Sets the silent Mode in all engine.
     */
    public static final ConfigKey SILENT_MODE_ENABLE = ConfigKey.builder()
            .key("logger.silentMode")
            .clazz(Boolean.class)
            .build();

    /**
     * Sets if the logs should go to the console output.
     */
    public static ConfigKey LOG_CONSOLE_ENABLE = ConfigKey.builder()
            .key("logger.console")
            .clazz(Boolean.class)
            .build();
    /**
     * Sets the path for the log file.
     */
    public static ConfigKey LOG_FILE_PATH = ConfigKey.builder()
            .key("logger.file")
            .clazz(Boolean.class)
            .build();

    /**
     * Sets the log Level
     */
    public static ConfigKey LOG_LEVEL = ConfigKey.builder()
            .key("logger.level")
            .clazz(Boolean.class)
            .build();

    public static ConfigKey LOG_FULL_STACK_TRACE_ENABLE = ConfigKey.builder()
            .key("logger.fullStackTraces")
            .clazz(Boolean.class)
            .build();

    public static ConfigKey LOG_ROTATE_ENABLE = ConfigKey.builder()
            .key("logger.rotate")
            .clazz(Boolean.class)
            .build();

    /**
     * Clear Cache in every computate method
     **/
    public static ConfigKey CLEAR_CACHE_ON_COMPUTATE_ENABLE = ConfigKey.builder()
            .key("priceRuleComponent.clearCache")
            .clazz(Boolean.class)
            .build();

    /***
     * Exports the current memory logs to a file.
     **/
    public static final ConfigKey EXPORT_LOG_HISTORY_FILE = ConfigKey.builder()
            .key("priceRuleLog.exportFile")
            .clazz(Boolean.class)
            .build();

    /***
     * Exports the current memory logs to a file.
     **/
    public static final ConfigKey OUTCOME_FORCE_ZERO_IF_NEGATIVE = ConfigKey.builder()
            .key("outcome.forceZeroIfNegative")
            .clazz(Boolean.class)
            .build();

    /***
     * when processing the rules, will return the greater discount (percentage or amount).
     **/
    public static final ConfigKey GIVE_THE_GREATER_DISCOUNT = ConfigKey.builder()
            .key("priceComponentRule.giveTheGreaterDiscount")
            .clazz(Boolean.class)
            .build();

    /***
     * when processing the rules, will return the greater surcharge (percentage or amount).
     **/
    public static final ConfigKey GIVE_THE_GREATER_SURCHARGE = ConfigKey.builder()
            .key("priceComponentRule.giveTheGreaterSurcharge")
            .clazz(Boolean.class)
            .build();

    /***
     * when processing the rules, will return the greater fixed price.
     **/
    public static final ConfigKey GIVE_THE_GREATER_FIXED_PRICE = ConfigKey.builder()
            .key("priceComponentRule.giveTheGreaterFixedPrice")
            .clazz(Boolean.class)
            .build();

    /***
     * when processing the rules, will return the greater price of the bonus products.
     **/
    public static final ConfigKey GIVE_THE_GREATER_BONUS_PRODUCT = ConfigKey.builder()
            .key("priceComponentRule.giveTheGreaterBonusProduct")
            .clazz(Boolean.class)
            .build();

    /***
     * when processing the rules, will return the greater price of the surcharge products.
     **/
    public static final ConfigKey GIVE_THE_GREATER_SURCHARGE_PRODUCT = ConfigKey.builder()
            .key("priceComponentRule.giveTheGreaterSurchargeProduct")
            .clazz(Boolean.class)
            .build();

    public Keys() {
    }

    public static KeysBuilder builder() {
        return new KeysBuilder();
    }

    public static class KeysBuilder {
        KeysBuilder() {
        }

        public Keys build() {
            return new Keys();
        }

        public String toString() {
            return "Keys.KeysBuilder()";
        }
    }
}
