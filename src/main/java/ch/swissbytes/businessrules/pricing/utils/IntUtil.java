package ch.swissbytes.businessrules.pricing.utils;

import java.util.UUID;


public class IntUtil {

    public static boolean between(int base, int low, int high){
        if(high == 0 && low==0)
            return true;
        if(high > 0)
            return base >= low && base <= high;
        else
            return base >= low;
    }

    public static long getRandomIdLong() {
        return Math.abs(UUID.randomUUID().hashCode());
    }

    public static String getRandomId() {
        return UUID.randomUUID().toString();
    }

    public static int getRandomIdInt(){
        return UUID.randomUUID().clockSequence();
    }
}
