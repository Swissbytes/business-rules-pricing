package ch.swissbytes.businessrules.pricing.utils;


public enum PriceRuleLogType {
    INFO,
    WARN,
    ERROR
}
