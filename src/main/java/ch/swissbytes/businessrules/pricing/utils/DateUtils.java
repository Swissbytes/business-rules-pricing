package ch.swissbytes.businessrules.pricing.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


public class DateUtils {
    public static final String FORMAT_DATE = "dd/MM/yyyy";
    private static final String FORMAT_DATETIME = "dd/MM/yyyy HH:mm:ss";

    public static Date fromStringToDate(String mmddyy){
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        Date date = new Date();
        try {
            date = df.parse(mmddyy);
            String newDateString = df.format(date);
        } catch (ParseException e) {
        }
        return date;
    }

    public static Date toShortDate(String dateString) throws ParseException {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(FORMAT_DATE);
        return simpleDateFormat.parse(dateString);
    }

    public static Date toLongDate(String dateString) throws ParseException {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(FORMAT_DATETIME);
        return simpleDateFormat.parse(dateString);
    }

    public static String toLongDateString(Date date) {
        if (date == null) {
            return "";
        }
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(FORMAT_DATETIME);
        return simpleDateFormat.format(date);
    }

    public static String toShortDateString(Date date) {
        if (date == null) {
            return "";
        }
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(FORMAT_DATE);
        return simpleDateFormat.format(date);
    }

}
