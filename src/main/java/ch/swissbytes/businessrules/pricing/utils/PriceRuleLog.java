package ch.swissbytes.businessrules.pricing.utils;

import ch.swissbytes.businessrules.pricing.enums.PriceComponentOutcomeTypeEnum;
import ch.swissbytes.businessrules.pricing.enums.ScaleTypeEnum;
import ch.swissbytes.businessrules.pricing.inputs.PriceComponentRuleInputSaleOrder;
import ch.swissbytes.businessrules.pricing.inputs.PriceComponentRuleInputSaleOrderProduct;
import ch.swissbytes.businessrules.pricing.models.PriceRule;

import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;


public class PriceRuleLog {

    private String date;
    private PriceRuleLogType type;
    private String detail;

    public PriceRuleLog(String date, PriceRuleLogType type, String detail) {
        this.date = date;
        this.type = type;
        this.detail = detail;
    }

    public PriceRuleLog() {
    }

    public static PriceRuleLog createInputSaleOrder(PriceComponentRuleInputSaleOrder saleOrder) {
        return PriceRuleLog.builder()
                .date(DateUtils.toLongDateString(new Date()))
                .type(PriceRuleLogType.INFO)
                .detail(BundleProvider.getString("logRegisterSaleOrder",
                        StringUtil.addJumpLineAtStart(saleOrder.toJson())))
                .build();
    }

    public static PriceRuleLog createRule(PriceRule rule) {
        return PriceRuleLog.builder()
                .date(DateUtils.toLongDateString(new Date()))
                .type(PriceRuleLogType.INFO)
                .detail(BundleProvider.getString("logRegisterRule",
                        StringUtil.addJumpLineAtStart(rule.toJson())))
                .build();
    }

    public static PriceRuleLog createBeginComputate(List<PriceRule> rules) {
        return PriceRuleLog.builder()
                .date(DateUtils.toLongDateString(new Date()))
                .type(PriceRuleLogType.INFO)
                .detail(BundleProvider.getString("logBeginComputate",
                        StringUtil.addJumpLineAtStart(ToJsonString.toJson(rules))))
                .build();
    }

    public static PriceRuleLog createWrittenBy() {
        return PriceRuleLog.builder()
                .date(DateUtils.toLongDateString(new Date()))
                .type(PriceRuleLogType.INFO)
                .detail(BundleProvider.getString("logWrittenBy"))
                .build();
    }

    public static PriceRuleLog createApplicableRules(List<PriceRule> rules) {
        return PriceRuleLog.builder()
                .date(DateUtils.toLongDateString(new Date()))
                .type(PriceRuleLogType.INFO)
                .detail(BundleProvider.getString("logApplicableRules",
                        StringUtil.addJumpLineAtStart(ToJsonString.toJson(rules.stream().map(
                                priceRule -> "id: " + priceRule.getId() + ", name: " + priceRule.getName()
                        ).collect(Collectors.toList())))))
                .build();
    }

    public static PriceRuleLog createBestRuleFound(PriceRule rule) {
        return PriceRuleLog.builder()
                .date(DateUtils.toLongDateString(new Date()))
                .type(PriceRuleLogType.WARN)
                .detail(BundleProvider.getString("logBestRuleFound",
                        StringUtil.addJumpLineAtStart(rule.getName())))
                .build();
    }

    public static PriceRuleLog createFoundBestRulesByBestValue(List<PriceRule> rules) {
        if (Objects.isNull(rules) || rules.isEmpty()) {
            return PriceRuleLog.builder()
                    .date(DateUtils.toLongDateString(new Date()))
                    .type(PriceRuleLogType.WARN)
                    .detail(BundleProvider.getString("logNoFoundBestRulesByBestValue"))
                    .build();
        }
        return PriceRuleLog.builder()
                .date(DateUtils.toLongDateString(new Date()))
                .type(PriceRuleLogType.INFO)
                .detail(BundleProvider.getString("logFoundBestRulesByBestValue",
                        StringUtil.addJumpLineAtStart(ToJsonString.toJson(rules.stream().map(
                                priceRule -> "id: " + priceRule.getId()
                                        + ", name: " + priceRule.getName()
                                        + ", validationType: " + priceRule.getValidationType()).collect(Collectors.toList())))))
                .build();
    }

    public static PriceRuleLog createFoundBestRulesByBestValueProduct(List<PriceRule> rules) {
        if (Objects.isNull(rules) || rules.isEmpty()) {
            return PriceRuleLog.builder()
                    .date(DateUtils.toLongDateString(new Date()))
                    .type(PriceRuleLogType.WARN)
                    .detail(BundleProvider.getString("logNoFoundBestRulesByBestValueProduct"))
                    .build();
        }
        return PriceRuleLog.builder()
                .date(DateUtils.toLongDateString(new Date()))
                .type(PriceRuleLogType.INFO)
                .detail(BundleProvider.getString("logFoundBestRulesByBestValueProduct",
                        StringUtil.addJumpLineAtStart(ToJsonString.toJson(rules.stream().map(
                                priceRule -> "id: " + priceRule.getId()
                                        + ", name: " + priceRule.getName()
                                        + ", validationType: " + priceRule.getValidationType()).collect(Collectors.toList())))))
                .build();
    }

    public static PriceRuleLog createFoundBestRulesByBestValueProduct(HashMap<PriceComponentRuleInputSaleOrderProduct, List<PriceRule>> bestRulesByProductFound) {
        if (Objects.isNull(bestRulesByProductFound) || bestRulesByProductFound.isEmpty()) {
            return PriceRuleLog.builder()
                    .date(DateUtils.toLongDateString(new Date()))
                    .type(PriceRuleLogType.WARN)
                    .detail(BundleProvider.getString("logNoFoundBestRulesByBestValueProduct"))
                    .build();
        }
        AtomicReference<String> values = new AtomicReference<>("");
        bestRulesByProductFound.forEach((product, priceRules) -> {
            priceRules.removeAll(Collections.singleton(null));
            values.set(values.get() + StringUtil.addJumpLineAtStart("Product Id: "+product.getProduct().getId() + " -> "+
                    ToJsonString.toJson(priceRules.stream().map(
                    priceRule -> "id: " + priceRule.getId()
                            + ", name: " + priceRule.getName()
                            + ", validationType: " + priceRule.getValidationType())
                            .collect(Collectors.toList()))));
        });
        return PriceRuleLog.builder()
                .date(DateUtils.toLongDateString(new Date()))
                .type(PriceRuleLogType.INFO)
                .detail(BundleProvider.getString("logFoundBestRulesByBestValueProduct", values.get()))
                .build();
    }


    public static PriceRuleLog createFoundBestRule(PriceComponentOutcomeTypeEnum outcomeType, PriceRule discountPercentageRule, BigDecimal value) {
        return PriceRuleLog.builder()
                .date(DateUtils.toLongDateString(new Date()))
                .type(PriceRuleLogType.INFO)
                .detail(BundleProvider.getString("logFoundBestRuleByOutcomeType", outcomeType, "id: " + discountPercentageRule.getId() +
                        ", name: " + discountPercentageRule.getName(), value))
                .build();
    }

    public static PriceRuleLog createFoundBestDsctPercRule(PriceRule discountPercentageRule) {
        return PriceRuleLog.builder()
                .date(DateUtils.toLongDateString(new Date()))
                .type(PriceRuleLogType.INFO)
                .detail(BundleProvider.getString("logFoundBestDsctPercRule",
                        StringUtil.addJumpLineAtStart("id: " + discountPercentageRule.getId() +
                                ", name: " + discountPercentageRule.getName())))
                .build();
    }

    public static PriceRuleLog createFoundBestDsctPercRuleAccumulative(PriceRule discountPercentageRule) {
        return PriceRuleLog.builder()
                .date(DateUtils.toLongDateString(new Date()))
                .type(PriceRuleLogType.INFO)
                .detail(BundleProvider.getString("logFoundBestDsctPercRuleAccumulative", discountPercentageRule.getId(), discountPercentageRule.getName()))
                .build();
    }

    public static PriceRuleLog createFoundBestDsctAmountRule(PriceRule discountAmountRule) {
        return PriceRuleLog.builder()
                .date(DateUtils.toLongDateString(new Date()))
                .type(PriceRuleLogType.INFO)
                .detail(BundleProvider.getString("logFoundBestDsctAmountRule",
                        StringUtil.addJumpLineAtStart(discountAmountRule.toJson())))
                .build();

    }

    public static PriceRuleLog createFoundBestSurchargeAmountRule(PriceRule surchargePercRule) {
        return PriceRuleLog.builder()
                .date(DateUtils.toLongDateString(new Date()))
                .type(PriceRuleLogType.INFO)
                .detail(BundleProvider.getString("logFoundBestSurchargeAmountRule",
                        StringUtil.addJumpLineAtStart(surchargePercRule.toJson())))
                .build();
    }

    public static PriceRuleLog createFoundBestSurchargePercRule(PriceRule surchargePercRule) {
        return PriceRuleLog.builder()
                .date(DateUtils.toLongDateString(new Date()))
                .type(PriceRuleLogType.INFO)
                .detail(BundleProvider.getString("logFoundBestSurchargePercRule",
                        StringUtil.addJumpLineAtStart(surchargePercRule.toJson())))
                .build();
    }

    public static PriceRuleLog createFoundBestBonificationRule(PriceRule bonificationRule) {
        return PriceRuleLog.builder()
                .date(DateUtils.toLongDateString(new Date()))
                .type(PriceRuleLogType.INFO)
                .detail(BundleProvider.getString("logFoundBestBonificationRule",
                        StringUtil.addJumpLineAtStart(bonificationRule.toJson())))
                .build();
    }

    public static PriceRuleLog createFoundBestProductSurchargeRule(PriceRule surchargeRule) {
        return PriceRuleLog.builder()
                .date(DateUtils.toLongDateString(new Date()))
                .type(PriceRuleLogType.INFO)
                .detail(BundleProvider.getString("logFoundBestProductSurchargeRule",
                        StringUtil.addJumpLineAtStart(surchargeRule.toJson())))
                .build();
    }

    public static PriceRuleLog createFoundBestFixedPriceRule(PriceRule fixedRule) {
        return PriceRuleLog.builder()
                .date(DateUtils.toLongDateString(new Date()))
                .type(PriceRuleLogType.INFO)
                .detail(BundleProvider.getString("logFoundBestFixedPriceRule",
                        StringUtil.addJumpLineAtStart(fixedRule.toJson())))
                .build();
    }

    public static PriceRuleLog createExporLogHistoryFileNotFound() {
        return PriceRuleLog.builder()
                .date(DateUtils.toLongDateString(new Date()))
                .type(PriceRuleLogType.ERROR)
                .detail(BundleProvider.getString("logExporLogHistoryFileNotFound"))
                .build();
    }

    //region Rule Validations
    public static PriceRuleLog createDeliveryModeRuleSuccess() {
        return PriceRuleLog.builder()
                .date(DateUtils.toLongDateString(new Date()))
                .type(PriceRuleLogType.INFO)
                .detail(BundleProvider.getString("logDeliveryModeRuleSuccess"))
                .build();
    }

    public static PriceRuleLog createDeliveryModeRuleFail() {
        return PriceRuleLog.builder()
                .date(DateUtils.toLongDateString(new Date()))
                .type(PriceRuleLogType.ERROR)
                .detail(BundleProvider.getString("logDeliveryModeRuleFail"))
                .build();
    }

    public static PriceRuleLog createPaymentConditionRuleFail() {
        return PriceRuleLog.builder()
                .date(DateUtils.toLongDateString(new Date()))
                .type(PriceRuleLogType.ERROR)
                .detail(BundleProvider.getString("logPaymentConditionRuleFail"))
                .build();
    }

    public static PriceRuleLog createPaymentConditionRuleSuccess() {
        return PriceRuleLog.builder()
                .date(DateUtils.toLongDateString(new Date()))
                .type(PriceRuleLogType.INFO)
                .detail(BundleProvider.getString("logPaymentConditionRuleSuccess"))
                .build();
    }

    public static PriceRuleLog createFromThruTotalAmountRuleSuccess() {
        return PriceRuleLog.builder()
                .date(DateUtils.toLongDateString(new Date()))
                .type(PriceRuleLogType.INFO)
                .detail(BundleProvider.getString("logFromThruTotalAmountRuleSuccess"))
                .build();
    }

    public static PriceRuleLog createFromThruTotalAmountRuleFail() {
        return PriceRuleLog.builder()
                .date(DateUtils.toLongDateString(new Date()))
                .type(PriceRuleLogType.ERROR)
                .detail(BundleProvider.getString("logFromThruTotalAmountRuleFail"))
                .build();
    }

    public static PriceRuleLog createBusinessUnitRuleSuccess() {
        return PriceRuleLog.builder()
                .date(DateUtils.toLongDateString(new Date()))
                .type(PriceRuleLogType.INFO)
                .detail(BundleProvider.getString("logBusinessUnitRuleSuccess"))
                .build();
    }

    public static PriceRuleLog createBusinessUnitRuleFail() {
        return PriceRuleLog.builder()
                .date(DateUtils.toLongDateString(new Date()))
                .type(PriceRuleLogType.ERROR)
                .detail(BundleProvider.getString("logBusinessUnitRuleFail"))
                .build();
    }

    public static PriceRuleLog createAgencyRuleSuccess() {
        return PriceRuleLog.builder()
                .date(DateUtils.toLongDateString(new Date()))
                .type(PriceRuleLogType.INFO)
                .detail(BundleProvider.getString("logAgencyRuleSuccess"))
                .build();
    }

    public static PriceRuleLog createAgencyRuleFail() {
        return PriceRuleLog.builder()
                .date(DateUtils.toLongDateString(new Date()))
                .type(PriceRuleLogType.ERROR)
                .detail(BundleProvider.getString("logAgencyRuleFail"))
                .build();
    }

    public static PriceRuleLog createRoleTypeRuleSuccess() {
        return PriceRuleLog.builder()
                .date(DateUtils.toLongDateString(new Date()))
                .type(PriceRuleLogType.INFO)
                .detail(BundleProvider.getString("logRoleTypeRuleSuccess"))
                .build();
    }

    public static PriceRuleLog createRoleTypeRuleFail() {
        return PriceRuleLog.builder()
                .date(DateUtils.toLongDateString(new Date()))
                .type(PriceRuleLogType.ERROR)
                .detail(BundleProvider.getString("logRoleTypeRuleFail"))
                .build();
    }

    public static PriceRuleLog createCustomerRuleSuccess() {
        return PriceRuleLog.builder()
                .date(DateUtils.toLongDateString(new Date()))
                .type(PriceRuleLogType.INFO)
                .detail(BundleProvider.getString("logCustomerRuleSuccess"))
                .build();
    }

    public static PriceRuleLog createCustomerRuleFail() {
        return PriceRuleLog.builder()
                .date(DateUtils.toLongDateString(new Date()))
                .type(PriceRuleLogType.ERROR)
                .detail(BundleProvider.getString("logCustomerRuleFail"))
                .build();
    }

    public static PriceRuleLog createPartyClassificationRuleSuccess() {
        return PriceRuleLog.builder()
                .date(DateUtils.toLongDateString(new Date()))
                .type(PriceRuleLogType.INFO)
                .detail(BundleProvider.getString("logPartyClassificationRuleSuccess"))
                .build();
    }

    public static PriceRuleLog createPartyClassificationRuleFail() {
        return PriceRuleLog.builder()
                .date(DateUtils.toLongDateString(new Date()))
                .type(PriceRuleLogType.ERROR)
                .detail(BundleProvider.getString("logPartyClassificationRuleFail"))
                .build();
    }

    public static PriceRuleLog createGeographicBoundaryRuleSuccess() {
        return PriceRuleLog.builder()
                .date(DateUtils.toLongDateString(new Date()))
                .type(PriceRuleLogType.INFO)
                .detail(BundleProvider.getString("logGeographicBoundaryRuleSuccess"))
                .build();
    }

    public static PriceRuleLog createGeographicBoundaryRuleFail() {
        return PriceRuleLog.builder()
                .date(DateUtils.toLongDateString(new Date()))
                .type(PriceRuleLogType.ERROR)
                .detail(BundleProvider.getString("logGeographicBoundaryRuleFail"))
                .build();
    }

    public static PriceRuleLog createSaleOrderOriginRuleSuccess() {
        return PriceRuleLog.builder()
                .date(DateUtils.toLongDateString(new Date()))
                .type(PriceRuleLogType.INFO)
                .detail(BundleProvider.getString("logSaleOrderOriginRuleSuccess"))
                .build();
    }

    public static PriceRuleLog createSaleOrderOriginRuleFail() {
        return PriceRuleLog.builder()
                .date(DateUtils.toLongDateString(new Date()))
                .type(PriceRuleLogType.ERROR)
                .detail(BundleProvider.getString("logSaleOrderOriginRuleRuleFail"))
                .build();
    }

    public static PriceRuleLog createPricelistRuleSuccess() {
        return PriceRuleLog.builder()
                .date(DateUtils.toLongDateString(new Date()))
                .type(PriceRuleLogType.INFO)
                .detail(BundleProvider.getString("logPricelistRuleSuccess"))
                .build();
    }

    public static PriceRuleLog createPricelistRuleFail() {
        return PriceRuleLog.builder()
                .date(DateUtils.toLongDateString(new Date()))
                .type(PriceRuleLogType.ERROR)
                .detail(BundleProvider.getString("logPricelistRuleFail"))
                .build();
    }

    public static PriceRuleLog createProductRuleSuccess(boolean isRestricted) {
        return PriceRuleLog.builder()
                .date(DateUtils.toLongDateString(new Date()))
                .type(PriceRuleLogType.INFO)
                .detail(isRestricted ?
                        BundleProvider.getString("logProductRuleRestrictedSuccess") :
                        BundleProvider.getString("logProductRuleGeneralSuccess"))
                .build();
    }

    public static PriceRuleLog createProductRuleFail(boolean isRestricted) {
        return PriceRuleLog.builder()
                .date(DateUtils.toLongDateString(new Date()))
                .type(PriceRuleLogType.ERROR)
                .detail(isRestricted ?
                        BundleProvider.getString("logProductRuleRestrictedFail") :
                        BundleProvider.getString("logProductRuleGeneralFail"))
                .build();
    }

    public static PriceRuleLog createProductFeatureRuleSuccess(boolean isRestricted) {
        return PriceRuleLog.builder()
                .date(DateUtils.toLongDateString(new Date()))
                .type(PriceRuleLogType.INFO)
                .detail(isRestricted ?
                        BundleProvider.getString("logProductFeatureRuleRestrictedSuccess") :
                        BundleProvider.getString("logProductFeatureRuleGeneralSuccess"))
                .build();
    }

    public static PriceRuleLog createProductFeatureRuleFail(boolean isRestricted) {
        return PriceRuleLog.builder()
                .date(DateUtils.toLongDateString(new Date()))
                .type(PriceRuleLogType.ERROR)
                .detail(isRestricted ?
                        BundleProvider.getString("logProductFeatureRuleRestrictedFail") :
                        BundleProvider.getString("logProductFeatureRuleGeneralFail"))
                .build();
    }

    public static PriceRuleLog createProductClassificationRuleSuccess(boolean isRestricted) {
        return PriceRuleLog.builder()
                .date(DateUtils.toLongDateString(new Date()))
                .type(PriceRuleLogType.INFO)
                .detail(isRestricted ?
                        BundleProvider.getString("logProductClassificationRuleRestrictedSuccess") :
                        BundleProvider.getString("logProductClassificationRuleGeneralSuccess"))
                .build();
    }

    public static PriceRuleLog createProductClassificationRuleFail(boolean isRestricted) {
        return PriceRuleLog.builder()
                .date(DateUtils.toLongDateString(new Date()))
                .type(PriceRuleLogType.ERROR)
                .detail(isRestricted ?
                        BundleProvider.getString("logProductClassificationRuleRestrictedFail") :
                        BundleProvider.getString("logProductClassificationRuleGeneralFail"))
                .build();
    }

    public static PriceRuleLog createRuleFailCheckScale(ScaleTypeEnum type) {
        switch (type) {
            case QUANTITY:
                return PriceRuleLog.builder()
                        .date(DateUtils.toLongDateString(new Date()))
                        .type(PriceRuleLogType.ERROR)
                        .detail(BundleProvider.getString("logRuleFailCheckScaleQty"))
                        .build();
            case AMOUNT:
            default:
                return PriceRuleLog.builder()
                        .date(DateUtils.toLongDateString(new Date()))
                        .type(PriceRuleLogType.ERROR)
                        .detail(BundleProvider.getString("logRuleFailCheckScaleAmount"))
                        .build();
        }
    }

    public static PriceRuleLog createRuleFailCheckFrequency(ScaleTypeEnum type) {
        switch (type) {
            case QUANTITY:
                return PriceRuleLog.builder()
                        .date(DateUtils.toLongDateString(new Date()))
                        .type(PriceRuleLogType.ERROR)
                        .detail(BundleProvider.getString("logRuleFailCheckFrequencyQty"))
                        .build();
            case AMOUNT:
            default:
                return PriceRuleLog.builder()
                        .date(DateUtils.toLongDateString(new Date()))
                        .type(PriceRuleLogType.ERROR)
                        .detail(BundleProvider.getString("logRuleFailCheckFrequencyAmount"))
                        .build();
        }
    }
    //endregion

    public static PriceRuleLog createRuleValidation(PriceRule priceRule) {
        return PriceRuleLog.builder()
                .date(DateUtils.toLongDateString(new Date()))
                .type(PriceRuleLogType.INFO)
                .detail(BundleProvider.getString("logRuleValidation",
                        priceRule.getId(), priceRule.getName(), priceRule.getRuleType().name()))
                .build();
    }

    public static PriceRuleLog createPriceRuleValid(PriceRule priceRule) {
        return PriceRuleLog.builder()
                .date(DateUtils.toLongDateString(new Date()))
                .type(PriceRuleLogType.INFO)
                .detail(BundleProvider.getString("logRuleIsValid", priceRule.getId(), priceRule.getName()))
                .build();
    }

    public static PriceRuleLog createPriceRuleFail(PriceRule priceRule) {
        return PriceRuleLog.builder()
                .date(DateUtils.toLongDateString(new Date()))
                .type(PriceRuleLogType.ERROR)
                .detail(BundleProvider.getString("logRuleIsNotValid", priceRule.getId(), priceRule.getName()))
                .build();
    }

    public static PriceRuleLog createFrequencyInvalid() {
        return PriceRuleLog.builder()
                .date(DateUtils.toLongDateString(new Date()))
                .type(PriceRuleLogType.ERROR)
                .detail(BundleProvider.getString("logFrequencyIsNotDefined"))
                .build();
    }

    public static PriceRuleLog createScalesInvalid() {
        return PriceRuleLog.builder()
                .date(DateUtils.toLongDateString(new Date()))
                .type(PriceRuleLogType.ERROR)
                .detail(BundleProvider.getString("logScalesAreNotDefined"))
                .build();
    }

    public static PriceRuleLog createProductClassificationTotalZero(ScaleTypeEnum scaleType) {
        switch (scaleType) {
            case QUANTITY:
                return PriceRuleLog.builder()
                        .date(DateUtils.toLongDateString(new Date()))
                        .type(PriceRuleLogType.WARN)
                        .detail(BundleProvider.getString("logProductClassificationSumZeroQty"))
                        .build();
            case AMOUNT:
            default:
                return PriceRuleLog.builder()
                        .date(DateUtils.toLongDateString(new Date()))
                        .type(PriceRuleLogType.WARN)
                        .detail(BundleProvider.getString("logProductClassificationSumZeroAmount"))
                        .build();
        }
    }

    public static PriceRuleLog createProductFeaturesTotalZero(ScaleTypeEnum scaleType) {
        switch (scaleType) {
            case QUANTITY:
                return PriceRuleLog.builder()
                        .date(DateUtils.toLongDateString(new Date()))
                        .type(PriceRuleLogType.WARN)
                        .detail(BundleProvider.getString("logProductFeaturesSumZeroQty"))
                        .build();
            case AMOUNT:
            default:
                return PriceRuleLog.builder()
                        .date(DateUtils.toLongDateString(new Date()))
                        .type(PriceRuleLogType.WARN)
                        .detail(BundleProvider.getString("logProductFeaturesSumZeroAmount"))
                        .build();
        }
    }

    public static PriceRuleLog createProductTotalZero(ScaleTypeEnum scaleType) {
        switch (scaleType) {
            case QUANTITY:
                return PriceRuleLog.builder()
                        .date(DateUtils.toLongDateString(new Date()))
                        .type(PriceRuleLogType.WARN)
                        .detail(BundleProvider.getString("logProductSumZeroQty"))
                        .build();
            case AMOUNT:
            default:
                return PriceRuleLog.builder()
                        .date(DateUtils.toLongDateString(new Date()))
                        .type(PriceRuleLogType.WARN)
                        .detail(BundleProvider.getString("logProductSumZeroAmount"))
                        .build();
        }
    }

    public static PriceRuleLog createFoundAccumulativeRulesAndValues(PriceComponentOutcomeTypeEnum outcomeType, List<PriceRule> rules, BigDecimal value) {
        List<String> rulesIds = new ArrayList<>();
        rules.stream().map(PriceRule::getId).forEach(id -> rulesIds.add(id.toString()));
        return PriceRuleLog.builder()
                .date(DateUtils.toLongDateString(new Date()))
                .type(PriceRuleLogType.INFO)
                .detail(BundleProvider.getString("logFoundAccumulativeRulesAndValues", outcomeType, rulesIds, value))
                .build();
    }

    public static PriceRuleLog createFoundAccumulativeRulesAndValues(PriceComponentOutcomeTypeEnum outcomeType,
                                                                     List<PriceRule> rules, BigDecimal value,
                                                                     PriceComponentRuleInputSaleOrderProduct product) {
        List<String> rulesIds = new ArrayList<>();
        rules.stream().map(PriceRule::getId).forEach(id -> rulesIds.add(id.toString()));
        return PriceRuleLog.builder()
                .date(DateUtils.toLongDateString(new Date()))
                .type(PriceRuleLogType.INFO)
                .detail(BundleProvider.getString("logFoundAccumulativeRulesAndValuesByProduct", outcomeType, rulesIds, value, product.getProduct().getId()))
                .build();
    }

    public static PriceRuleLog createBestAccumulativeRulesAndValues(PriceComponentOutcomeTypeEnum outcomeType, List<PriceRule> rules, BigDecimal value) {
        List<String> rulesIds = new ArrayList<>();
        rules.stream().map(PriceRule::getId).forEach(id -> rulesIds.add(id.toString()));
        return PriceRuleLog.builder()
                .date(DateUtils.toLongDateString(new Date()))
                .type(PriceRuleLogType.INFO)
                .detail(BundleProvider.getString("logBestAccumulativeRulesAndValues", outcomeType, rulesIds, value))
                .build();
    }

    public static PriceRuleLog createWarningNoProductsOnTargetProductRule(PriceRule rule) {
        return PriceRuleLog.builder()
                .date(DateUtils.toLongDateString(new Date()))
                .type(PriceRuleLogType.WARN)
                .detail(BundleProvider.getString("logWarningNoProductsOnTargetProductRule", rule.getId(), rule.getName()))
                .build();
    }

    public static PriceRuleLog createWarningComputateOutcomeIsSmallerThanZero(BigDecimal total, long id, PriceComponentOutcomeTypeEnum outcomeType) {
        return PriceRuleLog.builder()
                .date(DateUtils.toLongDateString(new Date()))
                .type(PriceRuleLogType.WARN)
                .detail(BundleProvider.getString("logWarningComputateOutcomeIsSmallerThanZero", id, outcomeType.name(), total.toPlainString()))
                .build();
    }

    public static PriceRuleLog createWarningFixedPriceTimes() {
        return PriceRuleLog.builder()
                .date(DateUtils.toLongDateString(new Date()))
                .type(PriceRuleLogType.WARN)
                .detail(BundleProvider.getString("logWarningFixedPriceTimes"))
                .build();
    }

    public static PriceRuleLog createInfoConfigurationsLoaded(String path) {
        return PriceRuleLog.builder()
                .date(DateUtils.toLongDateString(new Date()))
                .type(PriceRuleLogType.INFO)
                .detail(BundleProvider.getString("logInfoConfigurationsLoaded", path))
                .build();
    }

    public static PriceRuleLogBuilder builder() {
        return new PriceRuleLogBuilder();
    }

    @Override
    public String toString() {
        return date +
                "[" + type +
                "] - " + detail;
    }

    public String getDate() {
        return this.date;
    }

    public PriceRuleLogType getType() {
        return this.type;
    }

    public String getDetail() {
        return this.detail;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setType(PriceRuleLogType type) {
        this.type = type;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public boolean equals(final Object o) {
        if (o == this) return true;
        if (!(o instanceof PriceRuleLog)) return false;
        final PriceRuleLog other = (PriceRuleLog) o;
        if (!other.canEqual((Object) this)) return false;
        final Object this$date = this.getDate();
        final Object other$date = other.getDate();
        if (this$date == null ? other$date != null : !this$date.equals(other$date)) return false;
        final Object this$type = this.getType();
        final Object other$type = other.getType();
        if (this$type == null ? other$type != null : !this$type.equals(other$type)) return false;
        final Object this$detail = this.getDetail();
        final Object other$detail = other.getDetail();
        if (this$detail == null ? other$detail != null : !this$detail.equals(other$detail)) return false;
        return true;
    }

    protected boolean canEqual(final Object other) {
        return other instanceof PriceRuleLog;
    }

    public int hashCode() {
        final int PRIME = 59;
        int result = 1;
        final Object $date = this.getDate();
        result = result * PRIME + ($date == null ? 43 : $date.hashCode());
        final Object $type = this.getType();
        result = result * PRIME + ($type == null ? 43 : $type.hashCode());
        final Object $detail = this.getDetail();
        result = result * PRIME + ($detail == null ? 43 : $detail.hashCode());
        return result;
    }

    public static class PriceRuleLogBuilder {
        private String date;
        private PriceRuleLogType type;
        private String detail;

        PriceRuleLogBuilder() {
        }

        public PriceRuleLog.PriceRuleLogBuilder date(String date) {
            this.date = date;
            return this;
        }

        public PriceRuleLog.PriceRuleLogBuilder type(PriceRuleLogType type) {
            this.type = type;
            return this;
        }

        public PriceRuleLog.PriceRuleLogBuilder detail(String detail) {
            this.detail = detail;
            return this;
        }

        public PriceRuleLog build() {
            return new PriceRuleLog(date, type, detail);
        }

        public String toString() {
            return "PriceRuleLog.PriceRuleLogBuilder(date=" + this.date + ", type=" + this.type + ", detail=" + this.detail + ")";
        }
    }
}
