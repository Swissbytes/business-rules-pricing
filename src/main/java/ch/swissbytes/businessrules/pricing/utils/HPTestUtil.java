package ch.swissbytes.businessrules.pricing.utils;

import ch.swissbytes.businessrules.pricing.enums.*;
import ch.swissbytes.businessrules.pricing.factors.*;
import ch.swissbytes.businessrules.pricing.inputs.PriceComponentRuleInputSaleOrder;
import ch.swissbytes.businessrules.pricing.inputs.PriceComponentRuleInputSaleOrderProduct;
import ch.swissbytes.businessrules.pricing.models.PriceRule;
import ch.swissbytes.businessrules.pricing.outcomes.PriceComponentRuleOutcomeOutput;
import ch.swissbytes.businessrules.pricing.subjects.PriceComponentRuleSubjectProduct;
import ch.swissbytes.businessrules.pricing.subjects.PriceComponentRuleSubjectProductFeature;
import ch.swissbytes.businessrules.pricing.subjects.PriceComponentRuleUnitMeasure;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Rules Ids Range: 1-99
 * Customers ids range: 100-199
 * Business Unit Ids range: 200-299
 * Agencies Ids range: 300-399
 * Product Features Ids range: 400-499
 * Product Classification Ids range: 500-599
 * Party Classification Ids range: 600-699
 * Outcome Ids range: 700-799
 * Geographic Boundary Ids range: 800-899
 * Role Types Ids range: 900-999
 * Products Ids range: 1000-1999
 * Sale Order Product Details Ids range: 2000-2999
 * Sale Order Ids range: 3000-3099
 * Delivery Modes: 3100 - 3199
 * Output Outcomes: 3200 - 3299
 * Outcomes: 3300 - 3399
 * Scales: 3400-3499
 */
public class HPTestUtil {

    //region Price Rules
    public static PriceRule createRuleOstomizadoRetailCustomers(long id,
                                                                PriceRuleTypeEnum type,
                                                                ScaleTypeEnum validationType,
                                                                OutComeModeEnum outcomeMode) {
        return PriceRule.builder()
                .id(id)
                .name("Dscto 10% a Clientes Ostomizados")
                .description("Dscto 10% por la compra de cualquier producto de la marca Hollister a clientes Ostomizados Retail.")
                .businessUnits(Collections.singletonList(createBusinessUnitRetail()))
                .productClassifications(Collections.singletonList(createProductClassificationOstomia()))
                .partyClassifications(Collections.singletonList(createPartyClassificationOstomia()))
                .output(createOutputDiscountPercentage(new BigDecimal(10)))
                .ruleType(type)
                .validationType(validationType)
                .outComeMode(outcomeMode)
                .build();
    }

    public static PriceRule createRulePublicCreditSale(int id) {
        return PriceRule.builder()
                .id(id)
                .name("Descuento 15% Entidades Publicas a credito")
                .description("Descuento 15% Entidades Publicas a ventas al credito")
                .businessUnits(Collections.singletonList(createBusinessUnitPublicas()))
                .outComeMode(OutComeModeEnum.SINGLE)
                .validationType(ScaleTypeEnum.AMOUNT)
                .output(createOutputDiscountPercentage(new BigDecimal(15)))
                .build();
    }


    /**
     * Business Unit: "Distribuidores"
     * Party Classification: "Distribuidor Exclusivo" / "Distribuidor Autorizado"
     * FromAmount: 1'500.00 $
     * Outcome: Discount Percentage -> 10%
     */
    public static PriceRule createRuleDistribuidoresExclusivos(int id) {
        List<PriceComponentRuleFactorScale> scales = new ArrayList<>(0);
        scales.add(TestUtil.createScale(new BigDecimal(1500), null, ScaleTypeEnum.AMOUNT, TestUtil.createOutcomeDiscountPerc(new BigDecimal(10))));

        return PriceRule.builder()
                .id(id)
                .name("Descuento 10% Distribuidores Exclusivos")
                .description("Descuento 10% Distribuidores Exclusivos")
                .businessUnits(Collections.singletonList(createBusinessUnitDistribuidores()))
                .outComeMode(OutComeModeEnum.SCALE)
                .validationType(ScaleTypeEnum.AMOUNT)
                .output(createOutputScales(scales,
                        PriceComponentOutcomeTypeEnum.DISCOUNT_PERCENTAGE,
                        ScaleTypeEnum.AMOUNT))
                .target(TargetEnum.SALE_ORDER)
                .accumulative(false)
                .useSaleOrderTotalForOutcome(true)
                .build();
    }

    /**
     * Business Unit: "Entidades Privadas"
     * Customer: "Clinica Incor"
     * Outcome: Discount Percentage -> 5%
     * */
    public static PriceRule createRulePrivadasIncor(int id) {
        return PriceRule.builder()
                .id(id)
                .name("Descuento 5% Entidades Privadas / Clinica Incor")
                .description("Descuento 5%  Entidades Privadas / Clinica Incor")
                .businessUnits(Collections.singletonList(createBusinessUnitPrivadas()))
                .customers(Collections.singletonList(createClinicaIncorCustomer(999)))
                .outComeMode(OutComeModeEnum.SINGLE)
                .validationType(ScaleTypeEnum.AMOUNT)
                .output(createOutputDiscountPercentage(new BigDecimal(5)))
                .target(TargetEnum.SALE_ORDER)
                .accumulative(false)
                .useSaleOrderTotalForOutcome(true)
                .build();
    }

    /**
     * Business Unit: "Entidades Privadas"
     * Customer: "Clinica Fionanini"
     * Outcome: Discount Percentage -> 10%
     */
    public static PriceRule createRulePrivadasFioanini(int id) {
        return PriceRule.builder()
                .id(id)
                .name("Descuento 10% Entidades Privadas / Clinica Fionanini")
                .description("Descuento 10%  Entidades Privadas / Clinica Fionanini")
                .businessUnits(Collections.singletonList(createBusinessUnitPrivadas()))
                .customers(Collections.singletonList(createClinicaFioaniniCustomer(1000)))
                .outComeMode(OutComeModeEnum.SINGLE)
                .validationType(ScaleTypeEnum.AMOUNT)
                .output(createOutputDiscountPercentage(new BigDecimal(10)))
                .target(TargetEnum.SALE_ORDER)
                .accumulative(false)
                .useSaleOrderTotalForOutcome(true)
                .build();
    }

    //endregion

    //region Outcomes
    public static PriceComponentRuleOutcomeOutput createOutputDiscountPercentage(BigDecimal value) {
        return TestUtil.createOutcomeOutputDiscountPerc(value, ScaleTypeEnum.AMOUNT);
    }

    public static PriceComponentRuleOutcomeOutput createOutputScales(List<PriceComponentRuleFactorScale> scales, PriceComponentOutcomeTypeEnum type, ScaleTypeEnum validationType) {
        return TestUtil.createOutcomeOutput(22, null, validationType,
                OutComeModeEnum.SCALE,
                null, scales, type);
    }
    //endregion

    //region Party Classification
    public static PriceComponentRuleFactorPartyClassification createPartyClassificationOstomia() {
        return PriceComponentRuleFactorPartyClassification.builder()
                .id(601)
                .name("Clientes con carnet de Ostomia")
                .build();
    }


    public static PriceComponentRuleFactorPartyClassification createPartyClassificationClinicas() {
        return PriceComponentRuleFactorPartyClassification.builder()
                .id(602)
                .name("Clinicas")
                .build();
    }
    //endregion

    //region Product Classification
    public static PriceComponentRuleFactorProductClassification createProductClassificationOstomia() {
        return PriceComponentRuleFactorProductClassification.builder()
                .id(701)
                .name("Producto/Venta/COLOPROCTOLOGIA/OSTOMIA")
                .path("1/2/30/47")
                .build();
    }
    //endregion


    //region Business Units
    public static PriceComponentRuleFactorBusinessUnit createBusinessUnitRetail() {
        return PriceComponentRuleFactorBusinessUnit.builder()
                .id(1)
                .name("Retail")
                .build();
    }

    public static PriceComponentRuleFactorBusinessUnit createBusinessUnitDistribuidores() {
        return PriceComponentRuleFactorBusinessUnit.builder()
                .id(2)
                .name("Distribuidores")
                .build();
    }

    public static PriceComponentRuleFactorBusinessUnit createBusinessUnitPrivadas() {
        return PriceComponentRuleFactorBusinessUnit.builder()
                .id(3)
                .name("Entidades Privadas")
                .build();
    }

    public static PriceComponentRuleFactorBusinessUnit createBusinessUnitPublicas() {
        return PriceComponentRuleFactorBusinessUnit.builder()
                .id(4)
                .name("Entidades Publicas")
                .build();
    }
    //endregion

    //region SaleOrders
    public static PriceComponentRuleInputSaleOrder createSaleOrder(PriceComponentRuleFactorCustomer customer,
                                                                   PriceComponentRuleFactorBusinessUnit businessUnit,
                                                                   PriceComponentRuleFactorAgency agency,
                                                                   List<PriceComponentRuleInputSaleOrderProduct> products,
                                                                   PaymentConditionsTypeEnum paymentCondition,
                                                                   DeliveryModeEnum deliveryMode) {
        return PriceComponentRuleInputSaleOrder.builder()
                .id(RandomUtil.getRandomInt(TestUtil.RANGE_SALE_ORDER_MIN, TestUtil.RANGE_SALE_ORDER_MAX))
                .businessUnit(businessUnit)
                .productDetails(products)
                .agency(agency)
                .paymentCondition(paymentCondition)
                .deliveryMode(deliveryMode)
                .customer(customer)
                .build();
    }
    //endregion

    //region Create Sale Order Product
    public static PriceComponentRuleInputSaleOrderProduct createSaleOrderProductBolsaColostomiaUnidad() {
        return createSaleOrderProduct(createProductBolsaColostomiaUnidad(),
                new BigDecimal(10.50),
                BigDecimal.TEN,
                toProductClassificationList(createProductClassificationOstomia()),
                Collections.emptyList());
    }

    public static PriceComponentRuleInputSaleOrderProduct createSaleOrderProduct(PriceComponentRuleSubjectProduct product,
                                                                                 BigDecimal unitPrice,
                                                                                 BigDecimal qty,
                                                                                 List<PriceComponentRuleFactorProductClassification> classifications,
                                                                                 List<PriceComponentRuleSubjectProductFeature> features) {
        return PriceComponentRuleInputSaleOrderProduct.builder()
                .id(RandomUtil.getRandomInt(TestUtil.RANGE_SALE_ORDER_PRODUCT_MIN, TestUtil.RANGE_SALE_ORDER_PRODUCT_MAX))
                .product(product)
                .classifications(classifications)
                .features(features)
                .qty(qty)
                .unitPrice(unitPrice)
                .totalPrice(unitPrice.multiply(qty))
                .totalPriceGross(unitPrice.multiply(qty))
                .totalDiscount(BigDecimal.ZERO)
                .build();
    }
    //endregion

    //region Subject Product
    public static PriceComponentRuleSubjectProduct createProductBolsaColostomiaUnidad() {
        return PriceComponentRuleSubjectProduct.builder()
                .id(83)
                .code("22892")
                .name("BOLSA P/COLOSTOMIA C/KARAYA 44MM - 1\" 3/4 D/30CM ABIERTA")
                .originalPrice(new BigDecimal(10.50))
                .unitMeasure(createUnitMeasureUnidad())
                .build();
    }

    public static PriceComponentRuleSubjectProduct createProductBolsaColostomiaCaja() {
        return PriceComponentRuleSubjectProduct.builder()
                .id(83)
                .code("22892")
                .name("BOLSA P/COLOSTOMIA C/KARAYA 44MM - 1\" 3/4 D/30CM ABIERTA")
                .originalPrice(new BigDecimal(1050.50))
                .unitMeasure(createUnitMeasureCaja())
                .build();
    }
    //endregion

    //region Customers
    public static PriceComponentRuleFactorCustomer createRandomCustomer() {
        return PriceComponentRuleFactorCustomer.builder()
                .id(RandomUtil.getRandomInt(TestUtil.RANGE_CUSTOMER_MIN, TestUtil.RANGE_CUSTOMER_MAX))
                .name(RandomUtil.getRandomName())
                .roles(Collections.singletonList(RoleTypeEnum.CUSTOMER))
                .geographicBoundaries(Collections.singletonList(TestUtil.createGeographicBoundarySC()))
                .partyClassifications(Collections.singletonList(createPartyClassificationOstomia()))
                .build();
    }

    public static PriceComponentRuleFactorCustomer createRandomCustomer(int id, String name,
                                                                        List<PriceComponentRuleFactorPartyClassification> classifications) {
        return PriceComponentRuleFactorCustomer.builder()
                .id(id)
                .name(name)
                .roles(Collections.singletonList(RoleTypeEnum.CUSTOMER))
                .geographicBoundaries(Collections.singletonList(TestUtil.createGeographicBoundarySC()))
                .partyClassifications(classifications)
                .build();
    }

    public static PriceComponentRuleFactorCustomer createClinicaIncorCustomer(int id) {
        return PriceComponentRuleFactorCustomer.builder()
                .id(id)
                .name("Clinica Incor")
                .roles(Collections.singletonList(RoleTypeEnum.CUSTOMER))
                .geographicBoundaries(Collections.singletonList(TestUtil.createGeographicBoundarySC()))
                .partyClassifications(Collections.singletonList(createPartyClassificationClinicas()))
                .build();
    }

    public static PriceComponentRuleFactorCustomer createClinicaFioaniniCustomer(int id) {
        return PriceComponentRuleFactorCustomer.builder()
                .id(id)
                .name("Clinica Fioanini")
                .roles(Collections.singletonList(RoleTypeEnum.CUSTOMER))
                .geographicBoundaries(Collections.singletonList(TestUtil.createGeographicBoundarySC()))
                .partyClassifications(Collections.singletonList(createPartyClassificationClinicas()))
                .build();
    }
    //endregion

    //region Unit Measure
    private static PriceComponentRuleUnitMeasure createUnitMeasureCaja() {
        return PriceComponentRuleUnitMeasure.builder()
                .id(25)
                .abbreviation("CJ")
                .name("Caja")
                .build();
    }

    private static PriceComponentRuleUnitMeasure createUnitMeasureUnidad() {
        return PriceComponentRuleUnitMeasure.builder()
                .id(23)
                .abbreviation("UN")
                .name("Unidad(es)")
                .build();
    }

    private static PriceComponentRuleUnitMeasure createUnitMeasureCarton() {
        return PriceComponentRuleUnitMeasure.builder()
                .id(22)
                .abbreviation("CT")
                .name("Carton")
                .build();
    }
    //endregion

    //region Utils
    public static List<PriceComponentRuleFactorProductClassification> toProductClassificationList(PriceComponentRuleFactorProductClassification... classifications) {
        return Arrays.asList(classifications);
    }

    public static List<PriceComponentRuleFactorPartyClassification> toPartyClassificationList(PriceComponentRuleFactorPartyClassification... classifications) {
        return Arrays.asList(classifications);
    }

    public static List<PriceComponentRuleFactorRoleType> toRoleTypeList(PriceComponentRuleFactorRoleType... roleTypes) {
        return Arrays.asList(roleTypes);
    }

    public static List<PriceComponentRuleInputSaleOrderProduct> toSaleOrderProductsList(PriceComponentRuleInputSaleOrderProduct... products) {
        return Arrays.asList(products);
    }
    //endregion
}
