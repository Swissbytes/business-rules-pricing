package ch.swissbytes.businessrules.pricing.utils;

import ch.swissbytes.businessrules.pricing.enums.*;
import ch.swissbytes.businessrules.pricing.factors.*;
import ch.swissbytes.businessrules.pricing.inputs.PriceComponentRuleInputSaleOrder;
import ch.swissbytes.businessrules.pricing.inputs.PriceComponentRuleInputSaleOrderProduct;
import ch.swissbytes.businessrules.pricing.models.IdModel;
import ch.swissbytes.businessrules.pricing.models.PriceRule;
import ch.swissbytes.businessrules.pricing.outcomes.PriceComponentRuleOutcome;
import ch.swissbytes.businessrules.pricing.outcomes.PriceComponentRuleOutcomeOutput;
import ch.swissbytes.businessrules.pricing.outcomes.PriceComponentRuleOutcomeProduct;
import ch.swissbytes.businessrules.pricing.subjects.PriceComponentRuleSubjectProduct;
import ch.swissbytes.businessrules.pricing.subjects.PriceComponentRuleSubjectProductFeature;
import ch.swissbytes.businessrules.pricing.subjects.PriceComponentRuleUnitMeasure;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;


public class TestUtil {

    //region Range of ids values
    public static final int RANGE_OUTCOME_MIN = 700;
    public static final int RANGE_OUTCOME_MAX = 799;
    public static final int RANGE_SALE_ORDER_MIN = 3000;
    public static final int RANGE_SALE_ORDER_MAX = 3099;
    public static final int RANGE_SALE_ORDER_PRODUCT_MIN = 2000;
    public static final int RANGE_SALE_ORDER_PRODUCT_MAX = 2999;
    public static final int RANGE_PRODUCT_MIN = 1000;
    public static final int RANGE_PRODUCT_MAX = 1999;
    public static final int RANGE_CUSTOMER_MIN = 100;
    public static final int RANGE_CUSTOMER_MAX = 199;
    public static final int RANGE_DELIVERY_MODE_MIN = 3100;
    public static final int RANGE_DELIVERY_MODE_MAX = 3199;
    private static final int RANGE_RULE_MIN = 9000;
    private static final int RANGE_RULE_MAX = 9099;
    private static final int RANGE_SCALE_MIN = 3400;
    private static final int RANGE_SCALE_MAX = 3499;
    //endregion

    //region Create SaleOrders
    public static PriceComponentRuleInputSaleOrder createSaleOrderRetailTelma444850(DeliveryModeEnum deliveryMode, PaymentConditionsTypeEnum paymentConditions) {
        //products
        List<PriceComponentRuleInputSaleOrderProduct> products = new ArrayList<>();
        products.add(createSaleOrderProduct44());
        products.add(createSaleOrderProduct48());
        //sale order
        return PriceComponentRuleInputSaleOrder.builder()
                .id(RandomUtil.getRandomInt(RANGE_SALE_ORDER_MIN, RANGE_SALE_ORDER_MAX))
                .businessUnit(createBusinessUnitRetail())
                .customer(createPartyRoleCustomerTelmaAll())
                .agency(createAgencyLP())
                .productDetails(products)
                .deliveryMode(deliveryMode)
                .paymentCondition(paymentConditions)
                .build();
    }

    public static PriceComponentRuleInputSaleOrder createSaleOrderRamiro47(boolean allFeatures,
                                                                           DeliveryModeEnum deliveryMode,
                                                                           PaymentConditionsTypeEnum paymentConditions) {
        //products
        List<PriceComponentRuleInputSaleOrderProduct> products = new ArrayList<>();
        products.add(allFeatures ? createSaleOrderProduct47All() : createSaleOrderProduct47());
        return PriceComponentRuleInputSaleOrder.builder()
                .id(RandomUtil.getRandomInt(RANGE_SALE_ORDER_MIN, RANGE_SALE_ORDER_MAX))
                .businessUnit(createBusinessUnitRetail())
                .customer(createPartyRoleCustomerRamiro())
                .productDetails(products)
                .deliveryMode(deliveryMode)
                .paymentCondition(paymentConditions)
                .build();
    }

    public static PriceComponentRuleInputSaleOrder createSaleOrderRamiro4448() {
        //products
        List<PriceComponentRuleInputSaleOrderProduct> products = new ArrayList<>();
        products.add(createSaleOrderProduct44());
        products.add(createSaleOrderProduct48());
        //geographic Boundary
        PriceComponentRuleFactorGeographicBoundary geographicBoundary = createGeographicBoundarySimpsons();
//        products.add(createSaleOrderProduct47());
        //sale order
        return PriceComponentRuleInputSaleOrder.builder()
                .id(RandomUtil.getRandomInt(RANGE_SALE_ORDER_MIN, RANGE_SALE_ORDER_MAX))
                .businessUnit(createBusinessUnitRetail())
                .customer(createPartyRoleCustomerRamiroAll())
                .productDetails(products)
                .paymentCondition(PaymentConditionsTypeEnum.CASH)
                .deliveryMode(DeliveryModeEnum.MOBILE_STORE)
                .build();
    }

    public static PriceComponentRuleInputSaleOrder createSaleOrderRamiro444847() {
        //products
        List<PriceComponentRuleInputSaleOrderProduct> products = new ArrayList<>();
        products.add(createSaleOrderProduct44());
        products.add(createSaleOrderProduct48());
        products.add(createSaleOrderProduct47());
        //sale order
        return PriceComponentRuleInputSaleOrder.builder()
                .id(RandomUtil.getRandomInt(RANGE_SALE_ORDER_MIN, RANGE_SALE_ORDER_MAX))
                .businessUnit(createBusinessUnitRetail())
                .customer(createPartyRoleCustomerRamiroAll())
                .productDetails(products)
                .build();
    }


    public static PriceComponentRuleInputSaleOrder createSaleOrderRamiro48() {
        //products
        List<PriceComponentRuleInputSaleOrderProduct> products = new ArrayList<>();
        products.add(createSaleOrderProduct48());
        //sale order
        return PriceComponentRuleInputSaleOrder.builder()
                .id(RandomUtil.getRandomInt(RANGE_SALE_ORDER_MIN, RANGE_SALE_ORDER_MAX))
                .businessUnit(createBusinessUnitRetail())
                .customer(createPartyRoleCustomerRamiroAll())
                .productDetails(products)
                .deliveryMode(DeliveryModeEnum.MOBILE_STORE)
                .paymentCondition(PaymentConditionsTypeEnum.CASH)
                .agency(createAgencyLP())
                .build();
    }

    public static PriceComponentRuleInputSaleOrder createSaleOrderRamiro48(BigDecimal qty) {
        //products
        List<PriceComponentRuleInputSaleOrderProduct> products = new ArrayList<>();
        products.add(createSaleOrderProduct48(qty));
        //sale order
        return PriceComponentRuleInputSaleOrder.builder()
                .id(RandomUtil.getRandomInt(RANGE_SALE_ORDER_MIN, RANGE_SALE_ORDER_MAX))
                .businessUnit(createBusinessUnitRetail())
                .customer(createPartyRoleCustomerRamiroAll())
                .productDetails(products)
                .deliveryMode(DeliveryModeEnum.MOBILE_STORE)
                .paymentCondition(PaymentConditionsTypeEnum.CASH)
                .agency(createAgencyLP())
                .build();
    }

    public static PriceComponentRuleInputSaleOrder createSaleOrderRamiro48(boolean hasClassification,
                                                                           boolean hasFeatures) {
        //products
        List<PriceComponentRuleInputSaleOrderProduct> products = new ArrayList<>();
        products.add(createSaleOrderProduct48(hasFeatures, hasClassification));
        //sale order
        return PriceComponentRuleInputSaleOrder.builder()
                .id(RandomUtil.getRandomInt(RANGE_SALE_ORDER_MIN, RANGE_SALE_ORDER_MAX))
                .businessUnit(createBusinessUnitRetail())
                .customer(createPartyRoleCustomerRamiroAll())
                .productDetails(products)
                .deliveryMode(DeliveryModeEnum.MOBILE_STORE)
                .paymentCondition(PaymentConditionsTypeEnum.CASH)
                .agency(createAgencyLP())
                .build();
    }

    public static PriceComponentRuleInputSaleOrder createSaleOrderMalcom48(DeliveryModeEnum deliveryMode, PaymentConditionsTypeEnum paymentConditions) {
        //products
        List<PriceComponentRuleInputSaleOrderProduct> products = new ArrayList<>();
        products.add(createSaleOrderProduct48());
        //sale order
        return PriceComponentRuleInputSaleOrder.builder()
                .id(RandomUtil.getRandomInt(RANGE_SALE_ORDER_MIN, RANGE_SALE_ORDER_MAX))
                .businessUnit(createBusinessUnitEntidadesPrivadas())
                .agency(createAgencySC())
                .customer(createPartyRoleEmployeeMalcom())
                .productDetails(products)
                .deliveryMode(deliveryMode)
                .paymentCondition(paymentConditions)
                .build();
    }

    public static PriceComponentRuleInputSaleOrder createSaleOrderRamiro4448WithAllFactors(DeliveryModeEnum deliveryMode, PaymentConditionsTypeEnum paymentConditions) {
        //products
        List<PriceComponentRuleInputSaleOrderProduct> products = new ArrayList<>();
        products.add(createSaleOrderProduct44WithAll());
        products.add(createSaleOrderProduct48WithAll());
        //sale order
        return PriceComponentRuleInputSaleOrder.builder()
                .id(RandomUtil.getRandomInt(RANGE_SALE_ORDER_MIN, RANGE_SALE_ORDER_MAX))
                .businessUnit(createBusinessUnitRetail())
                .customer(createPartyRoleCustomerRamiroAll())
                .agency(createAgencySC())
                .productDetails(products)
                .deliveryMode(deliveryMode)
                .paymentCondition(paymentConditions)
                .build();
    }

    public static PriceComponentRuleInputSaleOrder createSaleOrderArrieroLtda707172WithAllFactors(DeliveryModeEnum deliveryMode,
                                                                                      PaymentConditionsTypeEnum paymentConditions) {
        //products
        List<PriceComponentRuleInputSaleOrderProduct> saleOrderLines = new ArrayList<>();
        saleOrderLines.add(createSaleOrderLine70All(new BigDecimal(4), new BigDecimal(48)));
        saleOrderLines.add(createSaleOrderLine71All(new BigDecimal(2), new BigDecimal(55)));
        saleOrderLines.add(createSaleOrderLine72All(new BigDecimal(1), new BigDecimal(35)));
        //sale order
        return PriceComponentRuleInputSaleOrder.builder()
                .id(RandomUtil.getRandomInt(RANGE_SALE_ORDER_MIN, RANGE_SALE_ORDER_MAX))
                .businessUnit(createBusinessUnitRetail())
                .customer(createPartyRoleCustomerRamiroAll())
                .agency(createAgencySC())
                .productDetails(saleOrderLines)
                .deliveryMode(deliveryMode)
                .paymentCondition(paymentConditions)
                .build();
    }

    public static PriceComponentRuleInputSaleOrder createSaleOrderAnais2110142229AllFactors(DeliveryModeEnum deliveryMode,
                                                                                                  PaymentConditionsTypeEnum paymentConditions) {
        //products
        List<PriceComponentRuleInputSaleOrderProduct> saleOrderLines = new ArrayList<>();
        saleOrderLines.add(createSaleOrderLine211014(new BigDecimal(97), new BigDecimal(28)));
        //sale order
        return PriceComponentRuleInputSaleOrder.builder()
                .id(RandomUtil.getRandomInt(RANGE_SALE_ORDER_MIN, RANGE_SALE_ORDER_MAX))
                .businessUnit(createBusinessUnitRetail())
                .customer(createPartyRoleCustomerRamiroAll())
                .agency(createAgencySC())
                .productDetails(saleOrderLines)
                .deliveryMode(deliveryMode)
                .paymentCondition(paymentConditions)
                .build();
    }

    public static PriceComponentRuleInputSaleOrder createSaleOrderRamiroWithAllFactors(List<PriceComponentRuleInputSaleOrderProduct> products,
                                                                                       DeliveryModeEnum deliveryMode, PaymentConditionsTypeEnum paymentConditions) {
        return PriceComponentRuleInputSaleOrder.builder()
                .id(RandomUtil.getRandomInt(RANGE_SALE_ORDER_MIN, RANGE_SALE_ORDER_MAX))
                .businessUnit(createBusinessUnitRetail())
                .customer(createPartyRoleCustomerRamiroAll())
                .agency(createAgencySC())
                .productDetails(products)
                .deliveryMode(deliveryMode)
                .paymentCondition(paymentConditions)
                .build();
    }
    //endregion

    //region Create Geographic Boundaries
    public static PriceComponentRuleFactorGeographicBoundary createGeographicBoundarySimpsons() {
        return PriceComponentRuleFactorGeographicBoundary.builder()
                .id(805)
                .name("C/ Siempreviva 742")
                .latitude(new BigDecimal(-17.64585))
                .longitude(new BigDecimal(-63.64205))
                .build();
    }

    public static PriceComponentRuleFactorGeographicBoundary createGeographicBoundarySC() {
        return PriceComponentRuleFactorGeographicBoundary
                .builder()
                .id(803)
                .name("SANTA CRUZ")
                .latitude(new BigDecimal(-63.452584))
                .longitude(new BigDecimal(-17.179420))
                .build();
    }

    public static PriceComponentRuleFactorGeographicBoundary createGeographicBoundaryLP() {
        return PriceComponentRuleFactorGeographicBoundary
                .builder()
                .id(802)
                .name("LA PAZ")
                .latitude(new BigDecimal(-63.123584))
                .longitude(new BigDecimal(-17.389420))
                .build();
    }
    //endregion

    //region Agency Creation
    public static PriceComponentRuleFactorAgency createAgencySC() {
        return PriceComponentRuleFactorAgency.builder()
                .id(301)
                .name("Santa Cruz")
                .build();
    }

    public static PriceComponentRuleFactorAgency createAgencyLP() {
        return PriceComponentRuleFactorAgency.builder()
                .id(302)
                .name("La Paz")
                .build();
    }

    public static PriceComponentRuleFactorAgency createAgencyCBBA() {
        return PriceComponentRuleFactorAgency.builder()
                .id(303)
                .name("Cochabamba")
                .build();
    }
    //endregion

    //region Create Business Units
    public static PriceComponentRuleFactorBusinessUnit createBusinessUnitRetail() {
        return PriceComponentRuleFactorBusinessUnit.builder()
                .id(201)
                .name("Retail")
                .build();
    }

    public static PriceComponentRuleFactorBusinessUnit createBusinessUnitEntidadesPublicas() {
        return PriceComponentRuleFactorBusinessUnit.builder()
                .id(204)
                .name("Entidades Públicas")
                .build();
    }

    public static PriceComponentRuleFactorBusinessUnit createBusinessUnitEntidadesPrivadas() {
        return PriceComponentRuleFactorBusinessUnit.builder()
                .id(203)
                .name("Entidades Privadas")
                .build();
    }

    public static PriceComponentRuleFactorBusinessUnit createBusinessUnitDistribuidores() {
        return PriceComponentRuleFactorBusinessUnit.builder()
                .id(202)
                .name("Distribuidores")
                .build();
    }
    //endregion

    //region Sale Order Line
    public static PriceComponentRuleInputSaleOrderProduct createSaleOrderProduct44() {
        return PriceComponentRuleInputSaleOrderProduct.builder()
                .id(RandomUtil.getRandomInt(TestUtil.RANGE_SALE_ORDER_PRODUCT_MIN, 3099))
                .product(createProductRule44())
                .qty(new BigDecimal(1))
                .unitPrice(new BigDecimal(300))
                .totalDiscount(BigDecimal.ZERO)
                .totalPrice(new BigDecimal(300))
                .totalPriceGross(new BigDecimal(300))
                .discountAmount(BigDecimal.ZERO)
                .features(Collections.emptyList())
                .classifications(Collections.emptyList())
                .build();
    }

    public static PriceComponentRuleInputSaleOrderProduct createSaleOrderProduct44WithAll() {
        return PriceComponentRuleInputSaleOrderProduct.builder()
                .id(IntUtil.getRandomIdLong())
                .product(createProductRule44())
                .qty(new BigDecimal(1))
                .unitPrice(new BigDecimal(300))
                .totalDiscount(BigDecimal.ZERO)
                .totalPrice(new BigDecimal(300))
                .totalPriceGross(new BigDecimal(300))
                .discountAmount(BigDecimal.ZERO)
                .features(createProductFeatureBrandHollisterList())
                .classifications(Collections.singletonList(createProductClassificationCaboPBisturi()))
                .build();
    }

    public static PriceComponentRuleInputSaleOrderProduct createSaleOrderProduct48() {
        return PriceComponentRuleInputSaleOrderProduct.builder()
                .id(IntUtil.getRandomIdLong())
                .product(createProductRule48())
                .qty(new BigDecimal(10))
                .unitPrice(new BigDecimal(100))
                .totalDiscount(BigDecimal.ZERO)
                .totalPrice(new BigDecimal(1000))
                .totalPriceGross(new BigDecimal(1000))
                .discountAmount(BigDecimal.ZERO)
                .features(createProductFeatureBrandHollisterList())
                .classifications(Collections.singletonList(createProductClassificationCaboPBisturi()))
                .build();
    }

    public static PriceComponentRuleInputSaleOrderProduct createSaleOrderProduct48(BigDecimal qty) {
        return PriceComponentRuleInputSaleOrderProduct.builder()
                .id(IntUtil.getRandomIdLong())
                .product(createProductRule48())
                .qty(qty)
                .unitPrice(new BigDecimal(100))
                .totalDiscount(BigDecimal.ZERO)
                .totalPrice(new BigDecimal(1000))
                .totalPriceGross(new BigDecimal(1000))
                .discountAmount(BigDecimal.ZERO)
                .features(createProductFeatureBrandHollisterList())
                .classifications(Collections.singletonList(createProductClassificationCaboPBisturi()))
                .build();
    }

    public static PriceComponentRuleInputSaleOrderProduct createSaleOrderProduct48RandomFature() {
        return PriceComponentRuleInputSaleOrderProduct.builder()
                .id(IntUtil.getRandomIdLong())
                .product(createProductRule48())
                .qty(new BigDecimal(10))
                .unitPrice(new BigDecimal(100))
                .totalDiscount(BigDecimal.ZERO)
                .totalPrice(new BigDecimal(1000))
                .totalPriceGross(new BigDecimal(1000))
                .discountAmount(BigDecimal.ZERO)
                .features(createProductFeatureBrandHollisterList())
                .classifications(Collections.singletonList(createProductClassificationCaboPBisturi()))
                .build();
    }

    public static PriceComponentRuleInputSaleOrderProduct createSaleOrderProduct48(boolean hasFeatures, boolean hasClassifications) {
        return PriceComponentRuleInputSaleOrderProduct.builder()
                .id(IntUtil.getRandomIdLong())
                .product(createProductRule48())
                .qty(new BigDecimal(10))
                .unitPrice(new BigDecimal(100))
                .totalDiscount(BigDecimal.ZERO)
                .totalPrice(new BigDecimal(1000))
                .totalPriceGross(new BigDecimal(1000))
                .discountAmount(BigDecimal.ZERO)
                .features(hasFeatures ? Collections.singletonList(createProductFeatureBrandFamous()) : Collections.emptyList())
                .classifications(hasClassifications ? Collections.singletonList(createProductClassificationCaboPBisturi()) : Collections.emptyList())
                .build();
    }

    public static PriceComponentRuleInputSaleOrderProduct createSaleOrderProduct48WithAll() {
        List<PriceComponentRuleFactorProductClassification> classifications = new ArrayList<>();
        classifications.add(createProductClassificationPediatria());
        classifications.add(createProductClassificationHaloCefalico());
        return PriceComponentRuleInputSaleOrderProduct.builder()
                .id(IntUtil.getRandomIdLong())
                .product(createProductRule48())
                .qty(new BigDecimal(10))
                .unitPrice(new BigDecimal(100))
                .totalDiscount(BigDecimal.ZERO)
                .totalPrice(new BigDecimal(1000))
                .totalPriceGross(new BigDecimal(1000))
                .discountAmount(BigDecimal.ZERO)
                .features(createProductFeatureBrandHollisterList())
                .classifications(classifications)
                .build();
    }

    public static PriceComponentRuleInputSaleOrderProduct createSaleOrderProduct47() {
        return PriceComponentRuleInputSaleOrderProduct.builder()
                .id(IntUtil.getRandomIdLong())
                .product(createProductRule47())
                .qty(new BigDecimal(10))
                .unitPrice(new BigDecimal(100))
                .totalDiscount(BigDecimal.ZERO)
                .totalPrice(new BigDecimal(1000))
                .totalPriceGross(new BigDecimal(1000))
                .discountAmount(BigDecimal.ZERO)
                .features(createProductFeatureBrandHollisterList())
                .classifications(Collections.singletonList(createProductClassificationHaloCefalico()))
                .build();
    }

    public static PriceComponentRuleInputSaleOrderProduct createSaleOrderProduct47All() {
        List<PriceComponentRuleFactorProductClassification> classifications = new ArrayList<>();
        classifications.add(createProductClassificationHaloCefalico());
        classifications.add(createProductClassificationQuirofano());
        return PriceComponentRuleInputSaleOrderProduct.builder()
                .id(IntUtil.getRandomIdLong())
                .product(createProductRule47())
                .qty(new BigDecimal(10))
                .unitPrice(new BigDecimal(100))
                .totalDiscount(BigDecimal.ZERO)
                .totalPrice(new BigDecimal(1000))
                .totalPriceGross(new BigDecimal(1000))
                .discountAmount(BigDecimal.ZERO)
                .features(createProductFeatureBrandHollisterList())
                .classifications(classifications)
                .build();
    }

    public static PriceComponentRuleInputSaleOrderProduct createSaleOrderProduct46All() {
        List<PriceComponentRuleFactorProductClassification> classifications = new ArrayList<>(0);
        classifications.add(createProductClassificationQuirofano());
        return PriceComponentRuleInputSaleOrderProduct.builder()
                .id(IntUtil.getRandomIdLong())
                .product(createProductRule46())
                .qty(new BigDecimal(4))
                .unitPrice(new BigDecimal(500))
                .totalDiscount(BigDecimal.ZERO)
                .totalPrice(new BigDecimal(2000))
                .totalPriceGross(new BigDecimal(2000))
                .discountAmount(BigDecimal.ZERO)
                .features(createProductFeatureBrandHollisterList())
                .classifications(classifications)
                .build();
    }

    public static PriceComponentRuleInputSaleOrderProduct createSaleOrderProduct49All(BigDecimal qty, BigDecimal unitPrice) {
        List<PriceComponentRuleFactorProductClassification> classifications = new ArrayList<>(0);
        classifications.add(createProductClassificationNeonatologia());
        classifications.add(createProductClassificationAdministrativo());

        BigDecimal total = unitPrice.multiply(qty);
        return PriceComponentRuleInputSaleOrderProduct.builder()
                .id(IntUtil.getRandomIdLong())
                .product(createProductRule49())
                .qty(qty)
                .unitPrice(unitPrice)
                .totalDiscount(BigDecimal.ZERO)
                .totalPrice(total)
                .totalPriceGross(total)
                .discountAmount(BigDecimal.ZERO)
                .features(createProductFeatureBrandHollisterList())
                .classifications(classifications)
                .build();
    }

    public static PriceComponentRuleInputSaleOrderProduct createSaleOrderLine70All(BigDecimal qty, BigDecimal unitPrice) {
        List<PriceComponentRuleFactorProductClassification> classifications = new ArrayList<>(0);
        classifications.add(createProductClassificationNeonatologia());
        classifications.add(createProductClassificationAdministrativo());

        BigDecimal total = unitPrice.multiply(qty);
        return PriceComponentRuleInputSaleOrderProduct.builder()
                .id(IntUtil.getRandomIdLong())
                .product(createProductRule70())
                .qty(qty)
                .unitPrice(unitPrice)
                .totalDiscount(BigDecimal.ZERO)
                .totalPrice(total)
                .totalPriceGross(total)
                .discountAmount(BigDecimal.ZERO)
                .features(createProductFeatureBrandHollisterList())
                .classifications(classifications)
                .build();
    }

    public static PriceComponentRuleInputSaleOrderProduct createSaleOrderLine71All(BigDecimal qty,
                                                                                   BigDecimal unitPrice) {
        List<PriceComponentRuleFactorProductClassification> classifications = new ArrayList<>(0);
        classifications.add(createProductClassificationNeonatologia());
        classifications.add(createProductClassificationAdministrativo());

        BigDecimal total = unitPrice.multiply(qty);
        return PriceComponentRuleInputSaleOrderProduct.builder()
                .id(IntUtil.getRandomIdLong())
                .product(createProductRule71())
                .qty(qty)
                .unitPrice(unitPrice)
                .totalDiscount(BigDecimal.ZERO)
                .totalPrice(total)
                .totalPriceGross(total)
                .discountAmount(BigDecimal.ZERO)
                .features(createProductFeatureBrandHollisterList())
                .classifications(classifications)
                .build();
    }

    public static PriceComponentRuleInputSaleOrderProduct createSaleOrderLine72All(BigDecimal qty,
                                                                                   BigDecimal unitPrice) {
        List<PriceComponentRuleFactorProductClassification> classifications = new ArrayList<>(0);
        classifications.add(createProductClassificationNeonatologia());
        classifications.add(createProductClassificationAdministrativo());

        BigDecimal total = unitPrice.multiply(qty);
        return PriceComponentRuleInputSaleOrderProduct.builder()
                .id(IntUtil.getRandomIdLong())
                .product(createProductRule72())
                .qty(qty)
                .unitPrice(unitPrice)
                .totalDiscount(BigDecimal.ZERO)
                .totalPrice(total)
                .totalPriceGross(total)
                .discountAmount(BigDecimal.ZERO)
                .features(createProductFeatureBrandHollisterList())
                .classifications(classifications)
                .build();
    }

    public static PriceComponentRuleInputSaleOrderProduct createSaleOrderLine211014(BigDecimal qty,
                                                                                   BigDecimal unitPrice) {
        List<PriceComponentRuleFactorProductClassification> classifications = new ArrayList<>(0);
        classifications.add(createProductClassificationNeonatologia());
        classifications.add(createProductClassificationAdministrativo());

        BigDecimal total = unitPrice.multiply(qty);
        return PriceComponentRuleInputSaleOrderProduct.builder()
                .id(IntUtil.getRandomIdLong())
                .product(createProductRule300175())
                .qty(qty)
                .unitPrice(unitPrice)
                .totalDiscount(BigDecimal.ZERO)
                .totalPrice(total)
                .totalPriceGross(total)
                .discountAmount(BigDecimal.ZERO)
                .features(createProductFeatureBrandHollisterList())
                .classifications(classifications)
                .build();
    }

    //endregion Sale Order Line

    //region Create Product Features
    private static List<PriceComponentRuleSubjectProductFeature> createProductFeatureBrandHollisterList() {
        List<PriceComponentRuleSubjectProductFeature> features = new ArrayList<>();
        features.add(createProductFeatureBrandHollister());
        return features;
    }

    public static PriceComponentRuleSubjectProductFeature createProductFeatureBrandHollister() {
        return PriceComponentRuleSubjectProductFeature.builder()
                .id(1)
                .name("Marca")
                .value("Hollister")
                .build();
    }

    public static PriceComponentRuleSubjectProductFeature createProductFeature(long id, String name, String value) {
        return PriceComponentRuleSubjectProductFeature.builder()
                .id(id)
                .name(name)
                .value(value)
                .build();
    }

    public static PriceComponentRuleSubjectProductFeature createProductFeatureBrandFamous() {
        return PriceComponentRuleSubjectProductFeature.builder()
                .id(13)
                .name("Marca")
                .value("Famous")
                .build();
    }
    //endregion


    //region OutcomeOutputs
    public static PriceComponentRuleOutcomeOutput createOutcomeOutput10Discount(ScaleTypeEnum validationType) {
        return PriceComponentRuleOutcomeOutput.builder()
                .id(3200)
                .outComeModeEnum(OutComeModeEnum.SINGLE)
                .validationType(validationType)
                .outcome(createOutcome10DiscountPerc())
                .build();
    }

    public static PriceComponentRuleOutcomeOutput createOutcomeOutputDiscountPerc(BigDecimal discount, ScaleTypeEnum validationType) {
        return PriceComponentRuleOutcomeOutput.builder()
                .id(3201)
                .outComeModeEnum(OutComeModeEnum.SINGLE)
                .validationType(validationType)
                .outcome(createOutcomeDiscountPerc(discount))
                .build();
    }

    public static PriceComponentRuleOutcomeOutput createOutcomeOutput10Discount(List<PriceComponentRuleFactorScale> scales, ScaleTypeEnum validationType) {
        return PriceComponentRuleOutcomeOutput.builder()
                .id(3202)
                .outComeModeEnum(OutComeModeEnum.SCALE)
                .validationType(validationType)
                .scales(scales)
                .outcome(createOutcome10DiscountPerc())
                .build();
    }

    public static PriceComponentRuleOutcomeOutput createOutcomeOutput10Discount(BigDecimal frequency, ScaleTypeEnum validationType) {
        return PriceComponentRuleOutcomeOutput.builder()
                .id(3203)
                .outComeModeEnum(OutComeModeEnum.FREQUENCY)
                .validationType(validationType)
                .frequency(frequency)
                .outcome(createOutcome10DiscountPerc())
                .build();
    }

    public static PriceComponentRuleOutcomeOutput createOutcomeOutputDiscountAmount(BigDecimal discount, ScaleTypeEnum validationType,
                                                                                    OutComeModeEnum outcomeMode, BigDecimal frequency,
                                                                                    List<PriceComponentRuleFactorScale> scales) {
        return PriceComponentRuleOutcomeOutput.builder()
                .id(3204)
                .outComeModeEnum(outcomeMode)
                .validationType(validationType)
                .outcome(createOutcomeDiscountAmount(discount))
                .frequency(frequency)
                .scales(scales)
                .outcomeType(PriceComponentOutcomeTypeEnum.DISCOUNT_AMOUNT)
                .build();
    }

    public static PriceComponentRuleOutcomeOutput createOutcomeOutputSurchargeAmount(BigDecimal surcharge, ScaleTypeEnum validationType,
                                                                                     OutComeModeEnum outcomeMode, BigDecimal frequency,
                                                                                     List<PriceComponentRuleFactorScale> scales) {
        return PriceComponentRuleOutcomeOutput.builder()
                .id(3205)
                .outComeModeEnum(outcomeMode)
                .validationType(validationType)
                .outcome(createOutcomeSurchargeAmount(surcharge))
                .frequency(frequency)
                .scales(scales)
                .outcomeType(PriceComponentOutcomeTypeEnum.SURCHARGE_AMOUNT)
                .build();
    }

    public static PriceComponentRuleOutcomeOutput createOutcomeOutputSurchargePerc(BigDecimal surchargePerc, ScaleTypeEnum validationType,
                                                                                   OutComeModeEnum outcomeMode, BigDecimal frequency,
                                                                                   List<PriceComponentRuleFactorScale> scales) {
        return PriceComponentRuleOutcomeOutput.builder()
                .id(3206)
                .outComeModeEnum(outcomeMode)
                .validationType(validationType)
                .outcome(createOutcomeSurchargePerc(surchargePerc))
                .frequency(frequency)
                .scales(scales)
                .outcomeType(PriceComponentOutcomeTypeEnum.SURCHARGE_PERCENTAGE)
                .build();
    }

    public static PriceComponentRuleOutcomeOutput createOutcomeOutputFixedPrice(BigDecimal fixedPrice, ScaleTypeEnum validationType,
                                                                                OutComeModeEnum outcomeMode, BigDecimal frequency,
                                                                                List<PriceComponentRuleFactorScale> scales) {
        return PriceComponentRuleOutcomeOutput.builder()
                .id(3207)
                .outComeModeEnum(outcomeMode)
                .validationType(validationType)
                .outcome(createOutcomeFixedPrice(fixedPrice))
                .frequency(frequency)
                .scales(scales)
                .outcomeType(PriceComponentOutcomeTypeEnum.FIXED_PRICE)
                .build();
    }

    public static PriceComponentRuleOutcomeOutput createOutcomeOutputProductBonus(
            List<PriceComponentRuleOutcomeProduct> products, ScaleTypeEnum validationType,
            OutComeModeEnum outcomeMode, BigDecimal frequency,
            List<PriceComponentRuleFactorScale> scales) {

        return PriceComponentRuleOutcomeOutput.builder()
                .id(3208)
                .outComeModeEnum(outcomeMode)
                .validationType(validationType)
                .outcome(createOutcomeProductBonus(products))
                .frequency(frequency)
                .scales(scales)
                .outcomeType(PriceComponentOutcomeTypeEnum.PRODUCT)
                .build();
    }

    public static PriceComponentRuleOutcomeOutput createOutcomeOutputProductSurcharge(List<PriceComponentRuleOutcomeProduct> products,
                                                                                      ScaleTypeEnum validationType,
                                                                                      OutComeModeEnum outcomeMode,
                                                                                      BigDecimal frequency,
                                                                                      List<PriceComponentRuleFactorScale> scales) {
        return PriceComponentRuleOutcomeOutput.builder()
                .id(3209)
                .outComeModeEnum(outcomeMode)
                .validationType(validationType)
                .outcome(createOutcomeProductSurcharge(products))
                .frequency(frequency)
                .scales(scales)
                .outcomeType(PriceComponentOutcomeTypeEnum.PRODUCT_SURCHARGE)
                .build();
    }

    public static PriceComponentRuleOutcomeOutput createOutcomeOutputDiscountAmount(List<PriceComponentRuleFactorScale> scales) {
        return PriceComponentRuleOutcomeOutput.builder()
                .id(3210)
                .outComeModeEnum(OutComeModeEnum.SCALE)
                .validationType(scales.get(0).getType())
                .scales(scales)
                .build();
    }

    public static PriceComponentRuleOutcomeOutput createOutcomeOutputDiscountPercFrequency(BigDecimal discount, ScaleTypeEnum validationType, BigDecimal frequency) {
        return PriceComponentRuleOutcomeOutput.builder()
                .id(3211)
                .outComeModeEnum(OutComeModeEnum.FREQUENCY)
                .validationType(validationType)
                .frequency(frequency)
                .outcome(createOutcomeDiscountPerc(discount))
                .build();
    }


    public static PriceComponentRuleOutcomeOutput createOutcomeOutputDiscountPerc(BigDecimal discount, ScaleTypeEnum validationType,
                                                                                    OutComeModeEnum outcomeMode, BigDecimal frequency,
                                                                                    List<PriceComponentRuleFactorScale> scales) {
        return PriceComponentRuleOutcomeOutput.builder()
                .id(3212)
                .outComeModeEnum(outcomeMode)
                .validationType(validationType)
                .outcome(createOutcomeDiscountPerc(discount))
                .frequency(frequency)
                .scales(scales)
                .outcomeType(PriceComponentOutcomeTypeEnum.DISCOUNT_PERCENTAGE)
                .build();
    }

    public static PriceComponentRuleOutcomeOutput createOutcomeOutput(long id, PriceComponentRuleOutcome outcome, ScaleTypeEnum validationType,
                                                                                  OutComeModeEnum outcomeMode, BigDecimal frequency,
                                                                                  List<PriceComponentRuleFactorScale> scales,
                                                                      PriceComponentOutcomeTypeEnum outcomeType) {
        return PriceComponentRuleOutcomeOutput.builder()
                .id(id)
                .outComeModeEnum(outcomeMode)
                .validationType(validationType)
                .outcome(outcome)
                .frequency(frequency)
                .scales(scales)
                .outcomeType(outcomeType)
                .build();
    }
    //endregion

    //region Outcomes
    public static PriceComponentRuleOutcome createOutcomeDiscountPerc(BigDecimal percentage) {
        return PriceComponentRuleOutcome.builder()
                .id(3300)
                .outcomeType(PriceComponentOutcomeTypeEnum.DISCOUNT_PERCENTAGE)
                .discountPercentage(percentage)
                .build();
    }

    public static PriceComponentRuleOutcome createOutcome10DiscountPerc() {
        return PriceComponentRuleOutcome.builder()
                .id(3301)
                .outcomeType(PriceComponentOutcomeTypeEnum.DISCOUNT_PERCENTAGE)
                .discountPercentage(new BigDecimal(10))
                .build();
    }

    public static PriceComponentRuleOutcome createOutcome15DiscountPerc() {
        return PriceComponentRuleOutcome.builder()
                .id(3308)
                .outcomeType(PriceComponentOutcomeTypeEnum.DISCOUNT_PERCENTAGE)
                .discountPercentage(new BigDecimal(15))
                .build();
    }

    public static PriceComponentRuleOutcome createOutcome10DiscountAmount() {
        return PriceComponentRuleOutcome.builder()
                .id(3302)
                .outcomeType(PriceComponentOutcomeTypeEnum.DISCOUNT_AMOUNT)
                .discountAmount(new BigDecimal(10))
                .build();
    }

    public static PriceComponentRuleOutcome createOutcome10SurchargePer() {
        return PriceComponentRuleOutcome.builder()
                .id(3303)
                .outcomeType(PriceComponentOutcomeTypeEnum.SURCHARGE_PERCENTAGE)
                .surchargePercentage(new BigDecimal(10))
                .build();
    }

    public static PriceComponentRuleOutcome createOutcome10SurchargeAmount() {
        return PriceComponentRuleOutcome.builder()
                .id(3304)
                .outcomeType(PriceComponentOutcomeTypeEnum.SURCHARGE_AMOUNT)
                .surchargeAmount(new BigDecimal(10))
                .build();
    }

    public static PriceComponentRuleOutcome createOutcome150FixedPrice() {
        return PriceComponentRuleOutcome.builder()
                .id(3305)
                .outcomeType(PriceComponentOutcomeTypeEnum.FIXED_PRICE)
                .fixedPrice(new BigDecimal(150))
                .build();
    }

    public static PriceComponentRuleOutcome createOutcome2ProductBonus() {
        List<PriceComponentRuleOutcomeProduct> productBonusList = new ArrayList<>();
        productBonusList.add(createProductBonus47(new BigDecimal(2)));
        productBonusList.add(createProductBonus48(new BigDecimal(2)));

        return PriceComponentRuleOutcome.builder()
                .id(3306)
                .outcomeType(PriceComponentOutcomeTypeEnum.PRODUCT)
                .products(productBonusList)
                .build();
    }

    public static PriceComponentRuleOutcome createOutcomeProductSurcharge() {
        List<PriceComponentRuleOutcomeProduct> productSurchargeList = new ArrayList<>();
        productSurchargeList.add(createProductBonus46(BigDecimal.ONE));

        return PriceComponentRuleOutcome.builder()
                .id(3307)
                .outcomeType(PriceComponentOutcomeTypeEnum.PRODUCT_SURCHARGE)
                .products(productSurchargeList)
                .build();
    }

    public static PriceComponentRuleOutcome createOutcomeDiscountAmount(BigDecimal amount) {
        return PriceComponentRuleOutcome.builder()
                .id(3308)
                .outcomeType(PriceComponentOutcomeTypeEnum.DISCOUNT_AMOUNT)
                .discountAmount(amount)
                .build();
    }

    public static PriceComponentRuleOutcome createOutcomeSurchargeAmount(BigDecimal amount) {
        return PriceComponentRuleOutcome.builder()
                .id(3309)
                .outcomeType(PriceComponentOutcomeTypeEnum.SURCHARGE_AMOUNT)
                .surchargeAmount(amount)
                .build();
    }

    public static PriceComponentRuleOutcome createOutcomeSurchargePerc(BigDecimal percentage) {
        return PriceComponentRuleOutcome.builder()
                .id(3310)
                .outcomeType(PriceComponentOutcomeTypeEnum.SURCHARGE_PERCENTAGE)
                .surchargePercentage(percentage)
                .build();
    }

    public static PriceComponentRuleOutcome createOutcomeFixedPrice(BigDecimal fixedPrice) {
        return PriceComponentRuleOutcome.builder()
                .id(3311)
                .outcomeType(PriceComponentOutcomeTypeEnum.FIXED_PRICE)
                .fixedPrice(fixedPrice)
                .build();
    }

    public static PriceComponentRuleOutcome createOutcomeProductBonus(List<PriceComponentRuleOutcomeProduct> products) {
        return PriceComponentRuleOutcome.builder()
                .id(3312)
                .outcomeType(PriceComponentOutcomeTypeEnum.PRODUCT)
                .products(products)
                .build();
    }

    public static PriceComponentRuleOutcome createOutcomeProductSurcharge(List<PriceComponentRuleOutcomeProduct> products) {
        return PriceComponentRuleOutcome.builder()
                .id(3313)
                .outcomeType(PriceComponentOutcomeTypeEnum.PRODUCT_SURCHARGE)
                .products(products)
                .build();
    }
    //endregion

    //region Rules By Factors
    public static PriceRule createRuleOnlyDeliveryMode1001(List<PriceComponentRuleFactorDeliveryMode> deliveryModes,
                                                           boolean accumulative, TargetEnum target,
                                                           PriceRuleTypeEnum ruleType) {
        return PriceRule.builder()
                .id(1001)
                .name("DESCUENTO DEL 10% ONLY DELIVERY MODE")
                .accumulative(accumulative)
                .deliveryModes(deliveryModes)
                .output(createOutcomeOutput10Discount(ScaleTypeEnum.AMOUNT))
                .target(target)
                .ruleType(ruleType)
                .build();
    }

    public static PriceRule createRuleOnlyPaymentCondition1002(PaymentConditionsTypeEnum paymentCondition,
                                                               boolean accumulative, TargetEnum target,
                                                               PriceRuleTypeEnum ruleType) {
        return PriceRule.builder()
                .id(1002)
                .name("DESCUENTO DEL 10% ONLY PAYMENT CONDITION")
                .accumulative(accumulative)
                .paymentCondition(paymentCondition)
                .output(createOutcomeOutput10Discount(ScaleTypeEnum.AMOUNT))
                .target(target)
                .ruleType(ruleType)
                .build();
    }

    public static PriceRule createRuleOnlyDeliveryModePaymentCondition1003(List<PriceComponentRuleFactorDeliveryMode> deliveryModes,
                                                                           PaymentConditionsTypeEnum paymentCondition,
                                                                           boolean accumulative, TargetEnum target,
                                                                           PriceRuleTypeEnum ruleType) {
        return PriceRule.builder()
                .id(1003)
                .name("DESCUENTO DEL 10% ONLY PAYMENT CONDITION, DELIVERY MODE")
                .accumulative(accumulative)
                .deliveryModes(deliveryModes)
                .paymentCondition(paymentCondition)
                .output(createOutcomeOutput10Discount(ScaleTypeEnum.AMOUNT))
                .target(target)
                .ruleType(ruleType)
                .build();
    }


    public static PriceRule createRuleOnlyBusinessUnit1004(List<PriceComponentRuleFactorBusinessUnit> businessUnits,
                                                           boolean accumulative, TargetEnum target, PriceRuleTypeEnum ruleType) {
        return PriceRule.builder()
                .id(1004)
                .name("DESCUENTO DEL 10% ONLY BusinessUnit")
                .accumulative(accumulative)
                .businessUnits(businessUnits)
                .output(createOutcomeOutput10Discount(ScaleTypeEnum.AMOUNT))
                .target(target)
                .ruleType(ruleType)
                .build();
    }

    public static PriceRule createRuleOnlyDeliveryPaymentBusinessUnit1005(List<PriceComponentRuleFactorDeliveryMode> deliveryModes,
                                                                          PaymentConditionsTypeEnum paymentCondition,
                                                                          List<PriceComponentRuleFactorBusinessUnit> businessUnits,
                                                                          boolean accumulative, TargetEnum target,
                                                                          PriceRuleTypeEnum ruleType) {
        return PriceRule.builder()
                .id(1005)
                .name("DESCUENTO DEL 10% ONLY PAYMENT CONDITION, DELIVERY MODE, BUSINESS UNIT")
                .accumulative(accumulative)
                .deliveryModes(deliveryModes)
                .paymentCondition(paymentCondition)
                .businessUnits(businessUnits)
                .output(createOutcomeOutput10Discount(ScaleTypeEnum.AMOUNT))
                .target(target)
                .ruleType(ruleType)
                .build();
    }

    public static PriceRule createRuleOnlyAgency1006(List<PriceComponentRuleFactorAgency> agencies,
                                                     boolean accumulative, TargetEnum target,
                                                     PriceRuleTypeEnum ruleType) {
        return PriceRule.builder()
                .id(1006)
                .name("DESCUENTO DEL 10% ONLY AGENCIAS")
                .accumulative(accumulative)
                .agencies(agencies)
                .output(createOutcomeOutput10Discount(ScaleTypeEnum.AMOUNT))
                .target(target)
                .ruleType(ruleType)
                .build();
    }

    public static PriceRule createRuleOnlyDeliveryPaymentBusinessUnitAgency1007(List<PriceComponentRuleFactorDeliveryMode> deliveryModes,
                                                                                PaymentConditionsTypeEnum paymentCondition,
                                                                                List<PriceComponentRuleFactorBusinessUnit> businessUnits,
                                                                                List<PriceComponentRuleFactorAgency> agencies,
                                                                                boolean accumulative, TargetEnum target,
                                                                                PriceRuleTypeEnum ruleType) {
        return PriceRule.builder()
                .id(1007)
                .name("DESCUENTO DEL 10% ONLY PAYMENT CONDITION, DELIVERY MODE, BUSINESS UNIT, AGENCY")
                .accumulative(accumulative)
                .deliveryModes(deliveryModes)
                .paymentCondition(paymentCondition)
                .businessUnits(businessUnits)
                .agencies(agencies)
                .output(createOutcomeOutput10Discount(ScaleTypeEnum.AMOUNT))
                .target(target)
                .ruleType(ruleType)
                .build();
    }

    public static PriceRule createRuleOnlyRoleType1008(List<PriceComponentRuleFactorRoleType> roleTypes,
                                                       boolean accumulative, TargetEnum target,
                                                       PriceRuleTypeEnum ruleType) {
        return PriceRule.builder()
                .id(1008)
                .name("DESCUENTO DEL 10% ONLY Role Type")
                .accumulative(accumulative)
                .roleTypes(roleTypes)
                .output(createOutcomeOutput10Discount(ScaleTypeEnum.AMOUNT))
                .target(target)
                .ruleType(ruleType)
                .build();
    }

    public static PriceRule createRuleOnlyDeliveryPaymentBusinessUnitAgencyRoleType1009(List<PriceComponentRuleFactorDeliveryMode> deliveryModes,
                                                                                        PaymentConditionsTypeEnum paymentCondition,
                                                                                        List<PriceComponentRuleFactorBusinessUnit> businessUnits,
                                                                                        List<PriceComponentRuleFactorAgency> agencies,
                                                                                        List<PriceComponentRuleFactorRoleType> roleTypes,
                                                                                        boolean accumulative, TargetEnum target,
                                                                                        PriceRuleTypeEnum ruleType) {
        return PriceRule.builder()
                .id(1009)
                .name("DESCUENTO DEL 10% ONLY PAYMENT CONDITION, DELIVERY MODE, BUSINESS UNIT, AGENCY, ROLE TYPE")
                .accumulative(accumulative)
                .deliveryModes(deliveryModes)
                .paymentCondition(paymentCondition)
                .businessUnits(businessUnits)
                .agencies(agencies)
                .roleTypes(roleTypes)
                .output(createOutcomeOutput10Discount(ScaleTypeEnum.AMOUNT))
                .target(target)
                .ruleType(ruleType)
                .build();
    }

    public static PriceRule createRuleOnlyCustomer1010(List<PriceComponentRuleFactorCustomer> customers,
                                                       boolean accumulative, TargetEnum target,
                                                       PriceRuleTypeEnum ruleType) {
        return PriceRule.builder()
                .id(1010)
                .name("DESCUENTO DEL 10% ONLY CUSTOMER")
                .accumulative(accumulative)
                .customers(customers)
                .output(createOutcomeOutput10Discount(ScaleTypeEnum.AMOUNT))
                .target(target)
                .ruleType(ruleType)
                .build();
    }

    public static PriceRule createRuleOnlyDeliveryPaymentBusinessUnitAgencyRoleTypeCustomer1011(List<PriceComponentRuleFactorDeliveryMode> deliveryModes,
                                                                                                PaymentConditionsTypeEnum paymentCondition,
                                                                                                List<PriceComponentRuleFactorBusinessUnit> businessUnits,
                                                                                                List<PriceComponentRuleFactorAgency> agencies,
                                                                                                List<PriceComponentRuleFactorRoleType> roleTypes,
                                                                                                List<PriceComponentRuleFactorCustomer> customers,
                                                                                                boolean accumulative, TargetEnum target,
                                                                                                PriceRuleTypeEnum ruleType) {
        return PriceRule.builder()
                .id(1011)
                .name("DESCUENTO DEL 10% ONLY PAYMENT CONDITION, DELIVERY MODE, BUSINESS UNIT, AGENCY, ROLE TYPE, CUSTOMER")
                .accumulative(accumulative)
                .deliveryModes(deliveryModes)
                .paymentCondition(paymentCondition)
                .businessUnits(businessUnits)
                .agencies(agencies)
                .roleTypes(roleTypes)
                .customers(customers)
                .output(createOutcomeOutput10Discount(ScaleTypeEnum.AMOUNT))
                .target(target)
                .ruleType(ruleType)
                .build();
    }

    public static PriceRule createRuleOnlyPartyClassification1012(List<PriceComponentRuleFactorPartyClassification> partyClassifications,
                                                                  boolean accumulative, TargetEnum target,
                                                                  PriceRuleTypeEnum ruleType) {
        return PriceRule.builder()
                .id(1012)
                .name("DESCUENTO DEL 10% ONLY PARTY CLASSIFICATION")
                .accumulative(accumulative)
                .partyClassifications(partyClassifications)
                .output(createOutcomeOutput10Discount(ScaleTypeEnum.AMOUNT))
                .target(target)
                .ruleType(ruleType)
                .build();
    }

    public static PriceRule createRuleOnlyDeliveryPaymentBusinessUnitAgencyRoleTypeCustomerPartyClassification1013(List<PriceComponentRuleFactorDeliveryMode> deliveryModes,
                                                                                                                   PaymentConditionsTypeEnum paymentCondition,
                                                                                                                   List<PriceComponentRuleFactorBusinessUnit> businessUnits,
                                                                                                                   List<PriceComponentRuleFactorAgency> agencies,
                                                                                                                   List<PriceComponentRuleFactorRoleType> roleTypes,
                                                                                                                   List<PriceComponentRuleFactorCustomer> customers,
                                                                                                                   List<PriceComponentRuleFactorPartyClassification> partyClassifications,
                                                                                                                   boolean accumulative, TargetEnum target,
                                                                                                                   PriceRuleTypeEnum ruleType) {
        return PriceRule.builder()
                .id(1013)
                .name("DESCUENTO DEL 10% ONLY PAYMENT CONDITION, DELIVERY MODE, BUSINESS UNIT, AGENCY, ROLE TYPE, CUSTOMER, PARTY CLASSIFICATION")
                .accumulative(accumulative)
                .deliveryModes(deliveryModes)
                .paymentCondition(paymentCondition)
                .businessUnits(businessUnits)
                .agencies(agencies)
                .roleTypes(roleTypes)
                .customers(customers)
                .partyClassifications(partyClassifications)
                .output(createOutcomeOutput10Discount(ScaleTypeEnum.AMOUNT))
                .target(target)
                .ruleType(ruleType)
                .build();
    }

    public static PriceRule createRuleOnlyGeographicBoundary1014(List<PriceComponentRuleFactorGeographicBoundary> geographicBoundaries,
                                                                 boolean accumulative, TargetEnum target,
                                                                 PriceRuleTypeEnum ruleType) {
        return PriceRule.builder()
                .id(1014)
                .name("DESCUENTO DEL 10% ONLY GEOGRAPHIC BOUNDARY")
                .accumulative(accumulative)
                .geographicBoundaries(geographicBoundaries)
                .output(createOutcomeOutput10Discount(ScaleTypeEnum.AMOUNT))
                .target(target)
                .ruleType(ruleType)
                .build();
    }

    public static PriceRule createRuleOnlyDlvPymntBuUnitAgencyRoleCstmrPartyClasGeo1015(List<PriceComponentRuleFactorDeliveryMode> deliveryModes,
                                                                                        PaymentConditionsTypeEnum paymentCondition,
                                                                                        List<PriceComponentRuleFactorBusinessUnit> businessUnits,
                                                                                        List<PriceComponentRuleFactorAgency> agencies,
                                                                                        List<PriceComponentRuleFactorRoleType> roleTypes,
                                                                                        List<PriceComponentRuleFactorCustomer> customers,
                                                                                        List<PriceComponentRuleFactorPartyClassification> partyClassifications,
                                                                                        List<PriceComponentRuleFactorGeographicBoundary> geographicBoundaries,
                                                                                        boolean accumulative, TargetEnum target,
                                                                                        PriceRuleTypeEnum ruleType) {
        return PriceRule.builder()
                .id(1015)
                .name("DESCUENTO DEL 10% ONLY PAYMENT CONDITION, DELIVERY MODE, BUSINESS UNIT, AGENCY, ROLE TYPE, CUSTOMER, PARTY CLASSIFICATION, GEOGRAPHIC BOUNDARY")
                .accumulative(accumulative)
                .deliveryModes(deliveryModes)
                .paymentCondition(paymentCondition)
                .businessUnits(businessUnits)
                .agencies(agencies)
                .roleTypes(roleTypes)
                .customers(customers)
                .partyClassifications(partyClassifications)
                .geographicBoundaries(geographicBoundaries)
                .output(createOutcomeOutput10Discount(ScaleTypeEnum.AMOUNT))
                .target(target)
                .ruleType(ruleType)
                .build();
    }


    public static PriceRule createRuleOnlyProductSingleOutput1016(List<PriceComponentRuleSubjectProduct> products,
                                                                  ScaleTypeEnum validationType,
                                                                  boolean accumulative, TargetEnum target,
                                                                  PriceRuleTypeEnum ruleType) {
        return PriceRule.builder()
                .id(1016)
                .name("DESCUENTO DEL 10% ONLY PRODUCTS")
                .accumulative(accumulative)
                .products(products)
                .output(createOutcomeOutput10Discount(validationType))
                .target(target)
                .ruleType(ruleType)
                .validationType(validationType)
                .build();
    }

    public static PriceRule createRuleOnlyProductFrequencyOutput1017(List<PriceComponentRuleSubjectProduct> products,
                                                                     ScaleTypeEnum validationType,
                                                                     BigDecimal frequency,
                                                                     boolean accumulative, TargetEnum target,
                                                                     PriceRuleTypeEnum ruleType) {
        return PriceRule.builder()
                .id(1017)
                .name("DESCUENTO DEL 10% ONLY PRODUCTS (FREQUENCY)")
                .accumulative(accumulative)
                .products(products)
                .output(createOutcomeOutput10Discount(frequency, validationType))
                .outComeMode(OutComeModeEnum.FREQUENCY)
                .target(target)
                .validationType(validationType)
                .ruleType(ruleType)
                .build();
    }

    public static PriceRule createRuleOnlyProductScaleOutput1018(List<PriceComponentRuleSubjectProduct> products,
                                                                 ScaleTypeEnum validationType,
                                                                 List<PriceComponentRuleFactorScale> scales,
                                                                 boolean accumulative, TargetEnum target,
                                                                 PriceRuleTypeEnum ruleType) {
        return PriceRule.builder()
                .id(1018)
                .name("DESCUENTO DEL 10% and 15% ONLY PRODUCTS (SCALES)")
                .accumulative(accumulative)
                .products(products)
                .output(createOutcomeOutput10Discount(scales, validationType))
                .outComeMode(OutComeModeEnum.SCALE)
                .target(target)
                .ruleType(ruleType)
                .validationType(validationType)
                .build();
    }

    public static PriceRule createRuleOnlyProductFeaturesScaleOutput1019(List<PriceComponentRuleSubjectProductFeature> products,
                                                                         ScaleTypeEnum validationType,
                                                                         List<PriceComponentRuleFactorScale> scales,
                                                                         boolean accumulative, TargetEnum target,
                                                                         PriceRuleTypeEnum ruleType) {
        return PriceRule.builder()
                .id(1019)
                .name("DESCUENTO DEL 10% and 15% ONLY PRODUCT FEATURES (SCALES)")
                .accumulative(accumulative)
                .productFeatures(products)
                .output(createOutcomeOutput10Discount(scales, validationType))
                .target(target)
                .ruleType(ruleType)
                .validationType(validationType)
                .build();
    }

    public static PriceRule createRuleOnlyProductClassificationScaleOutput1020(List<PriceComponentRuleFactorProductClassification> classifications,
                                                                               ScaleTypeEnum validationType,
                                                                               List<PriceComponentRuleFactorScale> scales,
                                                                               boolean accumulative, TargetEnum target,
                                                                               PriceRuleTypeEnum ruleType) {
        return PriceRule.builder()
                .id(1020)
                .name("DESCUENTO DEL 10% ONLY PRODUCT CLASSIFICATION (SCALES)")
                .accumulative(accumulative)
                .productClassifications(classifications)
                .output(createOutcomeOutput10Discount(scales, validationType))
                .target(target)
                .ruleType(ruleType)
                .validationType(validationType)
                .build();
    }

    public static PriceRule createRule1021RetailWithProduct4448(boolean accumulative,
                                                                PriceRuleTypeEnum ruleType,
                                                                TargetEnum target, PriceComponentRuleOutcomeOutput outcome,
                                                                ScaleTypeEnum validationType, OutComeModeEnum outComeModeEnum) {
        List<PriceComponentRuleFactorBusinessUnit> businessUnits = new ArrayList<>();
        businessUnits.add(createBusinessUnitRetail());
        //Parties
        List<PriceComponentRuleFactorCustomer> parties = new ArrayList<>();
        parties.add(createPartyRoleCustomerRamiro());
        //boundary
        List<PriceComponentRuleFactorGeographicBoundary> geographicBoundaries = new ArrayList<>();
        geographicBoundaries.add(createGeographicBoundarySC());
        geographicBoundaries.add(createGeographicBoundarySimpsons());
        //products in the rule
        List<PriceComponentRuleSubjectProduct> priceProducts = new ArrayList<>();
        priceProducts.add(createProductRule48());
        priceProducts.add(createProductRule44());
        //rule
        return PriceRule.builder()
                .id(1021)
                .name("REGLA PARA FARMACIAS Y SEGMENTO DE CLIENTES RETAIL")
                .accumulative(accumulative)
                .fromDate(new Date())
                .products(priceProducts)
                .productFeatures(Collections.emptyList())
                .productClassifications(Collections.emptyList())
                .businessUnits(businessUnits)
                .customers(parties)
                .ruleType(ruleType)
                .geographicBoundaries(geographicBoundaries)
                .partyClassifications(createPartyClassificationList())
                .validationType(validationType)
                .output(outcome)
                .target(target)
                .outComeMode(outComeModeEnum)
                .build();
    }

    public static PriceRule createRule1022RetailWithProduct44(boolean accumulative, PriceRuleTypeEnum ruleType, TargetEnum target, PriceComponentRuleOutcomeOutput output, ScaleTypeEnum validationType, OutComeModeEnum outComeModeEnum) {
        List<PriceComponentRuleFactorBusinessUnit> businessUnits = new ArrayList<>(0);
        businessUnits.add(createBusinessUnitRetail());
        //Parties
        List<PriceComponentRuleFactorCustomer> parties = new ArrayList<>();
        parties.add(createPartyRoleCustomerRamiro());

        //products in the rule
        List<PriceComponentRuleSubjectProduct> priceProducts = new ArrayList<>();
        priceProducts.add(createProductRule44());
        //rule
        return PriceRule.builder()
                .id(1022)
                .name("REGLA APLICABLE AL CLIENTE RAMIRO, SEGMENTO DE CLIENTES RETAIL EN PRODUCTOS: 44")
                .accumulative(accumulative)
                .fromDate(new Date())
                .products(priceProducts)
                .productFeatures(Collections.emptyList())
                .productClassifications(Collections.emptyList())
                .businessUnits(businessUnits)
                .customers(parties)
                .ruleType(ruleType)
                .output(output)
                .target(target)
                .validationType(validationType)
                .outComeMode(outComeModeEnum)
                .build();
    }

    public static PriceRule createRule1023RetailRamiroAllFactors(List<PriceComponentRuleSubjectProduct> priceProducts, boolean accumulative,
                                                                PriceRuleTypeEnum ruleType,
                                                                TargetEnum target, PriceComponentRuleOutcomeOutput output,
                                                                ScaleTypeEnum validationType, OutComeModeEnum outComeModeEnum) {
        List<PriceComponentRuleFactorBusinessUnit> businessUnits = new ArrayList<>();
        businessUnits.add(createBusinessUnitRetail());
        //Parties
        List<PriceComponentRuleFactorCustomer> parties = new ArrayList<>();
        parties.add(createPartyRoleCustomerRamiro());
        //boundary
        List<PriceComponentRuleFactorGeographicBoundary> geographicBoundaries = new ArrayList<>();
        geographicBoundaries.add(createGeographicBoundarySC());
        geographicBoundaries.add(createGeographicBoundarySimpsons());
        //rule
        return PriceRule.builder()
                .id(1023)
                .name("REGLA PARA RAMIRO, RETAIL, ALL PARTY CLASSIFICATIONS, GEO [SC, SIMPSONS]")
                .accumulative(accumulative)
                .fromDate(new Date())
                .products(priceProducts)
                .productFeatures(Collections.emptyList())
                .productClassifications(Collections.emptyList())
                .businessUnits(businessUnits)
                .customers(parties)
                .ruleType(ruleType)
                .geographicBoundaries(geographicBoundaries)
                .partyClassifications(createPartyClassificationList())
                .validationType(validationType)
                .output(output)
                .target(target)
                .outComeMode(outComeModeEnum)
                .build();
    }


    public static PriceRule createRuleOnlyProducts1024(List<PriceComponentRuleSubjectProduct> priceProducts,
                                                       boolean accumulative, TargetEnum target,
                                                       PriceRuleTypeEnum ruleType,
                                                       ScaleTypeEnum validationType,
                                                       PriceComponentRuleOutcomeOutput output,
                                                       OutComeModeEnum outcomeMode) {
        return PriceRule.builder()
                .id(1024)
                .name("RULE ONLY PRODUCTS")
                .accumulative(accumulative)
                .products(priceProducts)
                .output(output)
                .target(target)
                .ruleType(ruleType)
                .validationType(validationType)
                .target(target)
                .outComeMode(outcomeMode)
                .build();
    }

    public static PriceRule createRuleOnlyProductFeatures(long id, List<PriceComponentRuleSubjectProductFeature> productFeatures,
                                                       boolean accumulative, TargetEnum target,
                                                       PriceRuleTypeEnum ruleType,
                                                       ScaleTypeEnum validationType,
                                                       PriceComponentRuleOutcomeOutput output,
                                                       OutComeModeEnum outcomeMode) {
        return PriceRule.builder()
                .id(id)
                .name("RULE ONLY PRODUCTS FEATURES")
                .accumulative(accumulative)
                .productFeatures(productFeatures)
                .output(output)
                .target(target)
                .ruleType(ruleType)
                .validationType(validationType)
                .target(target)
                .outComeMode(outcomeMode)
                .build();
    }

    public static PriceRule createRuleOnlyProductClassifications(long id, List<PriceComponentRuleFactorProductClassification> productClassifications,
                                                       boolean accumulative, TargetEnum target,
                                                       PriceRuleTypeEnum ruleType,
                                                       ScaleTypeEnum validationType,
                                                       PriceComponentRuleOutcomeOutput output,
                                                       OutComeModeEnum outcomeMode) {
        return PriceRule.builder()
                .id(id)
                .name("RULE ONLY PRODUCTS CLASSIFICATIONS")
                .accumulative(accumulative)
                .productClassifications(productClassifications)
                .output(output)
                .target(target)
                .ruleType(ruleType)
                .validationType(validationType)
                .target(target)
                .outComeMode(outcomeMode)
                .build();
    }

    public static PriceRule createRuleOnlyProducts(long id, List<PriceComponentRuleSubjectProduct> priceProducts,
                                                       boolean accumulative, TargetEnum target,
                                                       PriceRuleTypeEnum ruleType,
                                                       ScaleTypeEnum validationType,
                                                       PriceComponentRuleOutcomeOutput output,
                                                       OutComeModeEnum outcomeMode) {
        return PriceRule.builder()
                .id(id)
                .name("RULE ONLY PRODUCTS")
                .accumulative(accumulative)
                .products(priceProducts)
                .output(output)
                .target(target)
                .ruleType(ruleType)
                .validationType(validationType)
                .target(target)
                .outComeMode(outcomeMode)
                .build();
    }

    public static PriceRule createRuleOnlyProducts(long id,
                                                   String name, List<PriceComponentRuleSubjectProduct> priceProducts,
                                                   boolean accumulative, TargetEnum target,
                                                   PriceRuleTypeEnum ruleType,
                                                   ScaleTypeEnum validationType,
                                                   PriceComponentRuleOutcomeOutput output,
                                                   OutComeModeEnum outcomeMode) {
        return PriceRule.builder()
                .id(id)
                .name(name)
                .accumulative(accumulative)
                .products(priceProducts)
                .output(output)
                .target(target)
                .ruleType(ruleType)
                .validationType(validationType)
                .target(target)
                .outComeMode(outcomeMode)
                .build();
    }

    public static PriceRule createRuleOnlyBusinessUnit(long id, List<PriceComponentRuleFactorBusinessUnit> businessUnits,
                                                   boolean accumulative, TargetEnum target,
                                                   PriceRuleTypeEnum ruleType,
                                                   ScaleTypeEnum validationType,
                                                   PriceComponentRuleOutcomeOutput output,
                                                   OutComeModeEnum outcomeMode) {
        return PriceRule.builder()
                .id(id)
                .name("RULE ONLY PRODUCTS")
                .accumulative(accumulative)
                .businessUnits(businessUnits)
                .output(output)
                .target(target)
                .ruleType(ruleType)
                .validationType(validationType)
                .target(target)
                .outComeMode(outcomeMode)
                .build();
    }
    //endregion

    //region Create Price Rules
    public static PriceRule createRule10DiscountPercentageRetailWithProduct44(boolean accumulative, PriceRuleTypeEnum ruleType, TargetEnum target) {
        List<PriceComponentRuleFactorBusinessUnit> businessUnits = new ArrayList<>(0);
        businessUnits.add(createBusinessUnitRetail());
        //Parties
        List<PriceComponentRuleFactorCustomer> parties = new ArrayList<>();
        parties.add(createPartyRoleCustomerRamiro());

        //products in the rule
        List<PriceComponentRuleSubjectProduct> priceProducts = new ArrayList<>();
        priceProducts.add(createProductRule44());
        //rule
        return PriceRule.builder()
                .id(10)
                .name("DESCUENTO DEL 10% AL CLIENTE RAMIRO, SEGMENTO DE CLIENTES RETAIL EN PRODUCTOS: 44")
                .accumulative(accumulative)
                .fromDate(new Date())
                .products(priceProducts)
                .productFeatures(Collections.emptyList())
                .productClassifications(Collections.emptyList())
                .businessUnits(businessUnits)
                .customers(parties)
                .ruleType(ruleType)
                .output(createOutcomeOutput10Discount(ScaleTypeEnum.AMOUNT))
                .target(target)
                .validationType(ScaleTypeEnum.AMOUNT)
                .build();
    }

    public static PriceRule createRule1013RetailWithProduct44(boolean accumulative, PriceRuleTypeEnum ruleType, TargetEnum target, PriceComponentRuleOutcomeOutput output, ScaleTypeEnum validationType) {
        List<PriceComponentRuleFactorBusinessUnit> businessUnits = new ArrayList<>(0);
        businessUnits.add(createBusinessUnitRetail());
        //Parties
        List<PriceComponentRuleFactorCustomer> parties = new ArrayList<>();
        parties.add(createPartyRoleCustomerRamiro());

        //products in the rule
        List<PriceComponentRuleSubjectProduct> priceProducts = new ArrayList<>();
        priceProducts.add(createProductRule44());
        //rule
        return PriceRule.builder()
                .id(1013)
                .name("REGLA APLICABLE AL CLIENTE RAMIRO, SEGMENTO DE CLIENTES RETAIL EN PRODUCTOS: 44")
                .accumulative(accumulative)
                .fromDate(new Date())
                .products(priceProducts)
                .productFeatures(Collections.emptyList())
                .productClassifications(Collections.emptyList())
                .businessUnits(businessUnits)
                .customers(parties)
                .ruleType(ruleType)
                .output(output)
                .target(target)
                .validationType(validationType)
                .build();
    }

    public static PriceRule createRule10DiscountPercentageRetailWithProduct4448(boolean accumulative,
                                                                                PriceRuleTypeEnum ruleType,
                                                                                TargetEnum target) {
        List<PriceComponentRuleFactorBusinessUnit> businessUnits = new ArrayList<>();
        businessUnits.add(createBusinessUnitRetail());
        //Parties
        List<PriceComponentRuleFactorCustomer> parties = new ArrayList<>();
        parties.add(createPartyRoleCustomerRamiro());
        //boundary
        List<PriceComponentRuleFactorGeographicBoundary> geographicBoundaries = new ArrayList<>();
        geographicBoundaries.add(createGeographicBoundarySC());
        geographicBoundaries.add(createGeographicBoundarySimpsons());
        //products in the rule
        List<PriceComponentRuleSubjectProduct> priceProducts = new ArrayList<>();
        priceProducts.add(createProductRule48());
        priceProducts.add(createProductRule44());
        //rule
        return PriceRule.builder()
                .id(1011)
                .name("DESCUENTO DEL 10% A FARMACIAS Y SEGMENTO DE CLIENTES RETAIL")
                .accumulative(accumulative)
                .fromDate(new Date())
                .products(priceProducts)
                .productFeatures(Collections.emptyList())
                .productClassifications(Collections.emptyList())
                .businessUnits(businessUnits)
                .customers(parties)
                .ruleType(ruleType)
                .geographicBoundaries(geographicBoundaries)
                .partyClassifications(createPartyClassificationList())
                .validationType(ScaleTypeEnum.AMOUNT)
                .output(createOutcomeOutput10Discount(ScaleTypeEnum.AMOUNT))
                .target(target)
                .build();
    }

    public static PriceRule createRule1012RetailWithProduct4448(boolean accumulative,
                                                                PriceRuleTypeEnum ruleType,
                                                                TargetEnum target, PriceComponentRuleOutcomeOutput outcome,
                                                                ScaleTypeEnum validationType) {
        List<PriceComponentRuleFactorBusinessUnit> businessUnits = new ArrayList<>();
        businessUnits.add(createBusinessUnitRetail());
        //Parties
        List<PriceComponentRuleFactorCustomer> parties = new ArrayList<>();
        parties.add(createPartyRoleCustomerRamiro());
        //boundary
        List<PriceComponentRuleFactorGeographicBoundary> geographicBoundaries = new ArrayList<>();
        geographicBoundaries.add(createGeographicBoundarySC());
        geographicBoundaries.add(createGeographicBoundarySimpsons());
        //products in the rule
        List<PriceComponentRuleSubjectProduct> priceProducts = new ArrayList<>();
        priceProducts.add(createProductRule48());
        priceProducts.add(createProductRule44());
        //rule
        return PriceRule.builder()
                .id(1012)
                .name("REGLA PARA FARMACIAS Y SEGMENTO DE CLIENTES RETAIL")
                .accumulative(accumulative)
                .fromDate(new Date())
                .products(priceProducts)
                .productFeatures(Collections.emptyList())
                .productClassifications(Collections.emptyList())
                .businessUnits(businessUnits)
                .customers(parties)
                .ruleType(ruleType)
                .geographicBoundaries(geographicBoundaries)
                .partyClassifications(createPartyClassificationList())
                .validationType(validationType)
                .output(outcome)
                .target(target)
                .build();
    }


    public static PriceRule createRule15DiscountPercentageRetailWithProduct48(boolean accumulative, PriceRuleTypeEnum ruleType, TargetEnum target) {
        List<PriceComponentRuleFactorBusinessUnit> businessUnits = new ArrayList<>();
        businessUnits.add(createBusinessUnitRetail());
        //products in the rule
        List<PriceComponentRuleSubjectProduct> priceProducts = new ArrayList<>();
        priceProducts.add(createProductRule48());
        //rule
        return PriceRule.builder()
                .id(12)
                .name("DESCUENTO DEL 15% A CLIENTES RETAIL CON PRODUCTO: 48")
                .accumulative(accumulative)
                .fromDate(new Date())
                .products(priceProducts)
                .productFeatures(Collections.emptyList())
                .productClassifications(Collections.emptyList())
                .businessUnits(businessUnits)
                .customers(Collections.emptyList())
                .ruleType(ruleType)
                .validationType(ScaleTypeEnum.AMOUNT)
                .output(createOutcomeOutputDiscountPerc(new BigDecimal(15), ScaleTypeEnum.AMOUNT))
                .target(target)
                .build();
    }

    public static PriceRule createRule1014RetailWithProduct48(boolean accumulative, PriceRuleTypeEnum ruleType, TargetEnum target, PriceComponentRuleOutcomeOutput output, ScaleTypeEnum validationType) {
        List<PriceComponentRuleFactorBusinessUnit> businessUnits = new ArrayList<>();
        businessUnits.add(createBusinessUnitRetail());
        //products in the rule
        List<PriceComponentRuleSubjectProduct> priceProducts = new ArrayList<>();
        priceProducts.add(createProductRule48());
        //rule
        return PriceRule.builder()
                .id(1014)
                .name("REGLA A CLIENTES RETAIL CON PRODUCTO: 48")
                .accumulative(accumulative)
                .fromDate(new Date())
                .products(priceProducts)
                .productFeatures(Collections.emptyList())
                .productClassifications(Collections.emptyList())
                .businessUnits(businessUnits)
                .customers(Collections.emptyList())
                .ruleType(ruleType)
                .validationType(validationType)
                .output(output)
                .target(target)
                .build();
    }

    public static PriceRule createRule100DiscountAmountRetail(boolean accumulative, PriceRuleTypeEnum ruleType, TargetEnum target) {
        List<PriceComponentRuleFactorBusinessUnit> businessUnits = new ArrayList<>();
        businessUnits.add(createBusinessUnitRetail());
        //rule
        return PriceRule.builder()
                .id(420)
                .name("DESCUENTO DEL 100 Bs A CLIENTES RETAIL")
                .accumulative(accumulative)
                .fromDate(new Date())
                .products(Collections.emptyList())
                .productFeatures(Collections.emptyList())
                .productClassifications(Collections.emptyList())
                .businessUnits(businessUnits)
                .customers(Collections.emptyList())
                .ruleType(ruleType)
                .target(target)
                .validationType(ScaleTypeEnum.AMOUNT)
                .output(createOutcomeOutput10Discount(ScaleTypeEnum.AMOUNT))
                .build();
    }

    public static PriceRule createRule1015Retail(boolean accumulative, PriceRuleTypeEnum ruleType, TargetEnum target, PriceComponentRuleOutcomeOutput output, ScaleTypeEnum validationType) {
        List<PriceComponentRuleFactorBusinessUnit> businessUnits = new ArrayList<>();
        businessUnits.add(createBusinessUnitRetail());
        //rule
        return PriceRule.builder()
                .id(1015)
                .name("REGLA APLICABLE A CLIENTES RETAIL")
                .accumulative(accumulative)
                .fromDate(new Date())
                .products(Collections.emptyList())
                .productFeatures(Collections.emptyList())
                .productClassifications(Collections.emptyList())
                .businessUnits(businessUnits)
                .customers(Collections.emptyList())
                .ruleType(ruleType)
                .target(target)
                .validationType(validationType)
                .output(output)
                .build();
    }

    public static PriceRule createRule25SurchargePercentageProduct48(boolean accumulative, PriceRuleTypeEnum ruleType) {
        //products in the rule
        List<PriceComponentRuleSubjectProduct> priceProducts = new ArrayList<>();
        priceProducts.add(createProductRule48());
        //output
        PriceComponentRuleOutcome outcome = PriceComponentRuleOutcome.builder()
                .outcomeType(PriceComponentOutcomeTypeEnum.SURCHARGE_PERCENTAGE)
                .discountPercentage(new BigDecimal(25))
                .build();
        //rule
        return PriceRule.builder()
                .id(1)
                .name("DESCUENTO DEL 10% A FARMACIAS Y SEGMENTO DE CLIENTES RETAIL")
                .accumulative(accumulative)
                .fromDate(new Date())
                .products(priceProducts)
                .productFeatures(Collections.emptyList())
                .productClassifications(Collections.emptyList())
                .businessUnits(Collections.emptyList())
                .customers(Collections.emptyList())
                .ruleType(ruleType)
                .output(createOutcomeOutput10Discount(ScaleTypeEnum.AMOUNT))
                .build();
    }

    public static PriceRule createRule15SurchargeAmountTelmaRamiro(boolean accumulative, PriceRuleTypeEnum ruleType) {
        List<PriceComponentRuleFactorCustomer> parties = new ArrayList<>();
        parties.add(createPartyRoleCustomerTelma());
        parties.add(createPartyRoleCustomerRamiro());
        //output
        PriceComponentRuleOutcome outcome = PriceComponentRuleOutcome.builder()
                .outcomeType(PriceComponentOutcomeTypeEnum.SURCHARGE_AMOUNT)
                .discountPercentage(new BigDecimal(15))
                .build();
        //rule
        return PriceRule.builder()
                .id(101)
                .name("DESCUENTO DEL 10% A FARMACIAS Y SEGMENTO DE CLIENTES RETAIL")
                .accumulative(accumulative)
                .fromDate(new Date())
                .products(Collections.emptyList())
                .productFeatures(Collections.emptyList())
                .productClassifications(Collections.emptyList())
                .businessUnits(Collections.emptyList())
                .customers(parties)
                .ruleType(ruleType)
                .output(createOutcomeOutput10Discount(ScaleTypeEnum.AMOUNT))
                .build();
    }

    public static PriceRule createRuleProductBonification4748OnlyWithPartyClassifications(List<PriceComponentRuleFactorDeliveryMode> deliveryModes, PaymentConditionsTypeEnum paymentCondition,
                                                                                          boolean accumulative, PriceRuleTypeEnum type) {
        //Party Classifications
        List<PriceComponentRuleFactorPartyClassification> partyClassifications = new ArrayList<>();
        partyClassifications.add(createPartyClassificationCustomerOztomizados());
        partyClassifications.add(createPartyClassificationEmployeeFirstLvl());
        //output product bonus
        List<PriceComponentRuleOutcomeProduct> productBonusList = new ArrayList<>();
        productBonusList.add(createProductBonus47(new BigDecimal(2)));
        productBonusList.add(createProductBonus48(new BigDecimal(2)));
        //output
        PriceComponentRuleOutcome outcome = PriceComponentRuleOutcome.builder()
                .outcomeType(PriceComponentOutcomeTypeEnum.PRODUCT)
                .products(productBonusList)
                .build();
        //rule
        return PriceRule.builder()
                .id(101)
                .name("BONIFICACION DE 2 PRODUCTOS (47,48) Con PartyClassification {Clientes Oztomizados, Empleados lvl 1} ")
                .accumulative(accumulative)
                .fromDate(new Date())
                .products(Collections.emptyList())
                .productFeatures(Collections.emptyList())
                .productClassifications(Collections.emptyList())
                .businessUnits(Collections.emptyList())
                .customers(Collections.emptyList())
                .partyClassifications(partyClassifications)
                .geographicBoundaries(Collections.emptyList())
                .ruleType(type)
                .deliveryModes(deliveryModes)
                .paymentCondition(paymentCondition)
                .output(createOutcomeOutput10Discount(ScaleTypeEnum.AMOUNT))
                .build();
    }

    public static PriceRule createRuleProductBonification47WithAllFactors(Date fromDate, Date thruDate,
                                                                          int fromQty, int thruQty,
                                                                          BigDecimal fromAmount, BigDecimal thruAmount,
                                                                          BigDecimal fromTotalAmount, BigDecimal thruTotalAmount,
                                                                          List<PriceComponentRuleFactorDeliveryMode> deliveryModes,
                                                                          PaymentConditionsTypeEnum paymentConditions,
                                                                          boolean accumulative, PriceRuleTypeEnum type) {
        //customers
        List<PriceComponentRuleFactorCustomer> parties = new ArrayList<>();
        parties.add(createPartyRoleCustomerTelma());
        parties.add(createPartyRoleCustomerRamiroAll());
        //agencies
        List<PriceComponentRuleFactorAgency> agencies = new ArrayList<>();
        agencies.add(createAgencyCBBA());
        agencies.add(createAgencySC());
        //business Units
        List<PriceComponentRuleFactorBusinessUnit> businessUnits = new ArrayList<>();
        businessUnits.add(createBusinessUnitRetail());
        businessUnits.add(createBusinessUnitEntidadesPublicas());
        businessUnits.add(createBusinessUnitEntidadesPrivadas());
        businessUnits.add(createBusinessUnitDistribuidores());
        //PRODUCTS
        List<PriceComponentRuleSubjectProduct> products = new ArrayList<>();
        products.add(createProductRule44());
        products.add(createProductRule48());
        //Product Features
        List<PriceComponentRuleSubjectProductFeature> productFeatures = new ArrayList<>();
        productFeatures.add(createProductFeatureBrandHollister());
        //Product Classifications
        List<PriceComponentRuleFactorProductClassification> productClassifications = new ArrayList<>();
        productClassifications.add(createProductClassificationCaboPBisturi());
        productClassifications.add(createProductClassificationHaloCefalico());
        //Party Classifications
        List<PriceComponentRuleFactorPartyClassification> partyClassifications = new ArrayList<>();
        partyClassifications.add(createPartyClassificationCustomerOztomizados());
        partyClassifications.add(createPartyClassificationEmployeeFirstLvl());
        //Geo
        List<PriceComponentRuleFactorGeographicBoundary> geographicBoundaries = new ArrayList<>();
        geographicBoundaries.add(createGeographicBoundaryLP());
        geographicBoundaries.add(createGeographicBoundarySC());
        //output product bonus
        List<PriceComponentRuleOutcomeProduct> productBonusList = new ArrayList<>();
        productBonusList.add(createProductBonus47(new BigDecimal(5)));
        productBonusList.add(createProductBonus48(BigDecimal.ONE));
        //output
        PriceComponentRuleOutcome outcome = PriceComponentRuleOutcome.builder()
                .outcomeType(PriceComponentOutcomeTypeEnum.PRODUCT)
                .products(productBonusList)
                .build();
        //rule
        return PriceRule.builder()
                .id(101)
                .name("BONIFICACION DE 2 PRODUCTOS (47) A CLIENTES RETAIL, CON GEO {LA_PAZ, SANTA_CRUZ}, CON PARTY {Ramiro o Telma}, PartyClassification {} ")
                .accumulative(accumulative)
                .fromDate(fromDate)
                .thruDate(thruDate)
                .products(products)
                .productFeatures(productFeatures)
                .productClassifications(productClassifications)
                .businessUnits(businessUnits)
                .customers(parties)
                .partyClassifications(partyClassifications)
                .agencies(agencies)
                .geographicBoundaries(geographicBoundaries)
                .ruleType(type)
                .deliveryModes(deliveryModes)
                .paymentCondition(paymentConditions)
                .output(createOutcomeOutput10Discount(ScaleTypeEnum.AMOUNT))
                .roleTypes(Collections.singletonList(createRoleTypeCustomer()))
                .build();
    }

    public static PriceRule createEmptyRule(boolean accumulative, PriceRuleTypeEnum type) {
        return PriceRule.builder()
                .id(RandomUtil.getRandomInt(RANGE_RULE_MIN, RANGE_RULE_MAX))
                .name(RandomUtil.getRandomRuleName())
                .ruleType(type)
                .accumulative(accumulative)
                .build();
    }
    //endregion

    //region Create Product Subject
    public static PriceComponentRuleSubjectProduct createProductRule48() {
        return PriceComponentRuleSubjectProduct.builder()
                .id(48)
                .name("CABO D/BISTURI #4 D/12CM STANDARD")
                .code("13069")
                .unitMeasure(PriceComponentRuleUnitMeasure.builder()
                        .id(142)
                        .abbreviation("UN")
                        .name("Unidad")
                        .build())
                .originalPrice(new BigDecimal(100))
                .build();
    }

    public static PriceComponentRuleSubjectProduct createProductRule44() {
        return PriceComponentRuleSubjectProduct.builder()
                .id(44)
                .name("HALO CEFALICO P/OXIGENACION # 1 MENOR A 1000GR")
                .code("11605")
                .unitMeasure(PriceComponentRuleUnitMeasure.builder()
                        .id(32)
                        .abbreviation("sb")
                        .name("Sobre")
                        .build())
                .originalPrice(new BigDecimal(300))
                .build();
    }

    public static PriceComponentRuleSubjectProduct createProductRule47() {
        return PriceComponentRuleSubjectProduct.builder()
                .id(47)
                .name("CABO D/BISTURI #3 D/12CM STANDARD")
                .code("13068")
                .unitMeasure(PriceComponentRuleUnitMeasure.builder()
                        .id(142)
                        .abbreviation("UN")
                        .name("Unidad")
                        .build())
                .originalPrice(new BigDecimal(100))
                .build();
    }

    public static PriceComponentRuleSubjectProduct createProductRule46() {
        return PriceComponentRuleSubjectProduct.builder()
                .id(46)
                .name("HILO D/ACIDO POLIGLACTIN #3/0 D/70CM C/AGUJA 1/2 CIRCULO PUNTA REDONDA D/36MM VICRYL")
                .code("11700")
                .unitMeasure(PriceComponentRuleUnitMeasure.builder()
                        .id(32)
                        .abbreviation("sb")
                        .name("Sobre")
                        .build())
                .originalPrice(new BigDecimal(500))
                .build();
    }

    public static PriceComponentRuleSubjectProduct createProductRule49() {
        return PriceComponentRuleSubjectProduct.builder()
                .id(49)
                .name("HILO CATGUT SIMPLE #1 D/70CM C/AGUJA 1/2 CIRCULO PUNTA REDONDA D/50MM")
                .code("11739")
                .unitMeasure(PriceComponentRuleUnitMeasure.builder()
                        .id(142)
                        .abbreviation("UN")
                        .name("Unidad")
                        .build())
                .originalPrice(new BigDecimal(250))
                .build();
    }


    public static PriceComponentRuleSubjectProduct createProductRule70() {
        return PriceComponentRuleSubjectProduct.builder()
                .id(70)
                .code("300868")
                .name("VAJILLERO LIMON 5000 ML NI")
                .unitMeasure(PriceComponentRuleUnitMeasure.builder()
                        .id(70)
                        .abbreviation("Bidón")
                        .name("Bidón")
                        .build())
                .originalPrice(new BigDecimal(48))
                .build();
    }

    public static PriceComponentRuleSubjectProduct createProductRule71() {
        return PriceComponentRuleSubjectProduct.builder()
                .id(71)
                .code("300884")
                .name("SACA GRASA EXTRA FUERTE 5000 ML NI")
                .unitMeasure(PriceComponentRuleUnitMeasure.builder()
                        .id(72)
                        .abbreviation("Bidón")
                        .name("Bidón")
                        .build())
                .originalPrice(new BigDecimal(55))
                .build();
    }

    public static PriceComponentRuleSubjectProduct createProductRule72() {
        return PriceComponentRuleSubjectProduct.builder()
                .id(72)
                .code("300372")
                .name("LIMPIA PISO CEDRON TE VERDE 5000 ml")
                .unitMeasure(PriceComponentRuleUnitMeasure.builder()
                        .id(72)
                        .abbreviation("Bidón")
                        .name("Bidón")
                        .build())
                .originalPrice(new BigDecimal(35))
                .build();
    }

    public static PriceComponentRuleSubjectProduct createProductRule300175() {
        return PriceComponentRuleSubjectProduct.builder()
                .id(300175)
                .code("300175")
                .name("MAYONESA KRIS SOBRE 50 ml")
                .unitMeasure(PriceComponentRuleUnitMeasure.builder()
                        .id(300175)
                        .abbreviation("Display")
                        .name("Display")
                        .build())
                .originalPrice(new BigDecimal(28))
                .build();
    }

    //endregion End Product Subject

    //region Create Product Classification
    public static PriceComponentRuleFactorProductClassification createProductClassificationPediatria() {
        return PriceComponentRuleFactorProductClassification.builder()
                .id(34)
                .name("CUIDADOS GENERALES/PEDIATRIA")
                .path("1/2")
                .build();
    }

    public static PriceComponentRuleFactorProductClassification createProductClassificationHaloCefalico() {
        return PriceComponentRuleFactorProductClassification.builder()
                .id(61)
                .name("CUIDADOS GENERALES/PEDIATRIA/NEONATOLOGIA/DISPOSITIVO MEDICO/HALO CEFALICO")
                .path("1/2/34/48/42")
                .build();
    }

    public static PriceComponentRuleFactorProductClassification createProductClassificationNeonatologia() {
        return PriceComponentRuleFactorProductClassification.builder()
                .id(66)
                .name("CUIDADOS GENERALES/PEDIATRIA/NEONATOLOGIA")
                .path("1/2/34/48")
                .build();
    }

    public static PriceComponentRuleFactorProductClassification createProductClassificationAdministrativo() {
        return PriceComponentRuleFactorProductClassification.builder()
                .id(69)
                .name("ADMINISTRACION")
                .path("98")
                .build();
    }

    public static PriceComponentRuleFactorProductClassification createProductClassificationQuirofano() {
        return PriceComponentRuleFactorProductClassification.builder()
                .id(39)
                .name("BLOQUE QUIRURGICO/QUIROFANO")
                .path("12/35")
                .build();
    }

    public static PriceComponentRuleFactorProductClassification createProductClassificationCaboPBisturi() {
        return PriceComponentRuleFactorProductClassification.builder()
                .id(49)
                .name("BLOQUE QUIRURGICO/QUIROFANO/INSTRUMENTAL/CABO P/BISTURI")
                .path("12/35/21/50/44/24")
                .build();
    }
    //endregion

    //region Create Party Classifications
    public static List<PriceComponentRuleFactorPartyClassification> createPartyClassificationList() {
        List<PriceComponentRuleFactorPartyClassification> partyClassifications = new ArrayList<>();
        partyClassifications.add(createPartyClassificationCustomerOztomizados());
        partyClassifications.add(createPartyClassificationEmployeeFirstLvl());
        partyClassifications.add(createPartyClassificationEmployeeSecondLvl());
        partyClassifications.add(createPartyClassificationEmployeeThirthLvl());
        partyClassifications.add(createPartyClassificationAdministrativos());
        partyClassifications.add(createPartyClassificationDirectivos());
        return partyClassifications;
    }

    public static PriceComponentRuleFactorPartyClassification createPartyClassificationCustomerOztomizados() {
        return PriceComponentRuleFactorPartyClassification.builder()
                .id(601)
                .name("Clientes Oztomizados")
                .build();
    }

    public static PriceComponentRuleFactorPartyClassification createPartyClassificationEmployeeFirstLvl() {
        return PriceComponentRuleFactorPartyClassification.builder()
                .id(602)
                .name("Empleados de Primer Nivel")
                .build();
    }

    public static PriceComponentRuleFactorPartyClassification createPartyClassificationEmployeeSecondLvl() {
        return PriceComponentRuleFactorPartyClassification.builder()
                .id(603)
                .name("Empleados de Segundo Nivel")
                .build();
    }

    public static PriceComponentRuleFactorPartyClassification createPartyClassificationEmployeeThirthLvl() {
        return PriceComponentRuleFactorPartyClassification.builder()
                .id(604)
                .name("Empleados de Tercer Nivel")
                .build();
    }

    public static PriceComponentRuleFactorPartyClassification createPartyClassificationAdministrativos() {
        return PriceComponentRuleFactorPartyClassification.builder()
                .id(605)
                .name("Ejecutivos / Administracion")
                .build();
    }

    public static PriceComponentRuleFactorPartyClassification createPartyClassificationDirectivos() {
        return PriceComponentRuleFactorPartyClassification.builder()
                .id(606)
                .name("Directivos de la Firma")
                .build();
    }
    //endregion

    //region Create Outcome Product Bonus
    public static PriceComponentRuleOutcomeProduct createProductBonus47(BigDecimal qty) {
        return PriceComponentRuleOutcomeProduct.builder()
                .id(47)
                .name("CABO D/BISTURI #3 D/12CM STANDARD")
                .code("13068")
                .unitMeasure(PriceComponentRuleUnitMeasure.builder()
                        .id(142)
                        .abbreviation("UN")
                        .name("Unidad")
                        .build())
                .originalPrice(new BigDecimal(200))
                .qty(qty)
                .build();
    }

    public static PriceComponentRuleOutcomeProduct createProductBonus48(BigDecimal qty) {
        return PriceComponentRuleOutcomeProduct.builder()
                .id(48)
                .name("CABO D/BISTURI #4 D/12CM STANDARD")
                .code("13069")
                .unitMeasure(PriceComponentRuleUnitMeasure.builder()
                        .id(142)
                        .abbreviation("UN")
                        .name("Unidad")
                        .build())
                .originalPrice(new BigDecimal(100))
                .qty(qty)
                .build();
    }

    public static PriceComponentRuleOutcomeProduct createProductBonus46(BigDecimal qty) {
        return PriceComponentRuleOutcomeProduct.builder()
                .id(46)
                .name("SERVICIO DE ENTREGA")
                .code("60098")
                .unitMeasure(PriceComponentRuleUnitMeasure.builder()
                        .id(142)
                        .abbreviation("UN")
                        .name("Unidad")
                        .build())
                .originalPrice(new BigDecimal(15))
                .qty(qty)
                .build();
    }

    public static PriceComponentRuleOutcomeProduct createProductBonus70(BigDecimal qty) {
        return PriceComponentRuleOutcomeProduct.builder()
                .id(70)
                .code("300866")
                .name("VAJILLERO LIMON 1050 ML NI")
                .unitMeasure(PriceComponentRuleUnitMeasure.builder()
                        .id(70)
                        .abbreviation("Botella")
                        .name("Botella")
                        .build())
                .originalPrice(new BigDecimal(11))
                .qty(qty)
                .build();
    }

    public static PriceComponentRuleOutcomeProduct createProductBonus71(BigDecimal qty) {
        return PriceComponentRuleOutcomeProduct.builder()
                .id(71)
                .code("300868")
                .name("VAJILLERO LIMON 5000 ML NI")
                .unitMeasure(PriceComponentRuleUnitMeasure.builder()
                        .id(71)
                        .abbreviation("Bidón")
                        .name("Bidón")
                        .build())
                .originalPrice(new BigDecimal(48))
                .qty(qty)
                .build();
    }

    public static PriceComponentRuleOutcomeProduct createProductBonus300107Display(BigDecimal qty) {
        return PriceComponentRuleOutcomeProduct.builder()
                .id(300107)
                .code("300107")
                .name("KETCHUP KRIS SOBRE 50 g")
                .unitMeasure(PriceComponentRuleUnitMeasure.builder()
                        .id(3001070)
                        .abbreviation("Display")
                        .name("Display")
                        .build())
                .originalPrice(new BigDecimal(28))
                .qty(qty)
                .build();
    }

    public static PriceComponentRuleOutcomeProduct createProductBonus300107Caja(BigDecimal qty) {
        return PriceComponentRuleOutcomeProduct.builder()
                .id(300107)
                .code("300107")
                .name("KETCHUP KRIS SOBRE 50 g")
                .unitMeasure(PriceComponentRuleUnitMeasure.builder()
                        .id(3001071)
                        .abbreviation("Caja")
                        .name("Caja")
                        .build())
                .originalPrice(new BigDecimal(224))
                .qty(qty)
                .build();
    }

    //endregion

    //region Create Delivery Modes
    public static List<PriceComponentRuleFactorDeliveryMode> createAllDeliveryModes() {
        List<PriceComponentRuleFactorDeliveryMode> deliveryModes = new ArrayList<>(0);
        deliveryModes.add(createDeliveryModeMobileStoreFast());
        deliveryModes.add(createDeliveryWithDeliveryMode(DeliveryModeEnum.CUSTOMER_DELIVERY_POINT));
        deliveryModes.add(createDeliveryModeInWarehouse());
        return deliveryModes;
    }

    public static PriceComponentRuleFactorDeliveryMode createDeliveryMode(DeliveryModeEnum deliveryMode) {
        return PriceComponentRuleFactorDeliveryMode.builder()
                .id(RandomUtil.getRandomInt(RANGE_DELIVERY_MODE_MIN + 3, RANGE_DELIVERY_MODE_MAX))
                .deliveryMode(deliveryMode)
                .build();
    }

    public static PriceComponentRuleFactorDeliveryMode createDeliveryModeMobileStoreFast() {
        return PriceComponentRuleFactorDeliveryMode.builder()
                .id(3101)
                .deliveryMode(DeliveryModeEnum.MOBILE_STORE)
                .build();
    }

    public static PriceComponentRuleFactorDeliveryMode createDeliveryWithDeliveryMode(DeliveryModeEnum deliveryModeEnum) {
        return PriceComponentRuleFactorDeliveryMode.builder()
                .id(3102)
                .deliveryMode(deliveryModeEnum)
                .build();
    }

    public static PriceComponentRuleFactorDeliveryMode createDeliveryModeInWarehouse() {
        return PriceComponentRuleFactorDeliveryMode.builder()
                .id(3101)
                .deliveryMode(DeliveryModeEnum.IN_WAREHOUSE)
                .build();
    }
    //endregion

    //region Create Customers
    public static PriceComponentRuleFactorCustomer createPartyRoleCustomerTelma() {
        return PriceComponentRuleFactorCustomer.builder()
                .id(128)
                .name("Telma Vela")
                .roles(Collections.singletonList(RoleTypeEnum.CUSTOMER))
                .build();
    }

    public static PriceComponentRuleFactorCustomer createPartyRoleCustomerTelmaAll() {
        return PriceComponentRuleFactorCustomer.builder()
                .id(128)
                .name("Telma Vela")
                .roles(Collections.singletonList(RoleTypeEnum.CUSTOMER))
                .partyClassifications(Collections.singletonList(createPartyClassificationEmployeeFirstLvl()))
                .geographicBoundaries(Collections.singletonList(createGeographicBoundaryLP()))
                .build();
    }

    public static PriceComponentRuleFactorCustomer createPartyRoleCustomerRamiro() {
        return PriceComponentRuleFactorCustomer.builder()
                .id(130)
                .name("Ramiro Crespo")
                .roles(Collections.singletonList(RoleTypeEnum.CUSTOMER))
                .partyClassifications(createPartyClassificationList())
                .build();
    }

    public static PriceComponentRuleFactorCustomer createPartyRoleCustomerRamiroAll() {
        List<PriceComponentRuleFactorPartyClassification> classifications = new ArrayList<>(0);
        classifications.add(createPartyClassificationEmployeeFirstLvl());
        classifications.add(createPartyClassificationCustomerOztomizados());

        return PriceComponentRuleFactorCustomer.builder()
                .id(130)
                .name("Ramiro Crespo")
                .roles(Collections.singletonList(RoleTypeEnum.CUSTOMER))
                .geographicBoundaries(Collections.singletonList(createGeographicBoundarySC()))
                .partyClassifications(classifications)
                .build();
    }
    //endregion

    //region Create Employee
    public static PriceComponentRuleFactorCustomer createPartyRoleEmployeeMalcom() {
        return PriceComponentRuleFactorCustomer.builder()
                .id(250)
                .name("Malcom H.")
                .roles(createRoleTypesEmployee().stream().map(PriceComponentRuleFactorRoleType::getRoleType).collect(Collectors.toList()))
                .partyClassifications(Collections.singletonList(createPartyClassificationEmployeeFirstLvl()))
                .geographicBoundaries(Collections.singletonList(createGeographicBoundarySimpsons()))
                .build();
    }

    //endregion

    //region Create Party Role Types
    public static PriceComponentRuleFactorRoleType createRoleTypeCustomer() {
        return PriceComponentRuleFactorRoleType.builder()
                .id(1)
                .roleType(RoleTypeEnum.CUSTOMER)
                .build();
    }

    public static PriceComponentRuleFactorRoleType createRoleTypeAgency() {
        return PriceComponentRuleFactorRoleType.builder()
                .id(2)
                .roleType(RoleTypeEnum.AGENCY)
                .build();
    }

    public static PriceComponentRuleFactorRoleType createRoleTypeProspect() {
        return PriceComponentRuleFactorRoleType.builder()
                .id(3)
                .roleType(RoleTypeEnum.PROSPECT)
                .build();
    }

    public static PriceComponentRuleFactorRoleType createRoleTypeSeller() {
        return PriceComponentRuleFactorRoleType.builder()
                .id(4)
                .roleType(RoleTypeEnum.SELLER)
                .build();
    }

    public static PriceComponentRuleFactorRoleType createRoleTypeDriver() {
        return PriceComponentRuleFactorRoleType.builder()
                .id(5)
                .roleType(RoleTypeEnum.DRIVER)
                .build();
    }

    public static List<PriceComponentRuleFactorRoleType> createRoleTypesEmployee() {
        List<PriceComponentRuleFactorRoleType> employeeRoles = new ArrayList<>();
        RoleTypeEnum.EMPLOYEE_ROLE.forEach(roleTypeEnum -> employeeRoles.add(PriceComponentRuleFactorRoleType.builder()
                .id(IntUtil.getRandomIdLong())
                .roleType(roleTypeEnum)
                .build()));
        return employeeRoles;
    }

    public static List<PriceComponentRuleFactorRoleType> createRoleTypesCustomerProspect() {
        List<PriceComponentRuleFactorRoleType> employeeRoles = new ArrayList<>();
        employeeRoles.add(createRoleTypeCustomer());
        employeeRoles.add(createRoleTypeProspect());
        return employeeRoles;
    }
    //endregion

    public static void reindexSaleOrderProductIds(List<PriceComponentRuleInputSaleOrderProduct> products, int index) {
        final int[] i = {index};
        products.forEach(saleOrderProduct -> {
            saleOrderProduct.setId(i[0]);
            i[0]++;
        });
    }

    public static List<IdModel> createIdModelList(long... ids) {
        List<IdModel> idModelList = new ArrayList<>();
        Arrays.stream(ids).forEach(value -> idModelList.add(IdModel.builder()
                .id(value)
                .build()));
        return idModelList;
    }

    public static PriceComponentRuleFactorScale createScale(BigDecimal from, BigDecimal thru, ScaleTypeEnum type, PriceComponentRuleOutcome outcome) {
        return PriceComponentRuleFactorScale.builder()
                .id(RandomUtil.getRandomInt(RANGE_SCALE_MIN, RANGE_SCALE_MAX))
                .from(from)
                .thru(thru)
                .type(type)
                .outcome(outcome)
                .build();
    }
}
