package ch.swissbytes.businessrules.pricing.utils;


public class StringUtil {

    public static String addJumpLineAtStart(String target) {
        return "\n" + target;
    }

    public static String addJumpLineAtEnd(String target) {
        return target + "\n";
    }

    public static String addJumpLines(String target) {
        return addJumpLineAtStart(target) + "\n";
    }
}
