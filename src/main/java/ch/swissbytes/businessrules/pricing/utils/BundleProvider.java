package ch.swissbytes.businessrules.pricing.utils;

import ch.swissbytes.businessrules.pricing.log.Logger;
import ch.swissbytes.businessrules.pricing.log.LoggerImpl;

import java.text.MessageFormat;
import java.util.*;


public final class BundleProvider {

    private static final Logger log = LoggerImpl.getLogger();

    private static ResourceBundle resourceBundle = ResourceBundle.getBundle("AppMessage");
//    private static ResourceBundle resourceBundle = ResourceBundle.getBundle("AppMessage", new Locale("es","BO"));

    public static String getString(String key) {
        String result = "???" + key + "??? not found";
        try {
            result = resourceBundle.getString(key);
        } catch (MissingResourceException e) {
            log.warn(result);
        }
        return result;
    }

    public static String getString(String key, Object... params) {
        String result = "???" + key + "??? not found";
        try {
            result = MessageFormat.format(getString(key), params);
        } catch (MissingResourceException e) {
            log.warn(result);
        }
        return result;
    }

    public static List<String> getMessageList(String... msgKeys) {
        List<String> messages = new ArrayList<>();
        Arrays.stream(msgKeys).forEach(s -> messages.add(getString(s)));
        return messages;
    }
}
