package ch.swissbytes.businessrules.pricing.utils;

import ch.swissbytes.businessrules.pricing.config.Config;
import ch.swissbytes.businessrules.pricing.config.Keys;
import ch.swissbytes.businessrules.pricing.log.LogFormatter;
import ch.swissbytes.businessrules.pricing.log.Logger;
import ch.swissbytes.businessrules.pricing.log.LoggerImpl;
import ch.swissbytes.businessrules.pricing.log.RollingFileHandler;

import java.io.File;
import java.net.URL;
import java.util.logging.ConsoleHandler;
import java.util.logging.Handler;
import java.util.logging.Level;


public class LogUtil {

    private static final Logger Log = LoggerImpl.getLogger();
    private static final String STACK_PACKAGE = "ch.swissbytes.businessrules.pricing";
    private static final int STACK_LIMIT = 3;
    private static boolean silentMode;

    public static void setupDefaultLogger() {
        String path = null;
        URL url = ClassLoader.getSystemClassLoader().getResource(".");
        if (url != null) {
            File jarPath = new File(url.getPath());
            File logsPath = new File(jarPath, "logs");
            if (!logsPath.exists() || !logsPath.isDirectory()) {
                logsPath = jarPath;
            }
            path = new File(logsPath, "business-rules-pricing.log").getPath();
        }
        setupLogger(path == null, path, Level.WARNING.getName(), false, true);
    }

    public static void setSilentMode(boolean isOn) {
        silentMode = isOn;
    }

    public static void setupLogger(Config config) {
        silentMode = config.getBoolean(Keys.SILENT_MODE_ENABLE);
        setupLogger(config.getBoolean(Keys.LOG_CONSOLE_ENABLE),
                config.getString(Keys.LOG_FILE_PATH),
                config.getString(Keys.LOG_LEVEL),
                config.getBoolean(Keys.LOG_FULL_STACK_TRACE_ENABLE),
                config.getBoolean(Keys.LOG_ROTATE_ENABLE));
    }

    private static void setupLogger(boolean console, String file, String levelString, boolean fullStackTraces, boolean rotate) {
        java.util.logging.Logger rootLogger = java.util.logging.Logger.getLogger("");
        for (Handler handler : rootLogger.getHandlers()) {
            rootLogger.removeHandler(handler);
        }
        Handler handler;
        if (console) {
            handler = new ConsoleHandler();
        } else {
            handler = new RollingFileHandler(file, rotate);
        }
        handler.setFormatter(new LogFormatter(fullStackTraces));

        Level level = Level.parse(levelString.toUpperCase());
        rootLogger.setLevel(level);
        handler.setLevel(level);
        handler.setFilter(record -> record != null && !record.getLoggerName().startsWith("sun"));

        rootLogger.addHandler(handler);
    }

    public static void error(String message) {
        if (!silentMode)
            Log.error(message);
    }

    public static void info(String message) {
        if (!silentMode)
            Log.info(message);
    }

    public static void warn(String message) {
        if (!silentMode)
            Log.warn(message);
    }

    public static void log(PriceRuleLog message) {
        if (silentMode)
            return;
        switch (message.getType()) {
            case INFO:
                info(message.getDetail());
                break;
            case WARN:
                warn(message.getDetail());
                break;
            case ERROR:
                error(message.getDetail());
                break;
        }
    }


    public static String exceptionStack(Throwable exception) {
        StringBuilder s = new StringBuilder();
        String exceptionMsg = exception.getMessage();
        if (exceptionMsg != null) {
            s.append(exceptionMsg);
            s.append(" - ");
        }
        s.append(exception.getClass().getSimpleName());
        StackTraceElement[] stack = exception.getStackTrace();

        if (stack.length > 0) {
            int count = STACK_LIMIT;
            boolean first = true;
            boolean skip = false;
            String file = "";
            s.append(" (");
            for (StackTraceElement element : stack) {
                if (count > 0 && element.getClassName().startsWith(STACK_PACKAGE)) {
                    if (!first) {
                        s.append(" < ");
                    } else {
                        first = false;
                    }

                    if (skip) {
                        s.append("... < ");
                        skip = false;
                    }

                    if (file.equals(element.getFileName())) {
                        s.append("*");
                    } else {
                        file = element.getFileName();
                        s.append(file, 0, file.length() - 5); // remove ".java"
                        count -= 1;
                    }
                    s.append(":").append(element.getLineNumber());
                } else {
                    skip = true;
                }
            }
            if (skip) {
                if (!first) {
                    s.append(" < ");
                }
                s.append("...");
            }
            s.append(")");
        }
        return s.toString();
    }

}
