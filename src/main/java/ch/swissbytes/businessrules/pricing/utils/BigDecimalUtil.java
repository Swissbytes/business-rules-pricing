package ch.swissbytes.businessrules.pricing.utils;

import java.math.BigDecimal;
import java.util.Objects;


public class BigDecimalUtil {

    public static boolean between(BigDecimal input,
                         BigDecimal low,
                         BigDecimal high) {
        if(low == null){
            low = BigDecimal.ZERO;
        }
        if(high == null){
            return(input.compareTo(low) >= 0);
        }else{
            return(input.compareTo(low) >= 0 && input.compareTo(high) <= 0);
        }
    }

    public static boolean isGreaterThanZero(BigDecimal value) {
        return Objects.nonNull(value) && value.compareTo(BigDecimal.ZERO) > 0;
    }
}
