package ch.swissbytes.businessrules.pricing.utils;

import java.util.Random;


public class RandomUtil {

    public static final String[] names = new String[]{ "Miguel Hurtado", "Angelica Aliaga", "Alejandro Muñoz",
            "Karen Zuzunaga", "Manfredo Sanchez", "David Lopez", "Alfredo Gomez", "Gabriela Clavijo", "Eduardo Flor",
            "Oscar Leon", "Daniel Paniagua", "Miguel Merlo", "Marcelo Epinosa", "Fernando Alarcón",
            "Cristian Herrera Alba", "Robert Cabrera", "Miguel Ordoñez", "Alvaro Cardozo", "Eduardo Daniel Moron Arce",
            "David Batista", "Alexander Aguilar", "Alexis Ardaya", "David Camacho", "Ramiro Sanchez", "Wendy Figueroa",
            "Favio Espinoza", "Gabriela Medina", "Telma Vela", "Valeria Pizarro", "Ismael Airoja", "Dahir Seas",
            "Jorge Burgos", "Gabriel Morales", "Dannae Collao Rodriguez", "Marius Lang", "Eliana Zuruguay",
            "Roberto Pérez", "Silvestre Terrazas", "Daniel Quino", "Verónica Lopez", "Malcolm Herraz", "Ramiro Crespo",
            "Rodrigo Guzman", "David Sarmiento", "Rubén Lopez", "Marines Lopez", "Liyina Veizaga", "Yann Huissond",
            "Cristofer Padilla", "Sergio Cabrera",
    };

    public static final String[] ruleNames = new String[]{
            "10% de Descuento por toda compra",
            "100 Bs. de Descuento por toda compra",
            "2 % de Recargo por toda compra",
            "20 Bs. de Recargo por toda compra",
            "Precio Fijo de 150 Bs por toda compra",
            "Bonificacion de un encendedor por toda compra",
            "Recargo de entrega por toda compra",
    };


    public static int getRandomInt(int min, int max) {
        Random random = new Random();
        return random.ints(min, (max + 1)).findFirst().orElse(min);
    }

    public static String getRandomName() {
        return names[getRandomInt(0, names.length - 1)];
    }

    public static String getRandomRuleName() {
        return ruleNames[getRandomInt(0, ruleNames.length - 1)];
    }
}
