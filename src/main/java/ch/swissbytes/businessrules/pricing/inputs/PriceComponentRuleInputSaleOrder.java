package ch.swissbytes.businessrules.pricing.inputs;

import ch.swissbytes.businessrules.pricing.enums.DeliveryModeEnum;
import ch.swissbytes.businessrules.pricing.enums.PaymentConditionsTypeEnum;
import ch.swissbytes.businessrules.pricing.factors.PriceComponentRuleFactorAgency;
import ch.swissbytes.businessrules.pricing.factors.PriceComponentRuleFactorBusinessUnit;
import ch.swissbytes.businessrules.pricing.factors.PriceComponentRuleFactorCustomer;
import ch.swissbytes.businessrules.pricing.factors.PriceComponentRuleFactorPricelist;
import ch.swissbytes.businessrules.pricing.models.BaseModel;
import ch.swissbytes.businessrules.pricing.enums.PriceRuleFactorSaleOrderOrigin;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;


public class PriceComponentRuleInputSaleOrder extends BaseModel {

    private long id;
    private PriceComponentRuleFactorCustomer customer;
    private List<PriceComponentRuleInputSaleOrderProduct> productDetails;
    private PriceComponentRuleFactorBusinessUnit businessUnit;
    private PriceComponentRuleFactorAgency agency;
    private DeliveryModeEnum deliveryMode;
    private PaymentConditionsTypeEnum paymentCondition;
    private PriceRuleFactorSaleOrderOrigin saleOrderOrigin;
    private PriceComponentRuleFactorPricelist pricelist;

    public PriceComponentRuleInputSaleOrder(long id,
                                            PriceComponentRuleFactorCustomer customer,
                                            List<PriceComponentRuleInputSaleOrderProduct> productDetails,
                                            PriceComponentRuleFactorBusinessUnit businessUnit,
                                            PriceComponentRuleFactorAgency agency,
                                            DeliveryModeEnum deliveryMode,
                                            PaymentConditionsTypeEnum paymentCondition,
                                            PriceRuleFactorSaleOrderOrigin saleOrderOrigin,
                                            PriceComponentRuleFactorPricelist pricelist) {
        this.id = id;
        this.customer = customer;
        this.productDetails = productDetails;
        this.businessUnit = businessUnit;
        this.agency = agency;
        this.deliveryMode = deliveryMode;
        this.paymentCondition = paymentCondition;
        this.saleOrderOrigin = saleOrderOrigin;
        this.pricelist = pricelist;
    }

    public PriceComponentRuleInputSaleOrder() {
    }

    public static PriceComponentRuleInputSaleOrderBuilder builder() {
        return new PriceComponentRuleInputSaleOrderBuilder();
    }

    public BigDecimal getTotalAmount() {
        return productDetails.stream().map(PriceComponentRuleInputSaleOrderProduct::getTotalPriceGross).reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    public BigDecimal getTotalQuantities() {
        return productDetails.stream().map(PriceComponentRuleInputSaleOrderProduct::getQty).reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    public PriceComponentRuleInputSaleOrder copyAndSetOnlyProduct(PriceComponentRuleInputSaleOrderProduct product) {
        return PriceComponentRuleInputSaleOrder.builder()
                .id(this.id)
                .customer(this.customer)
                .businessUnit(this.businessUnit)
                .agency(this.agency)
                .deliveryMode(this.deliveryMode)
                .paymentCondition(this.paymentCondition)
                .productDetails(Collections.singletonList(product))
                .saleOrderOrigin(this.saleOrderOrigin)
                .pricelist(this.pricelist)
                .build();
    }

    public long getId() {
        return this.id;
    }

    public PriceComponentRuleFactorCustomer getCustomer() {
        return this.customer;
    }

    public List<PriceComponentRuleInputSaleOrderProduct> getProductDetails() {
        return this.productDetails;
    }

    public PriceComponentRuleFactorBusinessUnit getBusinessUnit() {
        return this.businessUnit;
    }

    public PriceComponentRuleFactorAgency getAgency() {
        return this.agency;
    }

    public DeliveryModeEnum getDeliveryMode() {
        return this.deliveryMode;
    }

    public PaymentConditionsTypeEnum getPaymentCondition() {
        return this.paymentCondition;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setCustomer(PriceComponentRuleFactorCustomer customer) {
        this.customer = customer;
    }

    public void setProductDetails(List<PriceComponentRuleInputSaleOrderProduct> productDetails) {
        this.productDetails = productDetails;
    }

    public void setBusinessUnit(PriceComponentRuleFactorBusinessUnit businessUnit) {
        this.businessUnit = businessUnit;
    }

    public void setAgency(PriceComponentRuleFactorAgency agency) {
        this.agency = agency;
    }

    public void setDeliveryMode(DeliveryModeEnum deliveryMode) {
        this.deliveryMode = deliveryMode;
    }

    public void setPaymentCondition(PaymentConditionsTypeEnum paymentCondition) {
        this.paymentCondition = paymentCondition;
    }

    public PriceRuleFactorSaleOrderOrigin getSaleOrderOrigin() {
        return saleOrderOrigin;
    }

    public void setSaleOrderOrigin(PriceRuleFactorSaleOrderOrigin saleOrderOrigin) {
        this.saleOrderOrigin = saleOrderOrigin;
    }

    public PriceComponentRuleFactorPricelist getPricelist() {
        return pricelist;
    }

    public void setPricelist(PriceComponentRuleFactorPricelist pricelist) {
        this.pricelist = pricelist;
    }

    public String toString() {
        return "PriceComponentRuleInputSaleOrder(id=" + this.getId()
                + ", customer=" + this.getCustomer()
                + ", productDetails=" + this.getProductDetails()
                + ", businessUnit=" + this.getBusinessUnit()
                + ", agency=" + this.getAgency()
                + ", deliveryMode=" + this.getDeliveryMode()
                + ", paymentCondition=" + this.getPaymentCondition()
                + ", saleOrderOrigin=" + this.getSaleOrderOrigin()
                + ", pricelist=" + this.getPricelist()
                + ")";
    }

    public boolean equals(final Object o) {
        if (o == this) return true;
        if (!(o instanceof PriceComponentRuleInputSaleOrder)) return false;
        final PriceComponentRuleInputSaleOrder other = (PriceComponentRuleInputSaleOrder) o;
        if (!other.canEqual((Object) this)) return false;
        if (this.getId() != other.getId()) return false;
        final Object this$customer = this.getCustomer();
        final Object other$customer = other.getCustomer();
        if (this$customer == null ? other$customer != null : !this$customer.equals(other$customer)) return false;
        final Object this$productDetails = this.getProductDetails();
        final Object other$productDetails = other.getProductDetails();
        if (this$productDetails == null ? other$productDetails != null : !this$productDetails.equals(other$productDetails))
            return false;
        final Object this$businessUnit = this.getBusinessUnit();
        final Object other$businessUnit = other.getBusinessUnit();
        if (this$businessUnit == null ? other$businessUnit != null : !this$businessUnit.equals(other$businessUnit))
            return false;
        final Object this$agency = this.getAgency();
        final Object other$agency = other.getAgency();
        if (this$agency == null ? other$agency != null : !this$agency.equals(other$agency)) return false;
        final Object this$deliveryMode = this.getDeliveryMode();
        final Object other$deliveryMode = other.getDeliveryMode();
        if (this$deliveryMode == null ? other$deliveryMode != null : !this$deliveryMode.equals(other$deliveryMode))
            return false;
        final Object this$paymentCondition = this.getPaymentCondition();
        final Object other$paymentCondition = other.getPaymentCondition();
        if (this$paymentCondition == null ? other$paymentCondition != null : !this$paymentCondition.equals(other$paymentCondition))
            return false;

        final Object this$saleOrderOrigin = this.getSaleOrderOrigin();
        final Object other$saleOrderOrigin = other.getSaleOrderOrigin();
        if (this$saleOrderOrigin == null ? other$saleOrderOrigin != null : !this$saleOrderOrigin.equals(other$saleOrderOrigin))
            return false;

        final Object this$pricelist = this.getSaleOrderOrigin();
        final Object other$pricelist = other.getPricelist();
        if (this$pricelist == null ? other$pricelist != null : !this$pricelist.equals(other$pricelist))
            return false;

        return true;
    }

    protected boolean canEqual(final Object other) {
        return other instanceof PriceComponentRuleInputSaleOrder;
    }

    public int hashCode() {
        final int PRIME = 59;
        int result = 1;

        final long $id = this.getId();
        result = result * PRIME + (int) ($id >>> 32 ^ $id);

        final Object $customer = this.getCustomer();
        result = result * PRIME + ($customer == null ? 43 : $customer.hashCode());

        final Object $productDetails = this.getProductDetails();
        result = result * PRIME + ($productDetails == null ? 43 : $productDetails.hashCode());

        final Object $businessUnit = this.getBusinessUnit();
        result = result * PRIME + ($businessUnit == null ? 43 : $businessUnit.hashCode());

        final Object $agency = this.getAgency();
        result = result * PRIME + ($agency == null ? 43 : $agency.hashCode());

        final Object $deliveryMode = this.getDeliveryMode();
        result = result * PRIME + ($deliveryMode == null ? 43 : $deliveryMode.hashCode());

        final Object $paymentCondition = this.getPaymentCondition();
        result = result * PRIME + ($paymentCondition == null ? 43 : $paymentCondition.hashCode());

        final Object $saleOrderOrigin = this.getSaleOrderOrigin();
        result = result * PRIME + ($saleOrderOrigin == null ? 43 : $saleOrderOrigin.hashCode());

        final Object $pricelist = this.getPricelist();
        result = result * PRIME + ($pricelist == null ? 43 : $pricelist.hashCode());

        return result;
    }

    public static class PriceComponentRuleInputSaleOrderBuilder {
        private long id;
        private PriceComponentRuleFactorCustomer customer;
        private List<PriceComponentRuleInputSaleOrderProduct> productDetails;
        private PriceComponentRuleFactorBusinessUnit businessUnit;
        private PriceComponentRuleFactorAgency agency;
        private DeliveryModeEnum deliveryMode;
        private PaymentConditionsTypeEnum paymentCondition;
        private PriceRuleFactorSaleOrderOrigin saleOrderOrigin;
        private PriceComponentRuleFactorPricelist pricelist;

        PriceComponentRuleInputSaleOrderBuilder() {
        }

        public PriceComponentRuleInputSaleOrder.PriceComponentRuleInputSaleOrderBuilder id(long id) {
            this.id = id;
            return this;
        }

        public PriceComponentRuleInputSaleOrder.PriceComponentRuleInputSaleOrderBuilder customer(PriceComponentRuleFactorCustomer customer) {
            this.customer = customer;
            return this;
        }

        public PriceComponentRuleInputSaleOrder.PriceComponentRuleInputSaleOrderBuilder productDetails(List<PriceComponentRuleInputSaleOrderProduct> productDetails) {
            this.productDetails = productDetails;
            return this;
        }

        public PriceComponentRuleInputSaleOrder.PriceComponentRuleInputSaleOrderBuilder businessUnit(PriceComponentRuleFactorBusinessUnit businessUnit) {
            this.businessUnit = businessUnit;
            return this;
        }

        public PriceComponentRuleInputSaleOrder.PriceComponentRuleInputSaleOrderBuilder agency(PriceComponentRuleFactorAgency agency) {
            this.agency = agency;
            return this;
        }

        public PriceComponentRuleInputSaleOrder.PriceComponentRuleInputSaleOrderBuilder deliveryMode(DeliveryModeEnum deliveryMode) {
            this.deliveryMode = deliveryMode;
            return this;
        }

        public PriceComponentRuleInputSaleOrder.PriceComponentRuleInputSaleOrderBuilder paymentCondition(PaymentConditionsTypeEnum paymentCondition) {
            this.paymentCondition = paymentCondition;
            return this;
        }

        public PriceComponentRuleInputSaleOrder.PriceComponentRuleInputSaleOrderBuilder saleOrderOrigin(PriceRuleFactorSaleOrderOrigin priceRuleFactorSaleOrderOrigin) {
            this.saleOrderOrigin = priceRuleFactorSaleOrderOrigin;
            return this;
        }

        public PriceComponentRuleInputSaleOrder.PriceComponentRuleInputSaleOrderBuilder pricelist(PriceComponentRuleFactorPricelist pricelist) {
            this.pricelist = pricelist;
            return this;
        }

        public PriceComponentRuleInputSaleOrder build() {
            return new PriceComponentRuleInputSaleOrder(id,
                    customer,
                    productDetails,
                    businessUnit,
                    agency,
                    deliveryMode,
                    paymentCondition,
                    saleOrderOrigin,
                    pricelist);
        }

        public String toString() {
            return "PriceComponentRuleInputSaleOrder.PriceComponentRuleInputSaleOrderBuilder(id=" + this.id
                    + ", customer=" + this.customer
                    + ", productDetails=" + this.productDetails
                    + ", businessUnit=" + this.businessUnit
                    + ", agency=" + this.agency
                    + ", deliveryMode=" + this.deliveryMode
                    + ", paymentCondition=" + this.paymentCondition
                    + ", saleOrderOrigin=" + this.saleOrderOrigin
                    + ", pricelist=" + this.pricelist
                    + ")";
        }
    }
}
