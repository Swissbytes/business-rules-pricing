package ch.swissbytes.businessrules.pricing.inputs;

import ch.swissbytes.businessrules.pricing.factors.PriceComponentRuleFactorProductClassification;
import ch.swissbytes.businessrules.pricing.log.Logger;
import ch.swissbytes.businessrules.pricing.log.LoggerImpl;
import ch.swissbytes.businessrules.pricing.models.BaseModel;
import ch.swissbytes.businessrules.pricing.models.IdNameModel;
import ch.swissbytes.businessrules.pricing.outcomes.PriceComponentRuleOutcome;
import ch.swissbytes.businessrules.pricing.subjects.PriceComponentRuleSubjectProduct;
import ch.swissbytes.businessrules.pricing.subjects.PriceComponentRuleSubjectProductFeature;
import ch.swissbytes.businessrules.pricing.utils.BundleProvider;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;


public class PriceComponentRuleInputSaleOrderProduct extends BaseModel {

    private static final Logger Log = LoggerImpl.getLogger();
    private long id;//this id is the of the detail
    private PriceComponentRuleSubjectProduct product;
    private List<IdNameModel> rulesApplied;
    private BigDecimal qty;
    private BigDecimal unitPrice;
    private BigDecimal totalPriceGross;
    private BigDecimal discountAmount = BigDecimal.ZERO;
    private BigDecimal discountPercentage = BigDecimal.ZERO;
    private BigDecimal surchargeAmount = BigDecimal.ZERO;
    private BigDecimal surchargePercentage = BigDecimal.ZERO;
    private BigDecimal totalDiscount = BigDecimal.ZERO;
    private BigDecimal totalPrice = BigDecimal.ZERO;//totalPriceGross - totalDiscount
    private List<PriceComponentRuleSubjectProductFeature> features = new ArrayList<>(0);
    private List<PriceComponentRuleFactorProductClassification> classifications = new ArrayList<>(0);

    public PriceComponentRuleInputSaleOrderProduct(long id, PriceComponentRuleSubjectProduct product, List<IdNameModel> rulesApplied, BigDecimal qty, BigDecimal unitPrice, BigDecimal totalPriceGross, BigDecimal discountAmount, BigDecimal discountPercentage, BigDecimal surchargeAmount, BigDecimal surchargePercentage, BigDecimal totalDiscount, BigDecimal totalPrice, List<PriceComponentRuleSubjectProductFeature> features, List<PriceComponentRuleFactorProductClassification> classifications) {
        this.id = id;
        this.product = product;
        this.rulesApplied = rulesApplied;
        this.qty = qty;
        this.unitPrice = unitPrice;
        this.totalPriceGross = totalPriceGross;
        this.discountAmount = discountAmount;
        this.discountPercentage = discountPercentage;
        this.surchargeAmount = surchargeAmount;
        this.surchargePercentage = surchargePercentage;
        this.totalDiscount = totalDiscount;
        this.totalPrice = totalPrice;
        this.features = features;
        this.classifications = classifications;
    }

    public PriceComponentRuleInputSaleOrderProduct() {
    }

    public static PriceComponentRuleInputSaleOrderProductBuilder builder() {
        return new PriceComponentRuleInputSaleOrderProductBuilder();
    }

    public BigDecimal calculateTotalPriceGross() {
        totalPriceGross = unitPrice.multiply(qty);
        return totalPriceGross;
    }

    public void applyOutcome(PriceComponentRuleOutcome outcome) {
        switch (outcome.getOutcomeType()) {
            case DISCOUNT_AMOUNT:
                if (Objects.isNull(this.discountAmount)) {
                    Log.error(BundleProvider.getString("discountAmountNull"));
                    this.discountAmount = BigDecimal.ZERO;
                }
                this.discountAmount = this.discountAmount.add(outcome.getDiscountAmount());
                break;
            case DISCOUNT_PERCENTAGE:
                if (Objects.isNull(this.discountPercentage)) {
                    Log.error(BundleProvider.getString("discountPercentageNull"));
                    this.discountPercentage = BigDecimal.ZERO;
                }
                this.discountPercentage = this.discountPercentage.add(outcome.getDiscountPercentage());
                break;
            case SURCHARGE_AMOUNT:
                if (Objects.isNull(this.surchargeAmount)) {
                    Log.error(BundleProvider.getString("surchargeAmountNull"));
                    this.surchargeAmount = BigDecimal.ZERO;
                }
                this.surchargeAmount = this.surchargeAmount.add(outcome.getSurchargeAmount());
                break;
            case SURCHARGE_PERCENTAGE:
                if (Objects.isNull(this.surchargePercentage)) {
                    Log.error(BundleProvider.getString("surchargePercentageNull"));
                    this.surchargePercentage = BigDecimal.ZERO;
                }
                this.surchargePercentage = this.surchargePercentage.add(outcome.getSurchargePercentage());
                break;
            case FIXED_PRICE:
                if (Objects.isNull(outcome.getFixedPrice())) {
                    Log.error(BundleProvider.getString("outcomeFixedPriceNull"));
                    this.unitPrice = BigDecimal.ZERO;
                    this.totalPrice = BigDecimal.ZERO;
                    this.totalPriceGross = BigDecimal.ZERO;
                    break;
                }
                this.unitPrice = outcome.getFixedPrice();
                this.totalPrice = this.unitPrice.multiply(this.qty);
                this.totalPriceGross = this.totalPrice;
                break;
        }
    }


    public BigDecimal calculateOutcome(PriceComponentRuleOutcome outcome) {
        switch (outcome.getOutcomeType()) {
            case DISCOUNT_AMOUNT:
                if (Objects.isNull(this.discountAmount)) {
                    Log.error(BundleProvider.getString("discountAmountNull"));
                    return outcome.getDiscountAmount();
                }
                return this.discountAmount.add(outcome.getDiscountAmount());
            case DISCOUNT_PERCENTAGE:
                if (Objects.isNull(this.discountPercentage)) {
                    Log.error(BundleProvider.getString("discountPercentageNull"));
                    return outcome.getDiscountPercentage();
                }
                return this.discountPercentage.add(outcome.getDiscountPercentage());
            case SURCHARGE_AMOUNT:
                if (Objects.isNull(this.surchargeAmount)) {
                    Log.error(BundleProvider.getString("surchargeAmountNull"));
                    return outcome.getSurchargeAmount();
                }
                return this.surchargeAmount.add(outcome.getSurchargeAmount());
            case SURCHARGE_PERCENTAGE:
                if (Objects.isNull(this.surchargePercentage)) {
                    Log.error(BundleProvider.getString("surchargePercentageNull"));
                    return outcome.getSurchargePercentage();
                }
                return this.surchargePercentage.add(outcome.getSurchargePercentage());
            case FIXED_PRICE:
                if (Objects.isNull(outcome.getFixedPrice())) {
                    Log.error(BundleProvider.getString("outcomeFixedPriceNull"));
                    return BigDecimal.ZERO;
                }
                return outcome.getFixedPrice().multiply(this.qty);
            default:
                return BigDecimal.ZERO;
        }
    }

    public long getId() {
        return this.id;
    }

    public PriceComponentRuleSubjectProduct getProduct() {
        return this.product;
    }

    public List<IdNameModel> getRulesApplied() {
        return this.rulesApplied;
    }

    public BigDecimal getQty() {
        return this.qty;
    }

    public BigDecimal getUnitPrice() {
        return this.unitPrice;
    }

    public BigDecimal getTotalPriceGross() {
        return this.totalPriceGross;
    }

    public BigDecimal getDiscountAmount() {
        return this.discountAmount;
    }

    public BigDecimal getDiscountPercentage() {
        return this.discountPercentage;
    }

    public BigDecimal getSurchargeAmount() {
        return this.surchargeAmount;
    }

    public BigDecimal getSurchargePercentage() {
        return this.surchargePercentage;
    }

    public BigDecimal getTotalDiscount() {
        return this.totalDiscount;
    }

    public BigDecimal getTotalPrice() {
        return this.totalPrice;
    }

    public List<PriceComponentRuleSubjectProductFeature> getFeatures() {
        return this.features;
    }

    public List<PriceComponentRuleFactorProductClassification> getClassifications() {
        return this.classifications;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setProduct(PriceComponentRuleSubjectProduct product) {
        this.product = product;
    }

    public void setRulesApplied(List<IdNameModel> rulesApplied) {
        this.rulesApplied = rulesApplied;
    }

    public void setQty(BigDecimal qty) {
        this.qty = qty;
    }

    public void setUnitPrice(BigDecimal unitPrice) {
        this.unitPrice = unitPrice;
    }

    public void setTotalPriceGross(BigDecimal totalPriceGross) {
        this.totalPriceGross = totalPriceGross;
    }

    public void setDiscountAmount(BigDecimal discountAmount) {
        this.discountAmount = discountAmount;
    }

    public void setDiscountPercentage(BigDecimal discountPercentage) {
        this.discountPercentage = discountPercentage;
    }

    public void setSurchargeAmount(BigDecimal surchargeAmount) {
        this.surchargeAmount = surchargeAmount;
    }

    public void setSurchargePercentage(BigDecimal surchargePercentage) {
        this.surchargePercentage = surchargePercentage;
    }

    public void setTotalDiscount(BigDecimal totalDiscount) {
        this.totalDiscount = totalDiscount;
    }

    public void setTotalPrice(BigDecimal totalPrice) {
        this.totalPrice = totalPrice;
    }

    public void setFeatures(List<PriceComponentRuleSubjectProductFeature> features) {
        this.features = features;
    }

    public void setClassifications(List<PriceComponentRuleFactorProductClassification> classifications) {
        this.classifications = classifications;
    }

    public String toString() {
        return "PriceComponentRuleInputSaleOrderProduct(id=" + this.getId() + ", product=" + this.getProduct() + ", rulesApplied=" + this.getRulesApplied() + ", qty=" + this.getQty() + ", unitPrice=" + this.getUnitPrice() + ", totalPriceGross=" + this.getTotalPriceGross() + ", discountAmount=" + this.getDiscountAmount() + ", discountPercentage=" + this.getDiscountPercentage() + ", surchargeAmount=" + this.getSurchargeAmount() + ", surchargePercentage=" + this.getSurchargePercentage() + ", totalDiscount=" + this.getTotalDiscount() + ", totalPrice=" + this.getTotalPrice() + ", features=" + this.getFeatures() + ", classifications=" + this.getClassifications() + ")";
    }

    public boolean equals(final Object o) {
        if (o == this) return true;
        if (!(o instanceof PriceComponentRuleInputSaleOrderProduct))
            return false;
        final PriceComponentRuleInputSaleOrderProduct other = (PriceComponentRuleInputSaleOrderProduct) o;
        if (!other.canEqual((Object) this)) return false;
        if (this.getId() != other.getId()) return false;
        final Object this$product = this.getProduct();
        final Object other$product = other.getProduct();
        if (this$product == null ? other$product != null : !this$product.equals(other$product)) return false;
        final Object this$rulesApplied = this.getRulesApplied();
        final Object other$rulesApplied = other.getRulesApplied();
        if (this$rulesApplied == null ? other$rulesApplied != null : !this$rulesApplied.equals(other$rulesApplied))
            return false;
        final Object this$qty = this.getQty();
        final Object other$qty = other.getQty();
        if (this$qty == null ? other$qty != null : !this$qty.equals(other$qty)) return false;
        final Object this$unitPrice = this.getUnitPrice();
        final Object other$unitPrice = other.getUnitPrice();
        if (this$unitPrice == null ? other$unitPrice != null : !this$unitPrice.equals(other$unitPrice)) return false;
        final Object this$totalPriceGross = this.getTotalPriceGross();
        final Object other$totalPriceGross = other.getTotalPriceGross();
        if (this$totalPriceGross == null ? other$totalPriceGross != null : !this$totalPriceGross.equals(other$totalPriceGross))
            return false;
        final Object this$discountAmount = this.getDiscountAmount();
        final Object other$discountAmount = other.getDiscountAmount();
        if (this$discountAmount == null ? other$discountAmount != null : !this$discountAmount.equals(other$discountAmount))
            return false;
        final Object this$discountPercentage = this.getDiscountPercentage();
        final Object other$discountPercentage = other.getDiscountPercentage();
        if (this$discountPercentage == null ? other$discountPercentage != null : !this$discountPercentage.equals(other$discountPercentage))
            return false;
        final Object this$surchargeAmount = this.getSurchargeAmount();
        final Object other$surchargeAmount = other.getSurchargeAmount();
        if (this$surchargeAmount == null ? other$surchargeAmount != null : !this$surchargeAmount.equals(other$surchargeAmount))
            return false;
        final Object this$surchargePercentage = this.getSurchargePercentage();
        final Object other$surchargePercentage = other.getSurchargePercentage();
        if (this$surchargePercentage == null ? other$surchargePercentage != null : !this$surchargePercentage.equals(other$surchargePercentage))
            return false;
        final Object this$totalDiscount = this.getTotalDiscount();
        final Object other$totalDiscount = other.getTotalDiscount();
        if (this$totalDiscount == null ? other$totalDiscount != null : !this$totalDiscount.equals(other$totalDiscount))
            return false;
        final Object this$totalPrice = this.getTotalPrice();
        final Object other$totalPrice = other.getTotalPrice();
        if (this$totalPrice == null ? other$totalPrice != null : !this$totalPrice.equals(other$totalPrice))
            return false;
        final Object this$features = this.getFeatures();
        final Object other$features = other.getFeatures();
        if (this$features == null ? other$features != null : !this$features.equals(other$features)) return false;
        final Object this$classifications = this.getClassifications();
        final Object other$classifications = other.getClassifications();
        if (this$classifications == null ? other$classifications != null : !this$classifications.equals(other$classifications))
            return false;
        return true;
    }

    protected boolean canEqual(final Object other) {
        return other instanceof PriceComponentRuleInputSaleOrderProduct;
    }

    public int hashCode() {
        final int PRIME = 59;
        int result = 1;
        final long $id = this.getId();
        result = result * PRIME + (int) ($id >>> 32 ^ $id);
        final Object $product = this.getProduct();
        result = result * PRIME + ($product == null ? 43 : $product.hashCode());
        final Object $rulesApplied = this.getRulesApplied();
        result = result * PRIME + ($rulesApplied == null ? 43 : $rulesApplied.hashCode());
        final Object $qty = this.getQty();
        result = result * PRIME + ($qty == null ? 43 : $qty.hashCode());
        final Object $unitPrice = this.getUnitPrice();
        result = result * PRIME + ($unitPrice == null ? 43 : $unitPrice.hashCode());
        final Object $totalPriceGross = this.getTotalPriceGross();
        result = result * PRIME + ($totalPriceGross == null ? 43 : $totalPriceGross.hashCode());
        final Object $discountAmount = this.getDiscountAmount();
        result = result * PRIME + ($discountAmount == null ? 43 : $discountAmount.hashCode());
        final Object $discountPercentage = this.getDiscountPercentage();
        result = result * PRIME + ($discountPercentage == null ? 43 : $discountPercentage.hashCode());
        final Object $surchargeAmount = this.getSurchargeAmount();
        result = result * PRIME + ($surchargeAmount == null ? 43 : $surchargeAmount.hashCode());
        final Object $surchargePercentage = this.getSurchargePercentage();
        result = result * PRIME + ($surchargePercentage == null ? 43 : $surchargePercentage.hashCode());
        final Object $totalDiscount = this.getTotalDiscount();
        result = result * PRIME + ($totalDiscount == null ? 43 : $totalDiscount.hashCode());
        final Object $totalPrice = this.getTotalPrice();
        result = result * PRIME + ($totalPrice == null ? 43 : $totalPrice.hashCode());
        final Object $features = this.getFeatures();
        result = result * PRIME + ($features == null ? 43 : $features.hashCode());
        final Object $classifications = this.getClassifications();
        result = result * PRIME + ($classifications == null ? 43 : $classifications.hashCode());
        return result;
    }

    public static class PriceComponentRuleInputSaleOrderProductBuilder {
        private long id;
        private PriceComponentRuleSubjectProduct product;
        private List<IdNameModel> rulesApplied;
        private BigDecimal qty;
        private BigDecimal unitPrice;
        private BigDecimal totalPriceGross;
        private BigDecimal discountAmount = BigDecimal.ZERO;
        private BigDecimal discountPercentage = BigDecimal.ZERO;
        private BigDecimal surchargeAmount = BigDecimal.ZERO;
        private BigDecimal surchargePercentage = BigDecimal.ZERO;
        private BigDecimal totalDiscount = BigDecimal.ZERO;
        private BigDecimal totalPrice = BigDecimal.ZERO;//totalPriceGross - totalDiscount
        private List<PriceComponentRuleSubjectProductFeature> features = new ArrayList<>(0);
        private List<PriceComponentRuleFactorProductClassification> classifications = new ArrayList<>(0);

        PriceComponentRuleInputSaleOrderProductBuilder() {
        }

        public PriceComponentRuleInputSaleOrderProduct.PriceComponentRuleInputSaleOrderProductBuilder id(long id) {
            this.id = id;
            return this;
        }

        public PriceComponentRuleInputSaleOrderProduct.PriceComponentRuleInputSaleOrderProductBuilder product(PriceComponentRuleSubjectProduct product) {
            this.product = product;
            return this;
        }

        public PriceComponentRuleInputSaleOrderProduct.PriceComponentRuleInputSaleOrderProductBuilder rulesApplied(List<IdNameModel> rulesApplied) {
            this.rulesApplied = rulesApplied;
            return this;
        }

        public PriceComponentRuleInputSaleOrderProduct.PriceComponentRuleInputSaleOrderProductBuilder qty(BigDecimal qty) {
            this.qty = qty;
            return this;
        }

        public PriceComponentRuleInputSaleOrderProduct.PriceComponentRuleInputSaleOrderProductBuilder unitPrice(BigDecimal unitPrice) {
            this.unitPrice = unitPrice;
            return this;
        }

        public PriceComponentRuleInputSaleOrderProduct.PriceComponentRuleInputSaleOrderProductBuilder totalPriceGross(BigDecimal totalPriceGross) {
            this.totalPriceGross = totalPriceGross;
            return this;
        }

        public PriceComponentRuleInputSaleOrderProduct.PriceComponentRuleInputSaleOrderProductBuilder discountAmount(BigDecimal discountAmount) {
            this.discountAmount = discountAmount;
            return this;
        }

        public PriceComponentRuleInputSaleOrderProduct.PriceComponentRuleInputSaleOrderProductBuilder discountPercentage(BigDecimal discountPercentage) {
            this.discountPercentage = discountPercentage;
            return this;
        }

        public PriceComponentRuleInputSaleOrderProduct.PriceComponentRuleInputSaleOrderProductBuilder surchargeAmount(BigDecimal surchargeAmount) {
            this.surchargeAmount = surchargeAmount;
            return this;
        }

        public PriceComponentRuleInputSaleOrderProduct.PriceComponentRuleInputSaleOrderProductBuilder surchargePercentage(BigDecimal surchargePercentage) {
            this.surchargePercentage = surchargePercentage;
            return this;
        }

        public PriceComponentRuleInputSaleOrderProduct.PriceComponentRuleInputSaleOrderProductBuilder totalDiscount(BigDecimal totalDiscount) {
            this.totalDiscount = totalDiscount;
            return this;
        }

        public PriceComponentRuleInputSaleOrderProduct.PriceComponentRuleInputSaleOrderProductBuilder totalPrice(BigDecimal totalPrice) {
            this.totalPrice = totalPrice;
            return this;
        }

        public PriceComponentRuleInputSaleOrderProduct.PriceComponentRuleInputSaleOrderProductBuilder features(List<PriceComponentRuleSubjectProductFeature> features) {
            this.features = features;
            return this;
        }

        public PriceComponentRuleInputSaleOrderProduct.PriceComponentRuleInputSaleOrderProductBuilder classifications(List<PriceComponentRuleFactorProductClassification> classifications) {
            this.classifications = classifications;
            return this;
        }

        public PriceComponentRuleInputSaleOrderProduct build() {
            return new PriceComponentRuleInputSaleOrderProduct(id, product, rulesApplied, qty, unitPrice, totalPriceGross, discountAmount, discountPercentage, surchargeAmount, surchargePercentage, totalDiscount, totalPrice, features, classifications);
        }

        public String toString() {
            return "PriceComponentRuleInputSaleOrderProduct.PriceComponentRuleInputSaleOrderProductBuilder(id=" + this.id + ", product=" + this.product + ", rulesApplied=" + this.rulesApplied + ", qty=" + this.qty + ", unitPrice=" + this.unitPrice + ", totalPriceGross=" + this.totalPriceGross + ", discountAmount=" + this.discountAmount + ", discountPercentage=" + this.discountPercentage + ", surchargeAmount=" + this.surchargeAmount + ", surchargePercentage=" + this.surchargePercentage + ", totalDiscount=" + this.totalDiscount + ", totalPrice=" + this.totalPrice + ", features=" + this.features + ", classifications=" + this.classifications + ")";
        }
    }
}
