package ch.swissbytes.businessrules.pricing.interfaces;

import ch.swissbytes.businessrules.pricing.inputs.PriceComponentRuleInputSaleOrder;

public interface ISimplePriceRuleValidator {

    boolean isValid(PriceComponentRuleInputSaleOrder saleOrder);
}
