package ch.swissbytes.businessrules.pricing.interfaces;

import ch.swissbytes.businessrules.pricing.factors.PriceComponentRuleFactorScale;
import ch.swissbytes.businessrules.pricing.inputs.PriceComponentRuleInputSaleOrder;

import java.util.List;


public interface IScalePriceRuleValidator {

    boolean isValid(PriceComponentRuleInputSaleOrder order, List<PriceComponentRuleFactorScale> scales);
}
