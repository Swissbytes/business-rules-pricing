package ch.swissbytes.businessrules.pricing.interfaces;

import ch.swissbytes.businessrules.pricing.inputs.PriceComponentRuleInputSaleOrder;

import java.math.BigDecimal;


public interface IPriceRuleValidator {

    boolean isValid(PriceComponentRuleInputSaleOrder saleOrder,
                    BigDecimal fromAmount,
                    BigDecimal thruAmount,
                    int fromQty,
                    int thruQty);

}
