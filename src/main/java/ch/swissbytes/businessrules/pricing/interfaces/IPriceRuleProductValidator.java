package ch.swissbytes.businessrules.pricing.interfaces;

import ch.swissbytes.businessrules.pricing.inputs.PriceComponentRuleInputSaleOrderProduct;


public interface IPriceRuleProductValidator {

    boolean isValidProduct(PriceComponentRuleInputSaleOrderProduct saleOrderProduct);
}
