package ch.swissbytes.businessrules.pricing.enums;

public enum ExclusiveOutcomeEnum {
    NONE,
    OUTCOME_TYPE;

    public boolean isNone() {
        return this == NONE;
    }

    public boolean isOutcomeType() {
        return this == OUTCOME_TYPE;
    }
}
