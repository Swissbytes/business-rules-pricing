package ch.swissbytes.businessrules.pricing.enums;

import java.io.Serializable;


public enum PriceRuleFactorSaleOrderOrigin implements Serializable {
    WEB,
    APP,
    SCV,
    CATALOG,
    POS,
    MOBILE_STORE,
    OTHER,
    API;

}
