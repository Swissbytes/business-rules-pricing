package ch.swissbytes.businessrules.pricing.enums;

public enum PriceComponentOutcomeTypeEnum {
    DISCOUNT_AMOUNT,
    SURCHARGE_AMOUNT,
    DISCOUNT_PERCENTAGE,
    SURCHARGE_PERCENTAGE,
    FIXED_PRICE,
    PRODUCT,
    PRODUCT_SURCHARGE;

    public boolean isBonusProduct() {
        return this == PRODUCT;
    }

    public boolean isBonusOrSurchargeProduct() {
        return this == PRODUCT || this == PRODUCT_SURCHARGE;
    }

    public boolean isProductSurcharge() {
        return this == PRODUCT_SURCHARGE;
    }
}
