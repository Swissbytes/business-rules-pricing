package ch.swissbytes.businessrules.pricing.enums;


public enum DeliveryModeEnum {
    CUSTOMER_DELIVERY_POINT,
    MOBILE_STORE,
    IN_WAREHOUSE,
    DIRECT,
    CONSIGNEE_DIRECT
}
