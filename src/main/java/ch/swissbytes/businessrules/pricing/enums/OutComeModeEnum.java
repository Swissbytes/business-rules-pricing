package ch.swissbytes.businessrules.pricing.enums;


public enum OutComeModeEnum {
    SCALE,
    FREQUENCY,
    SINGLE;

    public boolean isScale() {
        return this == SCALE;
    }

    public boolean isFrequency(){
        return this == FREQUENCY;
    }

    public boolean isSingle(){
        return this == SINGLE;
    }

    public boolean isScaleOrFrequency(){
        return this == FREQUENCY || this == SCALE;
    }

}
