package ch.swissbytes.businessrules.pricing.enums;


public enum TargetEnum {
    SALE_ORDER,
    PRODUCT
}
