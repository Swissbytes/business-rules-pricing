package ch.swissbytes.businessrules.pricing.enums;


public enum ScaleTypeEnum {
    QUANTITY,
    AMOUNT;
}
