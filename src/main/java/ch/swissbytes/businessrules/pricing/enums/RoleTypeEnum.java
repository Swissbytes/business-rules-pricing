package ch.swissbytes.businessrules.pricing.enums;

import java.util.EnumSet;


public enum RoleTypeEnum {
    PROSPECT,
    CUSTOMER,
    SELLER,
    AGENT_CRM,
    COLLECTOR,
    CONSIGNEE,
    CONTACT,
    AGENCY,
    QUOTATION_MANAGER,
    DRIVER,
    CASHIER,
    CASHIER_SUPERVISOR,
    USER,
    REFUND_SUPERVISOR,
    REFUND_AGENT;

    public static final EnumSet<RoleTypeEnum> CUSTOMER_ROLE = EnumSet.of(PROSPECT, CUSTOMER);
    public static final EnumSet<RoleTypeEnum> EMPLOYEE_ROLE = EnumSet.of(SELLER, COLLECTOR, AGENT_CRM,
            QUOTATION_MANAGER, DRIVER, CASHIER, CASHIER_SUPERVISOR, REFUND_SUPERVISOR, REFUND_AGENT);
}
