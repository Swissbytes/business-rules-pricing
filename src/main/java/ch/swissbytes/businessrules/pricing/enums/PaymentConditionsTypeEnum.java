package ch.swissbytes.businessrules.pricing.enums;


public enum PaymentConditionsTypeEnum {
    CASH,
    CREDIT,
    CREDIT_ON_DELIVERY
}
