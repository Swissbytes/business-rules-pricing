package ch.swissbytes.businessrules.pricing.enums;


/**
 * GENERAL: Applies on activation, if one of the components match one of the conditions of the rule (OR CONDITION), the rule can be applied.
 * RESTRICTED: The rule is applied if all the components of the rule match the conditions (AND CONDITION).
 * */
public enum PriceRuleTypeEnum {
    GENERAL,
    RESTRICTED;

    public boolean isRestricted() {
        return this == RESTRICTED;
    }
}
