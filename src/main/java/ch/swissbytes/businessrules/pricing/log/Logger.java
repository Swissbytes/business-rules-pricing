package ch.swissbytes.businessrules.pricing.log;

public interface Logger {

    void error(String msg);

    void info(String msg);

    void warn(String msg);
}
