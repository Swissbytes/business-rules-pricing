package ch.swissbytes.businessrules.pricing.log;

import java.util.Objects;

public class LoggerImpl implements Logger{

    private static Logger log;
    private static String ERROR = "ERROR:";
    private static String INFO = "INFO:";
    private static String WARN = "WARN:";

    @Override
    public void error(String msg) {
        System.out.println(ERROR + msg);
    }

    @Override
    public void info(String msg) {
        System.out.println(INFO + msg);
    }

    @Override
    public void warn(String msg) {
        System.out.println(WARN + msg);
    }

    public static Logger getLogger() {
        if (Objects.isNull(log)) {
            log = new LoggerImpl();
        }
        return log;
    }
}
