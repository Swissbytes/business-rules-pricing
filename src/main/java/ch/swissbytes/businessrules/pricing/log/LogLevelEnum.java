package ch.swissbytes.businessrules.pricing.log;


public enum LogLevelEnum {
    TRACE,
    DEBUG,
    INFO,
    WARN,
    ERROR
}

