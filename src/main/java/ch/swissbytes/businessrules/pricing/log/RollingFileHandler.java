package ch.swissbytes.businessrules.pricing.log;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Handler;
import java.util.logging.LogRecord;


public class RollingFileHandler extends Handler {

    private String name;
    private String suffix;
    private Writer writer;
    private boolean rotate;

    public RollingFileHandler(String name, boolean rotate) {
        this.name = name;
        this.rotate = rotate;
    }

    public static RollingFileHandlerBuilder builder() {
        return new RollingFileHandlerBuilder();
    }

    @Override
    public synchronized void publish(LogRecord record) {
        if (isLoggable(record)) {
            try {
                String suffix = "";
                if (rotate) {
                    suffix = new SimpleDateFormat("yyyyMMdd").format(new Date(record.getMillis()));
                    if (writer != null && !suffix.equals(this.suffix)) {
                        writer.close();
                        writer = null;
                        if (!new File(name).renameTo(new File(name + "." + this.suffix))) {
                            throw new RuntimeException("Log file renaming failed");
                        }
                    }
                }
                if (writer == null) {
                    this.suffix = suffix;
                    writer = new BufferedWriter(
                            new OutputStreamWriter(new FileOutputStream(name, true), StandardCharsets.UTF_8));
                }
                writer.write(getFormatter().format(record));
                writer.flush();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }

    @Override
    public synchronized void flush() {
        if (writer != null) {
            try {
                writer.flush();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }

    @Override
    public synchronized void close() throws SecurityException {
        if (writer != null) {
            try {
                writer.close();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }

    public static class RollingFileHandlerBuilder {
        private String name;
        private boolean rotate;

        RollingFileHandlerBuilder() {
        }

        public RollingFileHandlerBuilder name(String name) {
            this.name = name;
            return this;
        }

        public RollingFileHandlerBuilder rotate(boolean rotate) {
            this.rotate = rotate;
            return this;
        }

        public RollingFileHandler build() {
            return new RollingFileHandler(name, rotate);
        }

        public String toString() {
            return "RollingFileHandler.RollingFileHandlerBuilder(name=" + this.name + ", rotate=" + this.rotate + ")";
        }
    }
}
