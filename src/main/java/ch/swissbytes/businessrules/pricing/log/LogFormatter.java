package ch.swissbytes.businessrules.pricing.log;

import ch.swissbytes.businessrules.pricing.utils.LogUtil;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Date;
import java.util.Objects;
import java.util.logging.Formatter;
import java.util.logging.Level;
import java.util.logging.LogRecord;


public class LogFormatter extends Formatter {

    private boolean fullStackTraces;

    public LogFormatter(boolean fullStackTraces) {
        this.fullStackTraces = fullStackTraces;
    }

    private static String formatLevel(Level level) {
        switch (level.getName()) {
            case "FINEST":
                return LogLevelEnum.TRACE.name();
            case "FINER":
            case "FINE":
            case "CONFIG":
                return LogLevelEnum.DEBUG.name();
            case "INFO":
                return LogLevelEnum.INFO.name();
            case "WARNING":
                return LogLevelEnum.WARN.name();
            case "SEVERE":
            default:
                return LogLevelEnum.ERROR.name();
        }
    }

    public static LogFormatterBuilder builder() {
        return new LogFormatterBuilder();
    }

    @Override
    public String format(LogRecord record) {
        StringBuilder message = new StringBuilder();

        if (Objects.nonNull(record.getMessage())) {
            message.append(record.getMessage());
        }

        if (Objects.nonNull(record.getThrown())) {
            if (message.length() > 0) {
                message.append(" - ");
            }
            if (fullStackTraces) {
                StringWriter stringWriter = new StringWriter();
                PrintWriter printWriter = new PrintWriter(stringWriter);
                record.getThrown().printStackTrace(printWriter);
                message.append(System.lineSeparator()).append(stringWriter.toString());
            } else {
                message.append(LogUtil.exceptionStack(record.getThrown()));
            }
        }

        return String.format("%1$tF %1$tT %2$5s: %3$s%n",
                new Date(record.getMillis()), formatLevel(record.getLevel()), message.toString());
    }

    public static class LogFormatterBuilder {
        private boolean fullStackTraces;

        LogFormatterBuilder() {
        }

        public LogFormatterBuilder fullStackTraces(boolean fullStackTraces) {
            this.fullStackTraces = fullStackTraces;
            return this;
        }

        public LogFormatter build() {
            return new LogFormatter(fullStackTraces);
        }

        public String toString() {
            return "LogFormatter.LogFormatterBuilder(fullStackTraces=" + this.fullStackTraces + ")";
        }
    }
}
